<?php
define( '__ROOT__', __DIR__ );
define( '__CLASSES__', __DIR__ . '/includes/classes');
define ( '__VIEWS__', __DIR__ . '/views');

require_once (__DIR__ . '/includes/Configu.php');

/*
 * TEMPLATE GERELATEERDE OPMAAK/CODE
 * --------------------------------------------------------------------
 */

$Pagina ['template'] ['header'] = isset ( $Pagina ['template'] ['header'] ) && file_exists ( __SECTION__ . '/template/header/' . $Pagina ['template'] ['header'] . '.php' ) ? $Pagina ['template'] ['header'] : 'default';
if (! isset ( $headerVar ) || ! is_array ( $headerVar ))
	$headerVar = null;

require_once (__SECTION__ . '/template/header/' . $Pagina ['template'] ['header'] . '.php');

if (isset ( $Pagina ['is_script'] ) && $Pagina ['is_script']) {
	$VIEW = [ ];
	if (isset ( $Pagina ['script'] ['src'] ) && ! empty ( $Pagina ['script'] ['src'] ))
		$VIEW [] = $Pagina ['script'] ['src'];
	
	if (isset ( $Pagina ['script'] ['view'] ) && ! empty ( $Pagina ['script'] ['view'] ))
		$VIEW [] = $Pagina ['script'] ['view'];
	
	if (isset ( $VIEW ) && count ( $VIEW ) > 0 && ! empty ( $VIEW [0] )) {
		$FILE = __DIR__ . '/' . implode ( '/', $VIEW ) . '.php';
		$ALT = str_replace ( strrchr ( $FILE, '/' ), '.php', $FILE );
	} else
		$FILE = $Pagina ['script_src'];
	

	// Script laden.
	if (file_exists ( $FILE ) && ! include ($FILE)) {
	} elseif (isset ( $ALT ) && file_exists ( $ALT ) && ! @include ($ALT))
		echo '<div class="row"><div class="col-xs-12"><h2>Faal</h2><p>Script kon niet worden ingeladen.</p></div></div>';
} else {
	if (! @include (__DIR__ . '/views/content.php')) {
		echo '<div class="row"><div class="col-xs-12"><h2>Faal</h2><p>Detailpagina kon niet worden ingeladen.</p></div></div>';
	}
}

$Pagina ['template'] ['footer'] = isset ( $Pagina ['template'] ['footer'] ) && file_exists ( __SECTION__ . '/template/footer/' . $Pagina ['template'] ['footer'] . '.php' ) ? $Pagina ['template'] ['footer'] : 'default';

require_once (__SECTION__ . '/template/footer/' . $Pagina ['template'] ['footer'] . '.php');