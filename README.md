# Bandwerk Framework

Deze repository is het Bandwerk Framework in de puurste vorm met de meest recente updates.

---
##Webpack compression
Wanneer compression niet werkt run volgende commando voordat je `npm build run` doet.

`npm install compression-webpack-plugin --save-dev`

---
## Inline bewerken
Om inline bewerken mogelijk te maken moet het de JS class BWEdit worden geinstantieerd in `src/app.js`.

---

### Input[type=text]
Normale tekstvelden uit de database kunnen aanpasbaar worden gemaakt door de volgende attributen toe te voegen aan het element waar de inhoud zich in bevindt:
```html
<h1 bw-edit="table" bw-edit-module="website" bw-edit-field="paginatitel" bw-edit-id="1"><?= HtmGen::page_title(); ?></h1>
``` 

#### `bw-edit="table"`
De waarde table in bw-edit zorgt dat er een inline-editor wordt ingeladen zodra de gebruiker geauthentificeerd is.

#### `bw-edit-module="website"`
Het attribuut ...-module bepaald uit welke tabel de gegevens worden geupdate.

#### `bw-edit-field="paginatitel""`
Field bepaald welke kolom uit de tabel wordt geupdate wanneer de gegevens worden opgeslagen.

#### `bw-edit-id="1"`
Ten slotte zorgt id dat het systeem weet welke record uit te tabel te updaten met de aangepaste gegevens.

### Textarea met editor
Wanneer `bw-edit-field` de waarde 'bericht' of 'inhoud' heeft wordt er een volledige editor laten zien met mogelijkheid om de tekst op te maken.

---

### Image
```html
<img bw-edit="image" bw-edit-config="nieuws_header_crop_config" bw-edit-album-id="74" bw-edit-replace-id="21" src="..."/>
```
#### `bw-edit="image"`
De waarde table in bw-edit zorgt dat er een afbeelding uploader wordt ingeladen zodra de gebruiker geauthentificeerd is.

#### `bw-edit-config="nieuws_header_crop_config"`
Dit waarde van dit attribuut is de naam van de configtabel van de afbeeldingmodule.

#### `bw-edit-album-id="4"`
Dit is het ID van de pagina/module-instantie waaraan de afbeelding is verbonden.

#### `bw-edit-replace-id="21"`
Om automatisch de voorgaande afbeelding te verwijderen wanneer de gebruiker opslaat, vul je hier het huidige afbeelding-id in. Dit is de bestandsnaam zonder extensie.

---

### Textarea met editor
Wanneer `bw-edit-field` de waarde 'bericht' of 'inhoud' heeft wordt er een volledige editor laten zien met mogelijkheid om de tekst op te maken.

---

### Constanten

Om constanten bewerkbaar te maken kun je die via PHP inladen met de volgende functie:
```php
inlineConstant('SITENAME_PLAIN');
```

De bovenstaande functie maakt een span aan met de benodigde attributen:
`bw-edit="constant"` en `bw-edit-attr="SITENAME_PLAIN"`
