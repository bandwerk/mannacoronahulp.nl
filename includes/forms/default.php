<?php

$ContactForm = new FormGen ( 'contact' );

$ContactForm->AddField ( 'text', 'naam', 'Naam:', 'naam', [], true );
$ContactForm->AddField ( 'email', 'email', 'E-mail:', 'email', [ ], true );
$ContactForm->AddField ( 'tel', 'tel', 'Telefoonnummer:', 'tel', [ ], true );

$ContactForm->AddField ( 'textarea', 'opmerking', 'Stel hier je vraag...', '', [ ], true );
$ContactForm->AddField ( 'submit', 'submit', 'Verzenden' );

if ($ContactForm->ValidateForm ()) {

    if ($Data = $ContactForm->GetFormData ()) {
        if ($ContactForm->ValidateForm ()) {
            $SEND = [
                'onderwerp' =>
                    'Contactformulier ingevoerd op ' . SITENAME_PLAIN,
                'body' =>
                    '<p>Er is een contactformulier via ' . SITENAME_PLAIN . ' opgestuurd.</p>
					<p>De volgende info is ingevoerd:</p>'
            ];


            $SEND ['formdata'] = [];
            $req = false;
            foreach ( $ContactForm->GetInput() as $field => $val ) {
                if (! isset ( $SEND ['formdata'] ))
                    $SEND ['formdata'] = [ ];
                $val = strip_tags ( $val );

                switch ($field) {
                    case 'nobots' :
                    case 'submit' :
                        $val = null;
                        break;
                    case 'tel' :
                        $val = '<a' . $HTM->get_href ( 'tel:' . preg_replace ( '/\D/', '', $val ), 'bel naar: ' . $val ) . '>' . $val . '</a><br />';
                        $req = true;
                        break;
                    case 'email' :
                        $SEND ['mailfrom'] = $val;
                        $val = '<a' . $HTM->get_href ( 'mailto:' . $val, 'Stuur een mail naar: ' . $val ) . '>' . $val . '</a><br />';
                        $req = true;
                        break;
                    case 'naam' :
                        $SEND ['naam'] = $val;
                        break;
                    case 'postcode':

                        $AdresZoek = $ContactForm->GetValue('adres') . ', '. $ContactForm->GetValue('postcode') . ' '. $ContactForm->GetValue('plaats');
                        $val = $ContactForm->getValue('adres').'<br />'.
                            $ContactForm->getValue('plaats') . ' ' . $ContactForm->GetValue('postcode') . '<br /><a'. HtmGen::href( 'https://www.google.com/maps/place/'. rawurlencode( $AdresZoek ), 'Bekijk op google maps' ) . '>Bekijk op google maps</a>';
                        break;

                    case 'adres':
                    case 'straat':
                    case 'huisnummer':
                    case 'plaats':
                        $val = null;
                        break;
                    default :
                        break;
                }

                if (isset ( $val ) && ! empty ( $val )) {
                    $SEND ['formdata'] [str_replace( '_', ' ', $field )] = $val;
                }
            }

            if( !$req )
            {
                $ContactForm->AddError( 'submit', 'Er is nog geen contactinformatie opgegeven' );
            }
            else
            {
                if(require(__DIR__ . '/mail/send.php'))
                {
                    $ContactForm->SetSuccess();
                    header('Location: '. SITELINK . '/bedankt');
                    die();
                }
                else
                {
                    $ContactForm->AddError ( 'submit', 'Er is iets mis gegaan met het versturen van de mail.' );
                }

                $ContactForm->ValidateForm();
            }

        }
    }
} else {
    $ContactForm->IsSuccess();
}


?>