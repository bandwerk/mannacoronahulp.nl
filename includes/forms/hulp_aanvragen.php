<?php

$AanvraagForm = new FormGen ('Hulp_aanvragen');

$AanvraagForm->AddField('text', 'voornaam', 'Voornaam:', '', [], true);
$AanvraagForm->AddField('text', 'achternaam', 'Achternaam:', '', [], true);
$AanvraagForm->AddField('tel', 'tel', 'Telefoonnummer:', 'tel', [], true);
$AanvraagForm->AddField('email', 'email', 'E-mail:', '', [], true);
$AanvraagForm->AddField('text', 'postcode', 'Postcode:', '', [], true);
$AanvraagForm->AddField('text', 'huisnummer', 'Huisnummer:', '', [], true);
$AanvraagForm->AddField('text', 'straat', 'Straat:', '', [], true);
$AanvraagForm->AddField('text', 'woonplaats', 'Woonplaats:', '', [], true);

$CoronaClient = new CoronaClient();
if ($AanvraagForm->ValidateForm()) {

    if ($Data = $AanvraagForm->GetFormData()) {
        if ($AanvraagForm->ValidateForm()) {
            $SEND = [
                'onderwerp' =>
                    'Contactformulier ingevoerd op ' . SITENAME_PLAIN,
                'body' =>
                    '<p>Er is een contactformulier via ' . SITENAME_PLAIN . ' opgestuurd.</p>
					<p>De volgende info is ingevoerd:</p>'
            ];


            $SEND ['formdata'] = [];
            $req = false;
            foreach ($AanvraagForm->GetInput() as $field => $val) {
                if (!isset ($SEND ['formdata']))
                    $SEND ['formdata'] = [];
                $val = strip_tags($val);

                switch ($field) {
                    case 'nobots' :
                    case 'submit' :
                        $val = null;
                        break;
                    case 'tel' :
                        $val = '<a' . $HTM->get_href('tel:' . preg_replace('/\D/', '', $val), 'bel naar: ' . $val) . '>' . $val . '</a><br />';
                        $req = true;
                        break;
                    case 'email' :
                        $SEND ['mailfrom'] = $val;
                        $val = '<a' . $HTM->get_href('mailto:' . $val, 'Stuur een mail naar: ' . $val) . '>' . $val . '</a><br />';
                        $req = true;
                        break;
                    case 'naam' :
                        $SEND ['naam'] = $val;
                        break;
                    case 'postcode':

                        $AdresZoek = $AanvraagForm->GetValue('adres') . ', ' . $AanvraagForm->GetValue('postcode') . ' ' . $AanvraagForm->GetValue('plaats');
                        $val = $AanvraagForm->getValue('adres') . '<br />' .
                            $AanvraagForm->getValue('plaats') . ' ' . $AanvraagForm->GetValue('postcode') . '<br /><a' . HtmGen::href('https://www.google.com/maps/place/' . rawurlencode($AdresZoek), 'Bekijk op google maps') . '>Bekijk op google maps</a>';
                        break;

                    case 'adres':
                    case 'straat':
                    case 'huisnummer':
                    case 'plaats':
                        $val = null;
                        break;
                    default :
                        break;
                }

                if (isset ($val) && !empty ($val)) {
                    $SEND ['formdata'] [str_replace('_', ' ', $field)] = $val;
                }
            }

            if (!$req) {
                $AanvraagForm->AddError('submit', 'Er is nog geen contactinformatie opgegeven');
            } else {
                /// PROCES DATA
                $adres = $_POST['Hulp_aanvragen_straat'] . " " . $_POST['Hulp_aanvragen_huisnummer'];
                $CoronaClient->newClient($_POST['Hulp_aanvragen_voornaam'], $_POST['Hulp_aanvragen_achternaam'], $_POST['Hulp_aanvragen_tel'], $_POST['Hulp_aanvragen_email'], $adres, $_POST['Hulp_aanvragen_postcode'], $_POST['Hulp_aanvragen_woonplaats'], $_POST['opmerking']);
                $CoronaClient->SetRequestTypes($_POST['hulpvraag']);

                if ($CoronaClient->SaveAsNew()) {

                    $AanvraagForm->SetSuccess();
                    header('Location: ' . SITELINK . '/bedankt');
                    die();
                } else {
                    $AanvraagForm->AddError('submit', 'Er is iets mis gegaan met het versturen van de mail.');
                }

                $AanvraagForm->ValidateForm();
            }

        }
    }
} else {
    $AanvraagForm->IsSuccess();
}


?>