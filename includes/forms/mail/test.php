<?php


require_once('../../Configu.php');
$SENDIT = isset( $_GET['mail'] ) ? true :  false ;

if( !defined( '__ROOT__' ) && strrchr( __DIR__, '/includes' ) ) {
	define( '__ROOT__', substr( __DIR__, 0, strlen( __DIR__ ) - strpos( __DIR__, '/includes' ) ) );
	
	
	$FILE = isset( $_GET['file'] ) ? str_ireplace( '.php', '', $_GET['file'] ) :  '' ;
	$FILE = ( file_exists( __DIR__ . '/template/' . $FILE .'.php') ) ? $FILE : 'default';
	require_once( __DIR__ . '/template/' . $FILE . '.php');

				
} elseif( !defined( '__ROOT__' ) ) { 
	define( '__ROOT__', substr( __DIR__, 0, strlen( __DIR__ ) - strpos( __DIR__, '\includes' ) ) );

	$FILE = isset( $_GET['file'] ) ? str_ireplace( '.php', '', $_GET['file'] ) :  '' ;
	$FILE = ( file_exists( __DIR__ . '\\template\\'. $FILE .'.php') ) ? $FILE : 'default';
	require_once( __DIR__. '\\template\\'. $FILE . '.php');
}


$SEND = isset( $SEND ) && is_array( $SEND ) ? $SEND 
	: [ 	'mailfrom' => CONTACT_FORM_EMAIL_ADDRESS, 
			'namefrom' => SITENAME_PLAIN,
			'mailto' => CONTACT_FORM_EMAIL_ADDRESS ];
		
if( empty( $SEND['mailfrom'] ) ) $SEND['mailfrom'] = CONTACT_FORM_EMAIL_ADDRESS; 
if( empty( $SEND['namefrom'] ) ) $SEND['namefrom'] = SITENAME_PLAIN; 
if( empty( $SEND['mailto'] ) ) $SEND['mailto'] = CONTACT_FORM_EMAIL_ADDRESS; 
	

$adresblok =  ( isset( $contact['adres']->inhoud ) && !empty( $contact['adres']->inhoud ) ) ? $contact['adres']->inhoud : '<h4>'. SITENAME_PLAIN . '</h4>';
$adresblok .= '<small>';
$adresblok .= ( isset( $contact['tel'] ) && !empty( $contact['tel'] ) ) ? $contact['tel'] . '<br />': '' ;
$adresblok .= ( isset( $contact['fax'] ) && !empty( $contact['fax'] ) ) ? $contact['fax'] . '<br />': '' ;
$adresblok .= ( isset( $contact['mailto'] ) && !empty( $contact['mailto'] ) ) ? $contact['mailto'] : '';
$adresblok .= '</small>';


$Vervang = [
		'{HEADER}'		=> SITELINK . '/email/header.jpg',
		'{SITELINK}'	=> SITELINK,
		'{SITENAME}'	=> SITENAME_PLAIN,
		'{ADRES}'		=> $adresblok,
		'{DOMAIN}'		=> DOMAIN,
		'{HEIGHT}'		=> '80px',
		'{LOGO}'		=> IMAGES . '/logo.png',
		'{ONDERWERP}'	=> $Onderwerp,
		'{TEKST}'		=> $Body,
		'{FOOTER}' 		=> $Footer ];

foreach ($Vervang as $find => $replace){
	$template = str_ireplace($find, $replace, $template);
}


if( $SENDIT ) {
	$mail = new PHPMailer();
	$mail->IsHTML(true);
	$mail->IsSMTP();
	$mail->SetFrom( $SEND['mailfrom'] , $SEND['namefrom']);
		// edit om contactform af te vanden voor testen.
	
	$mail->AddAddress( $SEND['mailto'] );
	$mail->Subject = $Onderwerp;
	$mail->Body = $template;
	if( $mail->Send() ) {
		echo 'Mail verzonden.<hr />';
		echo $template;
		return true;
		
	} else return false;
} else {
	echo $template;
}