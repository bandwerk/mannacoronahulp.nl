<?php
$FILE = ( isset( $FILE ) && file_exists( __DIR__ . '/template/' . $FILE .'.php') ) ? $FILE : 'default';
require( __DIR__ . '/template/' . $FILE . '.php');

if( isset( $Onderwerp ) & isset( $Body ) && isset( $Footer ) ):

	$SEND = isset( $SEND ) && is_array( $SEND ) ? $SEND
		: [ 	'mailfrom' => CONTACT_FORM_EMAIL_ADDRESS,
				'namefrom' => SITENAME_MAIL,
				'mailto' => CONTACT_FORM_EMAIL_ADDRESS ];
			
	if( empty( $SEND['mailfrom'] ) ) $SEND['mailfrom'] = CONTACT_FORM_EMAIL_ADDRESS;
	if( empty( $SEND['namefrom'] ) ) $SEND['namefrom'] = SITENAME_MAIL;
	if( empty( $SEND['mailto'] ) ) $SEND['mailto'] = CONTACT_FORM_EMAIL_ADDRESS;
	
	$adresblok =  ( isset( $Pagina['locatie'][0]->titel ) && !empty( $Pagina['locatie'][0]->titel) ) ? '<strong>'. $Pagina['locatie'][0]->titel . '</strong>': '<strong>'. SITENAME_PLAIN . '</strong>';
	$adresblok .= ( isset( $Pagina['locatie'][0]->adres ) && !empty( $Pagina['locatie'][0]->adres ) ) ? '<br />'. $Pagina['locatie'][0]->adres : '' ;
	$adresblok .= ( isset( $Pagina['locatie'][0]->postcode ) && !empty( $Pagina['locatie'][0]->postcode ) ) ? '<br />'. $Pagina['locatie'][0]->postcode : '' ;
	$adresblok .= ( isset( $Pagina['locatie'][0]->plaats ) && !empty( $Pagina['locatie'][0]->plaats ) ) ? ' '. $Pagina['locatie'][0]->plaats : '' ;
	$adresblok .= ( isset( $Pagina['locatie'][0]->tel ) && !empty( $Pagina['locatie'][0]->tel ) ) ? '<br />'. $Pagina['locatie'][0]->tel : '' ;
	$adresblok .= ( isset( $Pagina['locatie'][0]->fax ) && !empty( $Pagina['locatie'][0]->fax ) ) ? '<br />'. $Pagina['locatie'][0]->fax : '' ;
	$adresblok .= ( isset( $Pagina['locatie'][0]->email ) && !empty( $Pagina['locatie'][0]->email ) ) ? '<br />' . $Pagina['locatie'][0]->email : '';
	
	$Vervang = [
			'{HEADER}'		=> IMAGES . '/logo.png',
			'{SITELINK}'	=> SITELINK,
			'{SITENAME}'	=> SITENAME_MAIL,
			'{ADRES}'		=> $adresblok,
			'{DOMAIN}'		=> DOMAIN,
			'{HEIGHT}'		=> '52px',
			'{LOGO}'		=> IMAGES . '/logo.png',
			'{ONDERWERP}'	=> $Onderwerp,
			'{TEKST}'		=> $Body,
			'{FOOTER}' 		=> $Footer ];
	
	foreach ($Vervang as $find => $replace){
		$template = str_ireplace($find, $replace, $template);
	}
	
	$mail = new PHPMailer();
	$mail->IsHTML(true);
	$mail->IsSMTP();
	$mail->CharSet = 'UTF-8';
	
	// Verzenden via Mihosnet
	$mail->Host = 'mail.mdns.nl';
	$mail->Port = 587;
	
	$mail->SMTPAuth = true;                // enable SMTP authentication
	$mail->Username = 'send@hwid2.nl';
	$mail->Password = '8A5gvd$4';
	
	if( strpos( $SEND['mailfrom'], ',' ) > 0 ) {
		$SEND['mailfrom'] = explode( ',', $SEND['mailfrom'] );
		$SEND['mailfrom'] = array_shift( $SEND['mailfrom'] );
	}
	$mail->SetFrom( CONTACT_FORM_EMAIL_ADDRESS );
	$mail->AddReplyTo( $SEND['mailfrom'] , $SEND['namefrom']);
	
	if( strpos( $SEND['mailto'], ',' ) > 0 ) {
		$SEND['mailto'] = explode( ',', $SEND['mailto'] );
	} elseif( !is_array( $SEND['mailto'] ) ) {
		$SEND['mailto'] = [ $SEND['mailto'] ];
	}
	
	foreach( $SEND['mailto'] AS $SendMailTo ) {
		$mail->AddAddress( trim( $SendMailTo ) );
	}
	
	$mail->Subject = $Onderwerp;
	$mail->Body = $template;
	
	if( $mail->Send() ) {
		if( isset( $SEND['contacted'] ) && $SEND['contacted'] ) $_SESSION['contacted']++;

		return true;
		
	} else return false;

else:
	return false;
endif;