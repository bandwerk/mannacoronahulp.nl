<?php
if ( !$template = file_get_contents( __ROOT__ . '/email/email_template.html') ){
	die('template niet gevonden');
}

$email= $TF->get_value( 'email' );

try {
	$emailQ = $db->prepare( 'SELECT `id`, `hash`, `voornaam` FROM `us_gebruikers` WHERE `email`=:email;');
	$emailQ->bindValue( ':email', $email, PDO::PARAM_STR );
	$emailQ->execute();
	
	if( $emailQ->rowCount() == 1 ) {
		$persoon = $emailQ->fetchAll( PDO::FETCH_ASSOC );
		$persoon = $persoon[0];
	} else {
		return false;
	}
	
} catch(PDOException $ex ) {
	die( $ex->getMessage() );
}

$naam = $persoon['voornaam'];
$token = $session->wachtwoord_reset( $persoon['id'], $persoon['hash'] );
$wwlink = ACCOUNT_URL . '/reset-wachtwoord/' . $persoon['id'] . '/' . $persoon['hash'] . '/' . $token;


$Onderwerp = 'Wachtwoord aanvraag voor ' . SITENAME_PLAIN;

$Body = '<br />
							<h1>Beste '. $naam . ', </h1>
							<p>
							Er is een aanvraag gedaan voor een nieuw wachtwoord op '. SITELINK .'. Volg de link in de knop hieronder of kopieer de volgende tekst in de adresbalk van je browser:<br /><a'. $HTM->get_href( $wwlink, 'Reset je wachtwoord' ) . '>'.
							$wwlink
							.'</a>
							</p>
							<br />
		<table width="315" border=0 cellpadding=0 cellspacing=0>
			<tr>
				<td width="300" color="#ffffff" bgcolor="#ed922d">
					<a'. $HTM->get_href( $wwlink, 'Klik hier om je wachtwoord aan te passen', 'btn' ) .' style="width:100%;height:100%;margin:0">WACHTWOORD AANPASSEN</a>
				</td>
				<td width="15"></td>
			</tr>
		</table>';


$SEND['mailto'] = $email;

$Footer = '';

$Vervang = [
		'{SITELINK}'	=> SITELINK,
		'{ONDERWERP}'	=> $Onderwerp,
		'{TEKST}'		=> $Body,
		'{FOOTER}' 		=> $Footer ];

foreach ($Vervang as $find => $replace){
	$template = str_ireplace($find, $replace, $template);
}


?>