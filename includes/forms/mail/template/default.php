<?php

if ( !$template = file_get_contents( __ROOT__  . '/email/email_template.html' ) ) {
	if( !$template = file_get_contents( __ROOT__ . '\email\email_template.html' ) ) {
		die('template niet gevonden');
	}
}

$Naam = isset( $SEND['naam'] ) && is_string( $SEND['naam'] ) ? $SEND['naam'] : SITENAME_PLAIN;

$Onderwerp = isset( $SEND['onderwerp'] ) && is_string( $SEND['onderwerp'] ) ? $SEND['onderwerp'] : 'Mail van ' . SITENAME_PLAIN;

$Body = isset( $SEND['body'] ) && is_string( $SEND['body']) ? $SEND['body'] : '
		Er is een mail verzonden door '. $Naam . '.<br />
		<br />';

$Footer = isset( $SEND['formdata'] ) && is_array( $SEND['formdata'] ) ? echoFormData( $SEND['formdata'] ) : '' ;
$Footer .= isset( $SEND['footer'] ) && is_string( $SEND['footer'] ) ? $SEND['footer'] : '';

$Vervang = [
		'{SITELINK}'	=> SITELINK,
		'{ONDERWERP}'	=> $Onderwerp,
		'{TEKST}'		=> $Body,
		'{FOOTER}' 		=> $Footer ];

foreach ($Vervang as $find => $replace){
	$template = str_ireplace($find, $replace, $template);
}
?>