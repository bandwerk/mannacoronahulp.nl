<?php

$AanbiedForm = new FormGen ('Hulp_aanbieden');

$AanbiedForm->AddField('text', 'voornaam', 'Voornaam:', '', [], true);
$AanbiedForm->AddField('text', 'achternaam', 'Achternaam:', '', [], true);
$AanbiedForm->AddField('tel', 'tel', 'Telefoonnummer:', 'tel', [], true);
$AanbiedForm->AddField('email', 'email', 'E-mail:', '', [], true);
$AanbiedForm->AddField('text', 'postcode', 'Postcode:', '', [], true);
$AanbiedForm->AddField('text', 'huisnummer', 'Huisnummer:', '', [], true);
$AanbiedForm->AddField('text', 'straat', 'Straat:', '', [], true);
$AanbiedForm->AddField('text', 'woonplaats', 'Woonplaats:', '', [], true);


$CoronaCaregiver = new CoronaCaregiver();

if ($AanbiedForm->ValidateForm()) {

    if ($Data = $AanbiedForm->GetFormData()) {
        if ($AanbiedForm->ValidateForm()) {
            $SEND = [
                'onderwerp' =>
                    'Contactformulier ingevoerd op ' . SITENAME_PLAIN,
                'body' =>
                    '<p>Er is een contactformulier via ' . SITENAME_PLAIN . ' opgestuurd.</p>
					<p>De volgende info is ingevoerd:</p>'
            ];


            $SEND ['formdata'] = [];
            $req = false;
            foreach ($AanbiedForm->GetInput() as $field => $val) {
                if (!isset ($SEND ['formdata']))
                    $SEND ['formdata'] = [];
                $val = strip_tags($val);

                switch ($field) {
                    case 'nobots' :
                    case 'submit' :
                        $val = null;
                        break;
                    case 'tel' :
                        $val = '<a' . $HTM->get_href('tel:' . preg_replace('/\D/', '', $val), 'bel naar: ' . $val) . '>' . $val . '</a><br />';
                        $req = true;
                        break;
                    case 'email' :
                        $SEND ['mailfrom'] = $val;
                        $val = '<a' . $HTM->get_href('mailto:' . $val, 'Stuur een mail naar: ' . $val) . '>' . $val . '</a><br />';
                        $req = true;
                        break;
                    case 'naam' :
                        $SEND ['naam'] = $val;
                        break;
                    case 'postcode':

                        $AdresZoek = $AanbiedForm->GetValue('adres') . ', ' . $AanbiedForm->GetValue('postcode') . ' ' . $AanbiedForm->GetValue('plaats');
                        $val = $AanbiedForm->getValue('adres') . '<br />' .
                            $AanbiedForm->getValue('plaats') . ' ' . $AanbiedForm->GetValue('postcode') . '<br /><a' . HtmGen::href('https://www.google.com/maps/place/' . rawurlencode($AdresZoek), 'Bekijk op google maps') . '>Bekijk op google maps</a>';
                        break;

                    case 'adres':
                    case 'straat':
                    case 'huisnummer':
                    case 'plaats':
                        $val = null;
                        break;
                    default :
                        break;
                }

                if (isset ($val) && !empty ($val)) {
                    $SEND ['formdata'] [str_replace('_', ' ', $field)] = $val;
                }
            }

            if (!$req) {
                $AanbiedForm->AddError('submit', 'Er is nog geen contactinformatie opgegeven');
            } else {
                /// PROCES DATA
                $adres = $_POST['Hulp_aanbieden_straat'] . " " . $_POST['Hulp_aanbieden_huisnummer'];
                $CoronaCaregiver->newCaregiver($_POST['Hulp_aanbieden_voornaam'], $_POST['Hulp_aanbieden_achternaam'], $_POST['Hulp_aanbieden_tel'], $_POST['Hulp_aanbieden_email'], $adres, $_POST['Hulp_aanbieden_postcode'], $_POST['Hulp_aanbieden_woonplaats'], $_POST['opmerking']);
                $CoronaCaregiver->SetRequestTypes($_POST['hulpvraag']);

                if ($CoronaCaregiver->SaveAsNew()) {

                    $AanbiedForm->SetSuccess();
                    header('Location: ' . SITELINK . '/bedankt');
                    die();
                } else {
                    $AanbiedForm->AddError('submit', 'Er is iets mis gegaan met het versturen van de mail.');
                }

                $AanbiedForm->ValidateForm();
            }

        }
    }
} else {
    $AanbiedForm->IsSuccess();
}


?>