<?php
$queryUrl = parse_url($_SERVER['REQUEST_URI']);
if(array_key_exists('query', $queryUrl)) {
    $queryUrl = $queryUrl['query'];
    if(substr( $queryUrl, 0, 10 ) === "inlineAuth") {
        $hash = explode('=', explode('&', $queryUrl)[0])[1];
        if(strlen($hash) == 32) {
            $_SESSION['inlineAuth'] = $hash;
            echo '<script>sessionStorage.setItem("inlineAuth", "'.$hash.'");</script>';
        }
    }
}

define( '__INC__', __DIR__ );
if( !defined( '__CLASSES__' ) ) {
	define( '__CLASSES__', __DIR__ . '/classes');
}


require_once(__DIR__ . '/classes/autoloader.php');
require_once(__DIR__ . '/classes/class.coronaclient.php');
require_once(__DIR__ . '/classes/class.coronacaregiver.php');

//require_once (__DIR__ . '/dbconnection.php');

session_start ();

header ( 'Content-type: text/html; charset=UTF-8' );
error_reporting ( E_ALL );
ini_set ( 'display_errors', 1 );
setlocale( LC_ALL, 'nl_NL', 'nld_nld');

// Hoe lang een sessie mee gaat / een half uur ( 60 sec * 30 min )
define( 'SESSION_AGE', (60*30));
if( ( isset( $_SESSION['generated'] ) && $_SESSION['generated'] > time() ) )  {
	// Sessie verlengen
	$_SESSION['generated'] = time() + SESSION_AGE;

} else {
	//Sessie vernieuwen
	session_regenerate_id(true);
	$_SESSION['generated'] = time() + SESSION_AGE;
}

// Toegestane ip adressen voor 'dev' mode
$ipDev = [
		'185.41.137.86',
		'94.229.59.119',
        '81.204.3.84',
		'127.0.0.1',
		'::1'
];

// Definieeren van dev mode
$dev = ( isset( $_SERVER['REMOTE_ADDR'] ) && in_array ( $_SERVER ['REMOTE_ADDR'], $ipDev ) );
$FULL = ( !isset( $db ) || !is_object( $db ) ) && ( !defined('CRON') || !CRON );


if (! $dev) {
	header( 'https://www.bandwerk.nl' );
	error_reporting ( E_STRICT );
	ini_set ( 'display_errors', 0 );
} else {
	error_reporting ( E_ALL );
	ini_set ( 'display_errors', 1 );
}

define ( 'DOMAIN', getenv( '_klantenmap_' ) ); // Noodzakelijk voor bandwerkplus systeem.

require_once __DIR__ . '/classes/pdoc/class.pdoc.connectionmanager.php';

global $db;
$db = PDOC\ConnectionManager::get();


require_once(__DIR__ . '/Functionz.php');
checkProtocol(true);

//$POST = array_map ( "safe_text", $_POST );
$GET = array_map ( "safe_text", $_GET );


if ($FULL) :
	/*
	 * Kijken of de site in development mode is
	 */

    $noshow = false;
	if (! isset ( $dev ) || ! $dev) {
		try {
			$DQ = $db->prepare ( 'SELECT `value` FROM `constants`
					WHERE `attribute`="IN_DEVELOPMENT";' );
			$DQ->execute();
			
			if ($DQ->rowCount () > 0) {
				$noshow = in_array(
							$DQ->fetch ( PDO::FETCH_OBJ )->value,
							[ 'ja', 'true', 1 ] );
			} else {
				$noshow = true;
			}
		} catch ( PDOException $ex ) {
			die ( $ex->getMessage () );
		}
	}

    if( isset( $GET['url_titels'] ) ):
		$GET['url_titels'] = explode( '/', $GET['url_titels'] );


        $x=1;
		if( !empty( $GET['url_titels'] ) ):
			foreach( $GET['url_titels'] AS $val ):
				$GET['url_titel' . ( $x>1? $x : '' ) ] = $val;
				$x++;
			endforeach;
		else:
			$GET['p']=1;
		endif;

		unset( $GET['url_titels'] );
	endif;

    if( isset( $GET['ids'] )):
		$x=1;
		foreach( explode( '/', $GET['ids'] ) AS $val ):
			$key = $x>1? $x : '';
			$GET['id' . $key ] = (int) $val;
			$x++;
		endforeach;
	
	unset( $GET['ids'] );
	
	endif;

    require_once(__DIR__ . '/Bandwerkplus/class.pager.php');
	require_once(__DIR__ . '/Bandwerkplus/maandweergaves.php');
	require_once(__DIR__ . '/Bandwerkplus/func.bwplus.php');
	require_once(__DIR__ . '/classes/class_sef.php');
	require_once(__DIR__ . '/classes/class.phpmailer.php');
	require_once(__DIR__ . '/classes/class.smtp.php');

	define ( 'MAX_FORM_CONTACTED', 10 );
	if (! isset ( $_SESSION ['contacted'] ) || ! isset ( $_SESSION ['contact_CREATED'] )) {
		$_SESSION ['contacted'] = 0;
		$_SESSION ['contact_CREATED'] = time ();
	} else if ((time () - $_SESSION ['contact_CREATED']) > (10 * 60)) {
		// session started more than 10 minutes
		$_SESSION ['contact_CREATED'] = time (); // update creation time
		$_SESSION ['contacted'] = 0;
	}

endif; // $FULL

	define ( 'SEO', true ); // Gebruik van SEO links: true or false.
	define ( 'EchoDebug', $dev );
	define ( '__CONFIGU__', __DIR__ . '/configu');
	define ( '__FORMS__', __DIR__ . '/forms');
 // output informatie over bijvoorbeeld nieuwe tabellen in SEO modules etc}
	
	require_once(__CONFIGU__ . '/sitelink.php');
	
	/*
	 * Maximaal $_SESSION['contacted'] en reset na 10 minutes
	 */

if( !defined('PROTOCOL' ) ) define( 'PROTOCOL' , 'http' );

if ( stripos ( str_replace ( 'www.', '', DOMAIN ), $_SERVER ['HTTP_HOST'] ) == false ){



    try {
		$thema_domain = str_ireplace( 
									[ 'http:', 'https:', '//', 'www.' ], 
									'', 
									$_SERVER ['HTTP_HOST'] );

		
		$themasite = $db->prepare ( 'SELECT
					`t`.*,
					`t`.`id` AS `thema_id`,
					0 AS `id`,
					`t`.`header` AS `titel`,
					group_concat( DISTINCT `tpp`.`id` ) AS `producten`,
					group_concat( DISTINCT concat_ws( ".", `tp`.`id`, `tp`.`ext` ) 
						ORDER BY `tp`.`albumthumb` DESC, `tp`.`id` ASC
					) AS `images`
				FROM `themapagina` `t`
					LEFT JOIN `themapagina_producten` `tlp`
						ON `t`.`id`=`tlp`.`themapagina_id`
					LEFT JOIN `producten` `tpp`
						ON `tlp`.`producten_id`=`tpp`.`id`
							AND `tpp`.`aktief`="ja"
					LEFT JOIN `themapagina_crop_photos` `tp`
						ON `t`.`id`=`tp`.`album_id`
				WHERE `url` LIKE :url 

				GROUP BY `t`.`id`
				
				LIMIT 1;' );
		
		$themasite->bindValue ( ':url', '%' . $thema_domain . '%', PDO::PARAM_STR );
		$themasite->execute ();
		
		if ($themasite->rowCount () == 1) {
			if( stripos( $_SERVER['HTTP_HOST'], 'www.' ) === false )
				header( 'location: '. PROTOCOL . '://www.'. $_SERVER['HTTP_HOST'] );
				
			$Pagina['themapagina'] = true;
			$Pagina['script_src'] = 'themapagina';
			$Pagina['is_script'] = true;
			
			$Pagina['template']['header'] = 'themapagina';
			$Pagina['thema']['data'] = $themasite->fetch ( PDO::FETCH_ASSOC );

			if( strtolower( $Pagina['thema']['data']['aktief'] ) == 'nee' )
				header('location: '. PROTOCOL . '://' . DOMAIN );

			$Pagina['thema']['url'] = PROTOCOL . '://www.' . $thema_domain;
			define( 'SITELINK_THEMA', PROTOCOL . '://www.' . $thema_domain );
		} else 		
			$Pagina['themapagina'] = false;

	} catch ( PDOException $ex ) {
		$Pagina['themapagina'] = false;
	}
} else {
	$Pagina['themapagina'] = false;
}

$sitelink = isset( $Pagina['themapagina'] ) && $Pagina['themapagina'] && isset( $Pagina['thema']['url'] ) ? $Pagina['thema']['url'] : getSitelink() ;

define ( 'SITELINK', $sitelink );
define ( 'HOME', defined( 'LANGUAGE_CODE' ) ? $sitelink . '/' . LANGUAGE_CODE : $sitelink );

/**
 * Verscheidene standaard variabelen aanmaken
 */

require_once(__DIR__ . '/configu/sitefunctions.php');
require_once(__DIR__ . '/configu/constants.php');

$HEADER ['Description'] = '';
$HEADER ['Keywords'] = SITENAME_PLAIN;
$HEADER ['Title'] = SITENAME_PLAIN;
$HEADER ['MasterTitle'] = ' | '. SITENAME_PLAIN . ( defined( 'CONTACT_PLAATS' ) && strpos( SITENAME_PLAIN, CONTACT_PLAATS ) === false ? ' - ' .CONTACT_PLAATS : '' );

require_once(__DIR__ . '/configu/objects.php');

if ($FULL):
	require_once(__DIR__ . '/configu/requests.php');

endif;

return true;
?>