<?php


$request = json_decode( file_get_contents('php://input') );
$feedback = [];

define( 'CRON', true);

require_once(__DIR__ . '/Configu.php');




if( !session_id() )
	$feedback[] = [ 'log' => 'geen sessie gevonden.'];
	
	else {
		// Jquery $.ajax requests afhandelen
		
		// requests via ajaxHandler var req afhandelen
		if( !empty( $request ) ) {
			require_once(__DIR__ . '/configu/sitefunctions.php');
			foreach( $request AS $data ) {
				switch( $data->type ) {
					case 'meerladen':
						global $HTM;
						if( !is_object( $HTM ) ) $HTM = new htmlGenerator();
						$categorie = str_replace( ' ', '',$data->get );
						$settings = [];
						$settings['returnAsArray'] = true;
						
						switch ($categorie ):
							case 'klanten':
									$limit = isset( $data->limit ) ? (int) $data->limit : DIENST_PER_PAGINA;
									$offset = isset( $data->page ) ? (int) ( $data->page - 1 ) * $limit : null;
									
									$weergegevenItems = $data->page * $limit;
									$limit = $offset ? $limit . ' OFFSET ' . $offset : $limit;

									if (isset($data->cat) && !empty($data->cat)) {
                                        $settings['where'] = [
                                            '`dd`.`titel`' => $data->cat
                                        ];
                                    }
									$settings['limit'] = $limit;
									$_SESSION['page']['nieuws'] = $data->page;

									$Return = [];
									$THISDATA = get_model( $categorie , 'klanten', $settings );
									
									if( hasContent( $THISDATA ) ):
										foreach( $THISDATA AS $R ) {
											
											if( isset( $R->image ) && !empty( $R->image ) )
												$R->thumb = KLANT_SFEER . '/' . $R->image;
												
												$Return[] = $R;
												
										}
										
										$renderAs = 'klanten';
										$data->lastpage = ( $R->totaal <= $weergegevenItems );
									endif;
									
									break;
							
						endswitch;
							
						if( count( $Return ) > 0 && !empty( $Return ) ) {
							$feedback[] = [
									'render' => $renderAs,
									'data' => $Return,
									'input' => $data
							];
						}
						break;
						
				}
			}
			
		} else {
			if( empty( $feedback ) ) $feedback[] = 'Geen request?';
		}
	}
	
	header('Content-type: application/json');
	echo json_encode( array_filter( $feedback ) );
	
	