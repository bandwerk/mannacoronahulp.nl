<?php
global $MENU, $db, $HTM, $Q, $ProjectTypes;
$MENU = new stdClass ();
$HTM = is_callable ( [ 
		$HTM,
		'set_meta'
] ) ? $HTM : new htmlGenerator ();

if (! defined ( 'PageID' )) {
	define ( 'PageID', 0 );
}

try {
	$WebsiteTabel = 'website';

    $mQ = $db->prepare ( 'SELECT
							w.id AS w_id,
							w.titel AS w_titel,
							seo.url_titel AS w_url,
							sc.script AS w_script,
							wsub.id AS wsub_id,
							wsub.titel AS wsub_titel,
							seo1.url_titel AS wsub_url,
							sc1.script AS wsub_script,
							wdet.id AS wdet_id,
							wdet.titel AS wdet_titel,
							seo2.url_titel AS wdet_url,
							sc2.script AS wdet_script,
							wdet2.id AS wdet2_id,
							wdet2.titel AS wdet2_titel,
							seo3.url_titel AS wdet2_url,
							sc3.script AS wdet2_script,
							wdet3.id AS wdet3_id,
							wdet3.titel AS wdet3_titel,
							seo4.url_titel AS wdet3_url,
							sc4.script AS wdet3_script' . (defined ( 'WEBSITE_PHOTO' ) && WEBSITE_PHOTO ? ',
							group_concat( concat_ws( ".", `w_p`.`id`, `w_p`.`ext` ) ORDER BY `w_p`.`albumthumb` DESC, `w_p`.`id` ASC ) AS `w_images`,
							group_concat( concat_ws( ".", `wsub_p`.`id`, `wsub_p`.`ext` ) ORDER BY `wsub_p`.`albumthumb` DESC, `wsub_p`.`id` ASC ) AS `wsub_images`,
							group_concat( concat_ws( ".", `wdet_p`.`id`, `wdet_p`.`ext` ) ORDER BY `wdet_p`.`albumthumb` DESC, `wdet_p`.`id` ASC ) AS `wdet_images`,
							group_concat( concat_ws( ".", `wdet2_p`.`id`, `wdet2_p`.`ext` ) ORDER BY `wdet2_p`.`albumthumb` DESC, `wdet2_p`.`id` ASC ) AS `wdet2_images`,
							group_concat( concat_ws( ".", `wdet3_p`.`id`, `wdet3_p`.`ext` ) ORDER BY `wdet3_p`.`albumthumb` DESC, `wdet3_p`.`id` ASC ) AS `wdet3_images`' : '') . '

						FROM ' . $WebsiteTabel . ' w
							LEFT JOIN `SEO_Module_link` s
								ON w.id=s.website_id
							LEFT JOIN `SEO_Module` seo
								ON s.`SEO_ID`=seo.`SEO_ID`
							LEFT JOIN `scripts` sc
								ON w.script=sc.id
			
							LEFT JOIN ' . $WebsiteTabel . ' wsub ON w.id=wsub.parent AND wsub.aktief="Ja"
								LEFT JOIN `SEO_Module_link` s1
									ON wsub.id=s1.`website_id`
								LEFT JOIN `SEO_Module` seo1
									ON s1.`SEO_ID`=seo1.`SEO_ID`
								LEFT JOIN `scripts` sc1
									ON wsub.script=sc1.id
			
							LEFT JOIN ' . $WebsiteTabel . ' wdet ON wsub.id=wdet.parent AND wdet.aktief="Ja"
								LEFT JOIN `SEO_Module_link` s2
									ON wdet.id=s2.`website_id`
								LEFT JOIN `SEO_Module` seo2
									ON s2.`SEO_ID`=seo2.`SEO_ID`
								LEFT JOIN `scripts` sc2
									ON wdet.script=sc2.id

							LEFT JOIN ' . $WebsiteTabel . ' wdet2 ON wdet.id=wdet2.parent AND wdet2.aktief="Ja"
								LEFT JOIN `SEO_Module_link` s3
									ON wdet2.id=s3.`website_id`
								LEFT JOIN `SEO_Module` seo3
									ON s3.`SEO_ID`=seo3.`SEO_ID`
								LEFT JOIN `scripts` sc3
									ON wdet2.script=sc3.id

							LEFT JOIN ' . $WebsiteTabel . ' wdet3 ON wdet2.id=wdet3.parent AND wdet3.aktief="Ja"
								LEFT JOIN `SEO_Module_link` s4
									ON wdet3.id=s4.`website_id`
								LEFT JOIN `SEO_Module` seo4
									ON s4.`SEO_ID`=seo4.`SEO_ID`
								LEFT JOIN `scripts` sc4
									ON wdet3.script=sc4.id

							' . (defined ( 'WEBSITE_PHOTO' ) && WEBSITE_PHOTO ? 'LEFT JOIN ' . $WebsiteTabel . '_crop_photos w_p
								ON w_p.album_id=w.id
							LEFT JOIN ' . $WebsiteTabel . '_crop_photos wsub_p
								ON wsub_p.album_id=wsub.id
							LEFT JOIN ' . $WebsiteTabel . '_crop_photos wdet_p
								ON wdet_p.album_id=wdet.id
							LEFT JOIN ' . $WebsiteTabel . '_crop_photos wdet2_p
								ON wdet2_p.album_id=wdet2.id
							LEFT JOIN ' . $WebsiteTabel . '_crop_photos wdet3_p
								ON wdet3_p.album_id=wdet3.id' : '') . '

			
							WHERE w.aktief="Ja"
								AND w.parent=0

							GROUP BY `w_id`, `wsub_id`, `wdet_id`, `wdet2_id`, `wdet3_id`

							ORDER BY w.sort ASC, wsub.sort ASC, wdet.sort ASC, wdet2.sort ASC, wdet3.sort ASC' );

	$mQ->execute ();
	if ($mQ->rowCount () == 0) {
		die ( 'De website is nog niet correct ingesteld. Er is nog geen pagina gedefinieerd.' );
	}

	$NAV = [ ];
	$URLS = [ ];
	$Pagina ['kruimel'] = [ ];
	foreach ( $mQ->fetchAll ( PDO::FETCH_ASSOC ) as $N ) {
		if ($N ['w_id'] == 1)
			$Pagina ['kruimel'] [] = [ 
					'titel' => $N ['w_titel'],
					'url' => SITELINK
			];

		if (! defined ( 'CatID' ) && (PageID == $N ['w_id'] || PageID == $N ['wsub_id'] || PageID == $N ['wdet_id'] || PageID == $N ['wdet2_id'] || PageID == $N ['wdet3_id']) && isset ( $N ['w_id'] ) && is_numeric ( $N ['w_id'] )) {
			define ( 'CatID', $N ['w_id'] );
			$Pagina ['header'] = ! empty ( $N ['w_images'] ) ? explode ( ',', $N ['w_images'] ) : false;
		}

		if (isset ( $N ['w_script'] ) && ! defined ( strtoupper ( $N ['w_script'] ) . '_ID' )) {
			define ( strtoupper ( $N ['w_script'] ) . '_ID', $N ['w_id'] );
		}

		if (isset ( $N ['w_script'] ) && ! empty ( $N ['w_script'] )) {
			$URLS [$N ['w_script']] = $N ['w_id'] == 1 ? SITELINK : SITELINK . '/' . $N ['w_url'];
			if ($N ['w_id'] > 1 && array_key_exists ( 'is_script', $Pagina ) && $Pagina ['is_script'] == 0 && strpos ( HtmGen::url (), $URLS [$N ['w_script']] ) !== false) {
				$Pagina ['is_script'] = 1;
				$Pagina ['script_src'] = $N ['w_script'];
			}
		}

		if (isset ( $N ['w_script'] ) && ! empty ( $N ['w_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['w_script'] ) . '.php' )) {
			require (__DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['w_script'] ) . '.php');
		} else {
			$NAV [$N ['w_id']] ['id'] = $N ['w_id'];
			$NAV [$N ['w_id']] ['url'] = $N ['w_id'] == 1 ? '' : $N ['w_url'];
			$NAV [$N ['w_id']] ['titel'] = $N ['w_titel'];
			$NAV [$N ['w_id']] ['script'] = $N ['w_script'];
			$NAV [$N ['w_id']] ['image'] = ! empty ( $N ['w_images'] ) ? explode ( ',', $N ['w_images'] ) : false;
			$URLS [$N ['w_id']] = $N ['w_id'] == 1 ? SITELINK : SITELINK . '/' . $N ['w_url'];

			if (is_array ( $Pagina ['kruimel'] ) && PageID == $N ['w_id']) :
				$Pagina ['kruimel'] [] = [ 
						'titel' => $N ['w_titel'],
						'url' => SITELINK . '/' . $N ['w_url']
				];
			endif;

		}

		if (isset ( $N ['wsub_id'] ) & ! empty ( $N ['wsub_id'] ) && ! (isset ( $N ['w_script'] ) && ! empty ( $N ['w_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['w_script'] ) . '.php' ))) {
			if (isset ( $N ['wsub_script'] ) && ! defined ( strtoupper ( $N ['wsub_script'] ) . '_ID' )) {
				define ( strtoupper ( $N ['wsub_script'] ) . '_ID', $N ['wsub_id'] );

				$URLS [$N ['wsub_script']] = SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'];
				if (array_key_exists ( 'is_script', $Pagina ) && $Pagina ['is_script'] == 0 && strpos ( HtmGen::url (), $URLS [$N ['wsub_script']] ) !== false) {
					$Pagina ['is_script'] = 1;
					$Pagina ['script_src'] = $N ['wsub_script'];
				}
			}

			if (isset ( $N ['wsub_script'] ) && ! empty ( $N ['wsub_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wsub_script'] ) . '.php' )) {
				require (__DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wsub_script'] ) . '.php');
			} else {
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['id'] = $N ['wsub_id'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['url'] = $N ['wsub_url'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['titel'] = $N ['wsub_titel'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['script'] = $N ['wsub_script'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['image'] = ! empty ( $N ['wsub_images'] ) ? explode ( ',', $N ['wsub_images'] ) : false;
				$URLS [$N ['wsub_id']] = SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'];

				if (is_array ( $Pagina ['kruimel'] ) && PageID == $N ['wsub_id']) :
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['w_titel'],
							'url' => SITELINK . '/' . $N ['w_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wsub_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url']
					];
				endif;

			}
		}

		if (isset ( $N ['wdet_id'] ) & ! empty ( $N ['wdet_id'] ) && ! (isset ( $N ['wsub_script'] ) && ! empty ( $N ['wsub_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wsub_script'] ) . '.php' )) && ! (isset ( $N ['w_script'] ) && ! empty ( $N ['w_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['w_script'] ) . '.php' ))) {
			if (isset ( $N ['wdet_script'] ) && ! defined ( strtoupper ( $N ['wdet_script'] ) . '_ID' )) {
				define ( strtoupper ( $N ['wdet_script'] ) . '_ID', $N ['wdet_id'] );
				$URLS [$N ['wdet_script']] = SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url'];
				if ($Pagina ['is_script'] == 0 && strpos ( HtmGen::url (), $URLS [$N ['wdet_script']] ) !== false) {
					$Pagina ['is_script'] = 1;
					$Pagina ['script_src'] = $N ['wdet_script'];
				}
			}

			if (isset ( $N ['wdet_script'] ) && ! empty ( $N ['wdet_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wdet_script'] ) . '.php' )) {
				require (__DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wdet_script'] ) . '.php');
			} else {
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['id'] = $N ['wdet_id'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['url'] = $N ['wdet_url'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['titel'] = $N ['wdet_titel'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['script'] = $N ['wdet_script'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['image'] = ! empty ( $N ['wdet_images'] ) ? explode ( ',', $N ['wdet_images'] ) : false;
				$URLS [$N ['wdet_id']] = SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url'];

				if (is_array ( $Pagina ['kruimel'] ) && PageID == $N ['wdet_id']) :
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['w_titel'],
							'url' => SITELINK . '/' . $N ['w_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wsub_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wdet_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url']
					];
				endif;

			}
		}

		if (isset ( $N ['wdet2_id'] ) & ! empty ( $N ['wdet2_id'] ) && ! (isset ( $N ['wdet_script'] ) && ! empty ( $N ['wdet_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wdet_script'] ) . '.php' )) && ! (isset ( $N ['wsub_script'] ) && ! empty ( $N ['wsub_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wsub_script'] ) . '.php' )) && ! (isset ( $N ['w_script'] ) && ! empty ( $N ['w_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['w_script'] ) . '.php' ))) {
			if (isset ( $N ['wdet2_script'] ) && ! defined ( strtoupper ( $N ['wdet2_script'] ) . '_ID' )) {
				define ( strtoupper ( $N ['wdet2_script'] ) . '_ID', $N ['wdet2_id'] );
				$URLS [$N ['wdet2_script']] = SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url'] . '/' . $N ['wdet2_url'];
				if ($Pagina ['is_script'] == 0 && strpos ( HtmGen::url (), $URLS [$N ['wdet2_script']] ) !== false) {
					$Pagina ['is_script'] = 1;
					$Pagina ['script_src'] = $N ['wdet2_script'];
				}
			}

			if (isset ( $N ['wdet2_script'] ) && ! empty ( $N ['wdet2_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wdet2_script'] ) . '.php' )) {
				require (__DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wdet2_script'] ) . '.php');
			} else {
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['id'] = $N ['wdet2_id'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['url'] = $N ['wdet2_url'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['titel'] = $N ['wdet2_titel'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['script'] = $N ['wdet2_script'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['image'] = ! empty ( $N ['wdet2_images'] ) ? explode ( ',', $N ['wdet2_images'] ) : false;
				$URLS [$N ['wdet2_id']] = SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url'] . '/' . $N ['wdet2_url'];

				if (is_array ( $Pagina ['kruimel'] ) && PageID == $N ['wdet2_id']) :
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['w_titel'],
							'url' => SITELINK . '/' . $N ['w_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wsub_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wdet_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wdet2_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url'] . '/' . $N ['wdet2_url']
					];
				endif;

			}
		}

		if (isset ( $N ['wdet3_id'] ) & ! empty ( $N ['wdet3_id'] ) && ! (isset ( $N ['wdet2_script'] ) && ! empty ( $N ['wdet2_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wdet2_script'] ) . '.php' )) && ! (isset ( $N ['wdet_script'] ) && ! empty ( $N ['wdet_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wdet_script'] ) . '.php' )) && ! (isset ( $N ['wsub_script'] ) && ! empty ( $N ['wsub_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wsub_script'] ) . '.php' )) && ! (isset ( $N ['w_script'] ) && ! empty ( $N ['w_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['w_script'] ) . '.php' ))) {
			if (isset ( $N ['wdet3_script'] ) && ! defined ( strtoupper ( $N ['wdet3_script'] ) . '_ID' )) {
				define ( strtoupper ( $N ['wdet3_script'] ) . '_ID', $N ['wdet3_id'] );
				$URLS [$N ['wdet3_script']] = SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url'] . '/' . $N ['wdet2_url'] . '/' . $N ['wdet3_url'];
				if ($Pagina ['is_script'] == 0 && strpos ( HtmGen::url (), $URLS [$N ['wdet3_script']] ) !== false) {
					$Pagina ['is_script'] = 1;
					$Pagina ['script_src'] = $N ['wdet3_script'];
				}
			}

			if (isset ( $N ['wdet3_script'] ) && ! empty ( $N ['wdet3_script'] ) && file_exists ( __DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wdet3_script'] ) . '.php' )) {
				require (__DIR__ . '/nav/' . str_ireplace ( '.php', '', $N ['wdet3_script'] ) . '.php');
			} else {
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['sub'] [$N ['wdet3_id']] ['id'] = $N ['wdet3_id'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['sub'] [$N ['wdet3_id']] ['url'] = $N ['wdet3_url'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['sub'] [$N ['wdet3_id']] ['titel'] = $N ['wdet3_titel'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['sub'] [$N ['wdet3_id']] ['script'] = $N ['wdet3_script'];
				$NAV [$N ['w_id']] ['sub'] [$N ['wsub_id']] ['sub'] [$N ['wdet_id']] ['sub'] [$N ['wdet2_id']] ['sub'] [$N ['wdet3_id']] ['image'] = ! empty ( $N ['wdet3_images'] ) ? explode ( ',', $N ['wdet3_images'] ) : false;
				$URLS [$N ['wdet3_id']] = SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url'] . '/' . $N ['wdet2_url'] . '/' . $N ['wdet3_url'];

				if (is_array ( $Pagina ['kruimel'] ) && PageID == $N ['wdet3_id']) :
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['w_titel'],
							'url' => SITELINK . '/' . $N ['w_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wsub_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wdet_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wdet2_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url'] . '/' . $N ['wdet2_url']
					];
					$Pagina ['kruimel'] [] = [ 
							'titel' => $N ['wdet3_titel'],
							'url' => SITELINK . '/' . $N ['w_url'] . '/' . $N ['wsub_url'] . '/' . $N ['wdet_url'] . '/' . $N ['wdet2_url'] . '/' . $N ['wdet3_url']
					];
				endif;

			}
		}
	}
} catch ( PDOException $ex ) {
	die ( $ex->getMessage () );
}

if (! defined ( 'CatID' ))
	define ( 'CatID', 0 );

$MENU = (object) [
	'd' => '<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">',
];

foreach ( $NAV as $website_id => $W ) {
	if (isset ( $W ['script'] ) && ! empty ( $W ['script'] ) && file_exists ( __DIR__ . '/nav/menu/' . str_ireplace ( '.php', '', $W ['script'] ) . '.php' )) {
		require (__DIR__ . '/nav/menu/' . str_ireplace ( '.php', '', $W ['script'] ) . '.php');
	} elseif ($W ['id'] >= 1) {

		if (strpos ( $W ['url'], SITELINK ) === 0 || $W ['url'] === '#')
			$LINK = $W ['url'];
		else {
			$W ['url'] = generate_url_from_text ( $W ['url'] );
			$LINK = SITELINK . '/' . $W ['url'];
		}

		$W ['class'] = [ 'nav-item'];
		if (isset ( $W ['sub'] ))
			$W ['class'] [] = 'has-sub';
		if (isset ( $W ['cat_class'] ) && $W ['cat_class'])
			$W ['class'] [] = $W ['cat_class'];
		if( PageID == $W['id'] )
			$W['class'][] = 'active';

		$MENU->d .= '<li';
		if (count ( $W ['class'] ) > 0)
			$MENU->d .= ' class="' . implode ( ' ', $W ['class'] ) . '"';
		$MENU->d .= '>
			<a class="nav-link"' . HtmGen::href ( $LINK, 'Ga naar: ' . $W ['titel'], CatID == $W ['id'] ? 'active' : null ) . '>' . htmlentities ( $W ['titel'] ) . '</a>';

		if (isset ( $W ['sub'] )) {
			$MENU->d .= '<ul' . (CatID == $W ['id'] ? ' style="display:block"' : '') . ' class="sub-menu">';

			foreach ( $W ['sub'] as $sub_id => $S ) {
				if (isset ( $S ['script'] ) && ! empty ( $S ['script'] ) && file_exists ( __DIR__ . '/nav/menu/' . str_ireplace ( '.php', '', $S ['script'] ) . '.php' )) {
					require (__DIR__ . '/nav/menu/' . str_ireplace ( '.php', '', $S ['script'] ) . '.php');
				} else {

					if (strpos ( $S ['url'], SITELINK ) === 0)
						$LINK = $S ['url'];
					else {
						$S ['url'] = generate_url_from_text ( $S ['url'] );
						$LINK = SITELINK . '/' . $W ['url'] . '/' . $S ['url'];
					}

					$linkAttr = HtmGen::href ( $LINK, 'Ga naar: ' . $S ['titel'] );
					;

					$dClass = (isset ( $S ['sub'] ) && ! empty ( $S ['sub'] )) ? ' class="has-sub"' : '';
					$MENU->d .= '<li' . $dClass . '><a' . $linkAttr;
					if ((isset ( $S ['id'] ) && $website_id === $S ['id']) || (isset ( $Pagina ['CategorieID'] ) && CatID . '_' . $Pagina ['CategorieID'] == $S ['id']))
						$MENU->d .= ' class="active"';
					$MENU->d .= '>' . htmlentities ( $S ['titel'] ) . '</a>';

					if (isset ( $S ['sub'] )) {

						$MENU->d .= '<ul>';
						// $MENU->d->mobiel .= '<ul>';

						foreach ( $S ['sub'] as $det_id => $D ) {
							if (isset ( $D ['script'] ) && ! empty ( $D ['script'] ) && file_exists ( __DIR__ . '/nav/menu/' . str_ireplace ( '.php', '', $D ['script'] ) . '.php' )) {
								require (__DIR__ . '/nav/menu/' . str_ireplace ( '.php', '', $D ['script'] ) . '.php');
							} else {
								if (strpos ( $D ['url'], SITELINK ) === 0)
									$LINK = $D ['url'];
								else {
									$D ['url'] = generate_url_from_text ( $D ['url'] );
									$LINK = SITELINK . '/' . $W ['url'] . '/' . $S ['url'] . '/' . $D ['url'];
								}

								$link = '<a' . HtmGen::href ( $LINK, 'Ga naar: ' . $D ['titel'] ) . '>' . htmlentities ( $D ['titel'] ) . '</a>';
								$MENU->d .= '<li>' . $link . '</li>';
							}
						}
						$MENU->d .= '</ul><!-- .subsubsub -->';
					}

					$MENU->d .= '</li>';
				}
			}
			$MENU->d .= '</ul>';
		}
		$MENU->d .= '</li>';
	}
}

//if( defined( 'CONTACT_TEL' ) && CONTACT_TEL ):
//	$MENU->d .= '<li class="nav-item tel"><a'. HtmGen::href( 'tel:' . HtmGen::tel( CONTACT_TEL ), 'Bel naar '.  CONTACT_TEL ) .'>' . ' ' . htmlentities( CONTACT_TEL ) . '</a></li>';
//endif;

if (isset($_SESSION['userId'])) {
    $MENU->d .= "<li class='nav-item'><a class='nav-link' href='".SITELINK."/beheer' alt=\"Ga naar: Beheer\" title=\"Ga naar: Beheer\">Beheer</a></li>";
    $MENU->d .= "<li class='nav-item'><a class='nav-link' href='".SITELINK."/beheer?logout=1' alt=\"Ga naar: log uit\" title=\"Ga naar: log uit\">Log uit</a></li>";
}

$MENU->d .= '</ul></div>';

$desktopToMobileMenu = [
		'<div class="" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">' => '<ul class="mobile-menu">',
		' class="nav-item active"' => ' class="active"',
		'</ul></div>' => '</ul>'

];

$MENU->m = str_replace( array_keys( $desktopToMobileMenu ), array_values( $desktopToMobileMenu ), $MENU->d );

if (! isset ( $NAV [1] )) {
	$NAV [1] = [ 
			'id' => 1,
			'url' => SITELINK,
			'titel' => 'Home',
			'script' => 'home'
	];
	$URLS [1] = SITELINK;

	array_unshift ( $Pagina ['kruimel'], [ 
			'titel' => 'Home',
			'url' => SITELINK
	] );
}

?>