<?php

/*
 * Login formulier
 */

// Array waarin de bestanden die geladen moeten worden, worden toegevoegd
$FORM = [
		'default'
];

if( !defined( '__FORM__' ) )
	define( '__FORM__', __DIR__ . '/forms');

/**
 * Formulieren op basis van url_titel parameter
 */

if (isset ( $GET ['url_titel'] )) {
	switch ($GET ['url_titel']) {
		/*
		 * Formulier contact pagina
		 */
		case 'inschrijven' :
			$FORM [] = 'inschrijven';
			// $FORM [] = 'uploadphoto';
			
			break;
		case 'account' :
			if (isset ( $GET ['url_titel2'] )) {
				
				// Array waarin de uitzonderingen voor de redirect worden bijgehouden
				$notLoggedIn = [ 'registreer', 'registreren', 'wachtwoord-vergeten', 'reset-wachtwoord', 'wachtwoord-reset' ];
				
				global $ACCOUNT;
				switch ($GET ['url_titel2']) {
					case 'registreer' :
					case 'registreren' :
						$FORM [] = 'registreer';
						// $FORM [] = 'uploadphoto';
						
						break;
					case 'login' :
						
						break;
					case 'wijzig-persoons-gegevens' :
						$FORM [] = 'wijzig-login-gegevens';
					// $FORM [] = 'uploadphoto';
					
					default :
						$ACCOUNT = generate_url_from_text ( $GET ['url_titel2'] );
						break;
				}

				if (! $session->loggedin () && !in_array( $ACCOUNT, $notLoggedIn ) ) {
					$ACCOUNT = 'login';
				}
				
				$FORM [] = $ACCOUNT;
			} else {
				$ACCOUNT = 'login';
				$FORM [] = 'login';
			}
			break;
		default:
			$FORM [] = $GET ['url_titel'];
			break;
	}
}

/*
 * Formulieren voor de account pagina
 */
/*
 * Arrays voor select geboortedag/maand/jaar
 */
for($i = 1; $i <= 31; $i ++) {
	if ($i <= 31)
		$ArD [$i] = $i;
	if ($i <= 12)
		$ArM [$i] = ucfirst ( strftime ( '%B', mktime ( 0, 0, 0, $i ) ) );
}
for($i = strftime ( '%Y', time () ) - 18; strftime ( '%Y', time () ) - 115 < $i; $i --) {
	$ArJ [$i] = $i;
}

$dir = str_replace ( '.php', '', __FILE__ );
foreach ( array_unique ( $FORM ) as $FILE ) {
	if (file_exists ( $dir . '/' . $FILE . '.php' )) {
		require_once ($dir . '/' . $FILE . '.php');
	}
}



