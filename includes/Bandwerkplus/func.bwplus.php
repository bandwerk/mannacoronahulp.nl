<?php 
	/*
		Dit bestand moet een oplossing bieden om veel herhaalde stukken code gerelateerd aan de Bandwerkplus database structuur nu centraal te houden.
		De bedoeling is dat alle functions met betrekking tot content hier wordt geregeld. 
		Dus geen htmlspecialchars op 10 verschillende pagina's. Hopelijk verminderd dit de rommelige code op sommige plekken en maakt het alles wat overzichtelijker, hence kunnen we productiever werken.
	*/
	

	function GetPagina($pagina_id){
		// Haalt pagina info op. Returned array op positief en false op geen result.
		
		global $db;
		
		$Q = 'SELECT 
				`w`.*,
				CASE 
					WHEN `s`.`id` > 0 THEN true
					ELSE false
				END AS `is_script`,
				`s`.`script` AS `script_src`
			FROM `website` `w`
				LEFT JOIN `scripts` `s`
					ON `w`.`script`=`s`.`id`
			WHERE `w`.`id`=:id;';
		
		$Q = $db->prepare( $Q );
		$Q->bindValue( ':id', (int) $pagina_id, PDO::PARAM_INT );
		
		if( $Q->execute() && $Q->rowCount() > 0 ) {
			$Pagina = $Q->fetch(PDO::FETCH_ASSOC);
			if( isset( $Pagina['inhoud'] ) && !empty( $Pagina['inhoud'] ) )
				$Pagina['inhoud'] = ReplaceContent( $Pagina['inhoud'] );
			
			return $Pagina;
		}
		
		return false;
		
	}
	
	function GetNieuws($nieuws_id){
		global $maandweergave, $maandweergaveuitgebreid;
		
		// Haalt 1 rij uit nieuws tabel op.
		$Query = mysql_query("SELECT * FROM nieuws WHERE id = '$nieuws_id' AND actief = 'ja'");
		if (mysql_num_rows($Query) > 0){
			$Nieuws = mysql_fetch_assoc($Query);
			
			// Fotos ophalen
			$Query = mysql_query("SELECT * FROM photo_nieuws WHERE album_id = '$nieuws_id'");
			$Fotos = array();
			while ($P = mysql_fetch_assoc($Query)){
				$Fotos[] = $P;
			}
			
			$Nieuws['fotos'] = $Fotos;
			
			if (empty($Fotos)){
				$Nieuws['has_foto'] = 0;
			} else {
				$Nieuws['has_foto'] = 1;
			}
			
			// Previeuw foto berekenen.
			if (!empty($Nieuws['fotos'])){
				$Nieuws['preview_foto'] = NIEUWSIMAGE_THUMB.'/'.$Nieuws['fotos'][0]['id'].'.jpg';
				$Nieuws['preview_foto_klein'] = NIEUWSIMAGE_THUMBKLEIN.'/'.$Nieuws['fotos'][0]['id'].'.jpg';
				$Nieuws['preview_foto_big'] = NIEUWSIMAGE.'/'.$Nieuws['fotos'][0]['id'].'.jpg';
			} else {
				$Nieuws['preview_foto'] = DEFAULT_NEWS_PHOTO;
				$Nieuws['preview_foto_klein'] = DEFAULT_NEWS_PHOTO;
				$Nieuws['preview_foto_big'] = DEFAULT_NEWS_PHOTO;
			}
			
			list($jaar, $maand, $dag) = explode('-', $Nieuws['datum']);
			
			$maand = (int) $maand;
			$dag = (int) $dag;
			
			$Nieuws['datum_mooi'] = $dag.' '.$maandweergaveuitgebreid[$maand].' '.$jaar;
			$Nieuws['datum_mini'] = $dag.' '.$maandweergave[$maand];
			
			$preview_tekst = $Nieuws['korte_inleiding'];
			if (strlen($preview_tekst) < 5){
				$preview_tekst = $Nieuws['inhoud'];
			}
			
			$Nieuws['preview_tekst'] = Truncate(strip_tags($preview_tekst), $length=150, $trailing='...');
			$Nieuws['datum_nl'] = DateConvertTo($Nieuws['datum'], 'nl');
			$Nieuws['url'] = SITELINK.'/nieuws/'.$nieuws_id.'/'.generate_url_from_text($Nieuws['titel']).'.html';
			$Nieuws['inhoud'] = ReplaceContent($Nieuws['inhoud']);
			
			return $Nieuws;
		} else {
			return false;
		}
	}
	
	function GetProject($project_id){
		global $maandweergave, $maandweergaveuitgebreid;
		
		// Haalt 1 rij uit nieuws tabel op.
		$Query = mysql_query("SELECT * FROM projecten WHERE id = '$project_id' AND actief = 'ja'");
		if (mysql_num_rows($Query) > 0){
			$Nieuws = mysql_fetch_assoc($Query);
			
			// Fotos ophalen
			$Query = mysql_query("SELECT * FROM photo_spelers WHERE album_id = '$project_id'");
			$Fotos = array();
			while ($P = mysql_fetch_assoc($Query)){
				$Fotos[] = $P;
			}
			
			$Nieuws['fotos'] = $Fotos;
			
			if (empty($Fotos)){
				$Nieuws['has_foto'] = 0;
			} else {
				$Nieuws['has_foto'] = 1;
			}
			
			// Previeuw foto berekenen.
			if (!empty($Nieuws['fotos'])){
				$Nieuws['preview_foto'] = PROJECTIMAGE_THUMB.'/'.$Nieuws['fotos'][0]['id'].'.jpg';
				$Nieuws['preview_foto_klein'] = PROJECTIMAGE_THUMBKLEIN.'/'.$Nieuws['fotos'][0]['id'].'.jpg';
				$Nieuws['preview_foto_big'] = PROJECTIMAGE.'/'.$Nieuws['fotos'][0]['id'].'.jpg';
			} else {
				$Nieuws['preview_foto'] = DEFAULT_PROJECT_PHOTO;
				$Nieuws['preview_foto_klein'] = DEFAULT_PROJECT_PHOTO;
				$Nieuws['preview_foto_big'] = DEFAULT_PROJECT_PHOTO;
			}
			
			list($jaar, $maand, $dag) = explode('-', $Nieuws['datum']);
			
			$maand = (int) $maand;
			$dag = (int) $dag;
			
			$Nieuws['datum_mooi'] = $dag.' '.$maandweergaveuitgebreid[$maand].' '.$jaar;
			$Nieuws['datum_mini'] = $dag.' '.$maandweergave[$maand];

			$Nieuws['preview_tekst'] = Truncate(strip_tags($Nieuws['inhoud']), $length=100, $trailing='...');
			$Nieuws['datum_nl'] = DateConvertTo($Nieuws['datum'], 'nl');
			$Nieuws['url'] = SITELINK.'/projecten/'.$project_id.'/'.generate_url_from_text($Nieuws['titel']).'.html';
			$Nieuws['inhoud'] = ReplaceContent($Nieuws['inhoud']);
			
			return $Nieuws;
		} else {
			return false;
		}
	}

	function generate_url_from_text($strText){
		$strText = preg_replace('/[^A-Za-z0-9-]/', ' ', $strText);
		$strText = preg_replace('/ +/', ' ', $strText);
		$strText = preg_replace('/ amp/', '-en', $strText);		
		$strText = trim($strText);
		$strText = str_replace(' ', '-', $strText);
		$strText = preg_replace('/-+/', '-', $strText);
		$strText = strtolower($strText);
		return $strText;
	}
	
	function InputValue($name){
		// Deze functie bepaald of een input waarde een GET of POST waarde is. Met prioriteit voor POST
		if (isset($_POST[$name]) && !empty($_POST[$name])){
			return $_POST[$name];
		} else if (isset($_GET[$name]) && !empty($_GET[$name])){
			return ($_GET[$name]);
		} else {
			return false;
		}
	}

?>