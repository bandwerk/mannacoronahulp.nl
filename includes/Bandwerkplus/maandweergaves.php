<?php

$maandweergave = array();
$maandweergave[1] = "Jan";
$maandweergave[2] = "Feb";
$maandweergave[3] = "Mrt";
$maandweergave[4] = "Apr";
$maandweergave[5] = "Mei";
$maandweergave[6] = "Jun";
$maandweergave[7] = "Jul";
$maandweergave[8] = "Aug";
$maandweergave[9] = "Sep";
$maandweergave[10] = "Okt";
$maandweergave[11] = "Nov";
$maandweergave[12] = "Dec";

$maandweergaveuitgebreid = array();
$maandweergaveuitgebreid[1] = "Januari";
$maandweergaveuitgebreid[2] = "Februari";
$maandweergaveuitgebreid[3] = "Maart";
$maandweergaveuitgebreid[4] = "April";
$maandweergaveuitgebreid[5] = "Mei";
$maandweergaveuitgebreid[6] = "Juni";
$maandweergaveuitgebreid[7] = "Juli";
$maandweergaveuitgebreid[8] = "Augustus";
$maandweergaveuitgebreid[9] = "September";
$maandweergaveuitgebreid[10] = "Oktober";
$maandweergaveuitgebreid[11] = "November";
$maandweergaveuitgebreid[12] = "December";

$maandweergavecijfer = array();
$maandweergavecijfer[1] = "01";
$maandweergavecijfer[2] = "02";
$maandweergavecijfer[3] = "03";
$maandweergavecijfer[4] = "04";
$maandweergavecijfer[5] = "05";
$maandweergavecijfer[6] = "06";
$maandweergavecijfer[7] = "07";
$maandweergavecijfer[8] = "08";
$maandweergavecijfer[9] = "09";
$maandweergavecijfer[10] = "10";
$maandweergavecijfer[11] = "11";
$maandweergavecijfer[12] = "12";
?>