<?php
$partners = $D;
?>
<div class="partners">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title">Onze partners:</h2>
            </div>
        </div>
        <div class="row">
            <?php
            $counter = 0;
            foreach($partners as $partner) {
                echo ($counter%5 == 0 ? "</div><div class='row'>" : "");
                ?>
            <div class="col-md-20">
                <a href="<?= $partner->website ?>" target="_blank"><img src="<?= PARTNERS_MIDDEL . "/" . $partner->partner_image ?>" class="partner-image"></a>
            </div>
            <?php
                $counter++;
            } ?>

        </div>
    </div>
</div>
