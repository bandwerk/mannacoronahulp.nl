<?php
?>
<div class="hulp-cta">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cta">
                    <div class="row">
                        <div class="col-md-2"><span class="white-line"></span></div>
                        <div class="col-md-8"><p>Heeft u hulp nodig? <br>Of wilt u een ander graag helpen?</p></div>
                        <div class="col-md-2"><span class="white-line"></span></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?= SITELINK ?>/hulp-aanvragen" class="help-button left">Ik heb hulp nodig<div class="bg"></div></a>
                        </div>
                        <div class="col-md-6">
                            <a href="<?= SITELINK ?>/hulp-aanbieden" class="help-button right">Ik wil graag helpen<div class="bg"></div></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="hashtag">Voor alle inwoners in de gemeente Twenterand</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

