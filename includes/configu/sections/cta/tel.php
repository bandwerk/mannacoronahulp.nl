<div class="tel-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Liever telefonisch contact?</h3>
                <p>Bel met #nietalleen:</p>
                <a href="tel:08001322" class="btn btn-tel"><?= Svg::img("phone-alt"); ?>0800 1322</a>
                <p class="times">maandag t/m vrijdag: van 09.00 - 21.00 uur<br>
                    zaterdag en zondag: van 09.00 - 20.00 uur</p>
            </div>
        </div>
    </div>
</div>