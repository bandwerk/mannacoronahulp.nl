<?php 
if( is_object( $D ) && isset( $D->images ) ) {
	$D = $D->images;
} elseif( !is_array( $D ) ) {
	$D = [ $D ];
}

?>

<aside class="<?php if( isset( $class ) && !empty( $class ) ): echo $class; else: ?>col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12<?php endif; ?>">
	<div class="img-slider">
		<?php foreach( $D AS $Image ): ?>
					<div class="slide">
			<a<?php echo HtmGen::href( $padGroot . '/' . $Image, 'Foto bij ' . $titel ); ?>
				data-rel="lightcase::group"> <img
				<?php echo HtmGen::src( $padDetail . '/' . $Image, 'Foto bij ' . $titel, 'img-responsive' ); ?> />
			</a>
		</div>
		<?php endforeach; ?>
	</div>
<?php if( count( $D ) == 1  ): ?>
	<br /> <br />
<?php endif; ?>
</aside>