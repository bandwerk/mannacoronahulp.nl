<button class="<?php if( isset( $extra['class'] ) && !empty( $extra['class'] ) ): echo $extra['class']; else: ?>btn-default laden<?php endif; ?>"
   bw-event="click"  bw-status="init" bw-function="meerLaden" bw-cat="<?php echo isset($_GET['filter']) && !empty($_GET['filter']) ? $_GET['filter'] : ""; ?>"<?php
foreach ( $D as $ATTR => $VAL ) :
    if ($ATTR == 'titel')
        continue;
    ?>
    bw-<?php echo $ATTR; ?>="<?php echo $VAL; ?>" title="Meer laden..."
<?php endforeach; ?>><?php echo htmlentities( $D->titel ); ?><i class="fas fa-angle-right"></i></button>