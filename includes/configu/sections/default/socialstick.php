<div id="socialstick">
<?php if( defined( 'SM_Facebook' ) && SM_Facebook ): ?>
    <a<?php echo $HTM->get_href( SM_Facebook, 'Like ons op Facebook', 'noblock'); ?> target="_blank">
    	<i class="fa fa-facebook"></i>
    </a>
<?php endif; ?>
<?php if( defined( 'SM_Twitter' ) && SM_Twitter ): ?>
    <a<?php echo $HTM->get_href( SM_Twitter, 'Volg ons op twitter', 'noblock'); ?> target="_blank">
    	<i class="fa fa-twitter"></i>
    </a>
<?php endif; ?>
<?php if( defined( 'SM_Linkedin' ) && SM_Linkedin ): ?>
    <a<?php echo $HTM->get_href( SM_Linkedin, 'Volg ons op LinkedIn', 'noblock'); ?> target="_blank">
    	<i class="fa fa-linkedin"></i>
    </a>
<?php endif; ?>
<?php if( defined( 'SM_Youtube' ) && SM_Youtube ): ?>
    <a<?php echo $HTM->get_href( SM_Youtube, 'Bekijk ons Youtube kanaal', 'noblock'); ?> target="_blank">
    	<i class="fa fa-youtube"></i>
    </a>
<?php endif; ?>

</div>