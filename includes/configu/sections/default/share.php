<div class="social-share right">
	<span>Deel dit bericht op socialmedia:</span>

	<ul>
		<li><a
			<?php echo HtmGen::href( '/share', 'Deel de pagina op Facebook' ); ?>
			data-share="facebook"> <img
				<?php echo HtmGen::src( IMAGES . '/SVG/facebook.svg', 'Facebook icoon', 'svg' ); ?> />
		</a></li>
		<li><a
			<?php echo HtmGen::href( '/share', 'Deel de pagina via Twitter' ); ?>
			data-share="twitter" data-content="<?php echo htmlentities( $D ); ?>">
				<img
				<?php echo HtmGen::src( IMAGES . '/SVG/twitter.svg', 'Twitter icoon', 'svg' ); ?> />
		</a></li>
		<li><a
			<?php echo HtmGen::href( '/share', 'Deel de pagina via LinkedIn' ); ?>
			data-share="linkedin"
			data-content="<?php echo htmlentities( $D ); ?>"
			data-summary="<?php echo isset( $summary ) ? $summary : ''; ?>"> <img
				<?php echo HtmGen::src( IMAGES . '/SVG/linkedin.svg', 'LinkedIn icoon', 'svg' ); ?> />
		</a></li>
<?php /*?>		
		<li><a
			<?php echo HtmGen::href( 'whatsapp://send?text=' . rawurlencode( $D . ' | ' . SITENAME_SHORT . ' - ' . HtmGen::url() ), 'Deel de pagina via Whatsapp' ); ?>
			target="_blank"> <img
				<?php echo HtmGen::src( IMAGES . '/SVG/whatsapp.svg', 'Whatsapp icoon', 'svg' ); ?> />
		</a></li>
<?php */ ?>		
	</ul>
</div>