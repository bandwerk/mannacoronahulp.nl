<?php global $Pagina;  ?>
<div class="page-foto" style="background-image: url('<?php echo isset( $Pagina['header'] ) && !empty( $Pagina['header'] ) ? $Pagina['header'] : IMAGES . '/gettyimages-593319830_0.jpg' ; ?>')">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			<?php if( !isset( $D ) || $D !== false ): ?>
				<h1><?php
				if (isset ( $D ) && is_string ( $D )) {
					echo htmlentities ( $D );
				} elseif (isset ( $D ) && is_object ( $D )) {
					echo htmlentities ( $D->titel ) . ' <br /><small>' . htmlentities ( $D->subtitel ) . '</small>';
				} else {
//					echo HtmGen::page_title ();
				}
				?></h1>
			<?php endif; ?>
			</div>
		</div>
	</div>
</div>