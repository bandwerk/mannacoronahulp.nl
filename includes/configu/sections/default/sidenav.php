<?php global $NAV, $URLS; ?>

<div class="<?php if( isset( $class ) && !empty( $class ) ): echo $class; else: ?>col-md-2 hidden-sm hidden-xs noPad fixtop<?php endif;?>">

	<?php if( isset( $D ) && $D && is_object( $D ) ): ?>
		<div class="page-card-side">
			<span class="title"><?php echo $D->titel; ?></span>
			<ul>
			<?php foreach( $D->nav AS $N ):?>
	        	<li><a<?php echo HtmGen::href( $N['url'],	 $N['titel'] ); ?>><?php echo htmlentities( $N['titel'] ); ?></a></li>
				<?php if( isset( $NAV[CatID]['sub'][ $N['url'] ]['sub'] ) ): ?>
					<ul>
					<?php foreach( $NAV[CatID]['sub'][ $N['url'] ]['sub'] AS $NS ): ?>
						<li><a<?php echo HtmGen::href( $NS['url'], $NS['titel'] ); ?>><?php echo htmlentities( $NS['titel'] ); ?></a></li>
					<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			<?php endforeach; ?>
	        </ul>
		</div>
	
	<?php elseif( ( isset( $D ) && is_numeric( $D ) ) || defined('CatID') ): 
			$CatId = isset( $D ) && is_numeric( $D ) ? $D : CatID;
	
	?>

		<div class="page-card-side">
			<span class="title"><?php echo $NAV[ $CatId ]['titel']; ?></span>
			<?php if( isset( $NAV[ $CatId ]['sub']) ): ?>
			<ul>
				<?php foreach( $NAV[ $CatId ]['sub'] AS $N ):?>
	        	<li><a<?php echo HtmGen::href( $URLS[ $N['id'] ], $N['titel'] ); ?>><?php echo htmlentities( $N['titel'] ); ?></a></li>
				<?php if( isset( $NAV[ $CatId ]['sub'][ $N['id'] ]['sub'] ) ): ?>
					<ul class="hidden">
					<?php foreach( $NAV[ $CatId ]['sub'][ $N['id'] ]['sub'] AS $NS ): ?>
						<li><a<?php echo HtmGen::href( $URLS[ $NS['id'] ], $NS['titel'] ); ?>><?php echo htmlentities( $NS['titel'] ); ?></a></li>
					<?php endforeach; ?>
					</ul>
				<?php endif; 
				endforeach; ?>
	        </ul>
			<?php endif; ?>
		</div>
		
	<?php endif; ?>
</div>
