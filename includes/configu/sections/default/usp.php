<?php

if( isset( $D ) && !empty( $D ) ):?>
	<div id="usp" class="marbot">
		<div class="foto overlay bp" style="background-image: url('<?php echo IMAGES; ?>/biomassaheader.png');"></div>
		<div class="pad">
			<div class="container">
				<div class="row">
				<?php foreach( $D AS $U ): ?>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
						<div class="ups-card">
							<img src="<?php echo IMAGES; ?>/check.png" alt=""><?php echo htmlentities( $U->titel ); ?>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>