<ul>
<?php if( defined( 'CONTACT_NAAM') && CONTACT_NAAM ): ?>
            <li><?php echo htmlentities( CONTACT_NAAM ); ?></li>
<?php endif;
if( defined( 'CONTACT_ADRES') && CONTACT_ADRES ): ?>
            <li><?php echo htmlentities( CONTACT_ADRES ); ?></li>
<?php endif;
if( defined( 'CONTACT_POSTCODE') || defined( 'CONTACT_PLAATS' ) ):
	$line = [];
	if( defined( 'CONTACT_POSTCODE' ) && CONTACT_POSTCODE ) $line[] = htmlentities( $HTM->get_postcode( CONTACT_POSTCODE ) );
	if( defined( 'CONTACT_PLAATS' ) && CONTACT_PLAATS ) $line[] = htmlentities( CONTACT_PLAATS );
?>
            <li><?php echo htmlentities( implode( ' ', $line ) ); ?></li>
<?php endif;
if( defined( 'CONTACT_TEL') && CONTACT_TEL): ?>
            <li>Telefoon <a
		<?php echo $HTM->get_href( 'tel:'. $HTM->get_tel( CONTACT_TEL ), 'Bel naar '. CONTACT_TEL ); ?>><?php echo htmlentities( CONTACT_TEL ); ?></a></li>
<?php endif;
if( defined( 'CONTACT_FAX') && CONTACT_FAX): ?>
            <li>Fax: <?php echo htmlentities( CONTACT_FAX );?></li>
<?php endif;
if( defined( 'CONTACT_EMAIL') && CONTACT_EMAIL): ?>
            <li>E-mail: <a
		<?php echo $HTM->get_href( 'mailto:'. CONTACT_EMAIL, 'Mail naar '. CONTACT_EMAIL ); ?>><?php echo htmlentities( CONTACT_EMAIL );?></a></li>
<?php endif; ?>
</ul>