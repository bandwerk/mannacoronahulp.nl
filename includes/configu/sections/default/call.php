<?php

if (isset ( $D ) || defined ( 'CONTACT_TEL' )) :
	$Tel = isset ( $D ) && is_string ( $D ) ? $D : CONTACT_TEL;
	?>

<?php else: return true; endif; ?>
<div class="<?php if( isset( $class ) ): echo $class; else: ?>call-block overlay marbotbig<?php endif; ?>" style="background-image: url('<?php echo IMAGES; ?>/PCSPets-11.png')">
	<span class="title"><?php if( isset( $titel ) ): echo $titel; else: ?><strong>Heeft u een vraag en/of opmerking over uw huisdier?</strong><br />
	Bel dan met onze praktijk, wij staan u graag te woord!<?php endif; ?></span>
	<a<?php echo HtmGen::href( 'tel:'. HtmGen::tel( $Tel ), 'Bel naar '. $Tel, 'number' ); ?>><img src="<?php echo IMAGES; ?>/SVG/24-hours.svg" alt="Altijd bereikbaar icoon" class="svg icon white"/><?php echo $Tel; ?></a> 
	<span class="extra"><?php if( isset( $subtitel ) ): echo $subtitel; else: ?>(onze praktijk is 24/7 bereikbaar)<?php endif; ?></span>
</div>
