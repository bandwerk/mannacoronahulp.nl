<div class="row">
	<div class="<?php if( isset( $extra['class'] ) ): echo $extra['class']; else: ?>col-xs-12<?php endif; ?>">
		<ul class="breadcrumb">
<?php
$crumbLength = count( $D );
$x=0;
foreach( $D AS $C ):
	$x++;
?>			<li>
		<?php if( isset( $C['url'] ) && $x < $crumbLength ):
			?> <a<?php echo HtmGen::href( $C['url'], 'Ga naar '. $C['titel'] ); ?>>
					<?php echo htmlentities( $C['titel'] ); ?>
				</a><?php 
				
			else: 
				echo htmlentities( $C['titel'] ); 
			endif; ?>
			</li>
<?php endforeach; ?>
		</ul>
	</div>
</div>
<?php /*
Zoekoptie boven crumb.. origineel voor mobiel

if( !isset( $extra['search'] ) || $extra['search'] ):
	global $Pagina;
	$Term = isset( $Pagina['zoekterm'] ) && !empty( $Pagina['zoekterm'] ) ? $Pagina['zoekterm'] : false;
	render_element( 'default/search.ajax', $Term );
endif; */?>