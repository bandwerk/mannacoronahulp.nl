	<h4>Adres gegevens</h4>
	<ul class="reg">
<?php if( DEFINED( 'CONTACT_NAAM') ):?>
		<li class="uc"><?php echo htmlentities( CONTACT_NAAM ); ?></li>
<?php endif; ?>
<?php if( DEFINED( 'CONTACT_ADRES') ):?>
		<li><?php echo htmlentities( CONTACT_ADRES ); ?></li>
<?php endif; ?>
<?php if( DEFINED( 'CONTACT_POSTCODE' ) || DEFINED( 'CONTACT_PLAATS' ) ): ?>
		<li><?php if( defined( 'CONTACT_POSTCODE' ) ): echo $HTM->get_postcode( CONTACT_POSTCODE ) . ' '; endif;
		if( defined( 'CONTACT_PLAATS') ): echo htmlentities( CONTACT_PLAATS ); endif;?></li>
<?php endif; ?>
	</ul>

<?php if( defined( 'CONTACT_TEL' ) || defined( 'CONTACT_FAX' ) ):?>
	<table>
<?php if( defined( 'CONTACT_TEL' ) ): ?>
		<tr>
			<td>Tel: </td>
			<td><a<?php echo $HTM->get_href( 'tel://' . $HTM->get_tel( CONTACT_TEL ), 'Bel naar '. CONTACT_NAAM, 'calltracker' ); if( defined( 'AEN_NUMMER') ): ?> data-aen="<?php echo AEN_NUMMER; ?>"<?php endif;?>><?php echo CONTACT_TEL; ?></a></td>
		</tr>
<?php endif; ?>
<?php if( defined( 'CONTACT_FAX' ) ): ?>
		<tr>
			<td>Fax: </td>
			<td><a<?php echo $HTM->get_href( 'fax:' . $HTM->get_tel( CONTACT_FAX ), 'Fax naar '. CONTACT_NAAM ); ?>><?php echo CONTACT_FAX; ?></a></td>
		</tr>
<?php endif; ?>
	</table>
<?php endif; ?>
<?php if( defined( 'CONTACT_IBAN' ) || defined( 'CONTACT_BIC' ) ):?>
	<table>
<?php if( defined( 'CONTACT_IBAN' ) ): ?>
		<tr>
			<td>IBAN: <?php echo CONTACT_IBAN; ?></td>
		</tr>
<?php endif; ?>
<?php if( defined( 'CONTACT_BIC' ) ): ?>
		<tr>
			<td>Swiftcode/BIC: <?php echo CONTACT_BIC; ?></td>
		</tr>
<?php endif; ?>
	</table>
<?php endif; ?>