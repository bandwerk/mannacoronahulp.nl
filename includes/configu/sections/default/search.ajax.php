<?php if( !isset( $D ) || !$D ):
	$D = '';
endif;
?>

<div class="hidden-md hidden-lg col-xs-12">
	<div  class="row">
		<div class="col-xs-12 searchbar dropdown" bw-parent="msearch" bw-cat="product">
			<?php
			$ajaxHandler = [ 
					'term' => $D,
					'child' => 'msearch',
					'function' => 'search',
					'event' => 'input',
					'cat' => 'product' 
			];
			render_zoekform ( $ajaxHandler );
			?>
			<div bw-child="msearch" bw-attr="menu" class="dropdown menu search hidden"></div>
		</div>
	</div>
</div>