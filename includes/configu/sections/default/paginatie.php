<?php global $Pagina; ?>
<div class="row paginatie"<?php
		foreach ( $D as $ATTR => $VAL ) :
				if ($ATTR == 'titel')
					continue;
				
				if( $ATTR == 'href') {
					echo $ATTR.  '="'.$VAL.'"';
					continue;
				}
				?>
			bw-<?php echo $ATTR; ?>="<?php echo $VAL; ?>"
		<?php endforeach; ?>>
	<div class="col-xs-12">
<?php $Classes = [];
	$CurrentPage = isset( $D->currentPage ) && is_numeric( $D->currentPage ) ? $D->currentPage : $D->page - 1;

	$MaxPages = (int) ceil( ( $D->totaal / $D->limit ) );
	if( isset( $extra['class'] ) && !empty( $extra['class'] ) )
		$Classes[] = $extra['class']; 
	else
		$Classes[] = 'btn-default';
	
	$ThisClass = $Classes;
	$ThisClass[] = 'active';
	if( $CurrentPage <= 1 ) $ThisClass[] = 'hide';

?>	
		<a class="<?php if( count( $ThisClass ) > 0 ) echo implode(' ', $ThisClass ); ?>" bw-child="<?php echo $D->child; ?>" bw-event="click" bw-function="gotoPage" bw-attr="prev"><i class="fa fa-chevron-left"></i></a>
		
<?php 
	$SourceUrl = str_replace( SITELINK, '', getCurrentURL() );
	$SourceUrl = strpos( $SourceUrl, '.' ) ? substr( $SourceUrl, 0, strpos( $SourceUrl, '.' ) ): $SourceUrl;
	$SourceUrl = strpos( $SourceUrl, '/p_' ) ? substr( $SourceUrl, 0, strpos( $SourceUrl, '/p_') ) : $SourceUrl;
	$PagerRange = $D->paginatie;
	
	$ThisClass = $Classes;
	if( $CurrentPage !== 1 ) $ThisClass[] = 'active';
	if( $MaxPages > 1 ): ?>
	
		<a href="<?php echo $SourceUrl; if( !isset( $Pagina['filter_url'] ) || empty( $Pagina['filter_url'] ) ): ?>.html<?php endif; ?>" class="<?php if( count( $ThisClass ) > 0 ) echo implode(' ', $ThisClass ); ?>" bw-child="<?php echo $D->child; ?>" bw-event="click" bw-function="gotoPage" bw-attr="1">1</i></a>
		
<?php endif;	
	if( $MaxPages > 6 ): ?>
		<span class="spacer<?php if( $CurrentPage < $PagerRange ):?> hide<?php endif;?>">...</span>
<?php 
	endif;
	
	$Range = $CurrentPage - $PagerRange/2;
	if( $Range < 1 ) $Range = 1;
	if( $Range + $PagerRange > $MaxPages ) $Range = $MaxPages - $PagerRange + 1;
	
	for( $x=2;$x<$MaxPages;$x++): 
		$ThisClass = $Classes;
		if( $x !== $CurrentPage ) $ThisClass[] = 'active';
		$Url = $SourceUrl;
		if( $x === 1 ) {
			$Url .= '.html';	
		} else {
			$Url .= '/p_'. $x;
		}
		
		if(  $x < $Range || $x > $Range+6 ) $ThisClass[] = 'hide';
?>
		<a href="<?php echo $Url; ?>" class="<?php if( count( $ThisClass ) > 0 ) echo implode(' ', $ThisClass ); ?>" bw-child="<?php echo $D->child; ?>" bw-event="click" bw-function="gotoPage" bw-attr="<?php echo $x; ?>"><?php echo $x; ?></a>
		
		
<?php 	

	endfor; 			 

	if( $MaxPages > $PagerRange ): ?>
		<span class="spacer<?php if( $CurrentPage > ( $MaxPages - $PagerRange/2 ) ):?> hide<?php endif;?>">...</span>
<?php 
	endif;
	
	$ThisClass = $Classes;
	if( $MaxPages !== $CurrentPage ) $ThisClass[] = 'active';	
	if( $MaxPages !== 1 ):
?>
		<a href="<?php echo $SourceUrl; ?>/p_<?php echo $MaxPages; ?>" class="<?php if( count( $ThisClass ) > 0 ) echo implode(' ', $ThisClass ); ?>" bw-child="<?php echo $D->child; ?>" bw-event="click" bw-function="gotoPage" bw-attr="<?php echo $MaxPages; ?>"><?php echo $MaxPages; ?></a>
		
		
<?php 	
	endif;
	

	$ThisClass = $Classes;
	$ThisClass[] = 'active';
	if( $CurrentPage >= $MaxPages ) 
		$ThisClass[] = 'hide';
?>	
		<a class="<?php if( count( $ThisClass ) > 0 ) echo implode(' ', $ThisClass ); ?>" bw-child="<?php echo $D->child; ?>" bw-event="click" bw-function="gotoPage" bw-attr="next"><i class="fa fa-chevron-right"></i></a>

	</div>
</div>