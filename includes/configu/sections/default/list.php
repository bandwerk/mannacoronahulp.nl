<ul<?php if( isset( $extra ) && isset( $extra['class'] ) && !empty( $extra['class'] ) ):
	echo ' class="' . $extra['class'] . '"';
endif;

?>>
<?php

foreach( $D AS $P ):

	if( is_object( $P ) ):
?>


	<li<?php if( isset( $extra ) && isset( $extra['li-class'] ) && !empty( $extra['li-class'] ) ):?> class="<?php echo $extra['li-class']; ?>"
	<?php endif;?>>

<?php if( isset( $P->url ) && $P->url ):?>
		<a<?php echo $HTM->get_href( $P->url, 'Meer over '. $P->titel ); ?>>
<?php endif; ?>
			<?php echo htmlentities( $P->titel ); ?>
			<?php if( isset( $P->subtitel ) && !empty( $P->subtitel ) ): ?>
				- <?php echo htmlentities( $P->subtitel ); ?>
			<?php endif; ?>
<?php if( isset( $P->url ) && $P->url ):?>
		</a>
<?php endif; ?>
	</li>
	
<?php  else: ?>
	<li<?php if( isset( $extra )
			&& isset( $extra['li-class'] )
			&& !empty( $extra['li-class'] ) ):?> class="<?php echo $extra['li-class']; ?>"
	<?php endif;?>>
<?php if( isset( $P['url'] ) && $P['url'] ):?>
		<a<?php echo $HTM->get_href( $P['url'], 'Meer over '. $P['titel'] ); ?>>
<?php endif; ?>
			<?php echo htmlentities( $P['titel'] ); ?>
			<?php if( isset( $P['subtitel'] ) && !empty( $P['subtitel'] ) ): ?>
				- <?php echo htmlentities( $P['subtitel'] ); ?>
			<?php endif; ?>
<?php if( isset( $P['url'] ) && $P['url'] ):?>
		</a>
<?php endif; ?>
	</li>
<?php endif; ?>
<?php endforeach; ?>
</ul>
