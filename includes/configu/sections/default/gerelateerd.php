<?php if( isset( $D ) && is_array( $D ) ): ?>
<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
	<aside class="sideblok marbot">
		<div class="center">
			<span class="stitle"><?php if( isset( $titel ) ): echo htmlentities( $titel ); else:?>Gerelateerde items<?php endif;?></span>
		</div>
		<ul class="nieuws">
					<?php foreach( $D AS $I ):?>
						<li><a
				<?php echo HtmGen::href( $I->url, 'Meer over '. $I->titel ); ?>> <span
					class="title oneline"><?php echo htmlentities( $I->titel ); ?></span>
								<?php if( $I_content = HtmGen::text_shorten( $I->inhoud, 128, false, false ) ): ?>
									<span class="inhoud"><?php echo $I_content; ?></span>
								<?php endif;
								unset( $I_content );
								?>
							</a></li>
					<?php endforeach; ?>	
					</ul>
	</aside>
</div>
<?php endif; ?>