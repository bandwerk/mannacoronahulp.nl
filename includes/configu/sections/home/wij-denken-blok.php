<!--====== WIJ DENKEN MET U MEE MODULE ======-->
<div class="row">
    <div class="col-xl-5 col-lg-6 col-md-8">
        <div class="banner-content mt-70" style="visibility: visible; animation-name: fadeInLeft;">
            <h1 class="mb-40"><?php echo HtmGen::page_title(); ?></h1>
            <p>
                <?php echo HtmGen::page_content(); ?>
            </p>
            <div class="banner-btn text-right mt-80">
                <a class="main-btn" href="<?php echo HtmGen::url(); ?>">Lees meer <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
    </div>
</div>