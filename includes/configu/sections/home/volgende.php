<div id="home-score" class="center">
	<div class="inner">
		<div class="club-slider">
		<?php foreach( $D AS $M ): 
			$Datum = explode( ' ', $M->datum );
		?>
			<div class="slide">
				<span class="time-place">
					<span><?php echo $M->aanvangstijd; ?> uur</span>
					<img src="<?php echo IMAGES; ?>/SVG/field.svg" alt="Locatie icoon" class="svg icon"><?php echo htmlentities( $M->accommodatie . ', ' . ucfirst( $M->plaats ) ); ?>
					<small>/ <?php echo htmlentities( $M->klassepoule ); ?></small>
				</span>
				<div class="clubs">
					<span class="club1 oneline" title="<?php echo $M->thuisteam; ?>">
						<?php echo htmlentities( $M->thuisteam ); /*?> <span class="oneline">2e plaats</span> */?>
					</span> 
					
					<?php if( isset( $M->uitslag ) ): ?>
						<span class="uitslag"><?php echo $M->uitslag; ?></span>
					<?php else: ?>
						<span class="vs"><?php echo $Datum[0]; ?> <span><?php echo $Datum[1]; ?>.</span></span>
					<?php endif; ?>
					
					<span class="club2 oneline" title="<?php echo $M->uitteam; ?>">
						<?php echo htmlentities( $M->uitteam ); /*?> <span class="oneline">2e plaats</span> */?>
					</span>

					
				</div>
			</div>
		<?php endforeach; ?>
		

		</div>
	
		<span id="clubprevarr" class='arrows arrow-left'><img src='<?php echo IMAGES; ?>/SVG/left-arrow.svg' class='svg'></span> 
		<span id="clubnextarr" class='arrows arrow-right'><img src='<?php echo IMAGES; ?>/SVG/right-arrow.svg' class='svg'></span>
		
<?php if( isset( $teams ) && is_array( $teams ) ):
	global $HTM;
	$HTM->set_js( [JAVASCRIPT . '/ajax.js'] );
?>	
		<div class="right" bw-parent="selectTeam" bw-function="selectTeam">
			<form bw-child="selectTeam" bw-event="change">
				<label for="selecteer" class="btn btn-default mini" bw-child="selectTeam" bw-event="click"><span class="inner">Selecteer hier uw team</span></label>
				<div class="formgroup hidden">
					<div class="input-group">
						<select class="form-control" name="selecteer" id="selecteer">
						<?php foreach( $teams AS $Team ):?>
							<option value="<?php echo $Team->teamcode; ?>"<?php if( $_SESSION['mijnteam']===$Team->teamcode):?> selected<?php endif; ?>><?php echo htmlentities( $Team->teamnaam );?></option>
						<?php endforeach; ?>
						</select>
				    </div>
			    </div>
			
			</form>
		</div>
<?php endif; ?>		
	</div>
</div>