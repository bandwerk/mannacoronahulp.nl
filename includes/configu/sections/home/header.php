<?php $D = is_array( $D ) ? array_rand( $D ) : $D;  

if( !isset( $D->knop_tekst ) || empty( $D->knop_tekst ) ) {
	$D->knop_tekst = 'Meer info';
}
?>
<div class="header-foto"<?php echo isset( $D ) && is_array( $D ) && isset( $D[0]->image ) ?  ' style="background-image: url(\''.HEADER_GROOT . '/' . $D[0]->image . '\')' :''; ?>>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12">

				<span class="title1"><?php echo isset( $D->koptekst ) && !empty( $D->koptekst ) ? $D->koptekst : SITENAME_PLAIN; ?></span>
                <span class="title2"><?php echo $D->titel; ?></span>
            <?php if( isset( $D->url ) && !empty( $D->url )): ?>
                <a<?php echo HtmGen::href( $D->url, $D->knop_tekst, 'btn btn-default'); ?>><?php echo $D->knop_tekst; ?></a>
			<?php endif; ?>
			</div>
		</div>
	</div>
</div>