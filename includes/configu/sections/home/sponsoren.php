<?php 
	$D = is_array( $D ) ? $D : [ $D ];
?>
<div class="whitebg-wrap">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="ptitle gray"><?php if( isset( $Titel ) && !empty( $Titel ) ): echo htmlentities( $Titel ); else: ?>Top Support trotse hoofdsponsor van Excelsior’31<?php endif;?></h2>
				<div class="sponsorslider">
				<?php foreach( $D AS $S ):?>
					<div class="slide">
						<a<?php echo HtmGen::href( $S->url, $S->alt_text, 'logo' ); ?>> 
						<?php if( isset( $S->image ) && !empty( $S->image ) ): ?>
							<img<?php echo HtmGen::src( SPONSOR_THUMB . '/' . $S->image, 'Logo '. $S->titel ); ?>/>
						<?php else: ?>
							<span><?php echo htmlentities( $S->titel ); ?></span>
						<?php endif;  ?>
						</a>
					</div>
				<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div>
</div>