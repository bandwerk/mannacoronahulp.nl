<footer class="footer ">
    <div class="container-fluid">
        <div class="regular-footer">
            <div class="row">
                <div class="col-lg-2 col-sm-6">
                    <img src="<?= IMAGES ?>/logo-coronahulp.jpg">
                </div>
                <div class="col-lg-3 col-sm-6">
                    <h3 style="color: #88B332;">Stichting Manna</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                            <b>Postadres:</b><br>
                            Linderveld 4a<br>
                            7681 RA Vroomshoop
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div><br>
                            info@manna.nu<br>
                            06-12468888
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <h3 style="color: #67BCDD;">Manna Family Care</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                            De Linde<br>
                            Linderveld 4a<br>
                            7681 RA Vroomshoop
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div>
                            info@manna.nu<br>
                            06-12468888
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <h3 style="color: #E03B1E;">Manna Fashion en Happy Kids</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                            De Linde<br>
                            Linderveld 4a<br>
                            7681 RA Vroomshoop
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div>
                            <b>Openingstijden:</b><br>
                            info@manna.nu<br>
                            06-12468888
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="social-footer">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <a href="#" target="_blank"><img src="<?= IMAGES ?>/SVG/facebook.svg"></a>
            <a href="#" target="_blank"><img src="<?= IMAGES ?>/SVG/instagram.svg"></a>
            <a href="#" target="_blank"><img src="<?= IMAGES ?>/SVG/twitter.svg"></a>
        </div>
        <div class="col-md-6">

            <span class="payoff">Ontwerp & realisatie door: <a href="https://bandwerk.nl/" target="_blank">Bandwerk merkenbouwers</a> </span>
        </div>
    </div>
    </div>
</div>
<?php
echo $HTM->get_js();
if (isset($GoogleRemarketing)) {
    try {
        $GoogleRemarketing->render();
    } catch (Exception $ex) {
        die($ex->getMessage());
    }
}
?>

</body>
</html>