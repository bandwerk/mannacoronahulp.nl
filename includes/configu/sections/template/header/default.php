<?php
setPageSEO();

$HTM->set_keyword(MetaKeywords());
$Titletag = htmlentities(TitleTag());
if (!$HTM->get_meta('title'))
    $HTM->set_meta($Titletag, 'title');
if (!$HTM->get_meta('description'))
    $HTM->set_meta(str_replace(array(
        '&nbsp;',
        ' '
    ), ' ', MetaDescription()), 'description');

?><!DOCTYPE HTML>
<html class="no-js" lang="nl">
<head>


    <!--
              Concept, ontwerp & realisatie:
              Bandwerk Internet- en reclamebureau
              Neonstraat 3a
              7463 PE Rijssen
              T.0548 - 54 26 72
              http://www.bandwerk.nl
              info@bandwerk.nl
      -->

    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=6.0, width=device-width"/>

    <title><?php echo $Titletag; ?></title>
    <?php echo $HTM->get_meta(); ?>
    <link rel="stylesheet" type="text/css" href="<?= DIST ?>/style.css">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= SITELINK ?>/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= SITELINK ?>/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= SITELINK ?>/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= SITELINK ?>/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= SITELINK ?>/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= SITELINK ?>/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= SITELINK ?>/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= SITELINK ?>/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= SITELINK ?>/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= SITELINK ?>/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= SITELINK ?>/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= SITELINK ?>/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= SITELINK ?>/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= SITELINK ?>/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= SITELINK ?>/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="274a1e73-0862-45bb-bc12-8854fa970dd6" data-blockingmode="auto" type="text/javascript"></script>
</head>
<?php
//if title is not set, use category name or if both set, use title
$page_class = "";
if (empty($Pagina['titel']) && isset($Pagina['categorie'])) {
    $page_class = $Pagina['categorie'];
} elseif (empty($Pagina['categorie']) && isset($Pagina['titel'])) {
    $page_class = $Pagina['titel'];
} elseif (!empty($Pagina['categorie']) && isset($Pagina['titel'])) {
    $page_class = $Pagina['titel'];
}
$page_class = str_replace(' ', '-', strtolower($page_class . "-pagina"));
?>
<body class="<?php echo $page_class; ?>" style="overflow-x:hidden;">

    <?php if (defined('UA_CODE') && UA_CODE !== NULL) {
        ?>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', '<?php echo UA_CODE; ?>', 'auto');
            ga('set', 'anonymizeIp', true);
            ga('send', 'pageview');
        </script>

    <?php } ?>

    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="<?= SITELINK ?>"><img src="<?= IMAGES ?>/logo-coronahulp.jpg" class="navbar-brand-logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
            <?php
            /**
             * Prints the desktop menu.
             */
            echo $MENU->d;
            ?>
    </nav>

