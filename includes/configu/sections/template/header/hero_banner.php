<?php
$sfeer_image = $D;
?>

<div class="hero-banner" style="background-image: url('<?= WEBSITE_SFEER_GROOT . "/" . $sfeer_image ?>')">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title">Wat kunnen wij voor u doen?</h1>
            </div>
        </div>
    </div>
</div>

