<?php
$sfeer_image = $D;
?>

<div class="smaller-hero-banner" style="background-image: url('<?= WEBSITE_SFEER_GROOT . "/" . $sfeer_image ?>');">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title"><?= HtmGen::page_title() ?></h1>
            </div>
        </div>
    </div>
</div>

