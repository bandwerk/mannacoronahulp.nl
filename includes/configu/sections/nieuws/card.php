<?php $D = is_array( $D ) ? $D : [ $D ];
$delay = -100;
foreach( $D AS $newsItem ):
    $delay += 100;
    ?>


    <div class="col-sm-12 mb-5">
        <div class="single-news-page mt-30">
            <div class="news-image">
                <img class="image-fluid" style="max-width: 100%" src="<?php echo NIEUWS_HEADER . '/' . $newsItem->image; ?>" alt="<?php echo $newsItem->titel; ?>">
            </div>
            <div class="news-content shadow-sm newsitem">
                <h5 bw-edit="table" bw-edit-field="titel" bw-edit-module="nieuws" bw-edit-id="<?= $newsItem->id ?>"><?php echo $newsItem->titel?></h5>
                <p bw-edit="table" bw-edit-field="inleiding" bw-edit-module="nieuws" bw-edit-id="<?= $newsItem->id ?>"><?php echo $newsItem->inleiding?></p>
                <div class="work-btn text-right">
                    <a class="main-btn" href="<?php
                        if(isset($newsItem->SEOUrl)) {
                            echo SITELINK . '/nieuws/' . generate_url_from_text ( $newsItem->SEOUrl);
                        } else {
                            echo $newsItem->url;
                        }
                    ?>" title="<?php echo $newsItem->titel; ?>">Lees meer... <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
<?php
    if($delay == 200)
        $delay = -100;
endforeach; ?>