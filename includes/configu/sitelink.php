    <?php
function getSitelink(){
    if( !defined( 'PROTOCOL' ) ) define( 'PROTOCOL', 'http' );

    $sitelink = PROTOCOL . '://'.   DOMAIN  .'';

    if ( stripos ( $_SERVER ['HTTP_HOST'], $_SERVER['_klantennaam_'] ) !== false )
        $sitelink = PROTOCOL . '://' . $_SERVER['HTTP_HOST'];

    elseif (stripos ( $_SERVER ['HTTP_HOST'], $_SERVER['_klantenurl_'] ) !== false)
        $sitelink = PROTOCOL . '://'. $_SERVER['_klantenurl_'] .'';

    elseif (stripos ( $_SERVER ['HTTP_HOST'], 'localhost' ) !== false)
        $sitelink = PROTOCOL . '://localhost/' . $_SERVER['_klantennaam_'];

    elseif (stripos ( $_SERVER ['HTTP_HOST'], 'development.local') !== false)
        $sitelink = PROTOCOL . '://development.local/' . $_SERVER['_klantennaam_'];

    elseif (stripos ( $_SERVER ['HTTP_HOST'], '192.168.2' ) !== false)
        $sitelink = PROTOCOL . '://' . $_SERVER['HTTP_HOST'] . '/' . $_SERVER['_klantennaam_'];

    return $sitelink;
}
?>