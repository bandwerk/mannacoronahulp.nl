<?php
$HTM = isset( $HTM ) && get_class( $HTM ) === 'htmlGenerator' ? $HTM : new htmlGenerator ();


// SEO of niet, eerst kijken of we via ID iets binnen krijgen.
if( isset( $Pagina['thema'] ) && $Pagina['thema'] ):
	$Pagina = array_merge( $Pagina, $Pagina['thema']['data'] );

else:
    try {

		$SCRIPTID = isset( $GET['id'] ) ? 'true' : 'false';
		$WHERE='WHERE '; 
		$CASE=''; 
		$HAVING=''; 
		$URLTITEL=[];
		if( !defined( 'WEBSITE_PHOTO' ) ) {
		    define( 'WEBSITE_PHOTO', false );
		}
		
		foreach( $GET AS $k=>$v ) {
			if( strpos( $k, 'url_titel' ) !== false ) {
				$URLTITEL[]= '"'. $v . '"';
			}
			if( $k === 'p' ) {
				if( is_numeric( $v ) ) $WHERE .= 'w.id="'. $v .'" ';
			}
		}

		if( count( $URLTITEL ) > 0 ) {
			if( isset( $WHERE ) && $WHERE !== 'WHERE ' ) $WHERE .= 'AND ';
			$WHERE .= ' s1.url_titel IN (' . implode( ',', $URLTITEL ) . ')';
			$CASE = "( CASE";
			for( $x=0;$x<count($URLTITEL);$x++) {
				$url = $URLTITEL[ $x ];
				$CASE .= " WHEN s1.url_titel=$url THEN $x ";

			}
			$CASE .=  ' ELSE 0 END ) AS rank,';
		}
		if( $SCRIPTID=='true' ) $HAVING .= ' HAVING `is_script`=`script_id` ';

		if( $WHERE !== 'WHERE ' ){

			$Q='SELECT
					w.*,
					s1.*,
					CASE
						WHEN s2.id IS NOT NULL THEN s2.script
					ELSE NULL
					END AS `script_src`,
					CASE
						WHEN w.paginatitel="" OR w.paginatitel IS NULL THEN w.titel
					ELSE w.paginatitel
					END AS `h1`,
					CASE
						WHEN s2.id IS NOT NULL THEN true
						ELSE false
						END AS `is_script`,
					'. $CASE . '
					'. $SCRIPTID . ' AS `script_id` '.( WEBSITE_PHOTO ? ',
                    GROUP_CONCAT( DISTINCT CONCAT( `wp`.`id`, ".", `wp`.`ext` ) ORDER BY `wp`.`albumthumb` DESC, `wp`.`id` DESC ) AS `images`': '' ).'
					
				FROM website w
					LEFT JOIN `SEO_Module_link` s
						ON s.website_id=w.id
					LEFT JOIN `SEO_Module` s1
						ON s.`SEO_ID`=s1.`SEO_ID`
					LEFT JOIN scripts s2
						ON s2.id=w.script

					'.( WEBSITE_PHOTO ? ' LEFT JOIN `website_crop_photos` `wp` ON `w`.`id`=`wp`.`album_id`' : '' ).'
				'. $WHERE . '
				GROUP BY `w`.`id`
				'. $HAVING. '
				ORDER BY '. ( !empty( $CASE ) ? '`rank` DESC,' : '' ) . '`w`.`sort` DESC';

			$F = $db->prepare( $Q );
			$F->execute();

			if( $F->rowCount() > 0 ) {
				$Pagina = ReplaceContent( $F->fetch(PDO::FETCH_ASSOC ) );
				
			} elseif( $SCRIPTID ) {
				$Q='SELECT
						w.titel,
						w.script,
						CASE
							WHEN s2.id IS NOT NULL THEN s2.script
						ELSE NULL
						END AS `script_src`,
						CASE
							WHEN w.paginatitel="" OR w.paginatitel IS NULL THEN w.titel
						ELSE w.paginatitel
						END AS `h1`,
						CASE
							WHEN s2.id IS NOT NULL THEN true
							ELSE false
							END AS `is_script`,
						'. $SCRIPTID . ' AS `script_id`'.( WEBSITE_PHOTO ? ',
                    GROUP_CONCAT( DISTINCT CONCAT( `wp`.`id`, ".", `wp`.`ext` ) ORDER BY `wp`.`albumthumb` DESC, `wp`.`id` DESC ) AS `images`': '' ).'
				
					FROM website w
						LEFT JOIN `SEO_Module_link` s
							ON s.website_id=w.id
						LEFT JOIN `SEO_Module` s1
							ON s.`SEO_ID`=s1.`SEO_ID`
						LEFT JOIN scripts s2
							ON s2.id=w.script

                        '.( WEBSITE_PHOTO ? ' LEFT JOIN `website_crop_photos` `wp` ON `w`.`id`=`wp`.`album_id`' : '' ).'

						WHERE s1.url_titel=?
					GROUP BY `w`.`id`
					ORDER BY `w`.`sort` DESC';
				
					$F = $db->prepare( $Q );
					$F->bindValue( 1, $GET['url_titel'], PDO::PARAM_STR );
					$F->execute();
				
					if( $F->rowCount() > 0 ){ 
						$Pagina = ReplaceContent( $F->fetch(PDO::FETCH_ASSOC ) );
						
					} else {
						$Pagina['to_sitemap'] = true;
					}
			} else {
				$Pagina['to_sitemap'] = true;
			}
		}

	} catch( PDOException $ex ) {
		die( $ex->getMessage() );
	}
endif;


if( isset( $Pagina ) && !empty( $Pagina ) ):
	if( isset( $Pagina['titel'] ) ):
		$HEADER['Title'] = $Pagina['titel'];
	endif;
	if( !isset( $Pagina['id'] ) ): 
		$Pagina['id'] = 0;
	endif;
else:
	$Pagina = [];
	$Pagina['id']=1;
endif;

$Pagina['images'] = !empty( $Pagina['images'] ) ? explode( ',', $Pagina['images'] ) : false;
if( $Pagina['images'] && count( $Pagina['images'] ) > 0 ) {
	$Pagina['image'] = $Pagina['images'][0];
}


// Array met filtervolgorde voor redirect op product
$FilterSort = ['toon','sort'];

define ('PageID', $Pagina['id']);

require_once(__INC__ . '/Functionz.php');
require_once(__INC__ . '/menu.php');

$Session = new session( session_id(),'',SALT);
// Let op - het offerte aanvraag formulier werkt via de winkelwagen, waarbij de winkelwagen eerst wordt geleegd voordat deze gevult wordt voor het aanmaken van de offerte
//$Cart = new cart( $db, 'shop_winkelwagen', $Session->get_data() );
//$Order = new order( $db, 'acc_bestellingen');
//$FOOTER = new stdClass();