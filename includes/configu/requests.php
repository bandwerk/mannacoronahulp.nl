<?php

try {
	$SearchFor = $_SERVER['REQUEST_URI'];

	$Q = $db->prepare( 'SELECT `url_nieuw` FROM `redirect` WHERE `url_oud` LIKE :request;' );
	$Q->bindValue( ':request', '%' . $SearchFor, PDO::PARAM_STR );
	$Q->execute();

	if( $Q->rowCount() > 0 ) {
		$RedirectTo = $Q->fetch( PDO::FETCH_OBJ )->url_nieuw;

		header( 'HTTP/1.1 301 Moved Permanently');
		header('Location: '. $RedirectTo ); // Pagina bestaat niet. verstuur naar de sitemap! Was eerst homepage
	}
	
} catch( PDOException $ex ) {
	die( $ex->getMessage() );
}

require_once (__DIR__ . '/requests/default.php');

// Script ingesteld in Bandwerk Plus gebruiken
// Zoekt eerst door url_
$DIRS = [ '', 'url_titel', 'url_titel2', 'url_titel3' ];


if (isset ( $Pagina ['is_script'] ) && $Pagina ['is_script'] ) {
	$SCRIPT = str_ireplace ( '.php', '', $Pagina ['script_src'] ) . '.php';
	
	foreach( $DIRS AS $PATH ):
		$TARGET = array_filter( [ $PATH, $Pagina['script_src'] ] );
		$TARGET = __DIR__ . '/requests/'. implode( '/', $TARGET ) . '.php';
		if( file_exists( $TARGET ) ) break;
	endforeach;


} else {

	foreach( array_filter( $DIRS ) AS $PATH ):
		if( isset( $GET[ $PATH ] ) ):
			$TARGET = __DIR__ . '/requests/'. $PATH . '/' . $GET[ $PATH ] . '.php';
			if( file_exists( $TARGET ) ) break;

		endif;
		
	endforeach;

}

if( isset( $TARGET ) && $HTM->get_script( $TARGET ) ) require_once( $TARGET );


/*
 * formulieren afhandelen
 */

require_once (__INC__ . '/forms.php');

if( isset( $Pagina['to_sitemap'] ) && $Pagina['to_sitemap']) {
	
	try {
		$SearchFor = $_SERVER['REQUEST_URI'];

		$Q = $db->prepare( 'SELECT `url_nieuw` FROM `redirect` WHERE `url_oud` LIKE :request;' );
		$Q->bindValue( ':request', '%' . $SearchFor, PDO::PARAM_STR );
		$Q->execute();

		if( $Q->rowCount() > 0 ) {
			$RedirectTo = $Q->fetch( PDO::FETCH_OBJ )->url_nieuw;

		}
		
	} catch( PDOException $ex ) {
		die ( $ex->getMessage() );
		
	}
		
	if( isset( $RedirectTo ) && hasContent( $RedirectTo ) ) {
		if( !filter_var( $RedirectTo, FILTER_VALIDATE_URL ) ) {
			if( strpos( $RedirectTo, '/' ) === 0 )
				$RedirectTo = SITELINK . $RedirectTo;
			else 
				$RedirectTo = SITELINK . '/'. $RedirectTo;
		}
		
		header( 'HTTP/1.1 301 Moved Permanently');
		header('Location: '. $RedirectTo ); // Pagina bestaat niet. verstuur naar de sitemap! Was eerst homepage
	} else {

		header("HTTP/1.0 404 Not Found");
		header('Location: '. SITELINK .'/sitemap.html'); // Pagina bestaat niet. verstuur naar de sitemap! Was eerst homepage
		
	}
	
}

?>