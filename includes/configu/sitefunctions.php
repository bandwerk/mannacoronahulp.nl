<?php
require_once (str_replace ( basename ( __DIR__ ), '', __DIR__ ) . '/Bandwerkplus/func.bwplus.php');
require_once(__DIR__ . '/constants.php');

/*
 * function get_sidenav
 * @output array or false
 */
function get_sidenav() {
	// $NAV is een array met de sitestructuur
	global $NAV;
	
	// Afhankelijk van de pagina id en evt. een gedefinieerde categorie ( CatID ) de juiste vertakking van de site retourneren
	$SRC = defined ( 'CatID' ) ? $NAV [CatID] : $NAV [PageID];
	
	// Als er subnavigatie mogelijkheden zijn, de correcte boom als array retourneren, anders false
	if (isset ( $SRC ['sub'] )) :
		foreach ( $SRC as $KEY => $VAR ) :
			if ($KEY == 'sub') :
				
				foreach ( $VAR as $KE => $VA ) :
					if (is_numeric ( $KE )) :
						
						$RET [$SRC ['titel']] [$VA ['titel']] = SITELINK . '/' . $SRC ['url'] . '/' . $VA ['url'] . '.html';
					 elseif ($KE == 'sub') :
						foreach ( $VA as $K => $V ) :
							$RET [$SRC ['titel']] [$VA ['titel']] [$V ['titel']] = SITELINK . '/' . $SRC ['url'] . '/' . $VA ['url'] . '/' . $V ['url'] . '.html';
						endforeach
						;
					endif;
				endforeach
				;
			
			
			endif;
			
		endforeach
		;
		
		return $RET;
	else :
		return false;
	endif;
}

/*
 * function spanify
 * Gedeelte tekst (helft ong.) in span zetten
 * @input (string)
 * @output (string)
 */
function spanify($text, $invert = true) {
	// Titel text binnenhalen
	$titel = explode ( ' ', $text );
	// Tellen welk woord voor de helft van de zin zit
	$titel_span = (floor ( (count ( $titel ) - 1) / 2 ));
	// Afhankelijk van invert het eerste/laatse gedeelte tussen span wrappen
	switch ($invert) :
		case true :
			$titel [$titel_span] .= '<span>';
			$titel [(count ( $titel ) - 1)] .= '</span>';
			break;
		default :
			$titel [0] = '<span>' . $titel [0];
			$titel [$titel_span] .= '</span>';
	endswitch
	;
	// Eerste helft van de zin (ong) in span zetten
	
	return implode ( ' ', $titel );
}
function get_zoekform($customUrl = ZOEKEN_URL) {
	return ' method="POST"
			action="' . $customUrl . '/"
			onsubmit="window.open(
					this.action + encodeURIComponent(
						this.querySelector(\'input\').value ).split(\'%2F\').join(\'/\'), \'_self\' );
					return false;"';
}
function get_dealerform($url) {
	return ' method="POST"
			action="' . str_replace ( '.html', '', $url ) . '/"
			onsubmit="postcode=this.querySelector(\'input\').value.split(\' \').join(\'\');
					if( postcode.length!==6){
						if( postcode.length == 0 ) {
							window.open( this.action.slice(0, -1 ) + \'.html\', \'_top\' );
					
						} else {
							alert(\'Een postcode bestaat uit minimaal 6 tekens\');
						}
					} else {
						window.open(
						this.action + encodeURIComponent(
							postcode ) , \'_top\' );
					}
						return false;"';
}
function render_crumbs($CRUMB) {
	global $HTM;
	?>
<div class="row">
	<div class="col-xs-12">
		<ol class="breadcrumb">
			<li><a
				<?php echo $HTM->get_href( SITELINK, 'Ga naar de homepagina' ); ?>>
					Home </a></li>
	                <?php
	$CRUMBLENGTH = count ( $CRUMB );
	for($i = 0; $i < $CRUMBLENGTH; $i ++) :
		$class = ($i + 1) == $CRUMBLENGTH ? 'active' : null;
		$li = $CRUMB [$i];
		
		if (isset ( $li ['class'] ))
			$class .= ' ' . $li ['class'];
		if (strpos ( $li ['url'], '#' ) === false)
			$li ['url'] = strpos ( $li ['url'], SITELINK ) === false ? SITELINK . '/' . $li ['url'] . '.html' : $li ['url'];
		
		?>
            <li>
<?php if( ($i+1)<$CRUMBLENGTH ):?>
            	<a
				<?php echo $HTM->get_href( $li['url'] , 'Ga naar '. $li['titel'], $class ); ?>>
<?php endif; ?>
                	<?php echo htmlentities( ucfirst( $li['titel'] ) ); ?>
<?php if( ($i+1)<$CRUMBLENGTH ):?>
               </a>
<?php endif; ?>
               </li>
	<?php endfor; ?>
		</ol>
	</div>
</div>
<?php
}
function render_zoekform($term = '', $extra = null) {
	$customUrl = ZOEKEN_URL;
	
	if (is_array ( $term )) :
		$settings = $term;
		$term = $term ['term'];
		unset ( $settings ['term'] );
		
		if (isset ( $settings ['customUrl'] )) :
			$customUrl = $settings ['customUrl'];
			unset ( $settings ['customUrl'] );
		endif;
	endif;
		
	
	?>
<form <?php echo get_zoekform( $customUrl ); 
	
	if (isset ( $extra ) && is_array ( $extra )) :
		foreach ( $extra as $attribute => $value ) :
			if ($attribute !== 'placeholder')
				echo ' ' . $attribute . '="' . $value . '"';
		endforeach;
	endif; ?>>
	<button type="submit"><img src="<?php echo IMAGES; ?>/SVG/search.svg" alt="Icoon zoeken" class="svg"></button>
	<input type="search" class="search-input" <?php
	if (isset ( $term ) && ! empty ( $term )) :
		?> value="<?php echo $term; ?>" <?php 
			endif;
	
	if (isset ( $settings )) :
		foreach ( $settings as $attr => $val ) :
				?> bw-<?php echo $attr; ?>="<?php echo $val; ?>" <?php
			
		endforeach;
	endif;
	
	?> /><?php if( !isset ( $extra['noCloseButton'] ) || !$extra['noCloseButton'] ):
	?> <label><?php
	if (isset ( $extra ['placeholder'] ) && ! empty ( $extra ['placeholder'] )) :
		echo htmlentities ( $extra ['placeholder'] );
	 elseif (defined ( 'ZOEKEN_PLACEHOLDER' ) && ZOEKEN_PLACEHOLDER) :
		echo ZOEKEN_PLACEHOLDER;
	else :
		?>Zoeken op naam, product, artikelnummer<?php endif; ?></label> <?php endif; ?>
</form> 
<?php

}
function render_sidenav() {
	global $NAV;
	
	render_element ( 'sidenav', $NAV [CatID] );
	
	foreach ( $NAV as $SIDE ) :
		if ($SIDE ['id'] !== CatID)
			render_element ( 'sidenav', $SIDE );
	endforeach
	;
}
function render_element($view, $item = null, $extra = null) {
	global $HTM;
	if (isset ( $item ))
		$D = $item;
	$view = str_replace ( [ 
			'.php',
			'../' 
	], [ 
			'',
			'/' 
	], $view );
	$path = __DIR__ . '/sections/';
	
	if (! file_exists ( "{$path}{$view}.php" ) && file_exists ( "{$path}{$view}/default.php" ))
		$view = $view . '/default';
	
	if (isset ( $extra ) && is_array ( $extra )) :
		foreach ( $extra as $param => $value ) :
			$$param = $value;
		endforeach;
	endif;

	if (file_exists ( "{$path}{$view}.php" )) :
		switch ($view):
			default :
				if( isset( $extra['return'] ) && $extra['return'] ):
					return require ( "{$path}{$view}.php");
				elseif( isset( $extra['object'] ) && $extra['object'] ):
					ob_start ();
					require ("{$path}{$view}.php");
					return ob_end_clean ();
				else:

				require ("{$path}{$view}.php");
					return true;
				endif;
		endswitch;
	endif;
	
	return false;
}

function get_model( $type, $controller=null, $settings=null) {
	//Controleren of er een type is opgegeven en of deze bestaat
	if( !isset( $type ) || empty( $type ) ) return false;
	
	//sanitize type input
	$type = str_ireplace(
		['../','//',' ','.php'],
		['','/','',''],
		$type
	);

	if( file_exists( __DIR__ . '/models/' . $type . '.php' ) ):
		$file = __DIR__ . '/models/' . $type . '.php';
	elseif( is_dir( __DIR__ . '/models/' . $type ) && file_exists( __DIR__ . '/models/' . $type . '/default.php' ) ):
		$file = __DIR__ . '/models/' . $type . '/default.php';
	else:
		return false;
	endif;
	
	
	// PDO binnenhalen
	global $db;
	//default return as array
	$returnAsArray = true;
	
	//Extra variabellen ophalen
	if( isset( $settings ) && $settings ):
		if( is_array( $settings ) ):
			foreach( $settings AS $var => $val ):
				$$var = $val;
			endforeach;
			elseif( is_bool( $settings ) ):
				$returnAsArray = $settings;
			endif;
		endif;
	
		try {
			//Query info binnenhalen aan de hand van de return in de query/type dir
			$thisData = require( $file );
			
		} catch( PDOException $ex ) {
			die( $ex->getMessage() );
		}
	
	//Kijken hoe er geretourneerd moet worden
	if( isset( $controller ) && $controller ):
		return get_controller( $thisData, $controller, $returnAsArray );
	else:
		// Als het niet via get_info geretourneer moet worden, kijken of je een array terug wilt
		if( $returnAsArray == false )
			if( is_array( $thisData ) ) $thisData = $thisData[ array_keys( $thisData )[0] ];
			return $thisData;
		endif;
		
		
		// no return, no glory
		return false;
}

function get_controller($data, $type, $returnAsArray = true) {
	if (isset ( $data ) && ! empty ( $data ))
		$input = $data;
	else
		return false;
	
	if (! isset ( $input ) || empty ( $input ) || ! $input)
		return false;

    if( $returnAsArray === 'skip' ) {
        return $data;
    }

	$input = is_array ( $input ) && isset ( $input [0] ) ? $input : [ 
			$input 
	];
	$output = [ ];
	
	for($x = 0; $x < count ( $input ); $x ++) :
		if (! isset ( $output [$x] ))
			$output [$x] = new stdClass ();
		$R = array_values ( $input ) [$x];
		
		if (file_exists ( __DIR__ . '/controllers/' . $type . '.php' )) :
			require (__DIR__ . '/controllers/' . $type . '.php');
		else :
			require(__DIR__ . '/controllers/default.php');
		endif;
	endfor
	;
	
	if ($returnAsArray && ! is_array ( $output ))
		$output = [ 
				$output 
		];
	elseif (! $returnAsArray && is_array ( $output ) && count ( $output ) <= 1)
		$output = $output [0];
	
	return $output;
}
function change_array_index(array $Input, string $NewIndex, bool $Sort = true) {
	$Output = [ ];
	foreach ( $Input as $Row ) {
		if (is_array ( $Row )) {
			$Output [$Row [$NewIndex]] = $Row;
		} elseif (is_object ( $Row )) {
			$Output [$Row->$NewIndex] = $Row;
		}
	}
	
	if (empty ( $Output ))
		return false;
	
	if ($Sort)
		ksort ( $Output );
	
	return $Output;
}
function get_factuur($ID, $deBestelling, $HTM, $db) {
	require_once (__INC__ . '/pdf/tcpdf/tcpdf.php');
	require_once (__INC__ . '/pdf/fpdi/fpdi.php');
	
	$HEAD = [ ];
	
	$BI = $deBestelling ['bestelinfo'] [0];
	
	if ($BI->type_bestelling == 'zakelijk') :
		$HEAD ['Bedrijf'] = $BI->betaling_bedrijf;
		$HEAD ['Contactpersoon'] = str_replace ( '  ', ' ', $BI->betaling_voornaam . ' ' . $BI->betaling_tussenvoegsel . ' ' . $BI->betaling_achternaam );
	else :
		$HEAD ['Naam'] = str_replace ( '  ', ' ', $BI->betaling_voornaam . ' ' . $BI->betaling_tussenvoegsel . ' ' . $BI->betaling_achternaam );
	
	endif;
	
	$HEAD ['Adres'] = $BI->betaling_adres;
	$HEAD ['Postcode'] = $HTM->get_postcode ( $BI->betaling_postcode ) . ' ' . $BI->betaling_plaats;
	$HEAD ['Datum'] = strftime ( TIMEFORMAT_FACTUUR, strtotime ( $BI->datum_aangepast ) );
	$HEAD ['Leveringsmethode'] = ucfirst ( $BI->aflever_method ) . 'en';
	$HEAD ['Ordernummer'] = 'Order ' . $BI->invoice_no;
	
	$orderInfoClass = new order ( $db, '', '' );
	$HEAD ['Betaalmethode'] = ucfirst ( $orderInfoClass->betaalmethode_get ( $BI->betaling_method ) );
	if ($HEAD ['Betaalmethode'] === 'Oprekening')
		$HEAD ['Betaalmethode'] = 'Op rekening';
	unset ( $orderInfoClass );
	
	$pdf_naam = generate_url_from_text ( SITENAME_PLAIN ) . '_factuur-' . generate_url_from_text ( 'bestelling ' . $HEAD ['Ordernummer'] ) . '.pdf';
	$file = __INC__ . '/temp/' . $pdf_naam;
	
	if (file_exists ( $file ))
		return $file;
	
	/*
	 * Beginnen met bouwen PDF
	 */
	class PDF extends FPDI {
		var $_tplIdx;
		function Header() {
			if (is_null ( $this->_tplIdx )) {
				$this->setSourceFile ( __INC__ . '/pdf/briefpapier_website.pdf' );
				$this->_tplIdx = $this->importPage ( 1 );
			}
			$this->useTemplate ( $this->_tplIdx );
			
			// $this->SetFont('freesans', '', 11);
		}
		function Footer() {
		}
	}
	
	$pdf = new PDF ( 'P', 'px', 'A4', true, 'UTF-8', false );
	$pdf->SetMargins ( 40, 90, 40, true );
	$pdf->SetAutoPageBreak ( true, 90 );
	
	$pdf->AddPage ( 'P', 'A4' );
	$pdf->SetTextColor ( 0, 0, 0 );
	$pdf->SetFont ( "pdfatimes", "", 11 );
	
	// set document information
	$pdf->SetCreator ( 'Bandwerk internet- en reclamebureau' );
	$pdf->SetAuthor ( SITENAME_PLAIN );
	$pdf->SetTitle ( 'Bestelling bij ' . SITENAME_PLAIN );
	$pdf->SetSubject ( 'Orderbon ' . ucfirst ( $HEAD ['invoice_no'] ) );
	$pdf->SetKeywords ( SITENAME_PLAIN . ', Food, non-food, factuur, ' . $HEAD ['Factuurnummer'] );
	
	// set image scale factor
	$pdf->setImageScale ( PDF_IMAGE_SCALE_RATIO );
	
	// set JPEG quality
	$pdf->setJPEGQuality ( 75 );
	
	// X = <-->, Y = ^,
	// Titel van product
	$pdf->SetTextColor ( '33', '45', '110' );
	$pdf->SetFont ( "pdfatimesb", "", 16 );
	
	$thisY = 34;
	
	$pdf->Text ( 40, $thisY, 'Orderbon' );
	
	$thisY += 42;
	$pdf->SetTextColor ( '62', '64', '61' );
	
	$pdf->SetFont ( "pdfatimes", "", 11 );
	foreach ( $HEAD as $omschrijving => $waarde ) :
		if (! empty ( $waarde )) :
			// Artikelnummer
			$pdf->Text ( 40, $thisY, $omschrijving . ':' );
			
			$pdf->Text ( 200, $thisY, html_entity_decode ( $waarde ) );
			$thisY += 22;
		endif;
		
	endforeach
	;
	
	try {
		$Query = 'SELECT ab.*, b.`percentage` FROM `acc_bestelregels` ab
					LEFT JOIN `acc_btw` b
						ON ab.`btw_id`=b.`id`
		
		WHERE `acc_bestellingen_id`=:id ORDER BY `categorie` ASC;';
		
		$Query = $db->prepare ( $Query );
		$Query->bindValue ( ':id', $ID, PDO::PARAM_INT );
		
		if ($Query->execute ())
			$Bestelling = $Query->fetchAll ( PDO::FETCH_OBJ );
		else
			die ( 'Bestelling niet gevonden. ' );
		
		$Producten = [ ];
		$Bijproducten = [ ];
		
		foreach ( $Bestelling as $P ) :
			switch ($P->categorie) :
				case 'bijproduct' :
					$Bijproducten [$P->artikel_id] [$P->bijproduct_id] = $P;
					break;
				case 'product' :
					$Producten [$P->artikel_id] = $P;
					break;
				case 'bezorging' :
					$FOOT ['Bezorgkosten'] = $P->aantal;
					break;
				case 'korting' :
					$FOOT ['Korting'] = - 1 * $P->aantal;
					break;
				case 'toeslag' :
					$FOOT ['Lengtetoeslag: ' . $P->artikel] = [ 
							$P->prijs_exc,
							$P->prijs 
					];
			endswitch
			;
		endforeach
		;
	} catch ( PDOException $ex ) {
		die ( $ex->getMessage () );
	}
	
	$TABEL = '<table style="border-collapse:collapse" cellpadding="5" >
   				<thead>
   					<tr>
	   					<td style="width:44%;background-color:#212D6E;color:#FFF"><strong>Omschrijving</strong></td>
	   					<td style="width:12%;background-color:#212D6E;color:#FFF;text-align:right"><strong>p.st.</strong></td>
	   					<td style="width:12%;background-color:#212D6E;color:#FFF;text-align:right"><strong>aantal</strong></td>
	   					<td style="width:16%;background-color:#212D6E;color:#FFF;text-align:right"><strong>excl. BTW</strong></td>
	   					<td style="width:16%;background-color:#212D6E;color:#FFF;text-align:right"><strong>incl. BTW</strong></td>
   					</tr>
   				</thead>
   				<tbody>';
	
	$BTW = [ ];
	
	// Eerst alle producten met bijproducten verwerken
	foreach ( $Bijproducten as $ID => $C ) :
		$P = $Producten [$ID];
		
		foreach ( $Bijproducten [$ID] as $BID => $BC ) :
			$BTW [$BC->percentage] += $BC->prijs - $BC->prijs_exc;
			
			$TABEL .= '<tr>
	   					<td style="border-bottom: 1px solid #aaa;width:44%">' . $BC->bijproduct . ': ' . $BC->artikel . '</td>
	   					<td style="border-bottom: 1px solid #aaa;width:12%;text-align:right"><small>&euro;</small> ' . toMoney ( ($BC->prijs_exc / $BC->aantal) ) . '</td>
	   					<td style="border-bottom: 1px solid #aaa;width:12%;text-align:right">' . $BC->aantal . '</td>
	   					<td style="border-bottom: 1px solid #aaa;width:16%;text-align:right"><small>&euro;</small> ' . toMoney ( $BC->prijs_exc ) . '</td>
	   					<td style="border-bottom: 1px solid #aaa;width:16%;text-align:right"><small>&euro;</small> ' . toMoney ( $BC->prijs ) . '</td>
   					</tr>';
		endforeach
		;
		
		unset ( $Producten [$ID] );
	endforeach
	;
	
	// Alle overgebleven producten verwerken
	foreach ( $Producten as $ID => $C ) :
		$BTW [$BC->percentage] += $C->prijs - $C->prijs_exc;
		
		$TABEL .= '<tr>
			   					<td style="width:44%;border-bottom: 1px solid #aaa">' . $C->artikel . '</td>
			   					<td style="width:12%;border-bottom: 1px solid #aaa;text-align:right"><small>&euro;</small> ' . toMoney ( ($C->prijs_exc / $C->aantal) ) . '</td>
			   					<td style="width:12%;border-bottom: 1px solid #aaa;text-align:right">' . $C->aantal . '</td>
			   					<td style="width:16%;border-bottom: 1px solid #aaa;text-align:right"><small>&euro;</small> ' . toMoney ( $C->prijs_exc ) . '</td>
			   					<td style="width:16%;border-bottom: 1px solid #aaa;text-align:right"><small>&euro;</small> ' . toMoney ( $C->prijs ) . '</td>
		   					</tr>';
	endforeach
	;
	
	$TABEL .= '<tr style="background-color:#EDEEEF">
	    				<td colspan="3" style="border-top:2px solid #888; border-bottom: 1px solid #aaa">Totaalbedrag exclusief BTW</td>
	    				<td style="text-align:right;border-top:2px solid #888; border-bottom: 1px solid #aaa"><small>&euro;</small> ' . toMoney ( $BI->totaal - array_sum ( $BTW ) - array_sum ( $FOOT ) ) . '</td>
	    				<td style="border-top:2px solid #888; border-bottom: 1px solid #aaa"> </td>
	    			</tr>';
	
	foreach ( $BTW as $PC => $BTW ) :
		$TABEL .= '
	    			<tr>
	    				<td colspan="3" style="border-bottom: 1px solid #aaa">Totaalbedrag BTW ' . $PC . '&#37;</td>
	    				<td style="text-align:right;border-bottom:1px solid #aaa"><small>&euro;</small> ' . toMoney ( ($BTW) ) . '</td>
	    				<td style="text-align:right;border-bottom:1px solid #aaa"></td>
	    			</tr>';
	endforeach
	;
	
	if (isset ( $FOOT ) && is_array ( $FOOT )) :
		foreach ( $FOOT as $TYPE => $BEDRAG ) :
			if (is_array ( $BEDRAG )) :
				$TABEL .= str_replace ( '<small>&euro;</small>-', '- <small>&euro;</small> ', '
					    			<tr>
					    				<td colspan="3" style="border-bottom: 1px solid #aaa">' . $TYPE . '</td>
					    				<td style="text-align:right;border-bottom:1px solid #aaa"><small>&euro;</small> ' . toMoney ( $BEDRAG [0] ) . '</td>
					    				<td style="text-align:right;border-bottom:1px solid #aaa"><small>&euro;</small> ' . toMoney ( $BEDRAG [1] ) . '</td>

					    			</tr>' );
			 
			elseif ($BEDRAG != 0) :
				
				$TABEL .= str_replace ( '<small>&euro;</small>-', '- <small>&euro;</small> ', '
					    			<tr>
					    				<td colspan="4" style="border-bottom: 1px solid #aaa">' . $TYPE . '</td>
					    				<td style="text-align:right;border-bottom:1px solid #aaa"><small>&euro;</small> ' . toMoney ( ($BEDRAG) ) . '</td>
					    			</tr>' );
			
			endif;
		endforeach
		;
		
	endif;
	
	$TABEL .= '<tr>
    				<td colspan="4"><strong>Totaalbedrag inclusief BTW</strong></td>
    				<td style="text-align:right;border-bottom: 1px solid #aaa"><strong><small>&euro;</small> ' . toMoney ( $BI->totaal ) . '</strong></td>
    			</tr>
    		</tbody>
    		</table>';
	
	$pdf->writeHTMLCell ( 520, '', 40, $thisY + 20, $TABEL, 0, 0, 0, true, 'L', true );
	
	if ($pdf->Output ( $file, 'F' )) {
		while ( ! file_exists ( $file ) ) :
		endwhile
		;
		return $file;
	} else {
		return false;
	}
}

?>
