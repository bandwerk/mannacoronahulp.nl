<?php
    $getPlaatsnamen = $db->prepare("SELECT `plaatsnaam` FROM `plaatsen` WHERE `id` IN (SELECT `plaatsnaam_id` FROM `gebruiker_plaatsen` WHERE `gebruiker_id` = :id)");
    $getPlaatsnamen->bindValue(":id", $_SESSION['userId'], PDO::PARAM_STR);
    $getPlaatsnamen->execute();

    $names = [];

    while($res = $getPlaatsnamen->fetch(PDO::FETCH_OBJ)) {
        array_push($names, $res->plaatsnaam);
    }

    $_SESSION['plaatsnamen'] = $names;
