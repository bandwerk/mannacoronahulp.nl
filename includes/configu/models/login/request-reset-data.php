<?php
    if(isset($hash) && strlen($hash) == 32) {
    $emailQ = $db->prepare("SELECT `username` FROM `website_logins` WHERE `reset_hash` = :hash");
    $emailQ->bindValue(":hash", $hash, PDO::PARAM_STR);

    try {
        if($emailQ->execute()) {
            $email = ($emailQ->fetch(PDO::FETCH_OBJ))->username;
            return $email;
        }
    } catch (PDOException $e) {
        return false;
    }
}