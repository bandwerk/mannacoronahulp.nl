<?php

    // Check if user exists
    $userQ = $db->prepare('SELECT `id`, `username` FROM `website_logins` WHERE `username` = :username');
    $userQ->bindValue(":username", $email, PDO::PARAM_STR);

    if($userQ->execute() && $userQ->rowCount() == 1) {
        // Store user data if exists.
        $res = $userQ->fetch(PDO::FETCH_OBJ);
        $userId = $res->id;
        $userEmail = $res->username;
    } else {
        // Return false if no user found.
        return false;
    }

    // Generate reset hash
    $resethash = md5(date('dmyhis') . $userEmail . 'coronahulp');

    // Save the hash to the user's record.
    $updateQ = $db->prepare('UPDATE `website_logins` SET `reset_hash` = :hash WHERE `id` = :id');
    $updateQ->bindValue(":hash", $resethash, PDO::PARAM_STR);
    $updateQ->bindValue(":id", $userId, PDO::PARAM_INT);

    if(!$updateQ->execute()){
        return false;
    }

    // Send mail to account holder.
    $SEND = [];
    $SEND ['onderwerp'] = 'Wachtwoord herstellen Manna Coronahulp';

    $SEND ['body'] = '<p>Beste gebruiker,</p>';
    $SEND ['body'] .= '<p>Klik <a href="'.SITELINK.'/beheer/wachtwoord-vergeten/'.$resethash.'">hier</a> om uw wachtwoord opnieuw in te stellen.</p>';
    $SEND ['body'] .= '<p><em>Werkt dat niet? Kopieër dan de volgende url en plak deze in je browser:</em></p>';
    $SEND ['body'] .= '<p><strong>'.SITELINK.'/beheer/wachtwoord-vergeten/'.$resethash.'</strong></p>';
    $SEND ['body'] .= '<p>Heeft u niet zelf een nieuw wachtwoord opgevraagd? Neem dan contact op met de systeembeheerder.</p>';

    $SEND['mailto'] = $userEmail;

    $url = str_replace("http://", "", str_replace("https://", "", getSitelink()));
    if( !require(__ROOT__.'/includes/forms/mail/send.php') ) {
        echo "mislukt";
    }
