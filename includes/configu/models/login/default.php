<?php
    function salt($u, $p) {
        return strtolower($u)."wapeningsstaal".$p;
    }

    $getUserHash = $db->prepare("SELECT `id`, `password2`, `reset_hash` FROM `website_logins` WHERE `username` = :username AND `password` = ''");
    $getUserHash->bindValue(":username", $username, PDO::PARAM_STR);
    $getUserHash->execute();

    if($getUserHash->rowCount() == 1) {
        //Username exists and is activated
        $userData = $getUserHash->fetch();
        if(password_verify(salt(($username), $password), $userData['password2'])) {
            if($userData['reset_hash'] !== null) {
                $updateUser = $db->prepare("UPDATE `website_logins` SET `reset_hash` = null WHERE `id` = :id;");
                $updateUser->bindValue(":id", $userData['id'], PDO::PARAM_INT);
                $updateUser->execute();
            }
            $_SESSION['userId'] = $userData['id'];
            return $userData['id'];
        }
        return false;
    } else {
        //User does not exist or account is not activated

        $checkUser = $db->prepare("SELECT `id`, `reset_hash` FROM `website_logins` WHERE `username` = :username AND `password` = :password");
        $checkUser->bindValue(":username", $username, PDO::PARAM_STR);
        $checkUser->bindValue(":password", $password, PDO::PARAM_STR);
        $checkUser->execute();

        if($checkUser->rowCount() != 1) {
            return false;
        }

        $checkUser = $checkUser->fetch();

        //User firsttime login, generate hash
        $password_hash = password_hash(salt(($username), $password), PASSWORD_DEFAULT);
        $id = intval($checkUser['id']);

        if($checkUser['reset_hash'] !== null) {
            $updateUser = $db->prepare("UPDATE `website_logins` SET `reset_hash` = null WHERE `id` = :id;");
            $updateUser->bindValue(":id", $id, PDO::PARAM_INT);
            $updateUser->execute();
        }

        if(is_numeric($id)) {
            $updateUser = $db->prepare("UPDATE `website_logins` SET `password` = '', `password2` = :password WHERE `id` = :id;");
            $updateUser->bindValue(":password", $password_hash, PDO::PARAM_STR);
            $updateUser->bindValue(":id", $id, PDO::PARAM_INT);
            if($updateUser->execute()) {
                $_SESSION['userId'] = $id;
                return $id;
            }
            return false;
        }
    }
