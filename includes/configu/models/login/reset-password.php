<?php
    function salt($u, $p) {
        return strtolower($u)."wapeningsstaal".$p;
    }

    // Stop if passwords don't match.
    if($password1 !== $password2) {
        return false;
    }

    // Select the user id with their reset hash.
    $userQ = $db->prepare("SELECT `id`, `username` FROM `website_logins` WHERE `reset_hash` = :hash");
    $userQ->bindValue(":hash", $hash, PDO::PARAM_STR);

    // User is found.
    if($userQ->execute() && $userQ->rowCount() == 1) {
        $user = $userQ->fetch(PDO::FETCH_OBJ);

        //Generate password hash
        $password_hash = password_hash(salt(($user->username), $password1), PASSWORD_DEFAULT);
        $id = intval($user->id);
        if(is_numeric($id)) {
            $updateUser = $db->prepare("UPDATE `website_logins` SET `password` = '', `password2` = :password, `reset_hash` = null WHERE `id` = :id;");
            $updateUser->bindValue(":password", $password_hash, PDO::PARAM_STR);
            $updateUser->bindValue(":id", $id, PDO::PARAM_INT);
            if($updateUser->execute()) {
                return $id;
            }
            return false;
        }
    }
