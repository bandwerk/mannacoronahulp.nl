<?php
$Query = 'SELECT p.*,
group_concat(concat_ws(".", pcp.id, pcp.ext) ORDER BY pcp.albumthumb DESC, pcp.id DESC) as partner_image
FROM partners p
LEFT JOIN partners_crop_photos pcp ON p.id = pcp.album_id
GROUP BY p.id
ORDER BY RAND()
LIMIT 10';

return $db->query( $Query )->fetchAll( PDO::FETCH_OBJ );
?>


