<?php

$limit = isset( $limit ) && is_numeric( $limit ) ? 'LIMIT '. $limit : 'LIMIT 3';

return $db->query( 'SELECT 
						l.*
					FROM `link` l
					
					WHERE l.`aktief`="ja" AND l.`url`!="http://"
					
						ORDER BY l.`sort` ASC 
				'. $limit . ';')->fetchAll( PDO::FETCH_OBJ );
?>