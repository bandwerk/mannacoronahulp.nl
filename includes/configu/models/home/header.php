<?php

$limit = isset( $limit ) && is_numeric( $limit ) ? 'LIMIT '. $limit : 'LIMIT 3';

return $db->query( 'SELECT 
						h.*, 
						group_concat( concat_ws( ".", p.`id`, p.`ext` ) ) AS `images`

					FROM `header` h
					
					LEFT JOIN `header_crop_photos` `p`
						ON h.`id`=p.`album_id`

					
					GROUP BY `p`.`id`
					ORDER BY rand()
				'. $limit . ';')->fetchAll( PDO::FETCH_OBJ );
?>