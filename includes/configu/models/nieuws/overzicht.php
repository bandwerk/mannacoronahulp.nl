<?php
if( $limit === '*' ) 
	$limit = '';
else
	$limit = ! isset ( $limit ) || empty ( $limit ) ? ' LIMIT 3' : ' LIMIT ' . $limit;


if (isset ( $where )) :
	if (is_array ( $where )) :
		if (is_numeric ( array_keys ( $where ) [0] )) :
			$where = ' WHERE `n`.`id` IN (' . implode ( ',', $where ) . ')';
		 else :
			$whereQ = [ ];
			foreach ( $where as $field => $val ) :
				if (is_array ( $val )) :
					$whereQ [] = $field . ' IN (' . implode ( ',', $val ) . ') ';
				 else :
					switch ($val [0]) :
						case '!' :
							$whereQ [] = $field . '!="' . substr( $val, 1 ) . '" ';
							break;
						case '<=' :
							$whereQ [] = $field . '<="' . substr( $val, 2 ) . '" ';
							break;
						case '>=' :
							$whereQ [] = $field . '>="' . substr( $val, 2 ) . '" ';
							break;
						case '<' :
							$whereQ [] = $field . '<"' . substr( $val, 1 ) . '" ';
							break;
						case '>' :
							$whereQ [] = $field . '>"' . substr( $val, 1 ) . '" ';
							break;
						default :
							$whereQ [] = $field . '="' . $val . '" ';
					endswitch
					;
				endif;
			endforeach
			;
			
			$where = 'WHERE ' . implode ( ' AND ', $whereQ );
			unset ( $whereQ );
		endif;
	 elseif (is_numeric ( $where )) :
		$where = 'WHERE `n`.`id`=' . ( int ) $where;
	endif;
 else :
	$where = ' WHERE `n`.`datum` <= now()';
endif;

$Query = 'SELECT DISTINCT `n`.*,
			( SELECT count( `n`.`id` ) FROM `nieuws` `n` '. $where . ' ) AS `totaal`

		FROM `nieuws` `n`
			' . $where . '
				GROUP BY
					`n`.`id`
				ORDER BY `n`.`datum` DESC, `id` DESC
			' . $limit . ';';

return $db->query ( $Query )->fetchAll ( PDO::FETCH_OBJ );
?>

