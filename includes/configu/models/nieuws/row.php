<?php

$limit = !isset( $limit ) || empty( $limit ) ? ' LIMIT 3' : ' LIMIT '. $limit;

if( isset( $where ) ) $where = is_array( $where ) ? ' WHERE `n`.`id` IN ('. explode( ',', $where ) .')' : 'WHERE `n`.`id`='. (int) $where;
else $where = ' WHERE `n`.`publicatie_op` <= now()';


$where .= ' ';

$Query = 'SELECT DISTINCT 
			`n`.*,
			( SELECT count( `n`.`id` ) FROM `nieuws` `n` '. $where .' ) AS `totaal`,
			( SELECT concat_ws( ".", `np`.`id`, `np`.`ext` ) FROM
				`nieuws_header_crop_photos` `np`
				WHERE `np`.`album_id`=`n`.`id`
					ORDER BY `np`.`albumthumb` DESC, `np`.`id` DESC
				LIMIT 1 ) AS `image`

		FROM `nieuws` `n`
			'. $where .'
				GROUP BY
					`n`.`id`
				ORDER BY `n`.`publicatie_op` DESC, `id` DESC
			'. $limit .';';

return $db->query( $Query )->fetchAll( PDO::FETCH_OBJ );
?>

