<?php

$limit = !isset( $limit ) || empty( $limit ) ? ' LIMIT 3' : ' LIMIT '. $limit;

if( isset( $where ) ) $where = is_array( $where ) ? ' WHERE `n`.`id` IN ('. explode( ',', $where ) .')' : 'WHERE `n`.`id`='. (int) $where;
else $where = ' WHERE `n`.`publicatie_op` <= now()';

$Query = 'SELECT DISTINCT `n`.*,
			( SELECT count( `id` ) FROM `nieuws` `n` ) AS `totaal`,
			group_concat( concat_ws( ".", `nh`.`id`, `nh`.`ext` ) ORDER BY `nh`.`albumthumb` DESC, `nh`.`id` DESC) AS `nieuws_header`

		FROM `nieuws` `n`
            LEFT JOIN `nieuws_header_crop_photos` `nh`
                ON `n`.`id`=`nh`.`album_id`
			'. $where .'
				GROUP BY
					`n`.`id`
				ORDER BY
					`n`.`publicatie_op` DESC
			'. $limit .';';

return $db->query( $Query )->fetchAll( PDO::FETCH_OBJ );
?>