<?php
$return = '';
$join = '';

$limit = ! isset ( $limit ) || empty ( $limit ) ? '' : ($limit != '*' ? ' LIMIT ' . $limit : '');
$order = ! isset ( $order ) || empty ( $order ) ? ' ORDER BY rand()' : $order;

if (isset ( $where )) :
	if (is_array ( $where )) :
		if (is_numeric ( array_keys ( $where ) [0] )) :
			$where = ' WHERE `w`.`id` IN (' . implode ( ',', $where ) . ')';
		else :
			$whereQ = [ ];
			$implodeWith = ' AND ';
			
			foreach ( $where as $field => $val ) :
				switch ($field) :
					case 'OR' :
						$subQ = [ ];
						$subImplodeWith = ' OR ';
						
						foreach ( $val as $k => $v ) :
							switch ($v [0]) :
								case 'I' :
									switch ($v [1]) :
										case 'N' :
											$subQ [] = $k . 'IN (' . substr ( $v, 2 ) . ') ';
											break;
									endswitch
									;
									break;
								case '|' :
									$subQ [] = $k . '="' . substr ( $v, 1 ) . '" ';
									$subImplodeWith = ' OR ';
									break;
								case '!' :
									$subQ [] = $k . '!="' . substr ( $v, 1 ) . '" ';
									break;
								case '<' :
									$subQ [] = $k . '<"' . substr ( $v, 1 ) . '" ';
									break;
								case '>' :
									$subQ [] = $k . '>"' . substr ( $v, 1 ) . '" ';
									break;
								default :
									if (is_array ( $v ))
										$subQ [] = $k . ' IN (' . implode ( ',', $v ) . ') ';
									else
										$subQ [] = $k . '="' . $v . '" ';
							endswitch
							;
						endforeach
						;
						$whereQ [] = '(' . implode ( $subImplodeWith, $subQ ) . ')';
						break;
					default :
						switch ($val [0]) :
							case 'I' :
								switch ($val [1]) :
									case 'N' :
										$whereQ [] = $field . 'IN (' . substr ( $val, 2 ) . ') ';
										break;
								endswitch
								;
								break;
							case '|' :
								$whereQ [] = $field . '="' . substr ( $val, 1 ) . '" ';
								$implodeWith = ' OR ';
								break;
							case '!' :
								$whereQ [] = $field . '!="' . substr ( $val, 1 ) . '" ';
								break;
							case '<' :
								$whereQ [] = $field . '<"' . substr ( $val, 1 ) . '" ';
								break;
							case '>' :
								$whereQ [] = $field . '>"' . substr ( $val, 1 ) . '" ';
								break;
							default :
								if (is_array ( $val ))
									$whereQ [] = $field . ' IN (' . implode ( ',', $val ) . ') ';
								else
									$whereQ [] = $field . '="' . $val . '" ';
						endswitch
						;
				endswitch
				;
			endforeach
			;
			
			$where = 'WHERE ' . implode ( $implodeWith, $whereQ );
			unset ( $whereQ );
		endif;
	 elseif (is_numeric ( $where )) :
		$where = 'WHERE `w`.`id`=' . ( int ) $where;
	else :
		$where = 'WHERE ' . str_replace ( ';', '', $where );
	endif;
else :
	$where = '';
endif;

if (isset ( $like ) && is_array ( $like )) :
	$where .= empty ( $where ) && ! stripos ( $where, 'WHERE' ) ? 'WHERE (' : 'AND (';
	$whereQ = [ ];
	
	foreach ( $like as $field => $val ) :
		$whereQ [] = $field . ' LIKE "%' . $val . '%"';
	endforeach
	;
	
	$where .= implode ( ' OR ', $whereQ );
	unset ( $whereQ );
	$where .= ')';
			endif;

if (isset ( $having )) :
	if (is_array ( $having )) :
		if (is_numeric ( array_keys ( $having ) [0] )) :
			$having = ' WHERE `w`.`id` IN (' . implode ( ',', $having ) . ')';
		else :
			$havingQ = [ ];
			$implodeWith = ' AND ';
			
			foreach ( $having as $field => $val ) :
				switch ($val [0]) :
					case 'I' :
						switch ($val [1]) :
							case 'N' :
								$havingQ [] = $field . 'IN (' . substr ( $val, 2 ) . ') ';
								break;
						endswitch
						;
						break;
					case '|' :
						$implodeWith = ' OR ';
						$havingQ [] = $field . '="' . substr ( $val, 1 ) . '" ';
						break;
					case '!' :
						$havingQ [] = $field . '!="' . substr ( $val, 1 ) . '" ';
						break;
					case '<' :
						$havingQ [] = $field . '<"' . substr ( $val, 1 ) . '" ';
						break;
					case '>' :
						$havingQ [] = $field . '>"' . substr ( $val, 1 ) . '" ';
						break;
					default :
						$havingQ [] = $field . '="' . $val . '" ';
				endswitch
				;
			endforeach
			;
			
			$having = 'HAVING ' . implode ( $implodeWith, $havingQ );
			unset ( $havingQ );
		endif;
	 elseif (is_numeric ( $having )) :
		$having = 'HAVING `w`.`id`=' . ( int ) $having;
	endif;
else :
	$having = '';
endif;

$Query = 'SELECT
			`w`.*,
            GROUP_CONCAT( DISTINCT CONCAT( `wp`.`id`, ".", `wp`.`ext` ) ORDER BY  `wp`.`albumthumb` DESC, `wp`.`id` DESC ) AS `images`
    FROM `website` `w`
        LEFT JOIN `website_crop_photos` `wp`
            ON `w`.`id`=`wp`.`album_id`
		' . $join . '
		' . $where . '
		GROUP BY `w`.`id`
			' . $having . '
			' . $order . '
			' . $limit . ';';

if (isset ( $debug ) && $debug)
	echo '<pre>' . $Query . '</pre>';

return $db->query ( $Query )->fetchAll ( PDO::FETCH_OBJ );
?>
