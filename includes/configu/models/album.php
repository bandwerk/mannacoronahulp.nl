<?php
$where = isset( $where ) && is_numeric( $where ) ? 'WHERE `p`.`album_id`="'. (int) $where . '"' : '' ;
$limit = isset( $limit ) && !empty( $limit ) ? 'LIMIT '. trim( $limit ) : 'LIMIT 12';

$Query = 'SELECT
			`a`.`id`,
			`a`.`descr` AS `titel`,
		( SELECT concat_ws( ".", `p`.`id`, "jpg" )
			FROM `photo`
				WHERE `album_id`=`a`.`id`
				ORDER BY `updated_on` ASC, `created_on` ASC
					LIMIT 1 ) AS `image`,
		group_concat( DISTINCT concat_ws( ".", `p`.`id`, "jpg" ) ) AS `images`
			
			FROM `album` `a`
				LEFT JOIN `photo` `p`
					ON `a`.`id`=`p`.`album_id`
			
			'. $where . '
				
			GROUP BY `a`.`id`
		
			ORDER BY 
				`a`.`created_on` DESC,
				`a`.`updated_on` DESC
		
			'. $limit .';';

return $db->query( $Query )->fetchAll( PDO::FETCH_OBJ );