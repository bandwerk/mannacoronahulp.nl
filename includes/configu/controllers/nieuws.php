<?php
global $URLS;

foreach ( $R as $A => $V ) :
	switch ($A) :
        case 'id' :
            $output[ $x ]->SEOUrl = GetSEOSlug($V, 'nieuws');
            break;
		case 'img' :
		case 'ext' :
			if ((is_object ( $R ) && isset ( $R->img ) && ! empty ( $R->img )) || (is_array ( $R ) && isset ( $R ['img'] ) && ! empty ( $R ['img'] ))) {
				if (is_object ( $R ) && ! isset ( $R->ext ))
					$R->ext = 'jpg';
				elseif (is_array ( $R ) && ! isset ( $R ['ext'] ))
					$R ['ext'] = 'jpg';
				$output [$x]->image = is_object ( $R ) ? $R->img . '.' . $R->ext : $R ['img'] . '.' . $R ['ext'];
			} else {
				$output [$x]->image = '';
			}
			break;
		case 'begintijd' :
		case 'eindtijd' :
			if (! empty ( $V ) && $V !== '00:00:00')
				$output [$x]->$A = substr ( $V, 0, strlen ( $V ) - 3 );
			break;
		case 'created_on' :
		case 'datum' :
			$A = 'datum';
			$output [$x]->datetime = strtotime ( $V );
			$output [$x]->datum_url = strftime ( TIMEFORMAT . ' %Y', $output [$x]->datetime );
			$output [$x]->datum = new stdClass();
			$output [$x]->datum->kort = strftime ( TIMEFORMAT_KORT , $output [$x]->datetime );
			$output [$x]->datum->voluit = strftime ( TIMEFORMAT_VOLUIT , $output [$x]->datetime );
			$output [$x]->datum->standaard = strftime ( TIMEFORMAT . ' '. TIMEFORMAT_UUR, $output [$x]->datetime );
			
			$V = null;
			break;
		case 'images':
		case 'header_images':
			if( !empty( $V ) ){
				$V = explode(',', $V );
				if( is_array( $V ) && !empty( $V[ array_keys( $V )[0] ]) )
					$output[$x]->{substr( $A, 0, -1)} = $V[0];
					
					if( count( $V ) == 0 ) $A = null;
			} else
				$A = null;
				
				break;
		case 'url':
			switch( $V ):
				case 'http://':
				case 'https://':
						$V = '#';
					break;
				default:
					if( strpos( $V, '/' ) == 0 ) $V = SITELINK . $V;
					
					
					break;
			
			endswitch;
			
			break;
			case 'youtube_video':
				if( strpos( $V, '?v=' ) ){
					$V = substr( $V, strpos( $V, '?v=' ) + 3, strlen( $V ) - strpos( $V, '?v=') );
					if( strpos( $V, '&') ) $V = substr( $V, 0, strpos( $V,'&' ) );
					

				} elseif( strpos( $V, 'youtu.be/' ) ) {
					$V = substr( $V, strpos( $V, 'youtu.be/' ) + 9, strlen( $V ) - strpos( $V, 'youtue.be/') );
					if( strpos( $V, '?') ) $V = substr( $V, 0, strpos( $V,'?' ) );
					if( strpos( $V, '"') ) $V = substr( $V, 0, strpos( $V,'"' ) );

				} elseif( strpos( $V, '/embed/' ) ) {
					$V = substr( $V, strpos( $V, '/embed/' ) + 7, strlen( $V ) - strpos( $V, '/embed/') );
					if( strpos( $V, '?') ) $V = substr( $V, 0, strpos( $V,'?' ) );
					if( strpos( $V, '"') ) $V = substr( $V, 0, strpos( $V,'"' ) );
					
				} else {
					$A = null;
				}
			break;
	endswitch
	;
	
	if (isset ( $A ) && ! isset ( $output [$x]->$A ) && isset( $V ) )
		$output [$x]->$A = $V;
endforeach;

if( !isset( $output[ $x ]->url )  && isset( $output[ $x ]->titel, $output[ $x ]->id ) ) {
	if( !isset( $R->url ) )
		$output [$x]->url = $URLS['nieuws'] . '/'. $output[ $x ]->id . '/' . generate_url_from_text( $output[$x]->titel );
	
	if( !isset( $R->alt ) )
		$output [$x]->alt = 'Meer over '. $output[ $x ]->titel;
			
}

?>