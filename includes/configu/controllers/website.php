<?php
global $URLS; 

foreach ($R as $A => $V) :
    switch ($A) :
        case 'id':
            if( hasContent( $URLS ) && array_key_exists( $V, $URLS ) )
                $output[ $x ]->url = $URLS[ $V ];
            break;
        case 'titel':
            if (! isset($output[$x]->url) || empty($output[$x]->url))
                $output[$x]->url = SITELINK . '/' . $R->id . '/' . $type . '/' . generate_url_from_text($V) . '.html';
            break;
        case 'img':
        case 'ext':
            if ((is_object($R) && isset($R->img) && ! empty($R->img)) || (is_array($R) && isset($R['img']) && ! empty($R['img']))) {
                if (is_object($R) && ! isset($R->ext))
                    $R->ext = 'jpg';
                elseif (is_array($R) && ! isset($R['ext']))
                    $R['ext'] = 'jpg';
                $output[$x]->image = is_object($R) ? $R->img . '.' . $R->ext : $R['img'] . '.' . $R['ext'];
            } else {
                $output[$x]->image = '';
            }
            break;
        case 'begintijd':
        case 'eindtijd':
            if (! empty($V) && $V !== '00:00:00')
                $output[$x]->$A = substr($V, 0, strlen($V) - 3);
            break;
        case 'created_on':
        case 'datum':
            $A = 'datum';
            $output[$x]->datetime = strtotime($V);
            $output[$x]->datum_url = strftime(TIMEFORMAT . ' %Y', $output[$x]->datetime);
            $V = strftime(TIMEFORMAT, $output[$x]->datetime);
            break;
        case 'images':
            if (! empty($V)) {
                $V = explode(',', $V);
                if (is_array($V) && ! empty($V[array_keys($V)[0]]))
                    $output[$x]->image = $V[0];

                if (count($V) == 0)
                    $A = null;
            } else
                $A = null;

            break;
        case 'class':
            if ($V === 'geen') {
                $V = false;
            }
            break;
    endswitch
    ;

    if ($V == null) {
        $V = false;
    }

    if (isset($A) && ! isset($output[$x]->$A))
        $output[$x]->$A = $V;
endforeach
;
?>