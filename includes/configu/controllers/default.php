<?php
if(!is_object($R)) return;
foreach ( $R as $A => $V ) :
	if( $A == 'images' || ( strlen( $A ) > 7 && stripos( $A, '_images' ) == strlen( $A ) - 7 ) ):
		$prefix = substr( $A, 0, -7 );
		if( !empty( $V ) ){

			$V = explode(',', $V );
			if( is_array( $V ) && !empty( $V[ array_keys( $V )[0] ]) )
				$output[$x]->{ $prefix . 'image' } = $V[0];
			
			$output[$x]->{ $prefix . 'images' } = $V;
			

		} else {
			$output[$x]->{ $prefix . 'image' } = false;
		}
	
	else:
		switch ($A) :
			case 'titel' :
				if( !isset( $output[$x]->url ) || empty( $output[$x]->url ) ) $output [$x]->url = SITELINK . '/' . $R->id . '/' . $type . '/' . generate_url_from_text ( $V );
				if( !isset( $output[$x]->alt ) || empty( $output[$x]->alt ) ) $output [$x]->alt = 'Meer over '. $V;
				break;
			case 'img' :
			case 'ext' :
				if ((is_object ( $R ) && isset ( $R->img ) && ! empty ( $R->img )) || (is_array ( $R ) && isset ( $R ['img'] ) && ! empty ( $R ['img'] ))) {
					if (is_object ( $R ) && ! isset ( $R->ext ))
						$R->ext = 'jpg';
					elseif (is_array ( $R ) && ! isset ( $R ['ext'] ))
						$R ['ext'] = 'jpg';
					$output [$x]->image = is_object ( $R ) ? $R->img . '.' . $R->ext : $R ['img'] . '.' . $R ['ext'];
				} else {
					$output [$x]->image = '';
				}
				break;
			case 'begintijd' :
			case 'eindtijd' :
				if (! empty ( $V ) && $V !== '00:00:00')
					$output [$x]->$A = substr ( $V, 0, strlen ( $V ) - 3 );
				break;
			case 'created_on' :
			case 'datum' :
				$A = 'datum';
				$output [$x]->datetime = strtotime ( $V );
				$output [$x]->datum_url = strftime ( TIMEFORMAT . ' %Y', $output [$x]->datetime );
				$V = strftime ( TIMEFORMAT, $output [$x]->datetime );
				break;
			case 'class':
			    if( $V === 'geen' ) {
			        $V = false;
			    }
			    break;
		endswitch;
		
		if( $V == null ) {
			$V = false;
		}
		
		if (isset ( $A ) && ! isset ( $output [$x]->$A ))
			$output [$x]->$A = $V;
	endif;			
endforeach
;
?>