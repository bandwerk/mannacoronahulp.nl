<?php

	$find = [
		'/<(div|nav|ul|li|a|p|h[0-9]) [A-Za-z0-9-=\"\':\/]+>/i',
		'/<img[A-Za-z0-9-=\"\'\/]+>/i',
		'/class="[A-Za-z0-9 -="]+"/i',
		'/style="[A-Za-z0-9 -=;:()\/\_]+"/i'
			
	];
	
	$replace = [
			'<$1>',
			'',
			'',
			''


	];

	$Pagina['paginatitel'] = 'Pagina niet gevonden.';
	if( !isset( $Pagina['inhoud'] ) ) {
		$Pagina['inhoud'] = '';
	}
	$Pagina['inhoud'] .= '<div class="sitemap" style="color:#FFF;">
			<h3 style="color:#FFF;">Sitemap</h3>
			' .
		strip_tags( preg_replace(
				$find,
				$replace,
				str_replace( ' <label for="headerinput2">Zoeken</label>', '' , $MENU->m ) ), '<ul style="color:#FFF;"><li><a><nav><h2><h3><h4><h5>' ) 
		. '</div>';

	$Pagina['images'] = -1;	
	$Pagina['share'] = false;
?>