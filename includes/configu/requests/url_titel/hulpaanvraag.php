<?php
$hash = GetPageId();

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (strlen(GetPageId()) != 40) {
    Header('Location: ' . SITELINK);
    die();
}
require_once __INC__ . '/classes/class.coronaclient.php';
require_once __INC__ . '/classes/class.coronacaregiver.php';

$caregiver = new CoronaCareGiver();
$client = new CoronaClient();
$client->LoadByHash($hash);

$error = "";

$images = get_model('images', 'default', [
    'returnAsArray' => true,
    'id' => 1,
])[0];
$sfeer_image = explode(",", $images->sfeer_image)[0];
$content_images = explode(",", $images->content_afbeelding);
if (isset($_POST, $_POST['pin'])) {
    $caregiver->LoadByPincode($_POST['pin']);
    $permission = $caregiver->HasAccessToRequest($hash);
    if ($permission) {
        $_SESSION['hulpverlener'] = $caregiver->id;
        $_SESSION['hulpverlener_login'] = time();
    } else {
        $error = "Een gebruiker met deze pin heeft geen toegang tot deze hulpvraag.";
    }
}
if (isset($_SESSION['hulpverlener_login'])) {
    if (time() - $_SESSION['hulpverlener_login'] > 900) {
        session_unset();
    }
}

if (!isset($_SESSION['hulpverlener'])) {
    $Pagina['titel'] = "inloggen als hulpverlener";
    $HTM->set_script('hulpvraag_login');

} else {
    $caregiver->LoadById($_SESSION['hulpverlener']);
    $Pagina['titel'] = "Hulpaanvraag bekijken";
    $HTM->set_script('hulpvraag_bekijk');

    if (isset($_GET, $_GET['completed'])) {
        $client->SetCompleted();
    }
}