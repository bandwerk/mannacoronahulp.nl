<?php
$HTM->set_script('home');

$faqs = get_model('faqs/home', 'faq',[
    'returnAsArray' => true,
]);
$partners = get_model('partners', 'partners',[
    'returnAsArray' => true,
]);

$images = get_model('images', 'default', [
    'returnAsArray' => true,
    'id' => 1,
])[0];
$sfeer_image = explode(",",$images->sfeer_image)[0];
$content_images = explode(",",$images->content_afbeelding);