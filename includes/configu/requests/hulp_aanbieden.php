<?php
$HTM->set_script("hulp_aanbieden", "hulp_aanbieden");

$hulpvragen = get_model("hulpvragen", "hulpvragen", [
    'returnAsArray' => true
]);

$partners = get_model('partners', 'partners',[
    'returnAsArray' => true,
]);
require_once __FORMS__ . '/hulp_aanbieden.php';


$images = get_model('images', 'default', [
    'returnAsArray' => true,
    'id' => $Pagina['id'],
])[0];
$sfeer_image = explode(",",$images->sfeer_image)[0];
$content_images = explode(",",$images->content_afbeelding);