<?php

$medewerkers = get_model('medewerker', 'team', [
    'returnAsArray' => true
] );

if( !$scriptName = strpos( __FILE__, '/' ) !== false ? substr( strrchr( __FILE__, '/' ), 1, -4 ) : substr( strrchr( __FILE__, '\\' ), 1, -4 ) ) {
    die('geen scriptnaam bekend.');
}

$HTM->set_script( 'nieuws' );

$ID = GetPageId();

$Nieuws_p = isset( $_SESSION['nieuws']['page'] ) && is_numeric( $_SESSION['nieuws']['page'] ) ? $_SESSION['nieuws']['page'] : 1 ;


if( isset( $ID ) && $ID ):
    $HTM->set_view( 'detail' );

    $nieuwsItem = get_model( 'nieuws/detail', 'nieuws', [ 'returnAsArray' => false, 'where' => $ID ]);
    $klanten = get_model('klanten', 'klanten', [
        'returnAsArray'=>true, 'order' => 'ORDER BY rand()', 'limit'=> 1
    ] );

    $Pagina['paginatitel'] = $nieuwsItem->titel;
    if( isset( $nieuwsItem->subtitel ) && !empty( $nieuwsItem->subtitel ) ) $Pagina['sub_paginatitel'] = $nieuwsItem->subtitel;
    $Pagina['inhoud'] = $nieuwsItem->bericht;

    if( isset( $nieuwsItem->image ) && !empty( $nieuwsItem->image ) ):
        $Pagina['image'] = NIEUWS_THUMBNAIL . '/' . $nieuwsItem->image;
        $Pagina['header'] = NIEUWS_HEADER . '/'. $nieuwsItem->image;
    endif;

    if( isset( $nieuwsItem->header_image ) && !empty( $nieuwsItem->header_image ) ):
        $Pagina['header'] = NIEUWS_HEADER . '/'. $nieuwsItem->header_images[ array_rand( $nieuwsItem->header_images ) ];
    endif;

    if( isset( $nieuwsItem->images ) && !empty( $nieuwsItem->images ) ):
        $Pagina['images'] = $nieuwsItem->images;

        array_walk( $Pagina['images'], function(&$val){
            $val = NIEUWS_DETAIL . '/' . $val;
        });
    endif;
    $HTM->set_meta( NIEUWS_HEADER. '/' . $nieuwsItem->nieuws_header, 'image' );
    $HTM->set_meta( $nieuwsItem->titel , 'title' );

else:
    $HTM->set_js( [JAVASCRIPT . '/ajax.js'] );
    $NieuwsPerPagina = defined('NIEUWS_PER_PAGINA') ? NIEUWS_PER_PAGINA : CASE_PER_PAGINA;

    $Nieuws = get_model( 'nieuws/row', 'nieuws', [
        'returnAsArray' => true,
        'limit' => $NieuwsPerPagina * $Nieuws_p
    ] );

    if( isset( $Nieuws[0]->totaal ) && $Nieuws[0]->totaal > ( $NieuwsPerPagina * $Nieuws_p ) ):
        $HTM->set_js([JAVASCRIPT . '/ajax.js']);

        $Pagina['nav'] = new stdClass();
        $Pagina['nav']->child = 'nieuws';
        $Pagina['nav']->get= 'nieuws';
        $Pagina['nav']->titel = 'Meer laden...';
        $Pagina['nav']->page = 2;
        $Pagina['nav']->limit = $NieuwsPerPagina;


    endif;



endif;