<?php
$HTM->set_script("hulp_aanvragen", "hulp_aanvragen");


$hulpvragen = get_model("hulpvragen", "hulpvragen", [
    'returnAsArray' => true
]);

$partners = get_model('partners', 'partners',[
    'returnAsArray' => true,
]);
require_once __FORMS__ . '/hulp_aanvragen.php';

$images = get_model('images', 'default', [
    'returnAsArray' => true,
    'id' => $Pagina['id'],
])[0];
$sfeer_image = explode(",",$images->sfeer_image)[0];
$content_images = explode(",",$images->content_afbeelding);