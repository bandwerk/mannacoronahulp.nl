<?php
$HTM->set_script('faq');



$faq_cat = get_model('faqs/cat', 'faq', [
    'returnAsArray' => true,
]);
$faqs = [];
foreach( $faq_cat as $cat){
    $fcid = $cat->id;
    $data = get_model('faqs/faqs', 'faq',[
        'returnAsArray' => true,
        'where' => 'WHERE fcl.cat_id = '. $fcid . '',
    ]);
    $vragen = [];
    foreach($data as $vraag) {
        $vraag_antwoord = ["vraag" => $vraag->vraag, "antwoord" => $vraag->antwoord];
        array_push($vragen, $vraag_antwoord);
//        $vragen[$vraag->vraag] = $vraag->antwoord;
    }
    $faqs[$cat->categorie] = $vragen;
}


$partners = get_model('partners', 'partners',[
    'returnAsArray' => true,
]);


$images = get_model('images', 'default', [
    'returnAsArray' => true,
    'id' => $Pagina['id'],
])[0];
$sfeer_image = explode(",",$images->sfeer_image)[0];
$content_images = explode(",",$images->content_afbeelding);