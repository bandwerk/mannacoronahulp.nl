<?php
date_default_timezone_set('UTC');
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$LoginError = "";

// Post fields for password reset are filled out.
if (isset($_POST, $_POST['hash'], $_POST['password1'], $_POST['password2']) && !isset($_SESSION['support'])) {
    $result = get_model('login/reset-password', 'login', ['hash' => $_POST['hash'], 'password1' => $_POST['password1'], 'password2' => $_POST['password2'], 'returnAsArray' => 'skip'] );
    header("Location: " . HOME . "/" . $Pagina['url_titel']);

// Post fields for login are filled out.
} else if (isset($_POST, $_POST['username'], $_POST['password']) && !isset($_SESSION['support'])) {
    $result = get_model('login/default', 'login', ['username' => $_POST['username'], 'password' => $_POST['password'], 'returnAsArray' => 'skip'] );
    if($result != false) {
        $_SESSION['support'] = $result;
        header("Location: " . HOME . "/" . $Pagina['url_titel']);
    } else {
        $LoginError = "Ongeldige login";
    }

// Post fields for login reset request are filled out.
} else if (isset($_POST, $_POST['email']) && !isset($_SESSION['support'])) {
    $result = get_model('login/request-reset', 'login', ['email' => $_POST['email'], 'returnAsArray' => 'skip'] );
    if($result) {
        header("Location: " . HOME . "/" . $Pagina['url_titel'] . "/wachtwoord-vergeten/bevestig");
    } else {
        header("Location: " . HOME . "/" . $Pagina['url_titel'] . "/wachtwoord-vergeten/bevestig/onbekend");
    }
}
// User requested a password reset.
if(strpos($_SERVER['REQUEST_URI'], 'wachtwoord-vergeten/bevestig')) {
    $success = strpos($_SERVER['REQUEST_URI'], 'onbekend') == false;
    $HTM->set_script('login','reset-confirm');

// User enrolled in password reset.
} else if(strpos($_SERVER['REQUEST_URI'], 'wachtwoord-vergeten')) {
    //Check if there's a hash in the request URI
    $hash = explode('/', $_SERVER['REQUEST_URI']);
    $hash = htmlentities($hash[count($hash) - 1]);

    // URI contains a hash and length matches ours.
    if(strlen($hash) == 32) {
        // Let user reset PW
        $email = get_model('login/request-reset-data', 'login', ['hash' => $hash, 'returnAsArray' => 'skip'] );
        $HTM->set_script('login', 'reset');

        // There is no hash: show initial reset page.
    } else {
        $HTM->set_script('login', 'reset-request');
    }

// User is not logged in
} else if(!isset($_SESSION['support'])) {
    $HTM->set_script('login');

// User wants to log out
} else if(isset($_GET['logout'])) {
    session_destroy();
    header("Location: " . HOME . "/" . $Pagina['url_titel']);

// User is logged in: check ids.
} else {
    get_model('login/plaatsnamen', 'login', ['id' => $_SESSION['userId'], 'returnAsArray' => 'skip'] );

    //Get image for header
    $images = get_model('images', 'default', [
        'returnAsArray' => true,
        'id' => 1,
    ])[0];
    $sfeer_image = explode(",",$images->sfeer_image)[0];


    //Get statusses


    if(strpos($_SERVER['REQUEST_URI'], $Pagina['url_titel'].'/hulpaanvragen')) {
        $statuses = CoronaClient::GetStatuses();
        $filterId = false;
        if(isset($_GET['filter'])) {
            $filterId = intval($_GET['filter']);
        }
        $hulpvragen = CoronaClient::GetClients($filterId);
        $HTM->set_script('beheer', 'hulpvraag');
    } else if(strpos($_SERVER['REQUEST_URI'], $Pagina['url_titel'].'/hulpverleners')) {
        $statuses = CoronaCareGiver::GetStatuses();
        $filterId = false;
        if(isset($_GET['filter'])) {
            $filterId = $_GET['filter'];
        }
        $hulpverleners = CoronaCareGiver::GetCareGivers($filterId);
        $HTM->set_script('beheer', 'hulpverleners');
    } else if(strpos($_SERVER['REQUEST_URI'], $Pagina['url_titel'].'/update/hulpaanvraag')) {
        // Handle autosave ajax request
        if(!isset($_GET['name'], $_GET['id'], $_GET['value'], $_GET['timestamp']) || $_GET['id'] == 'undefined' || !is_numeric($_GET['id']) || !is_numeric($_GET['value']) || !is_numeric($_GET['timestamp'])) { die('false'); }

        $datetime = new DateTime('@'.$_GET['timestamp']);
        $datetime = $datetime->format('Y-m-d H:i:s');

        $client = new CoronaClient();
        $client->LoadById($_GET['id']);

        if($client->laatst_bewerkt != $datetime) {
            die('edited');
        }

        switch ($_GET['name']) {
            case 'caregiver':
                $client->SetCaregiver($_GET['value']);
                break;
            case 'status':
                $client->status = intval($_GET['value']);
                $client->SaveChanges();
                break;
            default:
                die('false');
                break;
        }
        die('true');
    } else if(strpos($_SERVER['REQUEST_URI'], $Pagina['url_titel'].'/update/hulpverlener')) {
        // Handle autosave ajax request
        if(!isset($_GET['name'], $_GET['id'], $_GET['value'], $_GET['timestamp']) || $_GET['id'] == 'undefined' || !is_numeric($_GET['id']) || !is_numeric($_GET['timestamp'])) { die('false'); }

        $datetime = new DateTime('@'.$_GET['timestamp']);
        $datetime = $datetime->format('Y-m-d H:i:s');

        $careGiver = new CoronaCareGiver();
        $careGiver->LoadById($_GET['id']);

        if($careGiver->laatst_bewerkt != $datetime) {
            die('edited');
        }

        switch ($_GET['name']) {
            case 'status':
                $careGiver->aktief = ($_GET['value'] == 'Ja' ? 'Ja' : 'Nee');
                $careGiver->SaveChanges(false);
                break;
            default:
                die('false');
                break;
        }
        die('true');
    } else {
        global $db;
        $in = "('".implode("', '", $_SESSION['plaatsnamen'])."')";

        $noReq1 = $db->prepare("SELECT COUNT(*) FROM `hulpaanvraag` WHERE `status` = 1 AND `plaats` IN " . $in);
        $noReq1->execute();
        $noReq1 = $noReq1->fetchColumn();

        $noReq2 = $db->prepare("SELECT COUNT(*) FROM `hulpverleners` WHERE `aktief` = 'Nee' AND `plaats` IN " . $in);
        $noReq2->execute();
        $noReq2 = $noReq2->fetchColumn();

        $HTM->set_script('beheer');
    }
}
?>