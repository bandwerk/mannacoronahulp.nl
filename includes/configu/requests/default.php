<?php

$HTM->set_js ( [
        DIST . '/bundle.js',
    'https://postcode.bandwerkplus.nl/dashboard/script/postCodeSearch.js'
//'https://localhost/postcode.bandwerk.nl/dashboard/script/postCodeSearch.js'
] );


if (isset ( $GET ['url_titel'] ))
	$Pagina ['categorie'] = str_replace ( '-', ' ', $GET ['url_titel'] );

	/*
 * Overzicht pagina's genereren als de pagina leeg is.
 */

if (isset ( $Pagina ['inhoud'] ) && empty ( $Pagina ['inhoud'] )) {
	$Pagina ['gegenereerd'] = true;
	
	if (((! defined ( 'CatID' ) || PageID == CatID) && isset ( $NAV [PageID] ['sub'] ) && count ( $NAV [PageID] ['sub'] ) > 0) || (defined ( 'CatID' ) && PageID != CatID && isset ( $NAV [CatID] ['sub'] [PageID] ['sub'] ) && count ( $NAV [CatID] ['sub'] [PageID] ['sub'] ) > 0)) {
		try {
            $oQ = 'SELECT 
					w.id, 
					w.titel, 
					w.inleiding, 
					s1.url_titel'.( defined( 'WEBSITE_PHOTO' ) && WEBSITE_PHOTO ? ',
					GROUP_CONCAT( DISTINCT CONCAT_WS( ".", `wp`.`id`, `wp`.`ext` ) ORDER BY `wp`.`albumthumb` DESC, `wp`.`id` ASC ) AS `images`' : '' ) . '
			FROM website w
				LEFT JOIN `SEO_Module_link` s
					ON w.id=s.website_id
				LEFT JOIN `SEO_Module` s1
					ON s.`SEO_ID`=s1.`SEO_ID`
				'. ( defined( 'WEBSITE_PHOTO') && WEBSITE_PHOTO ? 'LEFT JOIN `website_crop_photos` `wp`
					ON `w`.`id`=`wp`.`album_id`' : '' ) .'

				WHERE w.parent=:id AND w.aktief="ja"
				GROUP BY `w`.`id`
				ORDER BY `w`.`sort` ASC;';
			
			$oQ = $db->prepare ( $oQ );
			$oQ->bindValue ( ':id', ( int ) $Pagina ['id'], PDO::PARAM_INT );
			$oQ->execute ();
			
			if ($oQ->rowCount () > 0) {
				$Pagina ['inhoud'] .= '<div class="row">';
				foreach ( $oQ->fetchAll ( PDO::FETCH_OBJ ) as $R ) {
					$R->url = $URLS[ $R->id ];
					$R->alt_text = 'Meer over '. $R->titel;
					if( !empty( $R->images ) ) {
						$R->image = explode( ',', $R->images );
						$R->image = WEBSITE_THUMB . '/' . array_shift( $R->image );
					}
					
					ob_start(); ?>
					
					<div class="col-lg-4 col-md-6 col-xs-12">
                    	<a<?php echo HtmGen::href( $R->url, $R->alt_text, 'nieuws-card' ); ?>>
						<div class="borbot">
                    <?php if( isset( $R->image ) && hasContent( $R->image ) ): ?>
                          	<div class="img" style="background-image: url('<?php echo $R->image; ?>');"></div>
                    <?php else: ?>
                          	<div class="img fallback"></div>
                    <?php endif; ?>
						</div>
						<div class="content" ui-equaliser="content">
							<span class="title"><?php echo htmlentities( $R->titel ); ?></span>
							<?php echo isset( $R->inleiding ) && !empty( $R->inleiding ) ? HtmGen::text_shorten( $R->inleinding, 150 ) : ''; ?>
							<span class="play"><span class="inner"><img src="<?php echo IMAGES; ?>/SVG/play-button.svg" alt="Verder icoon" class="svg"></span></span>
						</div>
                        
                    </a>
                </div><?php 
                
                $Pagina['inhoud'] .= ob_get_clean();
				}
				$Pagina['inhoud'] .= '</div><!--.row-->';
			}
		} catch ( PDOException $ex ) {
			die ( $ex->getMessage () );
		}
	}
}

/*
 * Bestellingen
 */

if (! isset ( $_SESSION ['referer'] ) && isset ( $_SERVER ['HTTP_REFERER'] ))
	$_SESSION ['referer'] = $_SERVER ['HTTP_REFERER'];
$images = get_model('images', 'default', [
    'returnAsArray' => true,
    'id' => $Pagina['id'],
])[0];
$sfeer_image = explode(",",$images->sfeer_image)[0];
$content_images = explode(",",$images->content_afbeelding);