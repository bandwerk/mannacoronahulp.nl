<?php

spl_autoload_register( function( $ClassName ) {
	$Class = preg_replace('/[A-Z]/', '.\0', lcfirst( $ClassName ) );
	$GetFile = explode( '.', strtolower( $Class ) );
	array_pop( $GetFile );
	array_push( $GetFile, 'class.'.strtolower( $Class ) );
	array_filter( $GetFile );
	$File = '/' . implode( '/', $GetFile ) . '.php';
	
	$FileLocation = __CLASSES__ . $File;
	
	if( file_exists( $FileLocation ) ) {
		require_once ( $FileLocation );
		
	} 
	
});
	
	?>