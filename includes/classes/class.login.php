<?php


class login
{
	protected $db;
	protected $table;
	protected $salt;
	protected $protected = [
			'aktief',
			'wachtwoord',
			'wachtwoord_hash',
			'hash',
			'admin_hash',
			'type_account' ];
	
	function __construct( string $table, string $salt, PDO $PDO=NULL ) {
		if( !isset( $PDO ) ) {
			global $db;
			if( $db instanceof PDO ) {
				$this->db = $db;
				
			} else {
				die( 'Geen database connectie voor de sessie (is nodig voor eventuele gebruiker).');
			}
		} else {
			$this->db = $PDO;
		}

		
		$this->table = trim( $table );
		$this->salt = trim( $salt );
	}
	
	function check_password( $ww, $user = null ) {
		
		if( $this->loggedin() || ( isset( $user ) && filter_var( $user, FILTER_VALIDATE_EMAIL ) ) ) {
			$user = isset( $user ) ? $user : $_SESSION['loggedin']['email'];
		
			if( filter_var( $user, FILTER_VALIDATE_EMAIL ) ) {

				// Login informatie ophalen van de gebruiker
				$getLogin= $this->db->prepare( 'SELECT wachtwoord_hash AS hash, wachtwoord AS password2 FROM `'.$this->table.'` WHERE email=:user' );
				$getLogin->bindValue( ':user', $user, PDO::PARAM_STR );
				$getLogin->execute();
				
					//controleren of die er is
				if( $getLogin->rowCount() > 0 ) {
						// er is een match, data binnenhalen.
						try {
							$getLogin = $getLogin->fetch( PDO::FETCH_OBJ );
							$hash = $getLogin->hash;
				
							// wachtwoord encoderen
							$ww = $this->hashIt( $ww, $hash );

							if( $ww === $getLogin->password2 ) return $ww;
							else return false;
							
						} catch( PDOException $ex ) {
							die( $ex->getMessage() );
						}
				}
			}
		}
		
		return false;
	}
	
	function login( $user, $ww, $samenvoegen = false ) {
		global $_SERVER;

		$user = strtolower( $user );
		$omschrijving = 'Login: ' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		
		//controleren of die er is
		if( $ww = $this->check_password( $ww, $user ) ) {
			// er is een match, data binnenhalen.
			try {
				// informatie van de gebruiker binnenhalen
				$getLogin= $this->db->prepare( 'SELECT
						`u`.*,
						group_concat( DISTINCT concat_ws( "][", `uk`.`producten_categorie_id`, `uk`.`korting`, "#" ) )
							AS `gebruiker_korting`
					FROM `'.$this->table.'` `u`
						LEFT JOIN `'. $this->table . '_korting` `uk`
							ON `u`.`id`=`uk`.`us_gebruikers_id`
					WHERE email=:user AND wachtwoord=:pass
						GROUP BY `u`.`id` LIMIT 1;' );
				$getLogin->bindValue( ':user'	, $user, PDO::PARAM_STR );
				$getLogin->bindValue( ':pass'	, $ww, PDO::PARAM_STR );
				$getLogin->execute();
					
				$ud = $getLogin->fetch( PDO::FETCH_OBJ );
				$nu = strftime( '%Y-%m-%d %H:%M:%S', time() );
				
				// user array aanmaken voor return
				$user = [];
				
				
				
				// Data van db in array stoppen voor return
				foreach( $ud AS $field => $val ) {
					if( !in_array( $field, $this->protected ) ) {
						switch( $field ){
							case 'gebruiker_korting':
								if( isset( $val ) && $val ){
									$us_kortingen = explode( '][#', str_replace( '][#,', '][#', $val ) );
									
									$user[ $field ] = [];
									foreach( array_filter( $us_kortingen ) AS $us_k ) {
										$us_k = explode( '][', $us_k );
										$user[ $field ][ $us_k[0] ] = $us_k[1];
									}
								}
								break;
							default:
								$user[ $field ]  = $val;
						}
					}
				}
				
				// laatste_login overschrijven want in de database is van de vorige login
				$user[ 'laatste_login' ] = $nu;
				
				// voornaam, tussenvoegsel en achternaam tot user verwerken
				$user['user'] = str_replace( '  ', ' ', $ud->voornaam . ' ' . $ud->tussenvoegsel . ' ' . $ud->achternaam );
				
				// data aan object geven
				$this->user = $user;
				
				switch( $ud->type_account ) {
					case '99';
					$_SESSION['admin'] = $user;
					$_SESSION['admin']['type'] = 'super';
					break;
				}
				
				// hash aanmaken om de sessie te controleren
				$user['seshash']	= $this->hashIt( $ud->email, str_replace( '  ', ' ', $ud->voornaam . ' ' . $ud->tussenvoegsel . ' ' . $ud->achternaam ) . $ud->id );
				if( $user['geboortedatum'] == '0000-00-00' ) $user['geboortedatum'] = "";
				
				//wegschrijven in sessie
				$_SESSION['loggedin'] = $user;

				if( $samenvoegen ) {
					//Als de winkelwagen in de sessie al gevult is, deze toekennen aan een gebruiker
					$updateCart = $this->db->prepare( 'SELECT *, SUM( `aantal` ) AS `aantal` FROM `shop_winkelwagen` WHERE `personalnr`=:personalnr OR `us_gebruikers_id`=:userid GROUP BY `artikel_id`;');
					$updateCart->bindValue( ':userid', $ud->id, PDO::PARAM_INT );
					$updateCart->bindValue( ':personalnr', session_id() );
					$updateCart->execute();
					
					if( $updateCart->rowCount() > 0 ) {
						$cart = $updateCart->fetchAll( PDO::FETCH_ASSOC );
						
						$keys = [];
						$vals = [];
						$now = strftime( '%Y-%m-%d %H:%M:%S', time() );
						
						
						foreach( $cart AS $row ) {
							$vs = [];
							foreach( $row AS $key => $val ) {
								if( !isset( $firstRow ) ) $keys[] = $key;
								
								switch( $key ) {
									case 'us_gebruikers_id':
										$vs[] = $ud->id;
									break;
									case 'personalnr':
										$vs[] = '"' . session_id() . '"';
									break;
									case 'updated':
										$vs[] = '"'. $now . '"';
									break;
									default:
										$vs[] = '"' . $val . '"';
									break;
								}
								
							}
							
							$firstRow = true;
							$vals[] = '( '. implode( ', ', $vs ) . ' )';
						}
						
						$pdoQ = $this->db->prepare( 'DELETE FROM `shop_winkelwagen` WHERE `personalnr`=:personalnr OR `us_gebruikers_id`=:userid' );
						$pdoQ->bindValue( ':userid', $ud->id, PDO::PARAM_INT );
						$pdoQ->bindValue( ':personalnr', session_id() );
						
						if( $pdoQ->execute() ) {
							$this->db->query( 'INSERT INTO `shop_winkelwagen` ( '. implode( ', ', $keys ) . ' ) VALUES ' . implode( ', ', $vals ) . ';');
							
						}
				
					}
			} else {
				
				$pdoQ = $this->db->prepare( 'UPDATE `shop_winkelwagen` SET `us_gebruikers_id`=:userid WHERE `personalnr`=:personalnr' );
				$pdoQ->bindValue( ':userid', $ud->id, PDO::PARAM_INT );
				$pdoQ->bindValue( ':personalnr', session_id() );
				$pdoQ->execute();
			}
				
				$this->db->query( 'UPDATE `us_gebruikers` SET `laatste_login`="'. $nu .'" WHERE `id`='. $ud->id.';');
				
/*				if( isset( $_SESSION['quicklogin'] ) && $_SESSION['quicklogin'] == 'quicklogin' ) {
					$this->spaarpunten_add( $user['id'] , 'Bedankt voor de registratie!' );
					unset( $_SESSION['quicklogin'] );
					
				}
	*/
				$this->login_attempt($user['email'], $omschrijving );
				
				return $this->refresh( $user['email'] );
					
			} catch ( PDOException $e ) {
					return die( $e->getMessage() );
					$this->login_attempt($user, $omschrijving . ' / '. $e->getMessage(), 0 );
			}
			
		} else {
			$this->login_attempt($user, $omschrijving , 0 );
			unset( $_SESSION['loggedin'] );
			return false;
		}
	}
	
	function login_attempt( $user, $omschrijving, $succes = 1 ) {
		
		$fw = array_key_exists( 'HTTP_X_FORWARDED_FOR', $_SERVER ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
		
		$log = $this->db->prepare( 'INSERT INTO `inlogpogingen` (`email`, `ip`, `forwarded`, `datum`, `tijd`, `user_agent`, `succes`, `omschrijving` )
										VALUES ( :email, :ip, :fw, CURDATE(), CURTIME(), :ua, :suc, :opm );');
		
		$log->bindValue( ':email',  $user,  PDO::PARAM_STR );
		$log->bindValue( ':ip', 	$_SERVER['REMOTE_ADDR'] ,  PDO::PARAM_STR );
		$log->bindValue( ':fw', 	$fw ,  PDO::PARAM_STR );
		$log->bindValue( ':ua', 	$_SERVER['HTTP_USER_AGENT'],  PDO::PARAM_STR );
		$log->bindValue( ':suc', 	$succes,  PDO::PARAM_STR );
		$log->bindValue( ':opm', 	$omschrijving,  PDO::PARAM_STR );
		
		try{
			$log->execute();
			return true;
			
		} catch( PDOException $ex ) {
			return $ex->getMessage();
		}

		
	}
	
	function logout() {
		if( isset( $_SESSION['loggedin'] ) ) unset( $_SESSION['loggedin'], $_SESSION['bestelling'], $_SESSION['cart'], $_SESSION['loc_id'], $_SESSION['korting_id'], $_SESSION['admin'] );
		return true;
	}
	
	function loggedin() {
		if( isset( $_SESSION['loggedin'] ) && count( $_SESSION['loggedin'] > 1 ) ) {
			$id = $_SESSION['loggedin']['id'];
			$name = $_SESSION['loggedin']['user'];
			$email = $_SESSION['loggedin']['email'];
			$achternaam = $_SESSION['loggedin']['achternaam'];
			$this->user = $_SESSION['loggedin'];
			unset( $this->user['seshash'] );
				
			
			
			if( $this->hashIt( $email, $name . $id  ) == $_SESSION['loggedin']['seshash'] ) {
				$exists = $this->db->query( 'SELECT `id` FROM `us_gebruikers` WHERE `id`='. $id .';');
				if( $exists->rowCount() == 0 ) {
					@$this->logout();
					
					return false;
				}
				
				return true;
			
			} else {
				$this->logout();
				return false;
			}
		} else {
			unset( $_SESSION['loggedin'] );
			return false;
		}
	}
	
	function admin_userlogin( $id, $hash, $redir = '') {
		if( !isset( $_SESSION['admin'] ) || empty( $_SESSION['admin'] ) || !is_numeric( $id ) || !is_string( $hash ) ) {
			return false;
		}
		
		$getMail = $this->db->prepare( 'SELECT `email`, `id` FROM `'. $this->table .'` WHERE `id`=:id AND `hash`=:hash;');
		$getMail->bindValue( ':id', $id, PDO::PARAM_INT );
		$getMail->bindValue( ':hash', str_replace( ' ', '', $hash ), PDO::PARAM_STR );

		try {
			$getMail->execute();
			$email = $getMail->fetch( PDO::FETCH_OBJ );
			$id = $email->id;
			$email = $email->email;
			
		} catch( PDOEXception $ex ) {
			die( $ex->getMessage() );
		}
		
		$this->refresh( $email, $id );
			
		if( !empty( $redir ) ) {
			header( 'Location: '. str_replace(' ','', $redir ) );
		}
		
		
		
	}
	
	function admin_userlogout() {
		$this->refresh( $_SESSION['admin']['email'], $_SESSION['admin']['id'] );
		
	}
	
	function is_admin() {
		if( isset( $_SESSION['admin'] ) && !empty( $_SESSION['admin'] ) ) {
			
			$adm = $this->db->prepare('SELECT `id` FROM `'. $this->table .'` WHERE `id`=:id AND `type_account`=:type;');
			$adm->bindValue( ':id', (int) $_SESSION['admin']['id'], PDO::PARAM_INT );
			$adm->bindValue( ':type', 99, PDO::PARAM_INT );
			$adm->execute();
		
			if( $adm->rowCount() === 1 ){
				return true;
			}
			
			
		}
		
		return false;
	}
	
	
	function refresh( $email, $id = false ) {
		if( $id && $this->is_admin() ) {
			$user = [];
			$user['id'] = $id;
			$thisId = $id;
		} else {
			$user = $_SESSION['loggedin'];
			$thisId = $user['id'];
		}
		
		
		try {
			// informatie van de gebruiker binnenhalen
			$getLogin= $this->db->prepare( 'SELECT
						`u`.*,
						group_concat( DISTINCT concat_ws( "][", `uk`.`producten_categorie_id`, `uk`.`korting`, "#" ) )
							AS `gebruiker_korting`
					FROM `'.$this->table.'` `u`
						LEFT JOIN `'. $this->table . '_korting` `uk`
							ON `u`.`id`=`uk`.`us_gebruikers_id`
					WHERE `u`.`email`=:user AND `u`.`id`=:id
						GROUP BY `u`.`id` LIMIT 1;' );

			
			$getLogin->bindValue( ':user', $email , PDO::PARAM_STR );
			$getLogin->bindValue( ':id', $user['id'], PDO::PARAM_INT );
			$getLogin->execute();
			
			if( $getLogin->rowCount() == 0 ) {
				$this->logout();
			}
			
			
			$ud = $getLogin->fetchAll( PDO::FETCH_OBJ );
			$ud = $ud[0];
			
			
			// user array aanmaken voor return
			$user = [];
		
			// Data van db in array stoppen voor return
			foreach( $ud AS $field => $val ) {
				if( !in_array( $field, $this->protected ) ) {
					switch( $field ){
						case 'gebruiker_korting':
							if( isset( $val ) && $val ){
								$us_kortingen = explode( '][#', str_replace( '][#,', '][#', $val ) );
									
								$user[ $field ] = [];
								foreach( array_filter( $us_kortingen ) AS $us_k ) {
									$us_k = explode( '][', $us_k );
									$user[ $field ][ $us_k[0] ] = $us_k[1];
								}
							}
							break;
						default:
							$user[ $field ]  = $val;
					}
				}
				
			}
			
			// voornaam, tussenvoegsel en achternaam tot user verwerken
			$user['user'] = str_replace( '  ', ' ', $ud->voornaam . ' ' . $ud->tussenvoegsel . ' ' . $ud->achternaam );
			$user['postcode'] = substr( $user['postcode'], 0, 4 ) . ' '. substr( $user['postcode'], 4, 2 );
			if( $user['geboortedatum'] == '0000-00-00' ) $user['geboortedatum'] = "";
			
			
			if( $thisId !== $user['id'] && empty( $_SESSION['admin'] ) ) {
				return false;
			}
			
			// data aan object geven
			$this->user = $user;
		
			// hash aanmaken om de sessie te controleren
			$user['seshash']	= $this->hashIt( $ud->email, str_replace( '  ', ' ', $ud->voornaam . ' ' . $ud->tussenvoegsel . ' ' . $ud->achternaam ) . $ud->id );
			//wegschrijven in sessie
			$_SESSION['loggedin'] = $user;

			return $user;

		} catch( PDOException $ex ) {
			return $ex->getMessage();
		}
	}
	
	protected function hashIt( $input, $hash, $crypt = 'sha512' ) {
		$salt = $this->salt;
		
		$output = hash( $crypt, $input . $salt . $hash );
		return $output;
		
	}
}

?>