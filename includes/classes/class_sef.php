<?php

if (!defined('WEBSITE_TABEL')) {
    define('WEBSITE_TABEL', 'website');
}

if (!defined('IS_MOBILE')) {
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        define('IS_MOBILE', true);
    } else {
        define('IS_MOBILE', false);
    }
}

function url_titelFromId($website_id)
{
    global $db;

    try {

        //Haal op basis van het website ID de url_titel uit de SEO module tabel
        $Q = 'SELECT `s`.`url_titel`
	 			FROM `SEO_Module` `s`
		 			LEFT JOIN `SEO_Module_link` `l`
		 				ON `s`.`SEO_ID`=`l`.`SEO_ID`
	 			WHERE `l`.`website_id`=:website_id LIMIT 1';

        $Q = $db->prepare($Q);
        $Q->bindValue(':website_id', $website_id, PDO::PARAM_INT);

        // Als de query uitgevoerd kan worden, retourneer de 'url_titel';
        if ($Q->execute())
            return $Q->fetch(PDO::FETCH_OBJ)->url_titel;

    } catch (PDOException $ex) {
        die($ex->getMessage());
    }

    //Niets geretourneerd, dan false
    return false;
}

function SEO2BasicTitel($url_titel)
{
    global $db;

    try {

        //Haal op basis van het website ID de url_titel uit de SEO module tabel
        $Q = 'SELECT `w`.`titel`
	 			FROM ' . WEBSITE_TABEL . ' `w`
		 			LEFT JOIN `SEO_Module_link` `l`
		 				ON `w`.`id`=`l`.`website_id`
		 			LEFT JOIN `SEO_Module` `s`
		 				ON `l`.`SEO_ID`=`s`.`SEO_ID`
	 			WHERE `s`.`url_titel`=:url_titel LIMIT 1';

        $Q = $db->prepare($Q);
        $Q->bindValue(':url_titel', $url_titel, PDO::PARAM_STR);

        // Als de query uitgevoerd kan worden, retourneer de 'url_titel';
        if ($Q->execute())
            return $Q->fetch(PDO::FETCH_OBJ)->titel;

    } catch (PDOException $ex) {
        die($ex->getMessage());
    }

    //Niets geretourneerd, dan false
    return false;
}

function EchoDebug($Message)
{
    if (defined('EchoDebug') && EchoDebug) {
        echo '<pre style="color: red;">' . $Message . '</pre>';
    }
}

/*
 Deze functie kijkt aan de hand van een opgegeven titel welke pagina hier bij hoort.
 Return: id van website tabel
 */
function GetSEOPageID($url_titel)
{

    global $db;

    try {

        //Haal op basis van het website ID de url_titel uit de SEO module tabel
        $Q = 'SELECT `l`.`website_id`
	 			FROM `SEO_Module_link` `l`
		 			LEFT JOIN `SEO_Module` `s`
		 				ON `l`.`SEO_ID`=`s`.`SEO_ID`
	 			WHERE `s`.`url_titel`=:url_titel LIMIT 1';

        $Q = $db->prepare($Q);
        $Q->bindValue(':url_titel', $url_titel, PDO::PARAM_STR);

        // Als de query uitgevoerd kan worden, retourneer de 'website_id';
        if ($Q->execute())
            return $Q->fetch(PDO::FETCH_OBJ)->website_id;

    } catch (PDOException $ex) {
        die($ex->getMessage());
    }

    //Niets geretourneerd, dan false
    return false;

}

function GetSEOMETA($url_titel)
{
    global $HEADER, $db;

    try {

        //Haal op basis van de url_titel de andere seo data op
        $Q = 'SELECT `s`.*
   				FROM `SEO_Module` `s`
	 			WHERE `s`.`url_titel`=:url_titel;';

        $Q = $db->prepare($Q);
        $Q->bindValue(':url_titel', $url_titel, PDO::PARAM_STR);

        // Als de query uitgevoerd kan worden, retourneer de 'url_titel';
        if ($Q->execute() && $Q->rowCount() > 0)
            return $Q->fetch(PDO::FETCH_ASSOC);

    } catch (PDOException $ex) {
        die($ex->getMessage());
    }

    //Niets geretourneerd, dan default header
    return $HEADER;
}

function GetSEOMETAById($website_id)
{
    global $HEADER, $db;

    try {

        //Haal op basis van de url_titel de andere seo data op
        $Q = 'SELECT `s`.*
   				FROM `SEO_Module` `s`
    				LEFT JOIN `SEO_Module_link` `l`
    					ON `s`.`SEO_ID`=`l`.`SEO_ID`
	 			WHERE `l`.`website_id`=:website_id;';

        $Q = $db->prepare($Q);
        $Q->bindValue(':website_id', $website_id, PDO::PARAM_INT);

        // Als de query uitgevoerd kan worden, retourneer de 'url_titel';
        if ($Q->execute() && $Q->rowCount() > 0)
            return $Q->fetch(PDO::FETCH_ASSOC);
        else {
            // Geen seo data gevonden.. stuur meta vanuit config.
            $SEOMETA['Description'] = $HEADER['Description'];
            $SEOMETA['Keywords'] = $HEADER['Keywords'];
            $SEOMETA['Title'] = $HEADER['Title'];

        }
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
}

function table_exists($tablename)
{
    global $db;

    // Try a select statement against the table
    // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
    try {
        $result = $db->query("SELECT 1 FROM `$tablename` LIMIT 1");
    } catch (Exception $e) {
        // We got an exception == table not found
        return FALSE;
    }

    // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
    return $result !== FALSE;
}


function remove_accent($str)
{
    $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
    $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
    return str_replace($a, $b, $str);
}

function SEF_URL($str)
{
    return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), remove_accent($str)));
}

/*
 Functie: Telt eerst het aantal rows in de SEO tables en het aantal paginas.
 Als die niet gelijk zijn dan mist er een SEO row en moet die worden aangemaakt.
 */
function Update_Database_With_SEF()
{
    global $db;

    try {

        // bestaat de seo tabel wel? Zo niet => aanmaken.
        if (!table_exists('SEO_Module')) {
            $Structuur = '
	CREATE TABLE IF NOT EXISTS `SEO_Module` (
	  `SEO_ID` int(10) NOT NULL,
	  `Keywords` varchar(255) NOT NULL,
	  `Description` varchar(255) NOT NULL,
	  `titletag` varchar(255) NOT NULL,
	  `url_titel` varchar(255) NOT NULL,
	  UNIQUE KEY `website_id` (`SEO_ID`)
	) ENGINE=MyISAM DEFAULT CHARSET=utf8;
';
            $db->query($Structuur);
            EchoDebug('SEO Module aangemaakt');
        }

        if (!table_exists('SEO_Module_link')) {
            $Structuur2 = '
	CREATE TABLE IF NOT EXISTS `SEO_Module_link` (
	  `SEO_ID` int(10) NOT NULL AUTO_INCREMENT,
	  `website_id` int(10) DEFAULT NULL,
	  `nieuws_id` int(10) DEFAULT NULL,
	  PRIMARY KEY (`SEO_ID`)
	) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;
';
            $db->query($Structuur2);
            EchoDebug('SEO link tabel aangemaakt');
        }

        // Query om te kijken of er pagina's zijn waar geen SEO info voor is, en of er pagina's zijn waar SEO info is waar geen website pagina meer voor is
        $Q = 'SELECT
	*
	FROM
    	( SELECT
    		`w`.`id`,
    		`w`.`titel`,
    		`w`.`paginatitel`,
    		`l`.`website_id`,
    		`l`.`SEO_ID`,
    		`s`.`url_titel`
	    	FROM ' . WEBSITE_TABEL . ' `w`
		    	LEFT JOIN  `SEO_Module_link` `l`
	        		ON `w`.`id`=`l`.`website_id`
	    	    LEFT JOIN `SEO_Module` `s`
	        		ON `l`.`SEO_ID`=`s`.`SEO_ID`
    	UNION
    		SELECT
    			`w`.`id`,
	    		`w`.`titel`,
	    		`w`.`paginatitel`,
    			`l`.`website_id`,
    			`l`.`SEO_ID`,
    			`s`.`url_titel`
    		FROM `SEO_Module` `s`
		    	LEFT JOIN  `SEO_Module_link` `l`
		        	ON `s`.`SEO_ID`=`l`.`SEO_ID`
		        LEFT JOIN ' . WEBSITE_TABEL . ' `w`
		        	ON `l`.`website_id`=`w`.`id`
    	) `tempSEO`
    WHERE
    	`SEO_ID` IS NULL
    		OR `url_titel` IS NULL
    		OR `id` IS NULL
    		OR `website_id` IS NULL';

        $Q = $db->query($Q);

        // Als er paginas zijn waarvan de SEO module ontbreekt, of bestaat terwijl er geen pagina is dan verder
        if ($Q->rowCount() > 0):
            $SEOHandler = new stdClass();
            $SEOHandler->add = [];
            $SEOHandler->del = [];


            // Resultaat verwerken, kijken of de SEO info ontbreekt of de website pagina ontbreekt
            foreach ($Q->fetchAll(PDO::FETCH_OBJ) AS $SEOP):
                if ($SEOP->id && !$SEOP->url_titel):
                    $SEOHandler->add[] = [
                        'id' => $SEOP->id,
                        'url_titel' => isset($SEOP->paginatitel) && !empty($SEOP->paginatitel) ? SEF_URL($SEOP->paginatitel) : SEF_URL($SEOP->titel)

                    ];

                elseif ($SEOP->url_titel && !$SEOP->id && $SEOP->SEO_ID != null):
                    $SEOHandler->del[] = $SEOP->SEO_ID;
                endif;
            endforeach;

            // SEO informatie weghalen als pagina niet bestaat in website tabel
            if (is_array($SEOHandler->del) && !empty($SEOHandler->del)):

                //Checken of de pagina wel bestaat in een andere tabel

                $SEOv2Set = $db->prepare("
                        SELECT `SEO_ID`, `tabelnaam`, `tabelid`
                        FROM SEO_Module_link
                        WHERE
                        `SEO_ID` IS NOT NULL AND
                        `tabelnaam` IS NOT NULL AND
                        `tabelid` IS NOT NULL;");

                $SEOv2Set->execute();
                $SEOv2Set = $SEOv2Set->fetchAll(PDO::FETCH_OBJ);

                $dontDelete = [];
                foreach ($SEOv2Set as $match) {
                    $Q = $db->prepare('SELECT `id` FROM '.$match->tabelnaam.' WHERE `id`=:id');
                    $Q->bindValue(":id", $match->tabelid, PDO::PARAM_STR);
                    $Q->execute();
                    if($Q->rowCount() == 1) {
                        $dontDelete[] = $match->SEO_ID;
                    }
                }

                $SEOHandler->del = array_diff($SEOHandler->del, $dontDelete);

                $delPages = implode(',', $SEOHandler->del);
                if (!empty($delPages)):
                    $executed = 0;
                    $Q = 'DELETE FROM `SEO_Module` WHERE `SEO_ID` IN(' . $delPages . ');';
                    $Q = $db->prepare($Q);
                    $executed += $Q->execute();

                    $Q = 'DELETE FROM `SEO_Module_link` WHERE `SEO_ID` IN(' . $delPages . ');';
                    $Q = $db->prepare($Q);
                    $executed += $Q->execute();


                    EchoDebug('SEO Informatie verwijderd voor pagina(s). SEO ids: ' . $delPages);

                endif;
            endif;
            // SEO info aanmaken als website tabel niet bestaat
            if (is_array($SEOHandler->add) && !empty($SEOHandler->add)):
                $addPages = new stdClass();
                $addPages->values = [];
                $addPages->url_titels = [];
                $addPages->added = [];
                $addPages->ids = [];


                foreach ($SEOHandler->add AS $addPage):
                    $Q = 'INSERT IGNORE INTO
    						`SEO_Module_link` (`website_id`, `SEO_ID`)
    						VALUES (
    							?,
    	
    							( SELECT `SEO_ID`+1 FROM `SEO_Module` ORDER BY `SEO_ID` DESC LIMIT 1 ) );';
                    // seo link query
                    $Q = $db->prepare($Q);
                    $Q->bindValue(1, $addPage['id'], PDO::PARAM_INT);
                    if ($Q->execute()):

                        $Q = $db->prepare('INSERT INTO `SEO_Module` ( `SEO_ID`, `url_titel` )
    								VALUES(
			    						( SELECT `SEO_ID` FROM `SEO_Module_link` WHERE `website_id`=' . (int)$addPage['id'] . '),
    									?
    								)
			    				ON DUPLICATE KEY UPDATE `url_titel`=VALUES(`url_titel`);');
                        $Q->bindValue(1, $addPage['url_titel'], PDO::PARAM_STR);

                        if ($Q->execute() && $Q->rowCount() > 0):
                            $addPages->ids[] = (int)$addPage['id'];
                        else:
                            $Q = $db->prepare('SELECT `url_titel` FROM `SEO_Module` WHERE `url_titel` LIKE ? ORDER BY `SEO_ID` DESC LIMIT 1;');
                            $Q->bindValue(1, $addPage['url_titel'] . '%', PDO::PARAM_STR);
                            if ($Q->execute()):
                                $page = $Q->fetch(PDO::FETCH_OBJ)->url_titel;
                                $page = str_replace($addPage['url_titel'], '', $page);
                                $page = !empty($page) ? (int)$page + 1 : 1;

                                $Q = $db->prepare('INSERT INTO `SEO_Module` ( `SEO_ID`, `url_titel` )
		    								VALUES(
					    						( SELECT `SEO_ID` FROM `SEO_Module_link` WHERE `website_id`=' . (int)$addPage['id'] . '),
		    									?
		    								)
					    				ON DUPLICATE KEY UPDATE `url_titel`=VALUES(`url_titel`);');
                                $Q->bindValue(1, $addPage['url_titel'] . $page, PDO::PARAM_STR);

                                if (!$Q->execute() || $Q->rowCount() == 0):
                                    EchoDebug('Er is iets misgegaan bij het toevoegen van SEO data. (' . $addPage['id'] . ': ' . $addPage['url_titel'] . ')');
                                else:
                                    $addPages->ids[] = (int)$addPage['id'];
                                endif;
                            endif;


                        endif;

                    endif;

                endforeach;


                if (!empty($addPages->ids))
                    EchoDebug('Nieuwe SEO informatie voor pagina(s): ' . implode(', ', $addPages->ids));

            endif;
        endif;
    } catch (PDOException $ex) {
        die($ex->getCode() . ': ' . $ex->getMessage());
    }

    return true;


}

function SEF($url_titel)
{
    return str_replace('.html', '', $url_titel);
}

function SEO_strip_html($Strip)
{
    return str_replace('.html', '', $Strip);
}

// kijk of seo tabel is ingevuld..
Update_Database_With_SEF();

// eventuele .html codes strippen van GET
$GET = array_map("SEO_strip_html", $GET);

function GetSEOURL($website_id)
{
    // Deze functie return-ed de url_titel ZONDER .HTML van de opgegeven website_id.
    $Q = 'SELECT `seo`.`url_titel` FROM `SEO_Module` `seo`
			LEFT JOIN `SEO_Module_link` `sl`
				ON `seo`.`SEO_ID`=`sl`.`SEO_ID`
			WHERE `sl`.`website_id`=?;';

    try {
        $Q = $db->prepare($Q);
        $Q->bindValue(1, $website_id, PDO::PARAM_INT);
        if ($Q->execute() && $Q->rowCount() == 1)
            $Q = $Q->fetch(PDO::FETCH_OBJ);
        return $Q->url_titel;

    } catch (PDOException $ex) {
        die($ex->getMessage());
    }

}

function GetIDFromURLTitle($url_titel, $object) {
    global $db;

    $url_titel = explode('?', $url_titel)[0];

    try {
        $Q = $db->prepare('SELECT `l`.`tabelnaam`, `l`.`tabelid`
              FROM `SEO_Module_link` `l`
              WHERE `l`.`SEO_ID` = (
                  SELECT `s`.`SEO_ID`
                  FROM `SEO_Module` `s`
                  WHERE `s`.`url_titel` = :titel
                  LIMIT 1
              )');
        $Q->bindValue(':titel', $url_titel, PDO::PARAM_STR);
        if($Q->execute() && $Q->rowCount() == 1) {
            return $object ? ($Q->fetch(PDO::FETCH_OBJ)) : ($Q->fetch(PDO::FETCH_OBJ))->tabelid;
        } else {
            return $url_titel;
        }
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

function GetPageId($object = false) {
    $rawSlugParts = explode("/", $_SERVER['REQUEST_URI']);
    $ID = array_filter($rawSlugParts, function ($item) { return is_numeric($item);});
    if($ID !== null && count($ID) > 0) {
        //ID in query, save first value
        $ID = array_values($ID)[0];;
        // Return ID
        return intval($ID);
    } else if(count($rawSlugParts) > 1) {
        //No ID found, try with slug
        return GetIDFromURLTitle(end($rawSlugParts), $object);
    }
    return null;
}

function GetSEOSlug($id, $table) {
    if(!empty($id) && !empty($table) && is_numeric($id)) {
        global $db;

        try {
            $Q = $db->prepare("
                SELECT `url_titel`
                FROM `SEO_Module`
                WHERE `SEO_ID` = (SELECT `SEO_ID` FROM `SEO_Module_link` WHERE `tabelnaam` = :name AND `tabelid` = :id LIMIT 1);
            ");
            $Q->bindValue(":name", $table, PDO::PARAM_STR);
            $Q->bindValue(":id", $id, PDO::PARAM_INT);

            if($Q->execute() && $Q->rowCount() == 1) {
                return ($Q->fetch(PDO::FETCH_OBJ))->url_titel;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    return null;
}

?>
