<?php

	require_once('../dbconnection.php');
	require_once('class.query.php');
	require_once( 'product.php');
	require_once('subclass.php');

	
	$categorie = new listmemberInfo( 'categorie');
	$filter = new listmemberInfo( 'filtering');
	$ingredient = new listmemberInfo( 'ingredienten' );
	
	$catAr = $categorie->get_by_id( 2 );
	
	$product = new product( $db, 'producten' );
	if( $pr = $product->get_all( ) ) echo 'mooi.<br />';
	else echo 'error: '. $product->log_get() . '<br />';
	
	printAr( $catAr );
	printAr( $pr );
	
	function printAr( $ar ) { 
		foreach( $ar AS $key => $val ) {
			if( is_numeric( $key ) ) {
				echo '<hr /><p>---<strong> '. $key . ' </strong>-----<br />';
				foreach( $val AS $o => $p ) {
					echo '<i>-  '. $o .'</i>  : ';
					if( is_array( $p ) ) {
						foreach( $p AS $oo => $pp ) {
							echo '>>'. $oo . ' :  '. $pp . '<br />';
							
							if( is_array( $pp ) ) {
								foreach( $pp AS $ppp ) {
									echo '<small>'.$ppp.'</small><br />';
								}
							}
						
						}
					} else {
						echo ' '. $p . '<br />';
						
					}
				}
				echo '</p><hr />';
			} else {
				echo '<p>' . $key . ': ' . $val . '<br />';
			}
		}
	}
