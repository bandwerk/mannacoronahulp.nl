<?php
class cart extends query {
	protected 
		$allowed = [ 'shop_winkelwagen' ],
		$cart,
		$bijproduct,
		$size,
		$cartPrijs,
		$cartPrijsExc,
		$prijsuitzondering_provincie_id = false,
		$session;
	
	public 
		$kortingsCodes = false,
		$totaal,
		$korting,
		$bezorging = false,
		$bezorglokatie = null,
		$toeslag = false;	
	
	function __construct( PDO $pdo=null, $table='shop_winkelwagen', $sessionData = '') {
		if( !$pdo ) {
			global $db;
			if( $db instanceof PDO ) {
				$pdo = $db;
				
			} else {
				die( 'Error: Cart needs database connection' );
			}
		}
		parent::__construct($pdo, $table);
		$this->totaal = new stdClass();
		$this->set_prijsuitzondering_provincie_id(
				isset( $_SESSION['postcode_controle'] ) && isset( $_SESSION['postcode_controle']->prijsuitzondering_provincie_id ) ? 
					$_SESSION['postcode_controle']->prijsuitzondering_provincie_id : false 
			);
		$this->set_user( $sessionData );
		
		
		//Controleren of er actieve kortingscodes bestaan
		$Q = 'SELECT count(`id`) AS `aantal` FROM `kortingscodes`
				WHERE
			    	`aktief`="ja" AND
			    	`aantal_keer_te_gebruiken` > 0 AND
			        CURDATE() BETWEEN `begindatum` AND `einddatum`;';
		
		$this->check( $Q . 'SELECT' );
		if( $KC = $this->query( $Q ) ) {
			if( $KC[0]->aantal > 0 ) $this->kortingsCodes = true;
			
		}
		
		$this->get_cart();
	}
	
	function set_user( $sessionData = '' ) {
		$this->session = new stdClass();
		$this->session =  $sessionData;
		$this->session->user['id'] = !empty( $sessionData ) || !empty( $sessionData->user['id'] ) ? $sessionData->user['id'] : 0;
	
		
	}
	
	function set_prijsuitzondering_provincie( string $prijsuitzondering_provincie ) {
		if( $prijsuitzondering_provincie = get_model( 'product/provincie_prijsuitzondering', 'basic', ['where'=>$prijsuitzondering_provincie,'returnAsArray'=>false] ) ) {
			return $this->set_prijsuitzondering_provincie_id( $prijsuitzondering_provincie->provincie_id );
		}
		
		return false;
	}
	
	function set_prijsuitzondering_provincie_id( int $prijsuitzondering_provincie_id ) {
		//Controleren of er actieve kortingscodes bestaan
		$Q = 'SELECT DISTINCT
					`ppro`.`provincie_id`,
					count( `p`.`id` ) AS `aantal`
				FROM 
					`producten_prijs_provincie` `ppro`
					
					LEFT JOIN `product` `p`
						ON `ppro`.`provincie_id`=`p`.`id` AND `p`.`aktief`="ja"
				WHERE `ppro`.`provincie_id`='. (int) $prijsuitzondering_provincie_id .'
				HAVING
					`aantal`>0;';
			        
		$this->check( $Q . 'SELECT' );
		if( $KC = $this->query( $Q ) ) {
			$this->prijsuitzondering_provincie_id = (int) $KC[0]->provincie_id;
		} else {
			$this->prijsuitzondering_provincie_id = false;
		}
		
		return $this->prijsuitzondering_provincie_id;
	}
	
	function modify_cart( array $cart ) {
		$this->cart = [];
		
		foreach( $cart AS $pId => $pQua ) {
			$pId = $this->check( $pId );
			$pQua = $this->check( $pQua );
			$this->cart[ $pId ] = $pQua;
		}
		
		return true;
		
		$this->sync();
	}
	
	private function check_int( $input ) {
		if( is_numeric( $input ) ) return (int) $input;
		else return false;
	}
	
	function product_add_more( $ar ) {
		if( !is_array( $ar ) ) return false;
		
		foreach( $ar AS $pid => $amount ) {
			$id = $this->check_int( $pid );
			$amount = $this->check_int( $amount );
			
			$this->cart[ $id ] = isset( $this->cart[$id] ) ? $this->cart[$id] + $amount : $amount;
			if( $this->cart[ $id ] < 1 ) $this->cart[ $id ] == 0;
				
		}

		return $this->sync();
		
	}
	
	function product_add( $id, $amount = 1, $bijproduct = null ) {
		
		$id = $this->check_int( $id );
		$amount = $this->check_int( $amount );
		
		if( isset( $bijproduct ) && $bijproduct = (int) $this->check_int( $bijproduct ) ) {
			if( !isset( $this->bijproduct ) ) $this->bijproduct = [];
			if( !isset( $this->bijproduct[$id] ) ) $this->bijproduct[$id] = [];
			
			$this->bijproduct[ $id ][ $bijproduct ] = isset( $this->bijproduct[$id][ $bijproduct ] ) ?
				(int) ( $this->bijproduct[ $id ][ $bijproduct ] + $amount )
					:
				(int) $amount;
		}
		
		$this->cart[ $id ] = isset( $this->cart[$id] ) && is_numeric( $this->cart[ $id ] ) ? (int) $this->cart[$id] + $amount : (int) $amount;
		if( $this->cart[ $id ] < 1 ) $this->cart[ $id ] == 0;
		
		return $this->sync();
		
	}
	
	function product_size( $id, $size ) {
		$id = $this->check_int( $id );
		$size = is_string( $size ) ? trim( $size ) : false;
		
		if( $size ) {
			
		$this->size[ $id ] = $size;

		}
	}
	
	function product_remove( $id, $amount = 1, $bijproduct = null ) {
		$id = $this->check_int( $id );
		$amount = ( $amount === 'all' ) ? 0 : $this->check_int( $amount );
		if( isset( $bijproduct ) && $bijproduct = $this->check_int( $bijproduct ) ) {
			$current = isset( $this->bijproduct[ $id ][ $bijproduct ] ) ? $this->bijproduct[ $id ][ $bijproduct ] : 0;
			
			if( !isset( $this->bijproduct[ $id ][ $bijproduct ] ) || $this->bijproduct[ $id ][ $bijproduct ] < 1 || $amount == 0 ) $this->bijproduct[ $id ][ $bijproduct ] = 0;
			else $this->bijproduct[ $id ][ $bijproduct ] -= $amount;
			$amount = $current - $this->bijproduct[ $id ][ $bijproduct ];
		}
		

		if( !isset( $this->cart[$id] ) || $this->cart[ $id ] < 1 || $amount == 0 ) $this->cart[ $id ] = 0;
		else $this->cart[ $id ] -= $amount;

		return $this->sync();
	}
	
	function product_set( $id, $amount, $bijproduct = null ) {
		$amount = $this->check_int( $amount );
		if( isset( $bijproduct ) && $bijproduct = (int) $this->check_int( $bijproduct ) ) {
			if( !isset( $this->bijproduct ) ) $this->bijproduct = [];
			if( !isset( $this->bijproduct[$id] ) ) $this->bijproduct[$id] = [];
				
			$this->bijproduct[ $id ][ $bijproduct ] = $amount;
			$amount = array_sum( array_values( $this->bijproduct[ $id ] ) );
		}
		
		$this->cart[ $this->check_int( $id ) ] = $this->check_int( $amount );

		
		return $this->sync();
	}
	
	function product_in_cart( $id, $bijproduct=null ) {
		if( $this->sync() ):
			if( isset( $bijproduct ) && is_numeric( $bijproduct ) )
				$return = is_int( $id ) && array_key_exists( $id, $this->bijproduct ) && is_int( $bijproduct ) && array_key_exists( $bijproduct, $this->bijproduct[ $id ] ) ? $this->bijproduct[ $id ][ $bijproduct ] : false;
			else
				$return = is_int( $id ) && array_key_exists( $id, $this->cart ) ? $this->cart[ $id ] : false;
			return $return;
		endif;
	}
	
	
	
	function sync() {
		//Winkelwagen vanuit object in variabel om zo manipulatie alleen in de sync plaats te laten vinden
		$cart=$this->cart;

		//tijd voor db
		$now = strftime( '%Y-%m-%d %H:%M:%S', time() );
		
		
		
		/*
		 * Beginnen met bijproducten om zo dubbeling in db tegen te gaan
		 */
		//Query beginnen voor bijproducten
		$query = 'INSERT INTO `'. $this->table[0] .'` ( `categorie`,
						`artikel_id`,
						`bijproduct_id`,
						`aantal`,
						`us_gebruikers_id`,
						`personalnr`,
						`datum`,
						`updated` ) VALUES ';
		
		
		$values = [];
		
		if( isset( $this->bijproduct ) && is_array( $this->bijproduct ) ){
				$bpKeys = array_keys( $this->bijproduct );

				if( isset( $bpKeys ) && count( $bpKeys ) > 0 ) {
					// Bijproducten in winkelwagen toevoegen aan database in 1 query;
					foreach( $this->bijproduct AS $productId => $bijProduct ) {
						foreach( $bijProduct AS $bijProductId => $amount ){
							// Aantal nooit minder dan 0
							$amount = $amount < 0 ? 0 : (int) $amount;
				
							// Array aanmaken voor values
							$add = [];
							// Categorie aangeven
							$add[] = '"bijproduct"';
							// Id van het product waar het bijproduct bij past
							$add[] = (int) $productId;
							// Id van het bijproduct
							$add[] = (int) $bijProductId;
							//Aantal toevoegen
							$add[] = ' CASE
										( SELECT count( `minimaal_aantal` ) AS `aantal` FROM `product` WHERE `id`='. (int) $productId . '
												AND ( `minimaal_aantal` <= '. $amount . ' OR '. (int) $amount . ' = 0 ) )
									WHEN 1 THEN '. $amount . '
									ELSE ( SELECT `minimaal_aantal` AS `aantal` FROM `product` WHERE `id`='. (int) $productId . ')
								END';
							//Gebruikers id doorgeven
							$add[] = $this->session->user['id'];
							//Sessie id doorgeven
							$add[] = '"' . $this->session->id . '"';
							// Datum instellen
							$add[] = 'NOW()';
							// Update tijd instellen
							$add[] = 'NOW()';
								
							$values[] = '( '. implode( ', ', $add ) . ')';
						}
					}
				}
					
			$query .= implode( ', ', $values ) . '
					ON DUPLICATE KEY UPDATE `aantal`=VALUES(`aantal`), `updated`="'. $now .'";';
		
			$this->check( $query . 'INSERT');

			$update = $this->set( $query );
		}

		// Producten in winkelwagen toevoegen aan database in 1 query;
		$query = 'INSERT INTO `'. $this->table[0] .'` ( `categorie`,
						`artikel_id`,
						`aantal`,
						`maat`,
						`us_gebruikers_id`,
						`personalnr`,
						`datum`,
						`updated` ) VALUES ';
		
		//array waarin alle rijen worden toegevoegd
		$values = [];
		
		//inhoud producten winkelwagen verwerken
		foreach( $cart AS $pId => $amount ) {
			//kijken of er een maat is aangegeven
			$maat = isset( $this->size[ $pId ] ) ? '"' . $this->size[ $pId ] . '"' : 'NULL';
			//Minder dan 0 producten is uiteraard onmogelijk
			$amount = $amount < 0 ? 0 : (int) $amount;

			
				//Array aanmaken waarin de kolommen worden meegegeven
				$add = [];
				//categorie aangeven
				$add[] = '"product"';
				//artikelid aangeven
				$add[] = (int) $pId;
				//Aantal toevoegen
				$add[] = ' CASE
							( SELECT count( `minimaal_aantal` ) AS `aantal` FROM `product` WHERE `id`='. (int) $pId . '
									AND ( `minimaal_aantal` <= '. $amount . ' OR '. (int) $amount . ' = 0 ) )
						WHEN 1 THEN '. $amount . '
						ELSE ( SELECT `minimaal_aantal` AS `aantal` FROM `product` WHERE `id`='. (int) $pId . ')
					END';
				//Maat toevoegen
				$add[] = $maat;
				//Id van de gebruiker
				$add[] = $this->session->user['id'];
				//Id van de sessie
				$add[] = '"' . $this->session->id . '"';
				//datum
				$add[] = 'NOW()';
				//update tijd
				$add[] = 'NOW()';
			
			// Kolommen toevoegen aan rijen voor query
			$values[] = '( '. implode( ', ', $add ) . ')';
			
		}

		
		//rijen toevoegen aan de query en aangeven welke kolommen ge�pdate moeten worden als de rij al bestaat
		$query .= implode( ', ', $values ) . '
				ON DUPLICATE KEY UPDATE `aantal`=VALUES(`aantal`), `maat`=VALUES(`maat`), `updated`=NOW();';
		
		
		//Beveiligingscheck
		$this->check( $query . 'INSERT');
		
		//Query uitvoeren
		if( $this->set( $query ) ) {
			if( !isset( $update ) ) {
				$update =0;
			}
			$update++;
		}
		
		if( $update > 0 ) {
			$query = 'DELETE FROM
						`'. $this->table[0] .'`
					WHERE ( `aantal`=0 AND `categorie`="product" )
						OR `artikel_id` IN (
								( SELECT  `id` FROM `product` WHERE `aktief`="nee" OR `uitverkocht`="ja" ) );';
			
			$this->check( $query . 'DELETE');
			$this->query( $query, 'DELETE' );
			
			if( $this->update_cart() ) {
				if( $this->bezorglokatie ) {
					$Q = 'SELECT `id` FROM `producten_adressen`
							WHERE `postcode`="'.$this->bezorglokatie.'";';
					$this->check( $Q.'SELECT');
					$R = $this->query( $Q, 'SELECT' );
					if( is_array( $R ) ) {
						$plaats_id = is_object( $R[0] ) ? $R[0]->id : $R[0]['id'];
						return $this->set_bezorging( $plaats_id );
					} 
				} 
				return $this->get_cart();
			}
		}
	}
	
	function empty_cart( $sessionData, bool $keepSession=false ) {
		$Q = 'UPDATE `shop_winkelwagen` SET `besteld`=1 WHERE `personalnr`="'. $sessionData->id.'";';
		$this->check( $Q . 'UPDATE' );
		if( $this->query( $Q, 'UPDATE') ) {
			if( !$keepSession ) {
				session_regenerate_id( );
			}
			if( $this->update_cart() ) return true;
		}
		
		 return false;
	}
	
	function calc_korting( $input, $retour = 'korting' ) {
		$input = $input;
		$output = $input;
		
		if( isset( $this->korting ) && is_array( $this->korting ) ) {
			switch( array_keys( $this->korting )[0] ) {
				case 'procent':
					$output = round( $input - ( ( $input / 100 ) * number_format( $this->korting['procent'], 2 ) ), 2 );
					break;
				case 'euro':
					$output -= number_format( $this->korting['euro'], 2 );
					break;
			}

		}

		switch( $retour ) {
			case 'korting':
					return $input - $output;
				break;
			case 'totaal':
					return $output;
				break;
		}
		
	}
	
	function set_bezorging( $plaats_id = null ) {
		if( !is_numeric( $plaats_id ) ) {
			$Q = 'SELECT `id` FROM `producten_adressen` WHERE `postcode`=? LIMIT 1;';
			$this->check( $Q.'SELECT');
			$plaats_id = $this->query( $Q, 'SELECT');
			if( is_array( $plaats_id ) && count( $plaats_id ) > 0 ) {
				$plaats_id = (int) $plaats_id[0]['id'];
			}
		}
		if( is_numeric( $plaats_id ) ) {
				$cart = $this->get_cart( 'amount' );
				$amount = $cart[1];

				if( isset( $amount ) && is_object( $amount ) ) $amount = $amount->inc;
			
					$Q = 'INSERT INTO `'. $this->table[0] .'` ( `categorie`,
								`bezorging_id`,
								`aantal`,
								`us_gebruikers_id`,
								`personalnr`,
								`datum`,
								`updated` ) VALUES
							(
								"bezorging",
								'.   $plaats_id . ',
									( SELECT
										CASE
											WHEN "'. $amount .'"<`bezorging_gratis_vanaf`
												OR `bezorging_gratis_vanaf`=0

												THEN `bezorgkosten`
											ELSE 0
										END AS `aantal` FROM `producten_adressen`
										WHERE `id`='. $plaats_id.' LIMIT 1 ),
								'.   $this->session->user['id'] .',
								"'.  $this->session->id . '",
								NOW(),
								NOW()
							) ON DUPLICATE KEY UPDATE `aantal`=VALUES(`aantal`), `updated`=VALUES(`updated`), `bezorging_id`=VALUES(`bezorging_id`);';
		
				$check = 'INSERT';
				
		} else {
			$Q = 'DELETE FROM `'. $this->table[0] .'` WHERE `personalnr`="'. $this->session->id.'" AND `categorie`="bezorging" LIMIT 1;';
			$check = 'DELETE';
			
		}

		$this->check( $Q . $check );
		if( $this->query( $Q, $check ) )
			return $this->update_cart();


	}
	
	function korting_remove( $sessionData=null) {
		$personalnr = !empty( $sessionData ) ? trim( $sessionData->id ) : session_id();
		$Q = 'DELETE FROM `'. $this->table[0] .'` WHERE `categorie`="korting" AND `personalnr`="'. $personalnr .'";';
		
		$this->check( $Q . 'DELETE' );
		if( $this->query( $Q, 'DELETE') ) {
			return $this->update_cart('korting');
		}
	}
	
	function get_korting( $code, $controle = false ) {
			$minimum = ($controle) ? ' AND `minimaal_bestellingbedrag`<="'.  str_replace( ',', '.', $this->cartPrijs[1] ). '"' : '';
					
			
			$Q = 'SELECT `id`, `waarde`, `eenheid`, `minimaal_bestellingbedrag` FROM `kortingscodes`
					 WHERE `aantal_keer_te_gebruiken`>0
						'. $minimum .'
						AND `begindatum`<=DATE(now())
						AND `einddatum`>=DATE(now()) AND `code`="'. trim( $code ) .'" LIMIT 1;';
			$this->check( $Q . 'SELECT' );
			$return = $this->query( $Q, 'SELECT' );
			
			if( count( $return ) > 0 ) {
				
				$return = $return[0];
				
				$now = strftime( '%Y-%m-%d %H:%M:%S', time() );
				$Q = 'INSERT INTO `'. $this->table[0] .'` ( `categorie`,
						`korting_id`,
						`korting_type`,
						`aantal`,
						`us_gebruikers_id`,
						`personalnr`,
						`datum`,
						`updated` ) VALUES
					(
						"korting",
						'.   $return['id'] . ',
						"'.  $return['eenheid'] .'",
						"'.  $return['waarde'] .'",
						'.   $this->session->user['id'] .',
						"'.  $this->session->id . '",
						"' . $now . '",
						"' . $now . '"
					)
						 ON DUPLICATE KEY UPDATE `korting_id`=VALUES(`korting_id`), `korting_type`=VALUES(`korting_type`), `aantal`=VALUES(`aantal`), `updated`="'. $now .'";';
				

				$this->check( $Q . 'INSERT' );
				if( $this->query( $Q, 'INSERT' ) ) {
					if( $this->update_cart( 'korting' ) ) return $return;
				}
			}
			$this->update_cart( 'korting' );
		return false;
	}

	function update_cart( $cat = '') {
		if( empty( $cat ) || $cat == 'bezorging' ):
			$this->bezorging = false;
			$this->bezorglokatie = null;
		endif;
		if( empty( $cat ) || $cat == 'korting' ) $this->korting = 0;
		
		$cat = !empty( $cat ) ?  ' AND `w`.`categorie`="'. str_replace( ' ', '', $cat ) . '"' : '';

		$orId = !empty( $_SESSION['loggedin']['id'] ) && $_SESSION['loggedin']['id']> 0 ?
			' AND `w`.`us_gebruikers_id`='. (int) $_SESSION['loggedin']['id'] : '';
		
		$kortingCase = !empty( $_SESSION['loggedin']['id'] ) && $_SESSION['loggedin']['id']> 0 ?
								'CASE 
									WHEN `uk`.`korting` IS NOT NULL 
											AND
										`uk`.`korting`>0
											THEN
													( 100 - `uk`.`korting` ) / 100 
									ELSE 
											1
								END AS `gebruikerkorting`' :
								'1 AS `gebruikerkorting`';
		
		$joinKorting = !empty( $_SESSION['loggedin']['id'] ) && $_SESSION['loggedin']['id']> 0 ?
			' LEFT JOIN `producten_listmember_categorieen` `pl`
				ON `tp`.`id`=`pl`.`product`
				
			LEFT JOIN `producten_categorie` `pc`
				ON `pl`.`categorie`=`pc`.`id`
				
			LEFT JOIN `producten_categorie` `ppc`
				ON `pc`.`parent`=`ppc`.`id` AND `pc`.`parent` != 0
				
			LEFT JOIN `us_gebruikers_korting` `uk`
				ON ( `pl`.`categorie`=`uk`.`producten_categorie_id`
						OR `pc`.`parent`=`uk`.`producten_categorie_id` 
							OR `ppc`.`parent`=`uk`.`producten_categorie_id`
				)
				 AND `uk`.`us_gebruikers_id`="'. (int) $_SESSION['loggedin']['id'] . '"
				 AND `uk`.`us_gebruikers_id`!=0 AND `uk`.`producten_categorie_id`!=0	

				
			WHERE `tp`.`id` IN (
			 		( SELECT DISTINCT `artikel_id` FROM `shop_winkelwagen`
					 		WHERE ( `personalnr`="'. session_id() .'"'. str_replace( '`w`.', '', $orId ) .' )'. str_replace( '`w`.', '', $cat ) .' ) )
			 		
			GROUP BY `tp`.`id`' : '' ;

		$query = 'SELECT
				`p`.*,
				`w`.*,
				`w`.`artikel_id` AS `id`,
				SUM( `w`.`aantal` ) AS `aantal`,
				round( SUM( `w`.`aantal` ) * `p`.`prijs` * `p`.`gebruikerkorting` * (1+( `p`.`percentage`/100) ), 2 )
		            AS `prijs`,
				round( SUM( `w`.`aantal` ) * `p`.`prijs` * `p`.`gebruikerkorting` , 2 )
		            AS `prijs_exc`,
			
				`kc`.`code`,
				`kc`.`opmerking` AS `korting_titel`,
				`kc`.`minimaal_bestellingbedrag` AS `minimum`,
				`l`.`postcode` AS `bezorgcode`
				FROM `'. $this->table[0] .'` `w`
						LEFT JOIN
							( SELECT 
										`tp`.`id`,
										`tp`.`btw`,
										`tt`.`percentage`,
										"Exclusief" AS `type`,
										( CASE
											WHEN `tppro`.`id` IS NOT NULL THEN
												( CASE `tpprot`.`type`
														WHEN "Inclusief"
															THEN
														 ( CASE
															WHEN `tp`.`prijs_aanbieding` > 0
																THEN `tp`.`prijs_aanbieding`
																	ELSE `tppro`.`prijs_normaal`
														END )
																	-
														( CASE
															WHEN `tp`.`prijs_aanbieding` > 0
																THEN `tp`.`prijs_aanbieding`
																	ELSE `tppro`.`prijs_normaal`
														END ) * tt.percentage / (100+tt.percentage)
													ELSE
														( CASE
															WHEN `tp`.`prijs_aanbieding` > 0
																THEN `tp`.`prijs_aanbieding`
															ELSE `tppro`.`prijs_normaal`
														END )
												END )
											ELSE
												( CASE `tt`.`type`
														WHEN "Inclusief"
															THEN
														 ( CASE
															WHEN `tp`.`prijs_aanbieding` > 0
																THEN `tp`.`prijs_aanbieding`
																	ELSE `tp`.`prijs_normaal`
														END )
																	-
														( CASE
															WHEN `tp`.`prijs_aanbieding` > 0
																THEN `tp`.`prijs_aanbieding`
																	ELSE `tp`.`prijs_normaal`
														END ) * tt.percentage / (100+tt.percentage)
													ELSE
														( CASE
															WHEN `tp`.`prijs_aanbieding` > 0
																THEN `tp`.`prijs_aanbieding`
															ELSE `tp`.`prijs_normaal`
														END )
												END )
										END ) AS `prijs`,
										'. $kortingCase . '
										
									FROM `product` `tp`
										LEFT JOIN `acc_btw` `tt`
											ON `tp`.`btw`=`tt`.`id`
										LEFT JOIN `producten_prijs_provincie` `tppro`
											ON `tp`.`id`=`tppro`.`producten_id` AND `tppro`.`provincie_id`='. (int) $this->prijsuitzondering_provincie_id .'
										LEFT JOIN `acc_btw` `tpprot`
											ON `tppro`.`btw`=`tpprot`.`id`
						

								'. $joinKorting . '
							) `p`
							ON `w`.`artikel_id`=`p`.`id`
										
						LEFT JOIN
							( SELECT
								`id`,
								`btw`,
								CASE
									WHEN `prijs_aanbieding` > 0
								THEN `prijs_aanbieding`
									ELSE `prijs_normaal`
								END AS `prijs`
							FROM `bijproducten`) `b`
								ON `w`.`bijproduct_id`=`b`.`id`
						LEFT JOIN
							( SELECT
								`id`,
								`bezorgkosten` AS `prijs`,
								`postcode`
							FROM `producten_adressen`) `l` ON
							`w`.`bezorging_id`=`l`.`id`
						LEFT JOIN `kortingscodes` `kc` ON `w`.`korting_id`=`kc`.`id`
						LEFT JOIN `acc_btw` `t`
						 	ON
								CASE
									WHEN `w`.`bijproduct_id` > 0 THEN `t`.`id`=`b`.`btw`
									ELSE `t`.`id`=`p`.`btw`
								END


				WHERE ( `w`.`personalnr`="'. session_id() .'"'. $orId .' )
						'. $cat .'
				AND (
						( SELECT
							count( `id` )
						FROM `product`
						WHERE `id`=`w`.`artikel_id`
							AND `uitverkocht`="nee"
							AND `aktief`="ja"
						) > 0
							OR
						`w`.`categorie`!="product"
					) AND `besteld`="0"
				GROUP BY `w`.`artikel_id`, `w`.`bijproduct_id`, `w`.`korting_id`
				ORDER BY
					`w`.`categorie` DESC,
					`w`.`artikel_id` ASC,
					`prijs` ASC;';

		$this->check( $query . 'SELECT' );

		$cart = $this->query( $query );

		if( !empty( $cart ) ) {
			if( !is_array( $cart ) && !is_object( $cart ) )
				die( $cart );
			
			
			if( empty( $cat ) || in_array( $cat, ['product', 'bijproduct'] ) ) {
				$this->cart = [];
				$this->cartPrijs = [];
				$this->cartPrijsExc = [];
				$this->bijproduct = [];
			}
			
		
			foreach(  $cart AS $item ) {
				if( is_array( $item ) ){
					switch( $item['categorie'] ) {
						case 'product':
							$this->cart[$item['id']] = $item['aantal'];
							$this->cartPrijs[ $item['id'] ] = isset( $this->cartPrijs[ $item['id'] ] ) ? $this->cartPrijs[ $item['id'] ] + $item['prijs'] : $item['prijs'];
							$this->cartPrijsExc[ $item['id'] ] = isset( $this->cartPrijsExc[ $item['id'] ] ) ? $this->cartPrijsExc[ $item['id'] ] + $item['prijs_exc'] : $item['prijs_exc'];
							
							if( $item['maat'] ) 
								$this->size[$item['id']] = $item['maat'];
							
							if( ( isset( $item['prijs_toeslag'] ) || isset( $item['prijs_toeslag_exc'] ) ) && !is_object( $this->toeslag ) ):
								$this->toeslag = new stdClass();
								$this->toeslag->inc = 0;
								$this->toeslag->exc = 0;
							endif;
								
							if( isset( $this->toeslag->exc ) && isset( $item['prijs_toeslag_exc'] ) && $item['prijs_toeslag_exc'] > $this->toeslag->exc )
								$this->toeslag->exc = number_format( $item['prijs_toeslag_exc'] , 2, '.', '' );
							
							if( isset( $this->toeslag->inc ) && isset( $item['prijs_toeslag'] ) && $item['prijs_toeslag'] > $this->toeslag->inc )
								$this->toeslag->inc = number_format( $item['prijs_toeslag'], 2, '.', '' );
										
									
							break;
						case 'korting':
							$this->korting = [ $item['korting_type'] => $item['aantal'] , 'id' => $item['korting_id'], 'titel' => $item['korting_titel'], 'minimum' => $item['minimum'], 'code' => $item['code'] ];
							
							break;
						case 'bezorging':
							$this->bezorging = number_format( $item['aantal'], '2', '.', '' );
							$this->bezorglokatie = $item['bezorgcode'];
							break;
						case 'bijproduct':
							if( !isset( $this->bijproduct[ $item['id'] ] ) ) $this->bijproduct[ $item['id'] ] = [];
							$this->cartPrijs[ $item['id'] ] = isset( $this->cartPrijs[ $item['id'] ] ) ? $this->cartPrijs[ $item['id'] ] + $item['prijs'] : $item['prijs'];
							$this->cartPrijsExc[ $item['id'] ] = isset( $this->cartPrijsExc[ $item['id'] ] ) ? $this->cartPrijsExc[ $item['id'] ] + $item['prijs_exc'] : $item['prijs_exc'];
							$this->bijproduct[ $item['id'] ][ $item['bijproduct_id'] ] = $item['aantal'];

							break;
					}
				} elseif( is_object( $item )) {
					switch( $item->categorie ) {
						case 'product':
							$this->cart[$item->id ] = $item->aantal;
							$this->cartPrijs[ $item->id ] = isset( $this->cartPrijs[ $item->id ] ) ? $this->cartPrijs[ $item->id ] + $item->prijs : $item->prijs;
							$this->cartPrijsExc[ $item->id ] = isset( $this->cartPrijsExc[ $item->id ] ) ? $this->cartPrijsExc[ $item->id ] + $item->prijs_exc : $item->prijs_exc;
							if( $item->maat ) $this->size[$item->id] = $item->maat;
							
							if( ( isset( $item->prijs_toeslag ) || isset( $item->prijs_toeslag_exc ) ) && !is_object( $this->toeslag ) ):
								$this->toeslag = new stdClass();
								$this->toeslag->inc = 0;
								$this->toeslag->exc = 0;
							endif;

							if( isset( $this->toeslag->exc ) && isset( $item->prijs_toeslag_exc ) && $item->prijs_toeslag_exc > $this->toeslag->exc )
								$this->toeslag->exc = number_format( $item->prijs_toeslag_exc, 2, '.', '' );

							if( isset( $this->toeslag->inc ) && isset( $item->prijs_toeslag_exc ) && $item->prijs_toeslag > $this->toeslag->inc )
								$this->toeslag->inc = number_format( $item->prijs_toeslag, 2, '.', '' );
							
							
							break;
						case 'korting':
							$this->korting = [ $item->korting_type => $item->aantal , 'id' => $item->korting_id, 'titel' => $item->korting_titel, 'minimum' => $item->minimum, 'code' => $item->code ];
							
							break;
						case 'bezorging':
							$this->bezorging = number_format( $item->aantal, '2', '.', '' );
							$this->bezorglokatie = $item->bezorgcode;
							break;
						case 'bijproduct':
							if( !isset( $this->bijproduct[ $item->id ] ) ) $this->bijproduct[ $item->id ] = [];
							$this->cartPrijs[ $item->id ] = isset( $this->cartPrijs[ $item->id ] ) ? $this->cartPrijs[ $item->id ] + $item->prijs : $item->prijs;
							$this->cartPrijsExc[ $item->id ] = isset( $this->cartPrijsExc[ $item->id ] ) ? $this->cartPrijsExc[ $item->id ] + $item->prijs_exc : $item->prijs_exc;
							$this->bijproduct[ $item->id ][ $item->bijproduct_id ] = $item->aantal;
							
							break;
				
					}
				}
			}
			
			
			// Toeslag weghalen als er niet bezorgt wordt
			if( !isset( $this->bezorglokatie ) || empty( $this->bezorglokatie ) ) {
				$this->toeslag = false;
			} 
			
			// Bezorgkosten weghalen als er toeslag geheven wordt.
			if( isset( $this->toeslag ) && is_object( $this->toeslag ) ):
				$this->bezorgingBedrag = $this->bezorging;
				$this->bezorging = 0;
			else:
				if( isset( $this->bezorgingBedrag ) ) $this->bezorging = $this->bezorgingBedrag;
			endif;
		}
		
		return true;
	}
	
	function get_cart( $value = null, $cat = '' ) {
		$cSwitch = str_replace( ' ', '', $cat );
		
		
		if( !isset( $this->cart ) || !is_array( $this->cart ) || empty( $this->cart ) || count( $this->cart ) == 0 ) {
			$this->update_cart();
		}
		
		$this->totaal->aantal = is_array( $this->cart ) ? array_sum( $this->cart ) : 0;
		$this->totaal->bedrag = is_array( $this->cartPrijs ) ? new stdClass() : 0 ;
		if( is_object( $this->totaal->bedrag ) ):
			$this->totaal->bedrag->inc = number_format( array_sum( $this->cartPrijs ), '2', '.', '' );
			$this->totaal->bedrag->exc = number_format( array_sum( $this->cartPrijsExc ), '2', '.', '' );
		endif;
		
		switch( $value ) {
			case 'amount':
			case 'totaal':
			case 'aantal':
			case 'overzicht':
				if( is_array( $this->cart) && !empty( $this->cart ) ) {
					if( is_array( $this->korting ) && isset( $this->korting['titel'] ) ) {
							$korting = $this->calc_korting( $this->totaal->bedrag );
							$this->totaal->korting = $korting > $this->totaal->bedrag ? $this->totaal->bedrag : $korting;

							
					} else $this->totaal->korting = 0;
					
					return [
							$this->totaal->aantal,
							$this->totaal->bedrag,
							$this->bezorging,
							$this->totaal->korting,
							$this->korting,
							$this->toeslag
					];
				} else return [0,0,0,0,0,0];
				break;
			case 'bezorging':
				return $this->bezorging;
			case 'korting':
				return $this->korting;
			case 'lengtetoeslag':
			case 'toeslag':
				return $this->toeslag;
			case 'bijproduct':
			case 'bijproducten':
				return $this->bijproduct;
			default:
				switch( $cSwitch ) {
					case 'bezorging':
						return $this->bezorging;
					case 'korting':
						return $this->korting;
					case 'bijproduct':
					case 'bijproducten':
						return $this->bijproduct;
					default:
						return $this->cart;
				}
				break;
		}
	}
	
	function hash( $id, $session ) {
		if( !isset( $session->id ) && is_callable( [ $session, 'get_data'] ) ) $session = $session->get_data();
		$hash = isset( $session->id ) ? md5( $id . $session->id ) : false;
		if( $hash ) $hash = substr( $hash, 0, 11 ) . md5( $id ) . substr( $hash, 11 );
		return $hash;
	}
	
}