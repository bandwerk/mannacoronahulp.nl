<?php

class CoronaMail {
    private $recipient, $email,
        $subject, $content;

    private $sender = "info@mannacoronahulp.nl";
    private $organization = "Manna Coronahulp";

    function __construct($recipient, $email, $subject, $content) {
        $this->recipient = $recipient;
        $this->email = $email;
        $this->subject = $subject;
        $this->content = $content;
    }

    public function Send() {
        $template = file_get_contents(__INC__ . "/email.htm");

        ////Content dingen voor mail
        $titel = $onderwerp = $this->subject;
        $content = $this->content;
        $logo = __IMAGES__ . "/email/logo-coronahulp.jpg";
        $sfeer = __IMAGES__ . "/email/sfeer.jpg";
        $aanhef = "Beste " . $this->recipient . ",";
        $Vervang = [
            '{titel}' => $titel,
            '{afbeelding}' => $logo,
            '{sfeerafbeelding}' => $sfeer,
            '{aanhef}' => $aanhef,
            '{content}' => $content,
            '{steunkleur}' => "#0E306E",
            '{email}' => CONTACT_EMAIL,
            '{adres}' => CONTACT_ADRES,
            '{postcode}' => CONTACT_POSTCODE,
            '{plaats}' => CONTACT_PLAATS,
        ];

        foreach ($Vervang as $find => $replace) {
            $template = str_ireplace($find, $replace, $template);
        }
        $mail = new PHPMailer(true);


        $useMailServer = System::checkEmailDns($this->email);
        $mail->isSMTP();
        ///TODO juiste gegevens!!!
        try {
            if ($useMailServer == "outlook") {
                $mail->Host = 'smtp.outlook.com';                            // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                                 // Enable SMTP authentication
                $mail->Username = $this->sender;                        // SMTP username
                $mail->Password = 'Rod94468';
                $mail->SMTPSecure = "tls";
                $mail->Port = 587;

            } else {
                $mail->Host = 'mail.mdns.nl';                            // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                                 // Enable SMTP authentication
                $mail->Username = 'send@hwid2.nl';                        // SMTP username
                $mail->Password = '8A5gvd$4';                            // SMTP password
                $mail->Port = 587;

            }                                          // TCP port to connect to

            $mail->setFrom($this->sender, $this->organization);
            $mail->addAddress($this->email);                         // Add a recipient
            $mail->addReplyTo($this->sender);
            $mail->isHTML(true);                                    // Set email format to HTML

            $mail->Subject = $onderwerp;
            $mail->Body = $template;
            $mail->CharSet = 'UTF-8';
            $res = $mail->Send();

            $message = "Mail sent for " . $this->email . " from email service " . $useMailServer;
            System::writeToLog($message, 'SUCCESS', 'mail');
            return $res;
        } catch (phpmailerException $e) {
            echo "FAILED";
            $message2 = $e->errorMessage();
            System::writeToLog($message2, 'ERROR', 'mail');
            $message = "Mail not sent for  " . $this->email . " from email service " . $useMailServer . ". Trying different server...";
            System::writeToLog($message, 'ERROR', 'mail');

            try {

                if ($useMailServer == "outlook") {

                    $mail->Host = 'mail.mdns.nl';                            // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                                 // Enable SMTP authentication
                    $mail->Username = $this->sender;                        // SMTP username
                    $mail->Password = 'Rod94468';                            // SMTP password
                    $mail->Port = 587;
                    $fallback = "mdns";
                } else {
                    $mail->Host = 'smtp.outlook.com';                            // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                                 // Enable SMTP authentication
                    $mail->Username = $this->sender;                        // SMTP username
                    $mail->Password = 'Xav59226';                            // SMTP password
                    $mail->Port = 587;
                    $fallback = "outlook";

                }
                $mail->setFrom($this->sender, $this->organization);
                $mail->addAddress($this->email);                         // Add a recipient
                $mail->addReplyTo($this->sender);
                $mail->isHTML(true);                                    // Set email format to HTML

                $mail->Subject = $onderwerp;
                $mail->Body = $template;
                $mail->CharSet = 'UTF-8';

                $res = $mail->Send();

                $message = "Mail sent for " . $this->email . " from email service " . $fallback . " as fallback";
                System::writeToLog($message, 'SUCCESS', 'mail');

                return $res;
            } catch (phpmailerException $e) {
                echo "ALL FAILED";
                $message2 = $e->errorMessage();
                System::writeToLog($message2, 'ERROR', 'mail');
                $message = "Mail not sent for  " . $this->email . " from email service " . $fallback . "";
                System::writeToLog($message, 'ERROR', 'mail');
                return false;
            }
        }
    }
}