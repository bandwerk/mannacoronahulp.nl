<?php

class HtmGen {
	public static function alt( $string, $altonly=true ) {
		$string = htmlentities( trim( $string ) );
		$return = $altonly ? ' alt="'. $string .'"' : ' alt="'. $string .'" title="'. $string .'"';
		
		return $return;
		
	}
	
	public static function cart_js( $id, $hash, $alt="", $event = '' ) {
		$extra = '';
		
		if( is_array( $id ) ):
			if( isset( $id['bijproduct'] ) && count( $id['bijproduct'] ) > 0 && !empty( $id['bijproduct'][0] ) ) $extra .= ' data-product-bijproduct="true"';
			if( isset( $id['bijproduct'] ) && isset( $id['bijproduct']['id'] ) ) $extra .= ' data-vervolgkeuze="'. (int) $id['bijproduct']['id'] . '"';
			$id = (int) $id['id'];
		
		elseif( is_object( $id ) ):
			if( isset( $id->vervolgkeuze ) && count( $id->vervolgkeuze ) > 0 && !empty( $id->vervolgkeuze[0] ) ) $extra .= ' data-product-bijproduct="true"';
			if( isset( $id->vervolgkeuze ) && isset( $id->vervolgkeuze->id ) ) $extra .= ' data-vervolgkeuze="'. (int) $id->vervolgkeuze->id . '"';
			$id = (int) $id->id;
		
		else:
			$id = (int) $id;
		endif;
		
		
		$event = !empty( $event ) ? str_replace( ' ', '', $event ) : 'add_product';
		$return = ' bt-event-type="'. $event .'" data-artikel-id="'. $id .'" data-product-check="'. $hash .'"' . $extra;
		if( isset( $alt ) && !empty( $alt ) )
			$return .= self::alt( $alt );
			
			return $return;
			
	}
	
	public static function href( $link, $alt, $class='' ) {
		$ret = ' href="'. trim( $link ) . '"';
		$ret .= !empty( $alt ) ? self::alt( $alt, false ) : '';
		$ret .= !empty( $class ) ? ' class="'. $class . '"' : '';
		if( defined('SITELINK') && strpos( $link, '://' ) !== false && stripos( $link, SITELINK ) === false ) {
			$ret .= ' target="_blank"';
		}
		
		
		return $ret . ' ';
	}
	public static function src( $src, $alt, $class='', $altonly=true ) {
		$ret = strpos( $src, 'lazy:' ) === 0 ? ' src="'. IMAGES . '/info.png" data-lazy="'. substr( trim( $src ) , 5) . '"' : ' src="'. trim( $src ) . '"';
		$ret .= !empty( $alt ) ? self::alt( $alt, $altonly ) : '';
		$ret .= !empty( $class ) ? ' class="'. $class . '"' : '';
		
		return $ret;
	}
	
	/*
	 * Get current URL as string
	 */
	public static function url()
	{
		$currentURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
		$currentURL .= $_SERVER["SERVER_NAME"];
		
		if($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443")
		{
			$currentURL .= ":".$_SERVER["SERVER_PORT"];
		}
		
		$currentURL .= $_SERVER["REQUEST_URI"];
		return $currentURL;
	}
	
	
	
	public static function lightcase( $link, $alt, $groep, $titel, $class = '' ) {
		$this->lightbox = true;
		
		$ret = ' href="'. trim( $link ) . '"';
		$ret .= !empty( $alt ) ? self::get_alt( $alt ) : '';
		$ret .= ' title="'. trim( $titel ) . '"';
		$ret .= ' data-rel="lightcase:'. trim( $groep ) . '"';
		
		$ret .= !empty( $class ) ? ' class="'. $class . '"' : '';
		
		return $ret;
	}
	
	public static function opsomming( $array, $order = 'az' ) {
		switch( $order ) {
			case 'az';
			sort( $array );
			break;
		}
		$return = is_array( $array ) && count( $array ) > 1 ?' en ' . array_pop( $array ) : '';
		$return = implode( ', ', $array ) . $return;
		return $return;
	}
	
	public static function postcode( $postcode ) {
		$postcode = strtoupper( str_replace( ' ','', $postcode ) );
		return substr( $postcode, 0, 4 ) .' '. substr( $postcode, 4, 2 );
	}
	
	public static function tel( $tel, $setting = null ) {
		$REG = '/[^0-9]/';
		$landcode = '31 ';
		if( isset( $setting ) && is_array( $setting ) ):
		foreach( $setting AS $adjust => $value ):
		$$adjust = $value;
		endforeach;
		endif;
		
		if( $landcode ) return '+' . preg_replace( $REG, '', $landcode . substr( trim( $tel ), 1) );
		else return preg_replace( $REG, '', $tel );
		
	}
	
	public static function hex2rgba( $hexcolorcode, $opacity=false) {
		$Return = false;
		
		if( !empty( $hexcolorcode ) ) {
			$hexcolorcode = explode( '#', $hexcolorcode );
			$hexcolorcode = explode( ' ', array_pop( $hexcolorcode ) );
			$hexcolorcode = array_shift( $hexcolorcode );
			
			if( strlen( $hexcolorcode ) == 3 ) {
				$hexcolorcode = $hexcolorcode[0].$hexcolorcode[0].$hexcolorcode[1].$hexcolorcode[1].$hexcolorcode[2].$hexcolorcode[2];
			}
			
			if( strlen( $hexcolorcode === 6 ) ) 
				$Return = $hexcolorcode;
			
		}
		
		return $Return;

	}
	
	public static function money( float $value, string $decimalsSeperatedBy=',', string $thousandsSeperatedBy='' ) {
		return number_format( $value, 2, $decimalsSeperatedBy, $thousandsSeperatedBy );
	}
	
	/*
	 * string	$script	the javascript or url of the javascript to add
	 * bool		$url	(default = true) set to false for javascript, true for URL to javascript
	 */
	function set_js( $script, $url = true ) {
		if( $url == true ) {
			if( is_array( $script ) ) $script = array( $script );
			
			foreach( $script AS $path ) {
				if( is_array( $path ) ) {
					foreach( $path AS $js ) {
						if( !is_array( $js ) ) $js = [ $js ];
						if( !in_array( $js, $this->script ) ) array_push( $this->script, $js );
					}
				} else
					if( !in_array( $path, $this->script ) ) array_push( $this->script, $path );
			}
			
		} else {
			if( is_array($script ) ) {
				foreach( $script AS $js ) {
					$js = str_replace( 
							[ "\n", "\t", '     ', '    ', '   ', '  '],
							[ "\r", ' ', ' ', ' ', ' ', ' '],
							$js );
					if( !in_array( $js, $this->script ) ) array_push( $this->script, $js );
				}
				
			} else {
				$js = str_replace( 
						[ "\n", "\t", '     ', '    ', '   ', '  '],
						[ "\r", ' ', ' ', ' ', ' ', ' '],
						$script );
				if( !in_array( $js, $this->script ) ) array_push( $this->script, $js );
			}
			
		}
	}
	
	function get_js() {
		if( isset( $this->lightbox ) && $this->lightbox ) {
			$this->set_js( array( JAVASCRIPT . '/lightcase.js' ) );
			$this->set_js('$("a[data-rel^=\'lightcase\']").lightcase({
						showTitle: false,
					maxWidth: "94%",
					maxHeight: "94%"
				});', false );
		}
		
		if( count( $this->script ) > 0 ) {
			$scripts = $this->script;
			$Hl = '';
			
			foreach( $scripts AS $script ) {
				if( is_array( $script ) ) {
					foreach( $script AS $url ) {
						$attr = '';
						$url = explode( '##', $url );
						if( isset( $url[1] ) ) {
							$tempURL = $url;
							$url = [ array_shift( $tempURL ) ];
							$tempUrl = array_shift( $tempURL );
							$attr = is_string( $tempURL ) ? ' '. implode( ' ', explode( ',', $tempUrl ) ) : '';
						}
						$Hl .= '<script src="'. $url[0] .'"'.$attr.'></script>';
					}
				} else {
					$Hl .= '<script type="text/javascript" language="javascript">'. $script .'</script>';
				}
			}
			return $Hl;
		}
		
		return false;
	}
	
	function set_meta( $input, $type = null ) {
		if( isset( $input ) && ( is_array( $input ) || is_object( $input ) ) ) {
			foreach( $input AS $type => $value ):
			$meta[ trim( $type ) ] = trim( $value );
			endforeach;
			
		} elseif( is_string( $input ) && isset( $type ) && trim( $type ) !== '' ) {
			$cType = 1;
			
			switch( $type ) {
				case 'keyword':
				case 'keywoord':
				case 'keywords':
					$cType = 3;
					$type = array( 'keywords' );
					break;
					
				case 'description':
				case 'og:description':
				case 'twitter:description':
					$cType = 1;
					$type = array( 'description', 'twitter:description', 'og:description' );
					break;
					
				case 'title':
				case 'titel':
				case 'name':
				case 'twitter:title':
					$cType = 2;
					$type = array( 'name', 'twitter:title', 'og:title' );
					break;
					
				case 'image':
				case 'og:image':
				case 'twitter:image':
					$cType = 2;
					$type = array( 'image', 'twitter:image:src', 'og:image' );
					break;
			}
			
			if( is_string( $type ) ) {
				$type = array( $type );
			}
			foreach( $type AS $thisType ) {
				$thisType = trim( $thisType );
				switch( $cType ) {
					case 1:
						$this->meta[ $thisType ] = htmlentities( $this->text_shorten( str_replace( '&nbsp', ' ', trim( $input ) ), 200, '', false ) );
						break;
					case 2:
						$this->meta[ $thisType ] = trim( $input );
						break;
					case 3:
						if( !isset( $this->meta[ $thisType ] ) ) $this->meta[ $thisType ] = [];
						
						$tempInput = explode( ',', $input );
						foreach( $tempInput AS $input ):
						$input = strtolower( trim( $input ) );
						if( !in_array( $input, $this->meta[ $thisType ] ) ) $this->meta[ $thisType ][] = $input;
						endforeach;
						break;
				}
			}
		}
	}
	
	function set_keyword( $input ) {
		if( !empty( $input ) ):
		if( !is_array( $input ) ) $input = strip_tags( str_replace( '\n', ' ', $input ) );
		$this->set_meta( $input, 'keyword' );
		endif;
	}
	
	function get_meta( $type = null ){
		if( count( $this->meta ) == 0 )
			return false;
			
			
			$return = '';
			
			foreach( $this->meta AS $name => $content ) {
				if( !isset( $type ) || stripos( $name, $type ) !== false ){
					if( is_array( $content ) ) {
						$content = implode( ', ', array_filter( $content ) );
					}
					if( is_string( $content ) ){
						if( strlen( $content ) > 0 ) {
							if( strpos( $name, 'og:' ) !== false ) {
								$return .= '<meta property="'. $name . '" content="'. $content .'" />'."\n";
							} else {
								$return .= '<meta name="'. $name . '" content="'. $content .'" />'."\n";
							}
							
						}
					}
				}
			}
			
			return $return;
			
			
	}
	
	function get_titletag(){
		global $HEADER;
		$titel = ( isset( $this->meta['name'] ) && !empty( $this->meta['name'] ) ) ?
		htmlentities( $this->meta['name'] ) : TitleTag();
		return '<title>'. $titel . '</title>';
		
	}
	
	public static function most_used( $input, $limit=10 ){
		// Array om te retourneren
		$return = [];
		
		// Array met lidwoorden en veel voorkomende, te negeren worden
		$ignore = ['tot', 'het','een','hij','zij','wij','hen','den','ten','van','deze','voor','dat','dan','met','kan','bij','bijna','voor','heeft','worden','doet','kiest'];
		if( !is_string( $input ) ) $input = implode( ' ', $input );
		
		// Woorden splitsen en stuk voor stuk optellen
		foreach( explode( ' ', str_replace( [',', '.', ':' ], ' ', strtolower( strip_tags( $input ) ) ) ) AS $WORD ):
		//Spaties weghalen voor en achter het word
		$WORD = trim( $WORD );
		
		global $dev;
		
		//Als het woord langer is dan 2 karakters en niet in de ignore array zit toevoegen aan array
		if( strlen( $WORD ) > 2 && !in_array( $WORD, $ignore ) ) {
			// Key aanmaken in array, als bestaat dan bij op tellen
			$return[ $WORD ] = isset( $return[ $WORD ] ) ? $return[ $WORD ] + 1 : 1;
		}
		endforeach;
		// Array sorteren, meestvoorkomend boven
		arsort( $return );
		
		// Overbodige resultaten (overtallig aan 10/$return) weghalen
		array_splice( $return, $limit );
		
		// Meest voorkomende woorden retourneren
		return $return;
		
	}
	
	public static function modal( $id = 'mijnModal', $settings = null ) {
		
		$default = array(
				'class' 			=> 'modal fade',
				'tab-index' 		=> '-1',
				'role' 				=> 'dialog',
				'aria-labelledby'	=> 'mijnModalLabel',
				'aria-hidden' 		=> 'true' );
		
		if( !is_array( $settings ) ) {
			$settings = $default;
		} else {
			$settings = array_merge( $default, $settings );
		}
		
		$implodeSettings = array();
		$titel = '';
		$size  = '';
		
		foreach( $settings AS $key => $val ) {
			$push = false;
			
			switch( $key ) {
				case 'contentBody':
					$contentBody = trim( $val );
					break;
				case 'contentTitle':
					$titel = trim( $val );
					break;
				case 'contentFooter':
					$footer = trim( $val );
				case 'size':
					$size = ' '. trim( $val );
					break;
				default:
					array_push( $implodeSettings, $key . '="' . $val .'"' );
					break;
			}
		}
		
		if( !isset( $contentBody ) ) $contentBody = '<iframe id="'.$id.'iframe" name="'.$id.'iframe" width="100%" style="min-height: 400px;max-height: 80%" src="about::blank" frameborder="0" allowfullscreen></iframe>';
		if( !isset( $footer ) ) $footer = '<button type="button" class="btn btn-default" data-dismiss="modal" >Sluiten</button>';
		
		$id = (string) str_replace( '#', '', trim( $id ) );
		
		$modal = '  <!-- Modal -->
            <div id="'. $id .'" '. implode( ' ', $implodeSettings ) . '>
              <div class="modal-dialog'. $size .'">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="'. $settings['aria-labelledby'] .'">'. $titel . '</h4>
                  </div>
                  <div class="modal-body">
			    		'. $contentBody .'
                  </div>
                  <div class="modal-footer">
					'. $footer . '
	              </div>
                </div>
              </div>
            </div>';
		
		return $modal;
	}
	
	public static function modal_attr( $id = 'mijnModal', $extraAttr = "" ) {
		$id=(string) str_replace( "#", '', trim( $id ) );
		
		if( is_string( $extraAttr ) ) {
			$extraAttr = ' ' . trim( $extraAttr );
		} else {
			$extraAttr = '';
		}
		
		return 'data-toggle="modal" data-target="#'. $id . '"' . $extraAttr;
	}
	
	public static function urlify( string $text ) {
		$strText = trim( $text );
		$strText = preg_replace(
						[ 
							'/[^A-Za-z0-9-]/',
							'/ +/', 
							'/ amp/',
							'/ /',
							'/-+/'
						], 
						[
							' ',
							' ',
							'-en',
							'-',
							'-'
						], $strText );
		$strText = strtolower($strText);
		return $strText;
		
	}
	
	public static function text_link ( $string, $target = '_blank', $altprefix = 'Ga naar:' ) {
		return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" '.$this->get_alt( $altprefix . ' $1' ).' target="'. $target .'">$1</a>', $string);
	}
	
	public static function text_shorten ( $string, $lengte, $allowTags = false, $wrap = true ) {
		if( !is_string( $string ) ) return;
		$lengte = (int) trim( $lengte );
		
		if( is_string( $allowTags ) ) {
			$string = strip_tags( $string, $allowTags );
		} elseif( $allowTags == true ) {
			$string = strip_tags( $string, ALLOWED_TAGS );
		} else {
			$string = strip_tags( $string );
		}
		
		if (strlen($string) > $lengte) {
			// truncate string
			$stringCut = substr($string, 0, $lengte);
			
			// make sure it ends in a word so assassinate doesn't become ass...
			$string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
		}
		
		if( $wrap == true ) {
			$string = '<p>' . $string . '</p>';
		}
		
		return $string;
	}
	
	public static function text_social( $haystack, $needle, $prefix = '' ) {
		if( $pos = stripos( trim( $haystack ), $needle ) + strlen( $needle ) ) {
			return $prefix . substr( $haystack, $pos, strlen( $haystack ) );
		} else {
			return $prefix . trim( $haystack );
		}
	}
	
	public static function page_subtitle( $tag = '', $class = '', $array='') {
		global $Pagina;
		if( empty( $array ) ) $array = $Pagina;
		if( !isset( $array['sub_paginatitel'] ) ) return false;
		$subtitel = trim( htmlentities( $array['sub_paginatitel'] ) );
		
		if( empty( $subtitel ) ) return '';
		
		if( isset( $class ) && trim( $class !== '' ) ) {
			$class = ' class="'. $class .'"';
		} else {
			$class = '';
		}
		
		if( isset( $tag ) && trim( $tag ) !== '' ) {
			$tag = strtolower( str_ireplace( array( '<', '>', '/'), '', $tag ) );
			
			$subtitel = '<'. $tag . $class .'>' . $subtitel . '</'. $tag .'>';
			return $subtitel;
		} else {
			return $subtitel;
		}
		
	}
	
	public static function page_title( $array = null) {
		if( is_array( $array ) ) {
			$Pagina = $array;
		} elseif( is_string( $array ) ) {
			$Pagina['paginatitel'] = $array;
		} else {
			global $Pagina;
		}
		
		if( isset( $Pagina['paginatitel'] ) && $Pagina['paginatitel'] !== NULL && str_replace( ' ', '', $Pagina['paginatitel'] ) !== '' )  {
			return $Pagina['paginatitel'];
		} elseif( isset( $Pagina['titel'] ) ) {
			return $Pagina['titel'];
		} else {
			return false;
		}
	}
    public static function page_tagline( $array = null) {
        if( is_array( $array ) ) {
            $Pagina = $array;
        } elseif( is_string( $array ) ) {
            $Pagina['tagline'] = $array;
        } else {
            global $Pagina;
        }

        if( isset( $Pagina['tagline'] ) && $Pagina['tagline'] !== NULL && str_replace( ' ', '', $Pagina['paginatitel'] ) !== '' )  {
            return $Pagina['tagline'];
        } elseif( isset( $Pagina['titel'] ) ) {
            return $Pagina['titel'];
        } else {
            return false;
        }
    }

	public static function page_intro( $array = null, $striptags = false ) {
		if( is_array( $array ) )
			$Pagina = $array;
			elseif( is_string( $array ) )
			$Pagina['inleiding'] = $array;
			else
				global $Pagina;
				
				if( isset( $Pagina['inleiding'] ) && $Pagina['inleiding'] !== NULL && str_replace( ' ', '', strip_tags( $Pagina['inleiding'] ) ) !== '' )  {
					$return = !( stripos( $Pagina['inleiding'], '<p ' ) >=0 || stripos( $Pagina['inleiding'], '<p>')>=0 ) ?
					'<p>'. trim( $Pagina['inleiding'] ) .'</p>' : trim( $Pagina['inleiding'] );
					
					if( $striptags ) {
						$return = strip_tags( $Pagina['inleiding'] );
					}
					
					return $return;
					
					
				} else {
					return false;
				}
	}
	
	public static function page_content( $array = null, $striptags = false ) {
		if( is_array( $array ) )
			$Pagina = $array;
			elseif( is_string( $array ) )
			$Pagina['inhoud'] = $array;
			else
				global $Pagina;
				
				if( isset( $Pagina['inhoud'] ) && $Pagina['inhoud'] !== NULL && str_replace( ' ', '', strip_tags( $Pagina['inhoud'] ) ) !== '' )  {
					$return = !( stripos( $Pagina['inhoud'], '<p ' ) >=0 || stripos( $Pagina['inhoud'], '<p>')>=0 ) ?
					'<p>'. trim( $Pagina['inhoud'] ) .'</p>' : trim( $Pagina['inhoud'] );
					
					if( $striptags ) {
						$return = strip_tags( $Pagina['inhoud'] );
					}
					
					return $return;
					
					
				} else {
					return false;
				}
	}
	
	/*
	 * string	$msg	Message to log
	 * boolean	$encode	default true, if true, message in htmlentities( $msg, ENT_QUOTES, 'UTF-8' )
	 */
	public static function log( $msg, $encode = true ) {
		if( $encode ) {
			$msg = htmlentities( $msg, ENT_QUOTES, 'UTF-8');
		}
		echo '<script>console.log("'.$msg.'");</script>';
		return true;
	}
	
	public static function FormData( $input ) {
		if( is_array( $input ) ) {
			$ret = '<table>';
			foreach( $input AS $f => $v ) {
				if( hasContent( $v ) && !in_array( $f, ['nobots','submit'] )) {
					$v = is_array( $v ) ? $v : [ $v ];
					
					$ret .= '<tr>
							<td width="30">&nbsp;</td>
							<td width="150" style="font-size: 0.89rem;vertical-align:top">'. htmlentities( ucfirst( strtolower( str_replace( '_', ' ', $f ) ) ) ) . '</td>
							<td>'. str_replace( '_', ' ', implode( '<br />', $v ) ) . '</td>
							<td width="30">&nbsp;</td>
						</tr>';
				}
			}
			
			$ret .= '<tr><td colspan="4" height="30px"></td></tr>
					</table>';
			
		} else {
			$ret = '';
		}
		return $ret;
	}
	
	public static function page_images( bool $all=true ) {
		global $Pagina;
		
		if ( array_key_exists( 'images', $Pagina ) && $Pagina['images'] ) {
			if( !is_array( $Pagina['images'] ) && !empty( $Pagina['images'] ) ) {
				$Pagina['images'] = explode( ',', $Pagina['images'] );
				
			}
		}
		
		return array_key_exists( 'images', $Pagina ) ? ( $all ?  $Pagina['images'] : $Pagina['images'][0] ) :  false;
	}
}

