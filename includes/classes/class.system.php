<?php

class System {
    public static function writeToLog($message, $type, $file) {
        $date = date("d-m-Y");
        $prefix = "[" . date("H:i:s Y-m-d") . "][" . $type . "] ";
        $content = $prefix . $message . PHP_EOL;
        $absolute_file = dirname(__DIR__, 2) . "/logs/" . $file . "." . $date . ".log";
        $absolute_file = fopen($absolute_file, 'a+');
        fwrite($absolute_file, $content);
        fclose($absolute_file);
    }

    public static function checkEmailDns($email) {
        $domain = substr($email, strpos($email, '@') + 1);
        $dns = dns_get_record($domain, DNS_TXT);
        foreach ($dns as $entry) {
            $needle = 'v=spf1';
            $spf = preg_match("/{$needle}/i", $entry['txt']);
            if ($spf >= 1) {
                if (strpos($entry['txt'], 'outlook.com') >= 1 || strpos($entry['txt'], 'hotmail.com') >= 1) {
                    return "outlook";
                } elseif (strpos($entry['txt'], 'google.com') >= 1 || strpos($entry['txt'], 'googlemail.com') >= 1) {
                    return "mdns";
                } else {
                    return "mdns";
                }
            }
        }

    }

}