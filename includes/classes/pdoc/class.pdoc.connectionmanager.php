<?php

namespace PDOC;

global $basePath, $setting, $connection;
$basePath = \__INC__ . '/';
$setting = [];
$connection = [];
	

class Connectionmanager {
	const
		DEFAULT_USER = \DB_USER,
		DEFAULT_PASS = \DB_PASS,
		DEFAULT_HOST = \DB_HOST,
		DEFAULT_NAME = \DB_NAME;
	
	function __construct( string $host=null, string $user=null, string $pass=null, string $name=null) {
		global $basePath, $setting, $connection;
		
		$basePath = \__INC__;
		$setting = [];
		$connection = [];
		
		self::setSettings( 
				$host ? $host : self::DEFAULT_HOST,
				$user ? $user : self::DEFAULT_USER,
				$pass ? $pass : self::DEFAULT_PASS,
				$name ? $name : self::DEFAULT_NAME,
				'dbconnection.php'
		);
		
	}
	
	public static function get( string $settingsIndex = 'dbconnection.php') {
		global $basePath, $connection, $setting;
		if( !array_key_exists( $settingsIndex, $setting ) && file_exists( $basePath . $settingsIndex ) ) {
			$loginData = file_get_contents( $basePath . $settingsIndex );

			if( preg_match_all( '/define\s?\(\s?[\'|"]DB_([A-Z]+)[\'|"]\s?,\s?[\'|"]?(.+)[\'|"]\s?\)/i', $loginData, $thisSetting ) ) {
				foreach( array_combine( array_values( $thisSetting[1] ), array_values( $thisSetting[2] ) ) AS $parameter => $value ) {
					$parameter = strtolower( $parameter );
					${$parameter} = $value;
				}
					
			} elseif( preg_match( '/mysql_connect\s?\(\s?[\'|"](.+)[\'|"]\s?,\s?[\'|"](.+)[\'|"],\s?[\'|"](.+)?[\'"]\s?\).*\$database=[\'|\"]([A-Za-z_ 0-9]+)[\'|"]/s', $Login, $DBData ) ) {
				$host = $DBData[1];
				$user = $DBData[2];
				$pass = $DBData[3];
				$name = $DBData[4];
					
			}
			if( $host === 'localhost' ) {
				$host = 'hwid.nl';
			}
			
			self::setSettings( $host, $user, $pass, $name, $settingsIndex);
			
		} elseif( $settingsIndex === 'default' ) {
			self::connectManually( self::DEFAULT_HOST, self::DEFAULT_USER, self::DEFAULT_NAME, self::DEFAULT_PASS, $settingsIndex );
		}
		
		if( !array_key_exists( $settingsIndex, $connection ) ) {
			self::connect( $settingsIndex );
		} 
		
		return isset( $connection[ $settingsIndex ] ) ? $connection[ $settingsIndex ] : false;
	}
	
	public static function connectManually( string $host, string $user, string $pass, string $name, string $settingsIndex ) {
		global $setting;
		
		if( !array_key_exists( $settingsIndex, $setting ) ) {
			self::setSettings($host, $user, $pass, $name, $settingsIndex);
			
		}
		
		self::connect( $settingsIndex );
		
		return false;
	}
	
	public static function connect( $settingsIndex=null ) {
		global $connection, $setting;
		
		try {
			$connection[ $settingsIndex ] = new \PDO ( 'mysql:host=' . $setting[ $settingsIndex ]->host . ';dbname=' . $setting[ $settingsIndex ]->name, $setting[ $settingsIndex ]->user, $setting[ $settingsIndex ]->pass );
			$connection[ $settingsIndex ]->exec ( 'SET NAMES utf8' );
			$connection[ $settingsIndex ]->setAttribute ( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
			
		} catch ( \PDOException $ex ) {
			die ( $ex->getMessage () );
		}

		return true;
	}
	
	public static function setSettings( string $host, string $user, string $pass, string $name, string $settingsIndex ) {
		global $setting;
		
		$setting[ $settingsIndex ] = (object) [
			'host' => $host,
			'user' => $user,
			'pass' => $pass,
			'name' => $name
		];
		
	}
	
	function __destruct() {
	}
}



?>