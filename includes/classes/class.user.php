<?php

class user extends session {
	protected $db;
	protected $table;
	protected $salt;
	
	function update_user( $input, $user, $ww = null ) {
		// definieer variabel
		$allFields = [];
		$allVals = [];
		$emailCheck = '';

		//verwerk de input voor de query
		foreach( $input AS $fields => $val ) {
			switch( $fields ) {
				case 'email':
					if( !filter_var( $val, FILTER_VALIDATE_EMAIL ) ) return false;
					else $val = strtolower( trim( $val ) );
					$email = $val;
				
					break;
				case 'voornaam':
				case 'achternaam':
					$val = strtoupper( substr( $val, 0, 1 ) ) . substr( $val, 1, ( strlen( $val ) - 1 ) );
					break;
				case 'postcode':
					$val = strtoupper( str_replace( ' ', '', $val ) );
			}
				
			
			$allFields[] = '`'. $fields .'`=?';
			$allVals[] = $val;
			
		}
		
		// extra data voor wachtwoord.
		if( $ww ) {
			$hash = $this->shortHash(  implode( '', $user ) . time() , 7, 'us_gebruikers');
			$wwHash = hash( 'sha512', $email.$ww.$hash );
			$ww = $this->hashIt( $ww, $wwHash );
			
			$allFields[] = '`wachtwoord`=?';
			$allFields[] = '`wachtwoord_hash`=?';
			$allFields[] = '`hash`=?';
			$allVals[] = $ww;
			$allVals[] = $wwHash;
			$allVals[] = $hash;
			
		} else {
			$emailCheck = ' AND `email`=?';

			if( isset( $user['laatste_login'] )  ) $emailCheck .= ' AND `laatste_login`=?';
			$email = trim( $user['email'] );
		}
		
		
		try {
			$Q = 'UPDATE `'. $this->table .'`
				SET '. implode( ', ', $allFields ) .'
				WHERE `id`=?'. $emailCheck .';';
			
			
			$update = $this->db->prepare( $Q );
			
			for( $i=0, $field=1; $i < count( $allFields ); $i++, $field++ ) {
				$val = $allVals[$i];
				
				if( is_numeric( $val ) ) {
					$update->bindValue( $field, $val, PDO::PARAM_INT );
				} else {
					$update->bindValue( $field, $val, PDO::PARAM_STR );
				}
				
			}

			
			
			$update->bindValue( $field	, $user['id'], PDO::PARAM_STR );
	
			if( !empty( $emailCheck ) ) {
				$field++;
				$update->bindValue( $field	, $email, PDO::PARAM_STR );
	
			}
			

			if( !isset( $ww ) && isset( $user['laatste_login'] )  ){
					$field++;
				$update->bindValue( $field	, $user['laatste_login'] , PDO::PARAM_STR );
	
				
			}
			
			if( $test = $update->execute() ) {
				if( isset( $ww ) ) {
					$this->logout();
					$this->login( $email, $ww );
					return true;
				}
			
				return $this->refresh( $email );
			}
			
		} catch( PDOException $e ) {
			die( $e->getMessage() );
		}
		
		
		return false;
	}
	
	function create_user( $input, $ww, $login=false ) {
		$allFields = [];
		$allVals = [];
		$allV = [];
		$renameField = [ 'kvknummer' => 'kvk_nummer'];
		
		//verwerk de input voor de query
		foreach( $input AS $fields => $val ) {
			if( array_key_exists( $fields, $renameField ) ) $fields = $renameField[ $fields ]; 
			switch( $fields ) {
				case 'email':
					if( !filter_var( $val, FILTER_VALIDATE_EMAIL ) ) return false;
					else $val = strtolower( trim( $val ) );
					$email = $val;
		
					break;
				case 'voornaam':
				case 'achternaam':
					$val = strtoupper( substr( $val, 0, 1 ) ) . substr( $val, 1, ( strlen( $val ) - 1 ) );
					break;
				case 'postcode':
					$val = strtoupper( str_replace( ' ', '', $val ) );
			}
		
				
			$allFields[] = '`'. $fields .'`';
			$allVals[] = $val;
			$allV[] = '?';
				
		}
		
		if( isset( $input['email'] ) && filter_var( $input['email'], FILTER_VALIDATE_EMAIL ) ) {
			$email = trim( $input['email'] );
		} else return false;
		
		
		$hash = $this->shortHash(  implode( '', $input ) . time() , 7, 'us_gebruikers');
		$wwHash = hash( 'sha512', $email.$ww.$hash );
		$oriWw = $ww;
		$ww = $this->hashIt( $ww, $wwHash );
		
		$adminHash = $this->shortHash(  implode( '', $input ) . time() , 16, 'us_gebruikers');
		
		$checkUsers = $this->db->prepare( 'SELECT email FROM `'. $this->table .'` WHERE email=:email;');
		$checkUsers->bindValue( ':email', $email, PDO::PARAM_STR );
		$checkUsers->execute();
	
		if( $checkUsers->rowCount() > 0 ) {
			return false;
		} else {
			unset( $checkUsers );
		}
	
		if (!filter_var( $email , FILTER_VALIDATE_EMAIL)){
			return 'Het emailadres dat u heeft opgegeven is ongeldig.';
		}
		
		try {
			
			$Q = 'INSERT INTO `'. $this->table .'`
				( '. implode( ', ', $allFields ) .', `wachtwoord`, `wachtwoord_hash`, `hash`, `admin_hash` )
				VALUES (
					'. implode( ', ', $allV ) . ',
					?,
					?,
					?,
					?);';
			
			$createLogin = $this->db->prepare( $Q );
			

			for( $i=0, $field=1; $i < count( $allV ); $i++, $field++ ) {
				$val = $allVals[$i];
				
				if( is_numeric( $val ) ) {
					$createLogin->bindValue( $field, $val, PDO::PARAM_INT );
				} else {
					$createLogin->bindValue( $field, $val, PDO::PARAM_STR );
				}
			}
			
			$createLogin->bindValue( $field	, $ww, PDO::PARAM_STR );
				$field++;
			$createLogin->bindValue( $field	, $wwHash , PDO::PARAM_STR );
				$field++;
			$createLogin->bindValue( $field	, $hash , PDO::PARAM_STR );
				$field++;
			$createLogin->bindValue( $field	, $adminHash , PDO::PARAM_STR );

			if( $createLogin->execute() ) {
				
				
				$id = $this->db->query( 'SELECT `id` FROM `us_gebruikers` WHERE `email`="'. $input['email'] . '" LIMIT 1;');
				$id = $id->fetch( PDO::FETCH_OBJ );
				$id = $id->id;
				$this->spaarpunten_add( 25, 'Bedankt voor je registratie', $id );
				
				// Fotos nakijken of die zijn toegevoegd.
				$this->db->query( 'UPDATE `us_gebruikers_crop_photos` SET `album_id`='. $id .' WHERE `session`="'. session_id() . '" AND `album_id`=0;');
				
				$_SESSION['quicklogin']['ww'] = $oriWw;
				$_SESSION['quicklogin']['user'] = $input['email'];
				
				if( isset( $login ) && $login ) {
					return $this->login( $input['email'], $oriWw );
					
				} else {
					return $id;
					
				}
				
			}
	
		} catch( PDOException $e ) {
			die( $e->getMessage() );
		}
	
		return false;
	
	}

	function get_data_persoon( $field = null ) {
		if( isset( $_SESSION['loggedin'] ) ) {
			
			$ret = $field && array_key_exists( $field, $_SESSION['loggedin'] ) ? $_SESSION['loggedin'][ $field ] : $_SESSION['loggedin'];

			return $ret;
		}
		return false;
	}
	
	function get_naam( $return='volledig') {
		switch( $return ):
		case 'volledig':
		case 'naam':
			$ret = str_replace( '  ', ' ',  $this->get_data_persoon( 'voornaam' ) . ' ' . $this->get_data_persoon( 'tussenvoegsel' ) . ' ' . $this->get_data_persoon( 'achternaam' ) );
			break;
		case 'voornaam':
			$ret = $this->get_data_persoon( 'voornaam' );
			break;
		case 'achternaam':
			$ret = trim( $this->get_data_persoon( 'tussenvoegsel' ) . ' ' . $this->get_data_persoon( 'achternaam' ) );
			break;
		default:
			$ret = $this->get_data_persoon( 'voornaam' ) ? $this->get_data_persoon( 'voornaam' ) : $this->get_data_persoon( 'achternaam' ) ;
		endswitch;
		
		return $ret;
	}

	protected function hashIt($input, $hash, $crypt='sha512') {
		return parent::hashIt($input, $hash, $crypt);
	}
	
	protected function shortHash( $input, $length = 7, $table = null ) {
		$shorthash = substr( hash( 'md5', $input ), 0, $length );
		
		if( $table ) {
			$ori_table = '`' . str_replace( '`', '', trim( $table ) ) . '`;';
			
			$hashCheck = $this->db->prepare( 'SELECT * FROM ' . $ori_table . ' WHERE hash=:shorthash' );
			$hashCheck->bindValue(':shorthash', $shorthash, PDO::PARAM_STR );
			$hashCheck->execute();
			
			if( $hashCheck->rowCount() != 0 ) {
				for( $i = 0; $i < 500; $i++ ) {
					$shorthash = substr( hash( 'md5', $shorthash ), 0, $length );
			
					$hashCheck = $this->db->prepare( 'SELECT * FROM ' . $ori_table . ' WHERE hash=:shorthash' );
					$hashCheck->bindValue(':shorthash', $shorthash, PDO::PARAM_STR );
					$hashCheck->execute();
			
					if( $hashCheck->rowCount() == 0 ) {
						$i = 500;
					}
			
				}
			}
			
		}

		return $shorthash;
	}
	
	function spaarpunten_calc( cart $cart ) {
		$totaal = $cart->get_cart( 'amount' );
		$totaal = $totaal[1] + $totaal[2] - $totaal[3];
		return $this->spaarpunten_calc_punten( $totaal );
	}
	
	function spaarpunten_calc_punten( $totaal, $return = '' ) {
		$totaal = ceil( $totaal );
		
		switch( $return ) {
			case 'widget':
				if( $this->loggedin() ) {
					$usertotaal = $this->get_spaarpunten_user( 'total' );
					
					if( $usertotaal > 100 ) {
						$bonnen = floor( $usertotaal / 100 );
						$over = 100 - floor( $usertotaal % 100 );
						
						$bonnen = '<div class="row red-bg l-align-center">
							Nog '. $over .' punten sparen en u ontvangt een cadeaubon ter waarde van &euro; 5,-
							</div> <!-- .row -->';
						
					} else {
						$bonnen = '<div class="row red-bg l-align-center">
							Nog '. ( 100 - $usertotaal ) .' punten sparen en u ontvangt een cadeaubon ter waarde van &euro; 5,-
							</div> <!-- .row -->';
					}
					
					$widget = '<div class="widget widget-sparen">
					<div class="spaarpunten">
					
					<div class="row">
					<div class="icon">
					<img src="'. IMAGES .'/spaarvarken.png" class="img-responsive" />
					</div> <!-- .icon -->
					
					<div class="heading">
					
					<div class="text">
							<span class="large">+ '. $totaal .'</span>
							<span>Spaarpunten</span>
							</div> <!-- .text-large -->
					
							</div> <!-- .heading -->
							</div> <!-- .row -->
					
							'. $bonnen .'
					
							<div class="row spaarpunten-bottom l-align-center">
			<p>Je ontvangt 1 spaarpunt bij elke <br />euro die u besteedt</p>
								</div>
					
								</div> <!-- .spaarpunten -->
								</div> <!-- .widget-sparen -->';
				} else {
					$widget = '<div class="widget widget-sparen">
					<div class="spaarpunten">
					
					<div class="row">
					<div class="icon">
					<img src="'. IMAGES .'/spaarvarken.png" class="img-responsive" />
					</div> <!-- .icon -->
					
					<div class="heading">
					
					<div class="text">
							<span class="large">'. $totaal .'</span>
							<span>Spaarpunten</span>
							</div> <!-- .text-large -->
					
							</div> <!-- .heading -->
							</div> <!-- .row -->
									
							<div class="row red-bg l-align-center">
							Ontvangt een geregistreerde gebruiker bij het plaatsen van deze bestelling
							</div> <!-- .row -->
									
							<div class="row spaarpunten-bottom l-align-center">
			<p>Een geregistreerde besteller ontvangt 1 spaarpunt bij elke <br />euro die u besteedt</p>
								</div>
					
								</div> <!-- .spaarpunten -->
								</div> <!-- .widget-sparen -->';
				}
				return $widget;
			default:
			return $totaal;
			break;
		}
		
	}
	
	function search_user( $term ) {
		if( $this->is_admin() ) {
			$term = '%'. trim( $term ) . '%';
			
			$Q = $this->db->prepare( 'SELECT
					`id`, `hash`, REPLACE( CONCAT_WS( " ", `voornaam`, `tussenvoegsel`, `achternaam` ), "  ", " " ) AS `naam`,
					`email`, REPLACE( CONCAT_WS( "", `adres`, " ", `huisnummer`, `huisnummer_toevoegsel` ), "  ", " " ) AS `volledig_adres`,
					`plaats`, `telefoon`, `telefoon2`
					
					FROM `us_gebruikers` `u`
						WHERE `email` LIKE :email OR `telefoon`=:tel
							OR `telefoon2`=:tel2 OR `plaats` LIKE :plaats
							OR REPLACE( CONCAT( `voornaam`, `tussenvoegsel`, `achternaam` ), "  ", " " ) LIKE :naam
							OR REPLACE( CONCAT_WS( "", `adres`, " ", `huisnummer`, `huisnummer_toevoegsel` ), "  ", " " ) LIKE :adres
						ORDER BY `plaats`, `naam`, `email` ASC;');
			
				$Q->bindValue( ':email', $term, PDO::PARAM_STR );
				$Q->bindValue( ':naam', $term, PDO::PARAM_STR );
				$Q->bindValue( ':adres', $term, PDO::PARAM_STR );
				$Q->bindValue( ':tel', $term, PDO::PARAM_STR );
				$Q->bindValue( ':tel2', $term, PDO::PARAM_STR );
				$Q->bindValue( ':plaats', $term, PDO::PARAM_STR );

			try {
				$Q->execute();
				
			} catch( PDOException $ex ) {
				return $ex->getMessage();
			}
			
			if( $Q->rowCount() > 0 ) {
				return $Q->fetchAll( PDO::FETCH_ASSOC );
			} else {
				return false;
			}
		} else return false;
			
	}
	
	function spaarpunten_add( $punten, $opmerking, $id='' ) {
		return true;
		
		if( $this->loggedin() || !empty( $id ) ) {
			$id = !empty( $id ) ? $id :  $_SESSION['loggedin']['id'];
			$be = isset( $_SESSION['bestelling']['bestelling_id'] ) ?  $_SESSION['bestelling']['bestelling_id'] : 0 ;
			$o = (string) $opmerking;
			$p = (int) $punten;
			
			$add = $this->db->prepare('INSERT INTO `us_spaarpunten`
					(`us_gebruikers_id`, `bestelling_id`, `punten`, `opmerking`, `datum` )
					VALUES ( :userid, :bestelling, :punten, :opmerking, NOW() );');
			
			$add->bindValue(':userid', 		$id , PDO::PARAM_INT );
			$add->bindValue(':bestelling', 	$be , PDO::PARAM_STR );
			$add->bindValue(':punten', 		$p 	, PDO::PARAM_INT );
			$add->bindValue(':opmerking', 	$o 	, PDO::PARAM_INT );
			
			try {
				$add->execute();
				return true;
				
			} catch( PDOException $ex ) {
				die( $ex->getMessage() );
			}
		}
	}
	
	function check_korting_user( $return='check' ){
		if( $this->loggedin() ){
			switch ($return){
				case 'check';
					if( isset( $_SESSION['loggedin']['gebruiker_korting'] ) && !empty( $_SESSION['loggedin']['gebruiker_korting'] ) ) return true;

				break;
				default:
					if( isset( $_SESSION['loggedin']['gebruiker_korting'][ $return ] )
						&& is_array( $_SESSION['loggedin']['gebruiker_korting'][ $return ] ) ) {
							return $_SESSION['loggedin']['gebruiker_korting'][ $return ]['korting'];
							
					} elseif( isset( $_SESSION['loggedin']['gebruiker_korting'][ $return ] )
								&& is_numeric( $return ) ) {
						return $_SESSION['loggedin']['gebruiker_korting'][ $return ];

							
					}
			}
			
			return false;
		}
	}
	
	function get_spaarpunten_user( $return = '' ) {
		if( $this->loggedin() ) {
			$uid = (int) $this->user['id'];
			
			switch( $return ) {
				case 'total':
				case 'totaal':
					$add = $this->db->prepare( 'SELECT SUM(`punten`) AS `punten` FROM `us_spaarpunten` WHERE `us_gebruikers_id`=:userid AND `aktief`="ja"');
					$add->bindValue( ':userid', $uid, PDO::PARAM_INT );
					
					try {
						$add->execute();
						$add = $add->fetch(PDO::FETCH_OBJ);
						$p = $add->punten > 0 ? $add->punten : 0;
						
						return $p;
						
					} catch( PDOException $ex ) {
						die( $ex->getMessage() );
					}
				break;
				case 'widget':
					$totaal = $this->get_spaarpunten_user('total');
					$usertotaal = $this->get_spaarpunten_user( 'total' );
					if( $usertotaal > 100 ) {
						$bonnen = floor( $usertotaal / 100 );
						$over = 100 - floor( $usertotaal % 100 );
					
						$bonnen = '<div class="row red-bg l-align-center">
							Nog '. $over .' punten sparen en u ontvangt een cadeaubon ter waarde van &euro; 5,-
							</div> <!-- .row -->';
					
					} else {
						$bonnen = '<div class="row red-bg l-align-center">
							Nog '. ( 100 - $usertotaal ) .' punten sparen en u ontvangt een cadeaubon ter waarde van &euro; 5,-
							</div> <!-- .row -->';
					}
					
					$widget = '
					<div class="widget widget-sparen">
					<div class="spaarpunten">
					
					<div class="row">
					<div class="icon">
					<img src="'. IMAGES .'/spaarvarken.png" class="img-responsive" />
					</div> <!-- .icon -->
					
					<div class="heading">
					
					<div class="text">
							<span class="large">'. $totaal .'</span>
							<span>Spaarpunten</span>
							</div> <!-- .text-large -->
					
							</div> <!-- .heading -->
							</div> <!-- .row -->
					
							'. $bonnen .'
					
							<div class="row spaarpunten-bottom l-align-center">
			<p>U ontvangt 1 spaarpunt bij elke <br />euro die u besteedt</p>
								</div>
					
								</div> <!-- .spaarpunten -->
								</div> <!-- .widget-sparen -->';
					
					return $widget;
					break;
					
				default:
					$add = $this->db->prepare( 'SELECT `punten`, `opmerking` FROM `us_spaarpunten` WHERE `us_gebruikers_id`=:userid AND `aktief`="ja" ORDER BY `datum` ASC');
					$add->bindValue( ':userid', $uid, PDO::PARAM_INT );
					
					try {
						$add->execute();
						$add = $add->fetchAll(PDO::FETCH_ASSOC);
						
						return $add;
						
					} catch( PDOException $ex ) {
						die( $ex->getMessage() );
					}
					break;
			}
		}
		
		return false;
	}
	
	/*
	 * $return = case
	 * 		basic - default // Maak korting ar in sessie aan, [ categorie_id ] = kortingspercentage
	 * 		full - Maak korting tabel aan, inc. opmerking
	 * 				[ categorie_id ][ categorie|korting|opmerking ] = info
	 */
	function get_korting_user( $return='full' ) {
		if( $this->loggedin() ) {
				
			switch( $return ){
				case 'full':
					$Q = 'SELECT `producten_categorie_id`, `korting`, `opmerking` FROM
							`'. $this->table .'_korting`';
						
					if( $Korting = $this->db->query( $Q ) ){
						while( $Korting->fetch( PDO::FETCH_OBJ ) ){
							$_SESSION['loggedin']['gebruiker_korting'][ $Korting->producten_categorie_id ] = [
									'categorie' => $Korting->producten_categorie_id,
									'korting' => $Korting->korting,
									'opmerking' => $Korting->opmerking
							];
								
						}
					}
					break;
				default:
					$Q = 'SELECT `producten_categorie_id`, `korting` FROM
							`'. $this->table .'_korting`';
						
					if( $Korting = $this->db->query( $Q ) ){
						while( $Korting->fetch( PDO::FETCH_OBJ ) ){
							$_SESSION['loggedin']['gebruiker_korting'][ $Korting->producten_categorie_id ] = $Korting->korting;
								
						}
					}
						
			}
				
		}
	}
	
	
	function get_bestel_historie( $limit=false, $maxpp=false ) {
		if( $this->loggedin() ) {
			
		$limit = $limit? ' LIMIT '. $limit : '';
			
		$Q = 'SELECT
				group_concat( `artikel_id` ) AS `bestelling`,
				group_concat( concat_ws( "][",
					`a`.`aantal`,
					`a`.`artikel_id`,
					`a`.`artikel`,
					`p`.`artikelnummer`,
					CASE
						WHEN `a`.`aanbieding_prijs` > 0 THEN `a`.`aanbieding_prijs`
						ELSE `a`.`prijs_product`
					END,
					`a`.`btw_id`,
					( SELECT concat_ws( ".", `id`, `ext` ) FROM
						`producten_crop_photos` `pp`
						WHERE `pp`.`album_id`=`p`.`id`
							ORDER BY `pp`.`albumthumb` DESC, `id` ASC
							LIMIT 1 ),
					"##" )
				) AS `bestelinfo`,
					
				group_concat( `a`.`aantal` ) AS `aantal` ,
				`acc_bestellingen_id`,
				`b`.`us_gebruikers_id` ,
				`b`.`invoice_no`,
				`b`.`type_bestelling`,
				`b`.`betaling_method`,
				`b`.`datum_aangepast` AS `datum`,
				( SELECT
					count( `tab`.`bestelling_id` )
				FROM `acc_bestellingen` `tab`
					WHERE `tab`.`us_gebruikers_id`=:userid
						AND `tab`.`order_status_id`>1 ) AS `totaal`,
				
				CEIL(`b`.`totaal`) AS `punten`
				
				FROM `acc_bestelregels` `a`
					LEFT JOIN `acc_bestellingen` `b`
						ON `b`.`bestelling_id`=`a`.`acc_bestellingen_id`
					LEFT JOIN `producten` `p`
						ON `p`.`id`=`a`.`artikel_id`
				WHERE
					`b`.`us_gebruikers_id`=:userid AND `categorie`="product" AND `b`.`order_status_id`>1';

		if( $maxpp ) $Q .= ' AND `p`.`max_pp`>0 AND `p`.`max_pp` IS NOT NULL';
		
		$Q .= ' GROUP BY `acc_bestellingen_id` ORDER BY `b`.`datum_aangemaakt` DESC' . $limit;
		
		$get = $this->db->prepare( $Q );
		$get->bindValue( ':userid', $this->user['id'] );
		
		try{
			$get->execute();
			$get = $get->fetchAll( PDO::FETCH_ASSOC );
			
			array_walk_recursive( $get, function( &$val, $key ) {
				switch( $key ):
					case 'bestelling':
					case 'aantal':
							$val = explode( ',', $val );
					break;
					case 'bestelinfo':
						$data = explode( '##',
									str_replace( ['##,','][##'], '##', $val )
								);
						
						$keys = [ 'aantal', 'artikel_id', 'titel', 'artikelnummer', 'prijs', 'btw_id', 'image' ];

						$val = [];
						foreach( array_filter( $data ) AS $R ):
							$R = explode( '][', $R );
							array_filter( $R );
							
							$theseKeys = $keys;
							if( count( $R ) !== count( $theseKeys ) ):
								$theseKeys = array_splice( $theseKeys, 0, count( $R ) );
							endif;
							
							$val[] = array_combine(
										$theseKeys, $R
								);

							
						endforeach;

					break;
				endswitch;
			} );

			
			return $get;
			
			
		} catch( PDOException $ex ) {
			die( $ex->getMessage() );
		}
		
		
		}
		return false;
	}
	function get_bestelling_historie( $bestel_id, $maxpp=false ) {
		if( $this->loggedin() ) {
			
		$bestel_id = (int) $bestel_id;
			
		$Q = 'SELECT
				group_concat( `artikel_id` ) AS `bestelling`,
				group_concat( concat_ws( "][",
					`a`.`aantal`,
					`a`.`artikel_id`,
					`a`.`artikel`,
					`p`.`artikelnummer`,
					`a`.`prijs_exc`,
					`a`.`prijs`,
					`a`.`btw_id`,
					( SELECT concat_ws( ".", `id`, `ext` ) FROM
						`producten_crop_photos` `pp`
						WHERE `pp`.`album_id`=`p`.`id`
							ORDER BY `pp`.`albumthumb` DESC, `id` ASC
							LIMIT 1 ),
					"##" )
				) AS `bestelinfo`,
					
				group_concat( `a`.`aantal` ) AS `aantal` ,
				`acc_bestellingen_id`,
				`b`.`us_gebruikers_id` ,
				`b`.`invoice_no`,
				`b`.`type_bestelling`,
				`b`.`betaling_method`,
				`b`.`datum_aangepast` AS `datum` '. (
					( !$this->is_admin() ) ? ',
				( SELECT
					count( `tab`.`bestelling_id` )
				FROM `acc_bestellingen` `tab`
					WHERE `tab`.`us_gebruikers_id`=:userid
						AND `tab`.`order_status_id`>1 ) AS `totaal`,
				
				CEIL(`b`.`totaal`) AS `punten`' : '' ) . '
				
				FROM `acc_bestelregels` `a`
					LEFT JOIN `acc_bestellingen` `b`
						ON `b`.`bestelling_id`=`a`.`acc_bestellingen_id`
					LEFT JOIN `producten` `p`
						ON `p`.`id`=`a`.`artikel_id`
				WHERE `b`.`bestelling_id`=:bestellingid
					'. ( !$this->is_admin() ? ' AND `b`.`us_gebruikers_id`=:userid' : '' ) . ' AND `categorie`="product" AND `b`.`order_status_id`>0';

		if( $maxpp ) $Q .= ' AND `p`.`max_pp`>0 AND `p`.`max_pp` IS NOT NULL';
		
		
		$Q .= ' GROUP BY `acc_bestellingen_id` ORDER BY `b`.`datum_aangemaakt` DESC';
		
		$get = $this->db->prepare( $Q );
		$get->bindValue( ':bestellingid', $bestel_id, PDO::PARAM_INT );

		if( !$this->is_admin() ):
			$get->bindValue( ':userid', $this->user['id'] );
		endif;
		
		try{
			$get->execute();
			$get = $get->fetchAll( PDO::FETCH_ASSOC );
			
			array_walk_recursive( $get, function( &$val, $key ) {
				switch( $key ):
					case 'bestelling':
					case 'aantal':
							$val = explode( ',', $val );
					break;
					case 'bestelinfo':
						$data = explode( '##',
									str_replace( ['##,','][##'], '##', $val )
								);
						
						$keys = [ 'aantal', 'artikel_id', 'titel', 'artikelnummer', 'prijs', 'prijs_exc', 'btw_id', 'image' ];

						$val = [];
						foreach( array_filter( $data ) AS $R ):
							$R = explode( '][', $R );
							array_filter( $R );
							
							$theseKeys = $keys;
							if( count( $R ) !== count( $theseKeys ) ):
								$theseKeys = array_splice( $theseKeys, 0, count( $R ) );
							endif;
							
							$val[] = array_combine(
										$theseKeys, $R
								);

							
						endforeach;

					break;
				endswitch;
			} );

			
			return $get;
			
			
		} catch( PDOException $ex ) {
			die( $ex->getMessage() );
		}
		
		
		}
		return false;
	}
	function get_bestelling( $id=false ) {
		if( $this->loggedin() ) {
			
		$Q = 'SELECT
				group_concat( `a`.`artikel_id` ) AS `bestelling`,
				group_concat( `a`.`aantal` ) AS `aantal`

				FROM `acc_bestelregels` `a`
					LEFT JOIN `acc_bestellingen` `b`
						ON `b`.`bestelling_id`=`a`.`acc_bestellingen_id`
					LEFT JOIN `producten` `p`
						ON `p`.`id`=`a`.`artikel_id`
				WHERE '. 
				( !$this->is_admin() ? 
					'`b`.`us_gebruikers_id`=:userid AND ' 
						: 
					'' 
				). '`b`.`bestelling_id`=:orderid
				AND `b`.`order_status_id`>1
				AND `a`.`categorie`="product"
				
				GROUP BY `b`.`bestelling_id`
				LIMIT 1';

		
		$get = $this->db->prepare( $Q );
		if( !$this->is_admin() ):
			$get->bindValue( ':userid', $this->user['id'] );
		endif;
		$get->bindValue( ':orderid', (int) $id );
		
		try{
			$get->execute();
			$get = $get->fetch( PDO::FETCH_ASSOC );
			
			if( !empty( $get ) ):
				array_walk_recursive( $get, function( &$val, $key ) {
					switch( $key ):
						case 'bestelling':
						case 'aantal':
								$val = explode( ',', $val );
						break;
					endswitch;
				} );
			
				$get['cart'] = array_combine( $get['bestelling'], $get['aantal']);
			endif;
			
			return $get;
			
			
		} catch( PDOException $ex ) {
			die( $ex->getMessage() );
		}
		
		
		}			

		return false;
	}
	
	function wachtwoord_reset( $id, $hash, $token = '', $check = false ) {
		if( !$this->loggedin() && empty( $token ) ) {
			
			$token = $this->shortHash( $id.$hash.time(), 32, 'us_wachtwoord_vergeten' );
			
			$Q = 'INSERT INTO `us_wachtwoord_vergeten` (`us_gebruikers_id`, `hash`, `token`, `datum`) VALUES
					(:id, :hash, :token, NOW() ) ON DUPLICATE KEY UPDATE `token`=VALUES(`token`), `datum`=now();';
			
			$Q = $this->db->prepare( $Q );
			$Q->bindValue( ':id', $id, PDO::PARAM_INT );
			$Q->bindValue( ':hash', $hash, PDO::PARAM_STR );
			$Q->bindValue( ':token', $token, PDO::PARAM_STR );
			
			try{
				$Q->execute();
				return $token;
			} catch( PDOException $ex ) {
				die( $ex->getMessage() );
			}

			
		} elseif( $check ) {
			
			$Q = 'SELECT `id` FROM `us_wachtwoord_vergeten` WHERE `us_gebruikers_id`=:id AND `hash`=:hash AND `token`=:token AND `datum` > DATE_SUB( NOW(), INTERVAL 1 DAY ) AND ( SELECT count(id) FROM `us_gebruikers` WHERE `id`=:cid AND `hash`=:chash ) > 0;';
			$Q = $this->db->prepare( $Q );
			$Q->bindValue( ':id', $id, PDO::PARAM_INT );
			$Q->bindValue( ':hash', $hash, PDO::PARAM_STR );
			$Q->bindValue( ':cid', $id, PDO::PARAM_INT );
			$Q->bindValue( ':chash', $hash, PDO::PARAM_STR );
			$Q->bindValue( ':token', $token, PDO::PARAM_STR );
			
			try{
				$Q->execute();
				if( $Q->rowCount() === 1 ) {
					return true;
				} else {
					return false;
				}
				
				return $token;
				
			} catch( PDOException $ex ) {
				die( $ex->getMessage() );
			}
			
			
		}
		
		return false;
	}

	function wachtwoord_reset_new( $ww, $id, $hash, $token ) {
		if( is_numeric( $id ) && !empty( $ww ) && !empty( $hash ) && !empty( $token ) ) {
			$user = (int) $id;
				
			$Q = 'SELECT `id`, `email`, `voornaam`, `tussenvoegsel`, `achternaam`, `adres`, `huisnummer`, `hash`, `wachtwoord_hash` FROM `us_gebruikers` WHERE `id`=:id AND `hash`=:hash;';
			$Q = $this->db->prepare( $Q );
			$Q->bindValue( ':id', $user, PDO::PARAM_INT );
			$Q->bindValue( ':hash', $hash, PDO::PARAM_STR );
				
			try{
				$Q->execute();
				if( $Q->rowCount() === 1 ) {
					$Q = $Q->fetch( PDO::FETCH_ASSOC );
						
				}
		
			} catch( PDOException $ex ) {
				return $ex->getMessage();
			}
				
			
			$email = $Q['email'];
				
			$newhash = $this->shortHash(  implode( '', $Q ) . time() , 7, 'us_gebruikers');
			$wwHash = hash( 'sha512', $email.$ww.$hash );
			$ww = $this->hashIt( $ww, $wwHash );
			
			$Q = 'UPDATE `us_gebruikers` SET `wachtwoord`=:ww, `wachtwoord_hash`=:wwh, `hash`=:newhash WHERE `id`=:id AND `email`=:email';;
			$Q = $this->db->prepare( $Q );
			$Q->bindValue( ':ww', $ww, PDO::PARAM_STR );
			$Q->bindValue( ':wwh', $wwHash, PDO::PARAM_STR );
			$Q->bindValue( ':newhash', $newhash, PDO::PARAM_STR );
			$Q->bindValue( ':id', $id, PDO::PARAM_INT );
			$Q->bindValue( ':email', $email, PDO::PARAM_STR );

			try{
				$Q->execute();
				return true;
			} catch( PDOException $ex ) {
				return false;
			}
			
			
		}
		return false;
	}
	
}

?>