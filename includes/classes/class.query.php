<?php


abstract class log {
	private $log = [];
	
	function log_set( $input, $type = 'error' ) {
		$type = trim( $type );
		$input = trim( $input );
		
		switch( $type ) {
			case 'error':
			case 'err':
			case 'e':
				$type = 'error';
				break;
			case 'success':
			case 'succes':
			case 's':
				$type = 'success';
				break;
			default:
				$type = 'msg';
				break;
		}
		
		$this->log[ $type ]['last'] = $input;
		$this->log[ $type ][] = $input;
		$this->log['last'] = '('. $type .') '.$input;
	}
	
	function log_get( $last = true, $type = null ) {
		switch( $type ) {
			case 'error':
			case 'err':
			case 'e':
				$type = 'error';
				break;
			case 'success':
			case 'succes':
			case 's':
				$type = 'success';
				break;
			case 'msg':
			case 'default':
				$type = 'msg';
				break;
			default:
				$type = false;
		}
		
		
		if( $last === true ) {
			if( $type === false ) {
				return $this->log['last'];
			} else {
				return $this->log[ $type ]['last'];
			}
			
		} else {
			if( $type === false ) {
				return array_merge( $this->log['error'], $this->log['success'], $this->log['msg'] );
			} else {
				return $this->log[ $type ];
			}
			
		}
		
	}
}

abstract class query extends log {
	public $table;
	private $hash;
	private $data;
	private $joins = true;
	private $member = [];
	private $order;
	private $pdo;
	private $photo = [];
	private $rc = 0;
	private $where;
	
	function __construct( PDO $pdo, $table ) {
		$this->pdo = $pdo;

		if( in_array( trim( $table ) , $this->allowed ) ) $this->table = [ trim( $table ), trim( $table[0] ) ];
		else return false;
	}
	
	function join_add_listmember( $cat, $target ) {
		$cat = trim( $cat );
		$t = '`' . trim( $target[0] ) . $this->rc() . '`';
		$target = $this->secure( explode( '.', $target ) );

		$this->member[ $cat ] = [ $target, $t ];
		return true;
	}
	
	function join_add_image( $type = true, $extra = '' ) {
		foreach( $this->photo AS $ar ) {
			if( $ar[1] == $extra) return true;
		}
		
		$type = strtolower( $type );
		switch( strtolower( $type ) ) {
			case 'album':
			case 'thumb':
			case 'albumthumb':
				$this->photo[] = [ 'albumthumb', $extra ];
				break;
			case 1:
			case true:
			case 'true':
				$this->photo[] = [ true, $extra ];
				break;
			default:
				$this->photo[] = [ false, $extra ];
				break;
		}
		
	}

	
	function table_set( $table ) {
		if( is_array( $table ) ) {
			$this->table = $table;
		} else {
			$this->table = [ $table, $table[0] . $this->rc() ];
		}
		
		return true;
	}
	
	function where( $field, $is = null, $value = null, $operator = 'AND' ) {
		switch( $field ) {
			case 'reset':
			case null:
				$this->where = null;
				return true;
		}
		
		if( !is_array( $this->where ) ) $this->where = [];
		
		if( !is_array( $field ) ) {
			$field = explode('.', $field );
		}
		
		if( !is_array( $field ) ) {
			die( 'huh' );
		}
		
		$field = $this->join_table_check( $field );
		
		if( is_numeric( $value ) ) $value = (int) $value;
		elseif ( in_array( strtoupper( $value ), [ 'NOW()', 'CURDATE()', 'CURTIME()'])) $value = trim( $value );
		else $value = '"'. $value .'"';
		
		$operator = ' ' . trim( $operator ) . ' ';

		$this->where[] = [ $this->secure( $field ), $is, $value, $operator ];
		
	}
	
	function order( $fields, $order = 'DESC') {
			if( $fields == false || $fields == null) {
				$this->order = null;
				return true;
			}
			if( is_array( $fields ) ) {
				if( isset( $order ) && trim( strtolower( $order ) ) !== 'desc' && trim( strtolower( $order ) ) !== 'asc') {
					$eachfield = $fields;
					$fields = array();
					$order = '';
					foreach( $eachfield AS $field => $order ) {
						array_push( $fields, '`'. $field . '` '. $order );
					}
					$this->order = ' ORDER BY ' . implode( ', ' , $fields ) . ' ';
				} else {
						
					$this->order = ' ORDER BY ' . implode( ', ' , $fields ) . ' ' . $order . ' ';
				}
		
		
			} else {
				$this->order = ' ORDER BY ' . $fields . ' ' . $order . ' ';
			}
				
			return true;
	}
	
	function join_filter( $ar = null ) {
		if( $ar == 'reset' || $ar == false || $ar === 0 ) {
			$this->joins === null;
		} elseif( $ar === 'true' || $ar === true || $ar === 1 ) {
			$this->joins = array_keys( $this->member );
			
		} else {
			if( !is_array( $this->joins ) && is_bool( $this->joins ) ) {
				$this->joins = [];
			}
			
			if( !is_array( $ar ) && !is_numeric( $ar ) ) {
				$ar = [ trim( $ar ) ];
			}
			
			foreach( $ar AS $a ) {
				if( array_key_exists( $a, $this->member) && !in_array( $a, $this->joins ) ) $this->joins = array_merge( $this->joins, [$a] );
				
			}
		}
		
		return true;

	}
	
	function join_table_check( $input = null ) {
		if( in_array( trim( $input[0] ), $this->table ) ) {
			$input[0] = $this->table[1];
				
		} elseif( array_key_exists( trim( $input[0] ), $this->member ) ) {
			switch( count( $this->member[ trim( $input[0] ) ] ) ) {
				case 4:
					$offset = 2;
					break;
				default:
					$offset = 1;
					break;
			}
			
			$input[0] = $this->member[ trim( $input[0] ) ][ count( $this->member[ trim( $input[0] ) ] ) - $offset ];
		}
		return $input;
	}
	
	function get( $return = null, $limit = null, $distinct = '', $group = '') {
		$join 	= '';
		$where 	= '';
		$order 	= !empty( $this->order ) ? $this->order : '';
		$distinct = !empty( $distinct ) ? 'DISTINCT ' : '';
		$group 	= !empty( $group ) ? ' GROUP BY `'. trim( $group ) . '`': '';
		
		
		//Kijken of de return value eigenlijk een limit is
		if( $limit === null ) {
			if( !is_array( $return ) && is_numeric( implode( '', explode( ',', $return ) ) ) ) {
				$limit = $return;
				$return = null;
			}
		}
		
		if( $limit === null ) {
			$limit = '';
		} else {
			$limit = ' LIMIT ' . $limit;
		}
		
		if( $return === null || ( is_string( $return ) && trim( $return ) === '*' ) ) $return = '*';
		else {
			$return = !is_array( $return ) ? str_replace( '  ', ' ', $return ): str_replace( '  ', ' ', implode( ',', $return ) );
			$return = explode( ',', $return );
			
			if( !is_array( $return ) ) $return = [ trim( $this->table[1] . '.' . $return ) ];
			
			$retAr = [];
			foreach( $return AS $k => $ret ) {
				$ret = $this->secure( $this->join_table_check( explode( '.', $ret ) ) );
				array_push( $retAr, implode( '.', $ret ) );
			}
			$return = str_replace( '``', '`', implode( ', ', $retAr ) );
			unset( $retAr );
		}
		
		if( isset( $this->joins ) && is_array( $this->joins ) ) {
			
			$return = explode( ',', $return );
			
			
			// Listmember lijst leeghalen, als opsomming gesplitst middels kommas
			foreach( $this->joins AS $cat ) {
				$table = $this->member[ $cat ][0][0];
				$source = $this->member[ $cat ][0][1];
				$target = $this->member[ $cat ][0][2];
				$t = $this->member[ $cat ][1];
				$s = '`' . $this->table[1] . '`';
				$return[] = ' ( SELECT GROUP_CONCAT('. $t . '.'. $target .') FROM '. $table . ' ' . $t . ' WHERE '. $s.'.`id`='.$t.'.'.$source . ' AND '. $t . '.'. $target . ' !=0 ) AS `'.$cat .'`';
			}
			
			$return = implode( ', ', $return );
		}
		
		if( !empty( $this->photo ) ) {
			foreach( $this->photo AS $tphoto ) {
				$ttab = '`pc'. $this->rc() . '`';
				$img_order = ( $tphoto[0] === 'albumthumb' ) ? $ttab . '.`albumthumb` DESC, '. $ttab .'.`created_on` DESC' :  $ttab.'.`created_on` DESC';
				
				$return .= ', ( SELECT GROUP_CONCAT( CONCAT_WS( ".", `id` , `ext` )  ORDER BY '. $img_order. ' )
									AS `photo` FROM `'. $this->table[0] . $tphoto[1] . '_crop_photos` '. $ttab .'
									WHERE `'. $this->table[1] .  '`.`id`='. $ttab .'.`album_id` ) AS `images'. $tphoto[1] .'`';
			}
		}
		
		if( isset( $this->where ) && !empty( $this->where ) ) {
			$where = array();
			
			$whereRC = 1;
			
			foreach( $this->where AS $rule ) {
				if( $whereRC == count( $this->where ) ) {
					$operator = '';
				} else {
					$operator = $rule[3];
					$whereRC++;
				}
				
				$where[] = implode( '.', $rule[0] ) . $rule[1] . $rule[2] . $operator;
				
			}
			
			$where = ' WHERE '.  implode( ' ', $where );
		}
		
		
		$query = 'SELECT '. $distinct . $return .' FROM `'. $this->table[0] .'` `'. $this->table[1] . '` '. $join . $where . $group . $order . $limit . ';';
		$this->check( $query . 'SELECT' );
		
		if( $data = $this->query( $query ) ) {
			return $data;
		} else {
			$this->log_set( 'get(): geen data ontvangen' );
			return false;
		}
		
	}
	
	protected function set( $query ) {
		if( strpos( $query, 'INSERT INTO ' ) === 0 ) {
			return $this->query( $query, 'INSERT' );
			
		}

		return false;
		
	}
	
	protected function del( $query ) {
		if( strpos( $query, 'DELETE FROM `' ) === 0 ) {
			return $this->query( $query, 'DELETE' );
			
		}

		return false;
		
	}
	
	function update( $query ) {
		if( strpos( $query, 'UPDATE' ) === 0 ) {
			return $this->query( $query, 'UPDATE' );
			
		}

		return false;
	}
	
	function query( $query, $type = 'SELECT' ) {
		$this->data = null;
		
		if( md5( $query . $type ) === $this->hash ) $query = trim( $query );
		else return false;
		
		$pdo = $this->pdo;

		if( isset( $q ) ) unset( $q );
		if( isset( $u ) ) unset( $u );
		
		try {
			
			switch( strtoupper( $type ) ) {
				case 'SELECT':
					$q = $pdo->prepare( $query );
					$q->execute();
					$this->data = $q->fetchAll( PDO::FETCH_OBJ );
					$R = $this->data;
					break;
				case 'INSERT':
					$u = $this->pdo->prepare( $query );
					$R = $u->execute();
					break;
				case 'UPDATE':
					$u = $this->pdo->prepare( $query );
					$R = $u->execute();
					break;
				case 'DELETE':
					$u = $this->pdo->prepare( $query );
					$R = $u->execute();
					break;
			}
			
			
			unset( $q, $pdo );
			return $R;
			
		} catch( PDOException $ex ) {
			return $ex->getMessage();
		} catch( Exception $ex ) {
			return $ex->getMessage();
		}
		
		return true;
	}
	
	function reset() {
		$this->order( null );
		$this->where( null );
	}
	
	function check( $input ) {
		$this->hash = md5( $input );
		return true;
	}
	
	private function secure( $array ) {
		$newAr = [];
		
		foreach( $array AS $k => $v ) {
			if( !is_array( $v ) ) {
					if( $v !== '*' ) $v = '`' . $v . '`';
					$newAr[$k] = $v;
			} else {
				foreach( $v AS $ke => $va ) {
					if( $va !== '*' ) $va = '`' . $va . '`';
					$newAr[$k][$ke] = '`'. $va . '`';
				}
			}
		}
		
		return $newAr;
	}
	
	private function rc() {
		$rc = $this->rc;
		$this->rc++;
		return $rc;
	}
	
	function generate_url_from_text( $strText ) {
		$strText = preg_replace('/[^A-Za-z0-9-]/', ' ', $strText);
		$strText = preg_replace('/ +/', ' ', $strText);
		$strText = preg_replace('/ amp/', '-en', $strText);
		$strText = trim($strText);
		$strText = str_replace(' ', '-', $strText);
		$strText = preg_replace('/-+/', '-', $strText);
		$strText = strtolower($strText);
		return $strText;

	}
}