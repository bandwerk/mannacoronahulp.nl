<?php
date_default_timezone_set('UTC');

require_once 'class.coronamail.php';

class CoronaClient {
    public $id,
        $voornaam,
        $achternaam,
        $telefoon,
        $email,
        $adres,
        $postcode,
        $plaats,
        $inhoud,
        $datum_aanvraag,
        $laatst_bewerkt,
        $hash,
        $status;
    public $currentRequest;

    private $hulpvraag = [];
    public $adminmail = "info@mannacoronahulp.nl";

    function __construct() {
    }

    public function NewClient($voornaam, $achternaam, $telefoon, $email, $adres, $postcode, $plaats, $inhoud, $datum_aanvraag = null, $laatst_bewerkt = null, $hash = null, $status = 1) {
        $this->voornaam = $voornaam;
        $this->achternaam = $achternaam;
        $this->telefoon = $telefoon;
        $this->email = $email;
        $this->adres = $adres;
        $this->postcode = $postcode;
        $this->plaats = $plaats;
        $this->inhoud = $inhoud;
        $this->datum_aanvraag = $datum_aanvraag ?? date("Y-m-d H:i:s");
        $this->laatst_bewerkt = $laatst_bewerkt ?? date("Y-m-d H:i:s");
        $this->hash = $hash ?? null;
        $this->status = $status;
    }

    public function SaveAsNew() {
        global $db;

        if (count($this->hulpvraag) == 0) {
            return;
        }

        try {
            foreach ($this->hulpvraag as $hulpvraag_id) {
                $insertNew = $db->prepare("INSERT INTO `hulpaanvraag` (`voornaam`, `achternaam`, `telefoon`, `email`, `adres`, `postcode`, `plaats`, `inhoud`, `hulpvraag_id`, `datum_aanvraag`, `laatst_bewerkt`, `hash`, `status`, `sort`)
                                           VALUES (:voornaam, :achternaam, :telefoon, :email, :adres, :postcode, :plaats, :inhoud, :hulpvraag_id, NOW(), NOW(), :hash, :status, :sort);");
                $this->hash = $this->GenerateHash($hulpvraag_id);
                $insertNew->bindValue(":hulpvraag_id", $hulpvraag_id, PDO::PARAM_INT);
                $insertNew->bindValue(":sort", $this->GetNewSort(), PDO::PARAM_INT);
                if ($this->Save($insertNew)) {
                    $this->id = $db->lastInsertId();
                }
            }

            $this->SendBevestiging();
            $this->SendNotification();
            return true;

        } catch (PDOException $e) {
            die("Could not save" . $e->getMessage());
        }
    }

    public function SaveChanges() {
        global $db;

        if (!isset($this->id)) {
            $this->SaveAsNew();
            return;
        }

        try {
            $insertNew = $db->prepare("UPDATE `hulpaanvraag`
                                        SET `voornaam` = :voornaam, `achternaam` = :achternaam, `telefoon` = :telefoon, `email` = :email, `adres` = :adres, `postcode` = :postcode, `plaats` = :plaats, `inhoud` = :inhoud, `laatst_bewerkt` = NOW(), `hash` = :hash, `status` = :status
                                        WHERE `id` = :id");

            $insertNew->bindValue(':id', $this->id, PDO::PARAM_INT);
            $this->Save($insertNew);
        } catch (PDOException $e) {
            die("Could not save");
        }
    }

    private function Save($query) {
        $query->bindValue(':voornaam', $this->voornaam, PDO::PARAM_STR);
        $query->bindValue(':achternaam', $this->achternaam, PDO::PARAM_STR);
        $query->bindValue(':telefoon', $this->telefoon, PDO::PARAM_STR);
        $query->bindValue(':email', $this->email, PDO::PARAM_STR);
        $query->bindValue(':adres', $this->adres, PDO::PARAM_STR);
        $query->bindValue(':postcode', $this->postcode, PDO::PARAM_STR);
        $query->bindValue(':plaats', $this->plaats, PDO::PARAM_STR);
        $query->bindValue(':inhoud', $this->inhoud, PDO::PARAM_STR);
        $query->bindValue(':hash', $this->hash, PDO::PARAM_STR);
        $query->bindValue(':status', $this->status, PDO::PARAM_INT);

        if ($query->execute()) {
            return true;
        }
        return false;
    }

    public function LoadByHash($hash) {
        global $db;

        try {
            $getByHash = $db->prepare("SELECT * FROM `hulpaanvraag` WHERE `hash` = :hash LIMIT 1");
            $getByHash->bindValue(':hash', htmlentities($hash), PDO::PARAM_STR);
            if ($getByHash->execute() && $getByHash->rowCount() == 1) {
                $client = $getByHash->fetch(PDO::FETCH_OBJ);
                $this->id = $client->id;
                $this->currentRequest = $client->hulpvraag_id;
                $this->newFromObject($client);
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die("Unable to get client");
        }
    }

    public function LoadById($id) {
        global $db;

        try {
            $getById = $db->prepare("SELECT * FROM `hulpaanvraag` WHERE `id` = :id LIMIT 1");
            $getById->bindValue(':id', htmlentities($id), PDO::PARAM_STR);
            if ($getById->execute() && $getById->rowCount() == 1) {
                $client = $getById->fetch(PDO::FETCH_OBJ);
                $this->id = $client->id;
                $this->currentRequest = $client->hulpvraag_id;
                $this->NewFromObject($client);
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die("Unable to get client");
        }
    }

    private function NewFromObject($client) {
        $this->NewClient(
            $client->voornaam,
            $client->achternaam,
            $client->telefoon,
            $client->email,
            $client->adres,
            $client->postcode,
            $client->plaats,
            $client->inhoud,
            $client->datum_aanvraag,
            $client->laatst_bewerkt,
            $client->hash,
            $client->status
        );
    }

    private function GetNewSort() {
        global $db;
        try {
            $getSort = $db->prepare('SELECT MAX(`sort`) + 1 FROM `hulpaanvraag`');
            if ($getSort->execute() && $getSort->rowCount() == 1) {
                return $getSort->fetchColumn();
            }
        } catch (PDOException $e) {
            die('Could not get max sort');
        }
    }

    private function GenerateHash($hulpvraag_id = 0) {
        $this->hash = hash('ripemd160', $this->id . $this->achternaam . $this->postcode . $this->datum_aanvraag . $hulpvraag_id);
        return $this->hash;
    }

    public function PrintData() {
        echo "<table class='table table-striped'>";
        $this->PrintRow('naam', $this->voornaam);
        $this->PrintRow('achternaam', $this->achternaam);
        $this->PrintRow('telefoon', $this->telefoon);
        $this->PrintRow('email', $this->email);
        $this->PrintRow('adres', $this->adres);
        $this->PrintRow('postcode', $this->postcode);
        $this->PrintRow('plaats', $this->plaats);
        $this->PrintRow('inhoud', $this->inhoud);
        echo "</table>";
    }

    private function PrintRow($key, $value) {
        $key = ucfirst($key);
        if ($key == "Telefoon") {
            echo "<tr>
                <td>$key</td>
                <td><a href='tel:" . $value . "'>$value</a></td>
              </tr>\n";

        } else if ($key == "Email") {

            echo "<tr>
                <td>$key</td>
                <td><a href='mailto:" . $value . "'>$value</a></td>
              </tr>\n";
        } else {
            echo "<tr>
                <td>$key</td>
                <td>$value</td>
              </tr>\n";
        }
    }

    public function GetSelectedCaregiver() {
        global $db;
        try {
            $getCareGiver = $db->prepare('SELECT `id` FROM `hulpverleners` WHERE `id` = (SELECT `hulpverlener_id` FROM `hulpaanvraag_hulpverlener` WHERE `hulpaanvraag_id` = :hulpaanvraag)');
            $getCareGiver->bindValue(":hulpaanvraag", $this->id, PDO::PARAM_INT);

            if ($getCareGiver->execute() && $getCareGiver->rowCount() == 1) {
                return $getCareGiver->fetchColumn();
            }
            return -1;
        } catch (PDOException $e) {
            die('Could not get max sort');
        }
    }

    public function GetCaregivers() {
        global $db;
        try {
            $getCareGiver = $db->prepare('SELECT `id`, `voornaam`, `achternaam`, `telefoon` FROM `hulpverleners` WHERE `id` IN (SELECT `hulpverlener_id` FROM `hulpverleners_hulpvraag` WHERE `hulpvraag_id` = :hulpvraag) AND `aktief` = "Ja"');
            $getCareGiver->bindValue(":hulpvraag", $this->currentRequest, PDO::PARAM_INT);

            if ($getCareGiver->execute() && $getCareGiver->rowCount() >= 1) {
                return $getCareGiver->fetchAll(PDO::FETCH_OBJ);
            }
        } catch (PDOException $e) {
            die('Could not get max sort');
        }
    }

    private function SendBevestiging() {
        $content = "Uw hulpaanvraag is in goede orde ontvangen onder dossiernummer $this->id!<br>Onze medewerkers zullen uw vraag zo snel mogelijk koppelen aan een hulpverlener. Zodra we een geschikte hulpverlener voor u hebben gevonden krijgt u direct bericht van ons via de e-mail.<br><br>
Met vriendelijke groet, <br> Het corona hulpteam ";
        $mail = new CoronaMail($this->voornaam . " " . $this->achternaam, $this->email, "Uw aanvraag is ontvangen", $content);
        $mail->Send();
    }

    private function SendNotification() {
        // Notificatie naar admin
        $mail = new CoronaMail($this->voornaam . " " . $this->achternaam, $this->adminmail, "Nieuwe hulpaanvraag", "Bevestig deze aanvraag.");
        $mail->Send();

        // Notificaties naar sleutelfiguren
        global $db;
        $sleutelFiguren = $db->prepare("SELECT `g`.`username` FROM `website_logins` `g` WHERE `g`.`id` IN (SELECT `gebruiker_id` FROM `gebruiker_plaatsen` WHERE `plaatsnaam_id` = (SELECT `id` FROM `plaatsen` WHERE `plaatsnaam` = :plaats));");
        $sleutelFiguren->bindValue(":plaats", $this->plaats, PDO::PARAM_STR);
        if($sleutelFiguren->execute() && $sleutelFiguren->rowCount() > 1) {
            $content = "Er staat een nieuwe hulpvraag klaar om te worden verwerkt. Log in op <a href='".SITELINK."/beheer'>Manna Coronahulp</a> om de aanvraag te bekijken.";

            while($email = $sleutelFiguren->fetch(PDO::FETCH_OBJ)) {
                $mail = new CoronaMail("Manna Sleutelfiguur", $email->username, "Er staat een nieuwe hulpaanvraag klaar in uw plaats!", $content);
                $mail->Send();
            }
        }
    }

    public function SetRequestTypes($hulpvragen) {
        $this->hulpvraag = $hulpvragen;
    }

    public function GetRequestName() {
        if (!isset($this->currentRequest)) {
            return '';
        }

        global $db;
        try {
            $request = $db->prepare("SELECT `vraag` FROM `hulpvraag` WHERE `id` = :id");
            $request->bindValue(":id", $this->currentRequest, PDO::PARAM_INT);

            if ($request->execute() && $request->rowCount() == 1) {
                $res = $request->fetch(PDO::FETCH_OBJ);
                return $res->vraag;
            } else {
                return "";
            }
        } catch (PDOException $e) {
            return "";
        }
    }

    public function SetCompleted() {
        $this->status = 3;
        $this->SaveChanges();
    }

    public static function GetClients($filter = false) {
        global $db;
        try {
            $in = "('".implode("', '", $_SESSION['plaatsnamen'])."')";
            if(is_numeric($filter)) {
                $request = $db->prepare("SELECT `id` FROM `hulpaanvraag` WHERE `status` = :status AND `plaats` IN ".$in." ORDER BY `status` ASC, `datum_aanvraag` DESC");
                $request->bindValue(":status", $filter, PDO::PARAM_INT);
            } else {
                $request = $db->prepare("SELECT `id` FROM `hulpaanvraag` WHERE `plaats` IN ".$in." ORDER BY `status` ASC, `datum_aanvraag` DESC");
            }
            if ($request->execute() && $request->rowCount() >= 1) {
                $res = $request->fetchAll(PDO::FETCH_OBJ);
                $objects = [];
                foreach ($res as $cl) {
                    $client = new CoronaClient();
                    $client->LoadById($cl->id);
                    array_push($objects, $client);
                }
                return $objects;
            } else {
                return [];
            }
        } catch (PDOException $e) {
            die('Unable to get clients' . $e->getMessage());
        }
    }

    public static function GetStatuses()
    {
        global $db;
        try {
            $request = $db->prepare("SELECT `id`, `titel` FROM `status` ORDER BY `sort` ASC");
            if ($request->execute() && $request->rowCount() >= 1) {
                return $request->fetchAll(PDO::FETCH_OBJ);
            } else {
                return [];
            }
        } catch (PDOException $e) {
            die('Unable to get statuses'.$e->getMessage());
        }
    }

    public function SetCaregiver($value)
    {
        global $db;
        if($this->GetSelectedCaregiver() == -1) {
            //INSERT
            try {
                $setCaregiver = $db->prepare("INSERT INTO `hulpaanvraag_hulpverlener` (`hulpaanvraag_id`, `hulpverlener_id`) VALUES (:aanvraag, :verlener)");
                $setCaregiver->bindValue(":aanvraag", $this->id, PDO::PARAM_INT);
                $setCaregiver->bindValue(":verlener", $value, PDO::PARAM_INT);
                if($setCaregiver->execute() && $setCaregiver->rowCount() == 1) {
                    return true;
                }
                return false;
            } catch (PDOException $e) {
                die();
            }
        } else {
            //UPDATE
            try {
                $setCaregiver = $db->prepare("UPDATE `hulpaanvraag_hulpverlener` SET `hulpverlener_id` = :verlener WHERE `hulpaanvraag_id = :aanvraag`");
                $setCaregiver->bindValue(":aanvraag", $this->id, PDO::PARAM_INT);
                $setCaregiver->bindValue(":verlener", $value, PDO::PARAM_INT);
                if($setCaregiver->execute() && $setCaregiver->rowCount() == 1) {
                    return true;
                }
                return false;
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
    }
}