<?php

class order extends query {
	protected 
		$methode = [ 'Ideal' 	=> 10 /*,
							'Paypal' => 138,
							'Bancontact' => 436,
							'Giropay' => 694*/
	];
	private 
		$payNL = [ 
			/*
			// Ten Brinke
			
						'merchant_code' => 'M-3190-4620',
						'serviceId' => 'SL-7727-7001',
						'token' 	=> '4c7a62bc5abc8369e747677b6d28b345e6afe828'
						*/
			//Bandwerk test account 
					'merchant_code' => 'M-1817-3220',
					'serviceId' => 'SL-8716-3540',
					'token' 	=> '6160da117382dac66b2dbc6b10e7ba942c68c2a9'
			];
	
	protected 
		$allowed = [ 'shop_winkelwagen', 'acc_bestellingen', 'acc_bestelregels' ],
		$cart,
		$bijproduct,
		$size,
		$cartPrijs;
	
	public 
		$kortingsCodes = false,
		$totaal,
		$korting,
		$bezorging = false,
		$bezorglokatie = null,
		$toeslag = false,
		$loc,
		$orderStatus,
		$orderStatusOrder = [];
		
	private 
		$locById;
	
	function __construct( PDO $pdo=null, $table='acc_bestellingen', $afronden=null ) {
		if( !$pdo ) {
			global $db;
			if( $db instanceof PDO ) {
				$pdo = $db;
				
			} else {
				die( 'Error: Order needs database connection' );
			}
		}
		$loc = [];
		$locById = [];
		
		if( $afronden ) {
			$this->payNL[ 'finishUrl' ] = $afronden;
		}
		
		$lQ = $pdo->prepare( 'SELECT *,
	( SELECT GROUP_CONCAT( `datum` ORDER BY `datum`, `tijdstip` ASC ) FROM `producten_adressen_datum` WHERE `locatie`=`a`.`id` )
    	AS `datum`,
    ( SELECT GROUP_CONCAT( `tijdstip` ORDER BY `datum`, `tijdstip` ASC ) FROM `producten_adressen_datum` WHERE `locatie`=`a`.`id` )
    	AS `tijdstip`,
    ( SELECT GROUP_CONCAT( `uiterlijk_tijdstip_om_te_bestellen`  ORDER BY `datum`, `tijdstip` ASC ) FROM `producten_adressen_datum` WHERE `locatie`=`a`.`id` )
    	AS `uiterlijk`
		
		FROM `producten_adressen` `a`
			WHERE `aktief`="ja" ORDER BY `plaats`, `titel` ASC;' );
		
		try {
			$lQ->execute();
			$lQ = $lQ->fetchAll( PDO::FETCH_ASSOC );
		} catch( PDOException $ex ) {
			die(  $ex->getCode() . ': ' . $ex->getMessage() );
		}
		
		foreach( $lQ AS $item ) {
			if( !isset( $locById[ $item['id'] ] ) ) $locById[ $item['id'] ] = [];
			if( !isset( $loc[ $item['type_adres'] ] ) ) $loc[ $item['type_adres'] ] = [];
			
			array_walk( $item, function( &$val, $key ) {
				switch( $key ) {
					case 'datum':
					case 'tijdstip':
					case 'uiterlijk':
						$val = explode( ',', $val );
						break;
				}
			});
			
			$loc[ $item['type_adres'] ][] = $item;
			$locById[ $item['id'] ] = $item;
		
		}
		
		$this->loc = $loc;
		$this->locById = $locById;

		try {
			$osQ = 'SELECT `id`, `order_status`, `sort` FROM `acc_order_status` ORDER BY `sort`;';
			$osQ = $pdo->prepare( $osQ );
			if( $osQ->execute() ):
				foreach( $osQ->fetchAll( PDO::FETCH_OBJ ) AS $orderStatus ):
					$this->orderStatus[ $orderStatus->id ] = $orderStatus->order_status;
					$this->orderStatusOrder[] = $orderStatus->id;
			
				endforeach;
			
			endif;
			
		} catch( PDOException $ex ){
			die( $ex->getMessage() );
		}
		
		parent::__construct($pdo, $table);
		
		$this->set_prijsuitzondering_provincie_id(
				isset( $_SESSION['postcode_controle'] ) && isset( $_SESSION['postcode_controle']->prijsuitzondering_provincie_id ) ?
				$_SESSION['postcode_controle']->prijsuitzondering_provincie_id : false
				);
		
		
	}
	
	function set_prijsuitzondering_provincie( string $prijsuitzondering_provincie ) {
		if( $prijsuitzondering_provincie = get_model( 'product/provincie_prijsuitzondering', 'basic', ['where'=>$prijsuitzondering_provincie,'returnAsArray'=>false] ) ) {
			return $this->set_prijsuitzondering_provincie_id( $prijsuitzondering_provincie->provincie_id );
		}
		
		return false;
	}
	
	function set_prijsuitzondering_provincie_id( int $prijsuitzondering_provincie_id ) {
		//Controleren of er actieve kortingscodes bestaan
		$Q = 'SELECT DISTINCT
					`ppro`.`provincie_id`,
					count( `p`.`id` ) AS `aantal`
				FROM
					`producten_prijs_provincie` `ppro`
				
					LEFT JOIN `product` `p`
						ON `ppro`.`provincie_id`=`p`.`id` AND `p`.`aktief`="ja"
				WHERE `ppro`.`provincie_id`='. (int) $prijsuitzondering_provincie_id .'
				HAVING
					`aantal`>0;';
		
		$this->check( $Q . 'SELECT' );
		if( $KC = $this->query( $Q ) ) {
			$this->prijsuitzondering_provincie_id = (int) $KC[0]->provincie_id;
		} else {
			$this->prijsuitzondering_provincie_id = false;
		}
		
		return $this->prijsuitzondering_provincie_id;
	}
	
	function calc_korting( $input, $retour = 'korting' ) {
		$input = $input;
		$output = $input;
		
		if( isset( $this->korting ) && is_array( $this->korting ) ) {
			switch( array_keys( $this->korting )[0] ) {
				case 'procent':
					$output = round( $input - ( ( $input / 100 ) * number_format( $this->korting['procent'], 2 ) ), 2 );
					break;
				case 'euro':
					$output -= number_format( $this->korting['euro'], 2 );
					break;
			}

		}

		switch( $retour ) {
			case 'korting':
					return $input - $output;
				break;
			case 'totaal':
					return $output;
				break;
		}
		
	}
	
	function betaalmethode_get( $id ) {
		$methodes = [];
		foreach( $this->methode AS $naam => $mid ) {
			$methodes[ $mid ] = $naam;
		}
	
		if( array_key_exists( $id, $methodes ) ) return $methodes[ $id ];
		else return $id;
	
	}
	function korting_gebruikt( $id ) {
		if( isset(  $_SESSION['korting_id'] ) ) {
			$Q = 'UPDATE `kortingscodes` SET `aantal_keer_te_gebruiken`=`aantal_keer_te_gebruiken`-1 WHERE `id`="'.  $_SESSION['korting_id'] .'";';
			$this->check( $Q . 'UPDATE' );
			$this->query( $Q, 'UPDATE');
			unset( $_SESSION['korting_id']);
		}
	}
	
	function get_bestelling( $id, $value = '' ) {
		$this->bezorging = 0;
		$this->korting = 0;
		
		$check = 'SELECT `b`.`personalnr`, COUNT( `r`.`id`) AS `aantal`, `b`.`us_gebruikers_id` AS `userid`
				FROM `acc_bestellingen` `b`
					LEFT JOIN `acc_bestelregels` `r` ON
				    	`r`.`acc_bestellingen_id`=`b`.`bestelling_id`
					WHERE `bestelling_id`="'. $id .'"
					ORDER BY `bestelling_id`';
		
		$this->check( $check . 'SELECT');
		$check = $this->query( $check );
		$cartCheck = !( $check[0]->aantal == 0 );

		if( !$cartCheck ) {
			$cat = !empty( $cat ) ?  ' AND `w`.`categorie`="'. str_replace( ' ', '', $cat ) . '"' : '';
			
			$orId = !empty( $check[0]->userid ) && $check[0]->userid > 0 ?
			' AND `w`.`us_gebruikers_id`='. (int) $check[0]->userid : '';
			
			$kortingCase = !empty( $check[0]->userid ) && $check[0]->userid > 0 ?
			'CASE
									WHEN `uk`.`korting` IS NOT NULL
											AND
										`uk`.`korting`>0
											THEN
													( 100 - `uk`.`korting` ) / 100
									ELSE
											1
								END AS `gebruikerkorting`' :
											'1 AS `gebruikerkorting`';
			
			$joinKorting = !empty( $check[0]->userid ) && $check[0]->userid > 0 ?
			' LEFT JOIN `producten_listmember_categorieen` `pl`
				ON `tp`.`id`=`pl`.`product`
			
			LEFT JOIN `producten_categorie` `pc`
				ON `pl`.`categorie`=`pc`.`id`
		
			LEFT JOIN `us_gebruikers_korting` `uk`
				ON ( `pl`.`categorie`=`uk`.`producten_categorie_id`
						OR `pc`.`parent`=`uk`.`producten_categorie_id` )
				 AND `uk`.`us_gebruikers_id`="'. (int) $check[0]->userid . '"
				 AND `uk`.`us_gebruikers_id`!=0 AND `uk`.`producten_categorie_id`!=0
			
			
			WHERE `tp`.`id` IN (
			 		( SELECT DISTINCT `artikel_id` FROM `shop_winkelwagen`
					 		WHERE ( `personalnr`="'. $check[0]->personalnr.'"'. str_replace( '`w`.', '', $orId ) .' )'. str_replace( '`w`.', '', $cat ) .' ) )
			
			GROUP BY `tp`.`id`' : '' ;
			
			$Query = 'SELECT
				"" AS `id`,
				`w`.`acc_bestellingen_id`,
				`w`.`categorie`,
				`w`.`korting_id`,
				`w`.`korting_type`,
				`w`.`aantal`,
				`w`.`maat`,
				`w`.`artikel_id`,
				`w`.`artikel`,
				`w`.`bijproduct_id`,
				`w`.`bijproduct`,
				`w`.`prijs_normaal`,
				`w`.`prijs_aanbieding`,
				`w`.`datum`,
				`w`.`personalnr`,
				`w`.`btw_id`,
				`w`.`prijs_normaal`,
				`w`.`prijs_exc`
			FROM (
				SELECT
					`a`.`bestelling_id` AS `acc_bestellingen_id`,
					`ww`.`categorie`,
					`ww`.`korting_id`,
					`ww`.`korting_type`,
					`ww`.`aantal`,
					`ww`.`maat`,
					`ww`.`artikel_id`,
					`p`.`titel` AS `artikel`,
					`ww`.`bijproduct_id`,
					CASE
							WHEN `b`.`titel` IS NOT NULL THEN `b`.`titel`
							ELSE ""
					END AS `bijproduct`,
					CASE `ww`.`categorie`
							WHEN "bijproduct" THEN `b`.`prijs_normaal`
							ELSE `p`.`prijs_normaal`
					END AS `prijs`,
					CASE `ww`.`categorie`
							WHEN "bijproduct" THEN `b`.`prijs_aanbieding`
							ELSE `p`.`prijs_aanbieding`
					END AS `prijs_aanbieding`,
					NOW() AS `datum`,
					`ww`.`personalnr`,
					`p`.`btw` AS `btw_id`,
					round( SUM( `ww`.`aantal` ) * `p`.`prijs_normaal` * `p`.`gebruikerkorting` * (1+( `p`.`percentage`/100) ), 2 )
						AS `prijs`,
					round( SUM( `ww`.`aantal` ) * `p`.`prijs_normaal` * `p`.`gebruikerkorting` , 2 )
						AS `prijs_exc`,
					`ww`.`besteld`
				FROM `shop_winkelwagen` `ww`
				LEFT JOIN `acc_bestellingen` `a`
					ON `a`.`personalnr`=`ww`.`personalnr`
				LEFT JOIN
					( SELECT
						'. $kortingCase . ',
						`tp`.`id`,
						`tp`.`btw`,
						`tp`.`titel`,
						`tp`.`prijs_normaal`,
						`tp`.`prijs_aanbieding`,
						( CASE `tt`.`type`
								WHEN "Inclusief"
									THEN
								 ( CASE
									WHEN `tp`.`prijs_aanbieding` > 0
										THEN `tp`.`prijs_aanbieding`
											ELSE `tp`.`prijs_normaal`
								END )
											-
								( CASE
									WHEN `tp`.`prijs_aanbieding` > 0
										THEN `tp`.`prijs_aanbieding`
											ELSE `tp`.`prijs_normaal`
								END ) * `tt`.percentage / (100+`tt`.percentage)
							ELSE
								( CASE
									WHEN `tp`.`prijs_aanbieding` > 0
										THEN `tp`.`prijs_aanbieding`
									ELSE `tp`.`prijs_normaal`
								END )
						END ) AS `prijs`,
						"Exclusief" AS `type`,
						`tt`.`percentage`
				
						FROM `product` `tp`
							LEFT JOIN `acc_btw` `tt`
								ON `tp`.`btw`=`tt`.`id`
							'. $joinKorting . '
						) `p`
					ON `ww`.`artikel_id`=`p`.`id`
		
					LEFT JOIN
						( SELECT
							*,
							CASE
								WHEN `prijs_aanbieding` > 0
								THEN `prijs_aanbieding`
								ELSE `prijs_normaal`
							END AS `prijs`
							FROM `bijproducten`) `b`
								ON `ww`.`bijproduct_id`=`b`.`id`
					LEFT JOIN
						( SELECT
								`id`,
								`bezorgkosten` AS `prijs`
								FROM `producten_adressen`) `l` ON
						`ww`.`bezorging_id`=`l`.`id`
					LEFT JOIN `kortingscodes` `kc`
						ON `ww`.`korting_id`=`kc`.`id`
					LEFT JOIN `acc_btw` `t`
						ON `p`.`btw`=`t`.`id`
					WHERE `ww`.`personalnr`="'. $check[0]->personalnr .'"
						GROUP BY
							`categorie`, `artikel_id`, `bijproduct_id`, `korting_id`
			
				
			
						) `w`
						WHERE `w`.`personalnr`="'. $check[0]->personalnr .'"
							AND (
									( SELECT
											count( `id` )
											FROM `product`
											WHERE `id`=`w`.`artikel_id`
											AND `uitverkocht`="nee"
											AND `aktief`="ja"
											) > 0
									OR
									`w`.`categorie`!="product"
									) AND `besteld`="0"
											GROUP BY `categorie`,
											`artikel_id`,
											`bijproduct_id`,
											`korting_id`
											ORDER BY
											`categorie` DESC,
											`artikel_id` ASC,
											`bijproduct_id` ASC,
											`prijs` ASC;';
			
			$ud = 'INSERT INTO `acc_bestelregels` '. $Query ;
			
			$this->check( $ud . 'INSERT' );

			if( $this->query( $ud, 'INSERT' ) ) {
				$cartCheck = true;
			}
						
		}
		
		if( $cartCheck ) {

			if( empty( $cat ) || $cat == 'bezorging' ) $this->bezorging = 0;
			if( empty( $cat ) || $cat == 'korting' ) $this->korting = 0;
			
			$cat = !empty( $cat ) ?  ' AND `w`.`categorie`="'. str_replace( ' ', '', $cat ) . '"' : '';
			$orId = !empty( $_SESSION['loggedin']['id'] ) && $_SESSION['loggedin']['id']> 0 ? ' AND `us_gebruikers_id`='. (int) $_SESSION['loggedin']['id'] : '';
			
			
			$query = 'SELECT
				`w`.*,
				`w`.`artikel_id` AS `id`,
				SUM( `w`.`aantal` ) AS `aantal`,
			
				CASE
					WHEN `bijproduct_id` > 0 THEN `b`.`prijs_normaal`
			        ELSE `p`.`prijs_normaal`
				END AS `prijs`,
				`w`.`prijs_normaal` AS `prijs_inc`,
		
				`kc`.`code`,
				`kc`.`opmerking` AS `korting_titel`,
				`kc`.`minimaal_bestellingbedrag` AS `minimum`
			
				FROM `acc_bestelregels` `w`
						LEFT JOIN
							( SELECT
								`id`,
								CASE
									WHEN `prijs_aanbieding` > 0
										THEN `prijs_aanbieding`
									ELSE `prijs_normaal`
								END AS `prijs`
							FROM `product`) `p`
								ON `w`.`artikel_id`=`p`.`id`
						LEFT JOIN
							( SELECT
								`id`,
								CASE
									WHEN `prijs_aanbieding` > 0
								THEN `prijs_aanbieding`
									ELSE `prijs_normaal`
								END AS `prijs`
							FROM `bijproducten`) `b`
								ON `w`.`bijproduct_id`=`b`.`id`
						
						LEFT JOIN `kortingscodes` `kc` ON `w`.`korting_id`=`kc`.`id`
				WHERE `acc_bestellingen_id`="'. (int) $id.'"
				GROUP BY `w`.`categorie`, `w`.`artikel_id`, `w`.`bijproduct_id`, `w`.`korting_id`
				ORDER BY
					`categorie` DESC,
					`artikel_id` ASC,
					`prijs` ASC;';

			$this->check( $query . 'SELECT' );
			
			foreach( $this->query( $query ) AS $item ) {
				if( is_array( $item ) ){
					switch( $item['categorie'] ) {
						case 'product':
							$this->cart[$item['id']] = $item['aantal'];
							$this->cartPrijs[ $item['id'] ] = isset( $this->cartPrijs[ $item['id'] ] ) ? $this->cartPrijs[ $item['id'] ] + ( $item['aantal'] * $item['prijs'] ) : $item['aantal'] * $item['prijs'];
							if( $item['maat'] ) $this->size[$item['id']] = $item['maat'];
							break;
						case 'korting':
							$this->korting = [ $item['korting_type'] => $item['aantal'] , 'id' => $item['korting_id'], 'titel' => $item['korting_titel'], 'minimum' => $item['minimum'], 'code' => $item['code'] ];
							break;
						case 'bezorging':
							$this->bezorging = !isset( $this->toeslag ) || !$this->toeslag ? $item['aantal'] : 0;
							break;
						case 'bijproduct':
							if( !isset( $this->bijproduct[ $item['id'] ] ) ) $this->bijproduct[ $item['id'] ] = [];
							$this->cartPrijs[ $item['id'] ] = isset( $this->cartPrijs[ $item['id'] ] ) ? $this->cartPrijs[ $item['id'] ] + ( $item['aantal'] * $item['prijs'] ) : $item['aantal'] * $item['prijs'];
							$this->bijproduct[ $item['id'] ][ $item['bijproduct_id'] ] = $item['aantal'];
							break;
						case 'toeslag':
							if( ( isset( $item['prijs_exc'] ) || isset( $item['prijs'] ) ) && !is_object( $this->toeslag ) ):
								$this->toeslag = new stdClass();
								$this->toeslag->inc = 0;
								$this->toeslag->exc = 0;
								$this->bezorging = 0;
								
							endif;
							
							if( isset( $this->toeslag->exc ) && isset( $item['prijs_exc'] ) && $item['prijs_exc'] > $this->toeslag->exc ):
								$this->toeslag->exc = number_format( $item['prijs_exc'] , 2, '.', '' );
								$this->toeslag->inc = number_format( $item['prijs'], 2, '.', '' );
								$this->toeslag->titel = $item['artikel'];
								
							endif;
								
							break;
						
					}
				} elseif( is_object( $item ) ) {
					switch( $item->categorie ) {
						case 'product':
							$this->cart[$item->id] = $item->aantal;
							$this->cartPrijs[ $item->id ] = isset( $this->cartPrijs[ $item->id ] ) ? $this->cartPrijs[ $item->id ] + ( $item->aantal * $item->prijs ) : $item->aantal * $item->prijs;
							if( $item->maat ) $this->size[$item->id] = $item->maat;
							break;
						case 'korting':
							$this->korting = [ $item->korting_type => $item->aantal , 'id' => $item->korting_id, 'titel' => $item->korting_titel, 'minimum' => $item->minimum, 'code' => $item->code ];
							break;
						case 'bezorging':
							$this->bezorging = !isset( $this->toeslag ) || !$this->toeslag ? $item->aantal : 0;
							break;
						case 'bijproduct':
							if( !isset( $this->bijproduct[ $item->id ] ) ) $this->bijproduct[ $item->id ] = [];
							$this->cartPrijs[ $item->id ] = isset( $this->cartPrijs[ $item->id ] ) ? $this->cartPrijs[ $item->id ] + ( $item->aantal * $item->prijs ) : $item->aantal * $item->prijs;
							$this->bijproduct[ $item->id ][ $item->bijproduct_id ] = $item->aantal;
							break;
						case 'toeslag':
							if( ( isset( $item->prijs_exc ) || isset( $item->prijs ) ) && !is_object( $this->toeslag ) ):
								$this->toeslag = new stdClass();
								$this->toeslag->inc = 0;
								$this->toeslag->exc = 0;
								$this->bezorging = 0;
							endif;
								
							if( isset( $this->toeslag->exc ) && isset( $item->prijs_exc ) && $item->prijs_exc > $this->toeslag->exc ):
								$this->toeslag->exc = number_format( $item->prijs_exc , 2, '.', '' );
								$this->toeslag->inc = number_format( $item->prijs_inc, 2, '.', '' );
								$this->toeslag->titel = $item->artikel;
							endif;
							
							break;
							
						
					}
					
				}
			}
	
			if( !isset( $this->totaal ) || !is_object( $this->totaal ) ) $this->totaal = new stdClass();
			$this->totaal->aantal = is_array( $this->cart ) ? array_sum( $this->cart ) : 0;
			$this->totaal->bedrag = is_array( $this->cartPrijs ) ? array_sum( $this->cartPrijs ) : 0 ;
				
			
			switch( $value ) {
				case 'amount':
				case 'totaal':
				case 'aantal':
				case 'overzicht':
					if( is_array( $this->cart) && !empty( $this->cart ) ) {
						if( isset( $this->korting ) && is_array( $this->korting ) && isset( $this->korting['titel'] ) ) {
								$korting = $this->calc_korting( $this->totaal->bedrag );
								$this->totaal->korting = $korting > $this->totaal->bedrag ? $this->totaal->bedrag : $korting;
	
								
						} else {
							$this->korting = null;
							$this->totaal->korting = 0;
						}
						
						return [ 
								$this->totaal->aantal, 
								$this->totaal->bedrag, 
								$this->bezorging, 
								$this->totaal->korting, 
								$this->korting,
								$this->toeslag
						];
					} else return false;
					break;
				default:
					$query = 'SELECT * FROM `'. $this->table[0].'` WHERE `bestelling_id`='. $id .';';
					$this->check( $query . 'SELECT' );
					if( $bestelinfo = $this->query( $query, 'SELECT' ) ) {
						$return = [];
						$return['bestelinfo'] = $bestelinfo;
						$return['bezorging'] = $this->bezorging;
						$return['bijproduct'] = $this->bijproduct;
						$return['korting'] = !empty( $this->korting ) ? $this->korting : NULL;
						$return['cart'] = $this->cart;
						$return['cartPrijs'] = $this->cartPrijs;
						$return['toeslag'] = $this->toeslag;
						
					} else $return = false;
					
					return $return;
				break;
			}
		}
		return false;
	}
	
	function get_bezorging( $type = null ) {
		if( is_numeric( $type ) ) $loc = $this->locById[ $type ];
		else $loc = $type ? $this->loc[ $type ] : $this->loc;
		
		return $loc;
	}
	
	function get_bezorging_veld( $field, $type = null ) {
		if( isset( $this->locById ) && !empty( $this->locById ) ) {
			if( is_numeric( $type ) ) $loc = $this->locById[ $type ];
			$loc = $type ? $this->loc[ $type ] : $this->loc;
			
			$return = [];
			foreach( $loc AS $r ) {
				if( is_array( $field ) ):
					$r_data = [];
				
					foreach( $field AS $f ) {
						$r_data[ $f ] = $r[$f];
					}
					
					$return[] = $r_data;
				else:
					if( isset( $r[$field] ) ) $return[] = $r[ $field ];
				endif;
			}
	
			sort( $return );
			
			return $return;
		}
	}

	function reset_bestel( $sessionData ) {
		$pnr = $sessionData->id;
		
		$Q = 'DELETE FROM `acc_bestelregels` WHERE `acc_bestelregels`.`personalnr`="'. $pnr .'";';
		$this->check( $Q . 'DELETE' );
		if( $this->del( $Q ) ) return true;
		return false;
		
		//winkelwagen naar bestelregel kopieeren
	}
	
	function bestel( $fieldVal, $sessionData ) {
		// 		$bestelling['order_status_id']			= ;
		$update = !isset( $_SESSION['bestelling']->updated ) ? false : $_SESSION['bestelling']->updated;
		$pnr = $sessionData->id;
		$return = [];
		
		$invoice = $this->order_get_no( implode( '', $fieldVal ), 12, 'acc_bestellingen' );
		
		$fv = [];
		$fv[] = '`personalnr`="'. $pnr .'"';
		$fv[] = '`invoice_no`="'. $invoice .'"';
		$fv[] = '`datum_aangemaakt`=NOW()';
		$fv[] = '`datum_aangepast`=NOW()';
		
		
		if( isset( $fieldVal['id'] ) && isset( $fieldVal['invoice_no'] ) ) {
			$update = true;
			$fv[] = '`bestelling_id`='. $fieldVal['id'];
			$fv[] = '`invoice_no`='. $fieldVal['invoice_no'];
		} else {
			foreach( $fieldVal AS $f => $v ) {
				$fv[] = '`'. $f .'`="'. $v .'"';
			}
		}
		
		$Q = 'INSERT INTO `acc_bestellingen`
				SET ' . implode( ', ', $fv ) . '
						
				ON DUPLICATE KEY UPDATE 
						`betaling_method`=VALUES(`betaling_method`),
						`aflever_method`=VALUES(`aflever_method`),
						`levering_tijdstip`=VALUES(`levering_tijdstip`),
						`datum_aangepast`=VALUES(`datum_aangepast`),
						`personalnr`=VALUES(`personalnr`),
						`totaal`=VALUES(`totaal`);';

		$this->check( $Q . 'INSERT');
		$r = $this->set( $Q );
		
		if( $r ) unset( $_SESSION['bestelling'] );

		$cat = !empty( $cat ) ?  ' AND `w`.`categorie`="'. str_replace( ' ', '', $cat ) . '"' : '';
		
		$orId = !empty( $_SESSION['loggedin']['id'] ) && $_SESSION['loggedin']['id']> 0 ?
			' AND `w`.`us_gebruikers_id`='. (int) $_SESSION['loggedin']['id'] : '';
		
		$kortingCase = !empty( $_SESSION['loggedin']['id'] ) && $_SESSION['loggedin']['id']> 0 ?
								'CASE 
									WHEN `uk`.`korting` IS NOT NULL 
											AND
										`uk`.`korting`>0
											THEN
													( 100 - `uk`.`korting` ) / 100 
									ELSE 
											1
								END AS `gebruikerkorting`' :
								'1 AS `gebruikerkorting`';
		
		$joinKorting = !empty( $_SESSION['loggedin']['id'] ) && $_SESSION['loggedin']['id']> 0 ?
			' LEFT JOIN `producten_listmember_categorieen` `pl`
				ON `tp`.`id`=`pl`.`product`
				
			LEFT JOIN `producten_categorie` `pc`
				ON `pl`.`categorie`=`pc`.`id`
			
			LEFT JOIN `us_gebruikers_korting` `uk`
				ON ( `pl`.`categorie`=`uk`.`producten_categorie_id`
						OR `pc`.`parent`=`uk`.`producten_categorie_id` )
				 AND `uk`.`us_gebruikers_id`="'. (int) $_SESSION['loggedin']['id'] . '"
				 AND `uk`.`us_gebruikers_id`!=0 AND `uk`.`producten_categorie_id`!=0	

				
			WHERE `tp`.`id` IN (
			 		( SELECT DISTINCT `artikel_id` FROM `shop_winkelwagen`
					 		WHERE ( `personalnr`="'. session_id() .'"'. str_replace( '`w`.', '', $orId ) .' )'. str_replace( '`w`.', '', $cat ) .' ) )
			 		
			GROUP BY `tp`.`id`' : '' ;
		
		
		// Query om de prijs te achterhalen voor de winkelwagen met lengtetoeslag
		
		$Query = '
		SELECT
				"" AS `id`,
				`w`.`acc_bestellingen_id`,
				`w`.`categorie`,
				`w`.`korting_id`,
				`w`.`korting_type`,
				`w`.`aantal`,
				`w`.`maat`,
				`w`.`artikel_id`,
				`w`.`artikel`,
				`w`.`bijproduct_id`,
				`w`.`bijproduct`,
				`w`.`prijs_normaal`,
				`w`.`prijs_aanbieding`,
				`w`.`datum`,
				`w`.`personalnr`,
				`w`.`btw_id`,
				`w`.`prijs`,
				`w`.`prijs_exc`
			FROM (
				SELECT
					`a`.`bestelling_id` AS `acc_bestellingen_id`,
					`ww`.`categorie`,
					`ww`.`korting_id`,
					`ww`.`korting_type`,
					`ww`.`aantal`,
					`ww`.`maat`,
					`ww`.`artikel_id`,
					`p`.`titel` AS `artikel`,
					`ww`.`bijproduct_id`,
					( CASE
							WHEN `b`.`titel` IS NOT NULL THEN `b`.`titel`
							ELSE ""
					END ) AS `bijproduct`,
					( CASE `ww`.`categorie`
							WHEN "bijproduct" THEN `b`.`prijs_normaal`
							ELSE `p`.`prijs_normaal`
					END ) AS `prijs_normaal`,
					( CASE `ww`.`categorie`
							WHEN "bijproduct" THEN `b`.`prijs_aanbieding`
							ELSE `p`.`prijs_aanbieding`
					END ) AS `prijs_aanbieding`,
					NOW() AS `datum`,
					`ww`.`personalnr`,
					`p`.`btw` AS `btw_id`,
					round( round( SUM( `ww`.`aantal` ) * `p`.`prijs` * `p`.`gebruikerkorting`, 2 ) * (1+( `p`.`percentage`/100) ), 2 )
						AS `prijs`,
					round( SUM( `ww`.`aantal` ) * `p`.`prijs` * `p`.`gebruikerkorting` , 2 )
						AS `prijs_exc`,
					`ww`.`besteld`
				FROM `shop_winkelwagen` `ww`
				LEFT JOIN `acc_bestellingen` `a`
					ON `a`.`personalnr`=`ww`.`personalnr`
				LEFT JOIN
					( SELECT 
						'. $kortingCase . ',
						`tp`.`id`,
						`tp`.`btw`,
						`tp`.`titel`,
						( CASE 
							WHEN `tppro`.`prijs_normaal` IS NOT NULL THEN `tppro`.`prijs_normaal`
							ELSE `tp`.`prijs_normaal`
						END) AS `prijs_normaal`,
						`tp`.`prijs_aanbieding`,
						( CASE
								WHEN `tppro`.`id` IS NOT NULL THEN
									( CASE `tpprot`.`type`
											WHEN "Inclusief"
												THEN
											 ( CASE
												WHEN `tp`.`prijs_aanbieding` > 0
													THEN `tp`.`prijs_aanbieding`
														ELSE `tppro`.`prijs_normaal`
											END )
														-
											( CASE
												WHEN `tp`.`prijs_aanbieding` > 0
													THEN `tp`.`prijs_aanbieding`
														ELSE `tppro`.`prijs_normaal`
											END ) * tt.percentage / (100+tt.percentage)
										ELSE
											( CASE
												WHEN `tp`.`prijs_aanbieding` > 0
													THEN `tp`.`prijs_aanbieding`
												ELSE `tppro`.`prijs_normaal`
											END )
									END )
								ELSE
									( CASE `tt`.`type`
											WHEN "Inclusief"
												THEN
											 ( CASE
												WHEN `tp`.`prijs_aanbieding` > 0
													THEN `tp`.`prijs_aanbieding`
														ELSE `tp`.`prijs_normaal`
											END )
														-
											( CASE
												WHEN `tp`.`prijs_aanbieding` > 0
													THEN `tp`.`prijs_aanbieding`
														ELSE `tp`.`prijs_normaal`
											END ) * tt.percentage / (100+tt.percentage)
										ELSE
											( CASE
												WHEN `tp`.`prijs_aanbieding` > 0
													THEN `tp`.`prijs_aanbieding`
												ELSE `tp`.`prijs_normaal`
											END )
									END )
							END ) AS `prijs`,
						"Exclusief" AS `type`,
						`tt`.`percentage`
							
						FROM `product` `tp`
							LEFT JOIN `acc_btw` `tt`
								ON `tp`.`btw`=`tt`.`id`
							LEFT JOIN `producten_prijs_provincie` `tppro`
								ON `tp`.`id`=`tppro`.`producten_id` AND `tppro`.`provincie_id`='. (int) $this->prijsuitzondering_provincie_id .'
							LEFT JOIN `acc_btw` `tpprot`
								ON `tppro`.`btw`=`tpprot`.`id`
						
							'. $joinKorting . '
						) `p`
					ON `ww`.`artikel_id`=`p`.`id`
			
					LEFT JOIN
						( SELECT
							*,
							CASE
								WHEN `prijs_aanbieding` > 0
								THEN `prijs_aanbieding`
								ELSE `prijs_normaal`
							END AS `prijs`
							FROM `bijproducten`) `b`
								ON `ww`.`bijproduct_id`=`b`.`id`
					LEFT JOIN
						( SELECT
								`id`,
								`bezorgkosten` AS `prijs`
								FROM `producten_adressen`) `l` ON
						`ww`.`bezorging_id`=`l`.`id`
					LEFT JOIN `kortingscodes` `kc`
						ON `ww`.`korting_id`=`kc`.`id`
					LEFT JOIN `acc_btw` `t`
						ON `p`.`btw`=`t`.`id`
					WHERE `ww`.`personalnr`="'. session_id() .'"
						GROUP BY 
							`categorie`, `artikel_id`, `bijproduct_id`, `korting_id`
				
						) `w`
						WHERE `w`.`personalnr`="'. session_id() .'"
							AND (
									( SELECT
											count( `id` )
											FROM `product`
											WHERE `id`=`w`.`artikel_id`
											AND `uitverkocht`="nee"
											AND `aktief`="ja"
											) > 0
									OR
									`w`.`categorie`!="product"
									) AND `besteld`="0"
											GROUP BY `categorie`,
											`artikel_id`,
											`bijproduct_id`,
											`korting_id`
											ORDER BY
											`categorie` DESC,
											`artikel_id` ASC,
											`bijproduct_id` ASC,
											`prijs` ASC;';
		
		
		if( !$update ) {
			$Q = 'INSERT INTO `acc_bestelregels` '. $Query ;
			//ON DUPLICATE KEY UPDATE
			
			$this->check( $Q . 'INSERT' );
			if( $this->set( $Q ) ):
				if( !isset( $_SESSION['bestelling'] ) ) { 
					$_SESSION['bestelling'] = new stdClass();
				}
				$_SESSION['bestelling']->updated = true;
			
			else: 
				return false;
			endif;
		}
			
		$this->where('reset');
		$this->where( 'personalnr', '=', $pnr );
		$bestelling = $this->get( ['bestelling_id', 'invoice_no', 'invoice_prefix', 'personalnr', 'order_status_id' ] );

		$_SESSION['bestelling'] = is_array( $bestelling ) && count( $bestelling ) >= 1 ? $bestelling[0] : $bestelling;
		
		return true;
	}
	
	function bestelling_update_status( $bestelId, $bestelStatusId ){
		$bestelId = (int) $bestelId;
		$bestelStatusId = (int) $bestelStatusId;
		
		if( array_key_exists( $bestelStatusId, $this->orderStatus ) ):
			
			$Q = 'UPDATE `acc_bestellingen` 
						SET `order_status_id`="'. $bestelStatusId .'" 
					WHERE `bestelling_id`="'. $bestelId .'";';
		
			$this->check( $Q . 'UPDATE' );
			
			if( $this->update( $Q ) ):
				return $this->orderStatus[ $bestelStatusId ];
			endif;
		endif;
		
		return false;
	}
	
	
	function bestelling_afronden() {
		$invoice = $_SESSION['bestelling']->invoice_no;
				
		$fv[] = '`invoice_no`="'. $invoice .'"';
		
		if( $this->bestelling_verstuurd( 'bon' ) ) {
			
			$Q = 'UPDATE `acc_bestellingen`
					SET `order_status_id`=1
				WHERE '. implode( $fv );
			
			$this->check( $Q . 'UPDATE' );
			if( $this->update( $Q ) ) {
				
				unset( $_SESSION['userid_bestelling'], $_SESSION['bestelling'], $_SESSION['cart'], $_SESSION['transactie'], $_SESSION['loc_id'] );
				$_SESSION['bestelling'] = 100;
				return true;
			} else {
				return false;
			}
		}
		
	}
	
	function bestelling_verstuurd( $type ) {
		if( is_string( $type ) && in_array( $type, ['mail', 'bon', 'factuur','ga'] ) && isset( $_SESSION['bestelling']->invoice_no ) ) {
			
			try{
				$Q = 'UPDATE `acc_bestellingen` `a`
						SET `'. $type .'_verstuurd`=1
					WHERE `invoice_no`="'. $_SESSION['bestelling']->invoice_no .'";';
				
				$this->check( $Q . 'UPDATE' );
				if( $this->update( $Q ) ) return true;
				
				
			} catch( PDOException $ex ) {
				$this->log_set( 'sql', $ex->getMessage() );
				
				return false;
			}
			
		}
		return false;
	}
	
	function betaling_request( $url, $argumentsArray=null ){
		if( !isset( $argumentsArray ) || !is_array( $argumentsArray ) ) $argumentsArray = [];
		
		# Prepare complete API URL
		$strUrl = $url .http_build_query( $argumentsArray );

		# Get API result
		$strResult = @file_get_contents($strUrl);

		return json_decode( $strResult );
	}
	
	function betaling_get_methodes() {
		return $this->methode;
	}
	
	function betaling_get_methodes_paynl(){
		# Setup API Url
		$strUrl = 'https://token:'. $this->payNL['token'] .'@rest-api.pay.nl/v3/Service/getAvailablePaymentOptions/json?';
		
		# Add arguments
		$arrArguments = array();
		$arrArguments['serviceId'] = $this->payNL['serviceId'];
		
		if( $result = $this->betaling_request( $strUrl, $arrArguments ) ){
			$this->allmethodes = [];
			foreach( $result AS $r ) {
				$this->allmethodes[ $r->naam ] = $r->id;
			}
			
			return $this->allmethodes;
			
		} else {
			return false;
		}
	}
	
	function betaling_get_banken(){
		return $this->betaling_request( 'https://rest-api.pay.nl/v5/Transaction/getBanks/json' );
	}
	
	
	function betaling_starten( $methode, $totaalbedrag, $optie=null ) {
		if( !isset( $_SESSION['bestelling'] ) ) return false;
	
		if( in_array( $methode, array_values( $this->methode ) ) ) {
			$methode = (int) $methode;
		} elseif( in_array( $methode, array_keys( $this->methode ) ) ) {
			$methode = (int) $this->methode[ $methode ];
		} else {
	
			return false;
		}
		
		 $strUrl = 'https://rest-api.pay.nl/v5/Transaction/start/json?';

		 if( is_array( $totaalbedrag ) ) {
		 	$producten = $totaalbedrag['producten'];
		 	$totaalbedrag = $totaalbedrag['bedrag'];
		 }
		 
		 $totaalbedrag =  str_replace( ['.-',','], ['.00','.'], $totaalbedrag );
		 $bedrag = (int) str_replace( ',', '', number_format( $totaalbedrag, 2, "", "" ) );

		 $omschrijving = 'Order nummer: ' . $_SESSION['bestelling']->invoice_no . ', ' . $_SESSION['bestelling']->invoice_prefix;
		 
		 # Add arguments
		 $arrArguments = [];
		 $arrArguments['token'] = (string) $this->payNL['token'];
		 $arrArguments['serviceId'] = (string) $this->payNL['serviceId'];
		 $arrArguments['amount'] = $bedrag;
		 $arrArguments['ipAddress'] = $_SERVER['REMOTE_ADDR'];
		 $arrArguments['finishUrl'] = (string) $this->payNL['finishUrl'];
		 $arrArguments['transaction']['description'] = (string) $omschrijving;
		 $arrArguments['paymentOptionId'] = (int) $methode;
		 if( $optie && is_numeric( $optie ) ) $arrArguments['paymentOptionSubId'] = (int) $optie;

		 if( $status = $this->betaling_request( $strUrl, $arrArguments ) ) {
			 
			 if( $status->request->result ) {
				 $t_stat = is_array( $_SESSION['transactie']) ? $_SESSION['transactie'] : [];
				 $t_stat['transactionId'] 	=  $status->transaction->transactionId;
				 $t_stat['paymentURL'] 		=  $status->transaction->paymentURL;
				 $t_stat['paymentReference']= $status->transaction->paymentReference;
				 
				 $_SESSION['transactie'] = $t_stat;
			 } else {
			 	$_SESSION['transactie']['result'] = $status;
			 	$_SESSION['transactie']['error'] = true;
			 	return false;
			 }
			 
			 $Q = 'UPDATE `acc_bestellingen`
			 		SET `transactie_id`="'. $status->transaction->transactionId .'",
			 			`transactie_json`="'. rawurlencode( $jsonResult ).'",
			 			`datum_aangepast`=NOW()
			 		WHERE `invoice_no`="'. $_SESSION['bestelling']->invoice_no.'";';
			
			 $this->check( $Q . 'UPDATE');
			 $this->update( $Q );
			 
			 
			return $strUrl;
		} else {
			return false;
		}
	}
	
	function betaling_controle( $transactieId = null ) {
		
		if( !isset ($_SESSION['bestelling'] ) ) return false;
		if( isset( $transactieId ) && !empty( $transactieId  ) ) $transactieId = $transactieId;
		elseif( isset( $_SESSION['transactie']['transactionId'] ) && ( !isset( $_SESSION['transactie']['error'] ) || !$_SESSION['transactie']['error'] ) ) $transactieId = $_SESSION['transactie']['transactionId'];
		
		if( $transactieId ) {
			
		 # Setup API URL
		 $strUrl = 'https://token:'. $this->payNL['token'] .'@rest-api.pay.nl/v5/Transaction/info/json?';
		
		 # Add arguments
		 $arrArguments['transactionId'] = $transactieId;
		 if( $status = $this->betaling_request( $strUrl, $arrArguments ) ) {
			 if( $status->request->result ) {
			 	
			 	 $Q = 'UPDATE `acc_bestellingen`
				 		SET `transactie_id`="'. $_SESSION['transactie']['transactionId'] .'",
				 			`betaling_code`='. $status->paymentDetails->state .',
				 			`datum_aangepast`=NOW()
				 		WHERE `invoice_no`="'. $_SESSION['bestelling']->invoice_no.'";';
				 
				 $this->check( $Q . 'UPDATE');
				 $this->update( $Q );
	
				 
				 return $status->paymentDetails->state;
				
			 }
		 }
		 
		} else {
			
			$Q = 'SELECT `betaling_code`
					FROM `acc_bestellingen`
				WHERE `invoice_no`="'. $_SESSION['bestelling']->invoice_no.'";';
			
			$this->check( $Q . 'SELECT');
			$status = $this->query( $Q );

			if( isset( $status[0]->betaling_code ) && is_numeric( $status[0]->betaling_code ) ) {
				return $status[0]->betaling_code;
				
			}
				
		}
		
		return false;
		 
	}
	
	function order_get_no( $input, $length = 7, $table = null ) {
		try {
			$Q ='SELECT `invoice_no` + 1 AS `totaal` FROM `acc_bestellingen` ORDER BY `invoice_no` DESC LIMIT 1;';
			$this->check( $Q . 'SELECT' );
			$invoice_no = $this->query( $Q );
						
		} catch( PDOException $ex ) {
			$invoice_no = (object) ['totaal'=>1];

		}
		
		if( is_array( $invoice_no ) && isset( $invoice_no[0] ) ) $invoice_no = $invoice_no[0];
		return isset( $invoice_no->totaal ) ? $invoice_no->totaal : 1;
		
		/*$shorthash = substr( hash( 'md5', $input ), 0, $length );
		
		if( $table ) {
			$ori_table = '`' . str_replace( '`', '', trim( $table ) ) . '`;';
			
			$this->where('reset');
			$this->where( 'invoice_no', '=', $shorthash );
			$hashCheck = $this->get();
			
			if( $hashCheck !== false ) {
				for( $i = 0; $i < 500; $i++ ) {
					$shorthash = substr( hash( 'md5', $shorthash ), 0, $length );
			
					$this->where('reset');
					$this->where( 'invoice_no', '=', $shorthash );
					$hashCheck = $this->get();
					
					if( $hashCheck === false ) {
						$i = 500;
					}
				}
			}
			
		}

		return $shorthash;*/
	}
	
	
}