<?php

/**
 * postcodeData( $db )
 * 
 * @param	object	$db - PDO Database class
 */
class postcodeData {
	private $db;

	function __construct( $db ) {
		if( isset( $db ) ) {
			$this->db = $db;
		} else {
			return false;
		}
	}
	
	
	/**
	 * jsLog( $msg )
	 * 
	 * @param	string	echo console.log("$msg");
	 */

	function jsLog ( $msg ) {
		$msg = htmlentities( $msg, ENT_QUOTES, 'UTF-8');
		echo '<script>console.log("'.$msg.'");</script>';
		return true;

	}


	/**
	 * getLatLng( $postcode )
	 * 
	 * @param	string	$postcode ( AZAZ99 )
	 * returns:
	 * @param	object	$opject->lat|lon
	 */
	function getData( $postcode, $return = '*', $distinct = '' ) {
		$postcode = htmlentities( trim( $postcode ), ENT_QUOTES, 'UTF-8' );
		$postcode = str_split( $postcode, 4 );
		$postcode[1] = trim( $postcode[1] );
		$pc_check = $postcode[0];
		
		$distinct = !empty( $distinct ) ? ' DISTINCT' : '';
		$return = is_array( $return ) ? implode( ', `postcodes`.', $return ) : $return;
			
		if( is_numeric( $postcode[0] ) && !is_numeric( $postcode[1] ) ) {
				
			$postcode = strtoupper( implode( ' ', $postcode ) );
			$Q = 'SELECT'.$distinct.' '. $return .' FROM `postcodes` WHERE `postcode`=:postcode;';

			$get = $this->db->prepare( $Q );
			$get->bindValue( ':postcode', str_replace( ' ', '', $postcode ) , PDO::PARAM_STR );

			try{
				$get->execute();
				
			} catch( PDOException $ex ) {
				return $ex->getMessage();
			}
			
			if( $get->rowCount() > 0 ) {
				return $get->fetchAll( PDO::FETCH_ASSOC );;
			} else { 
				return false;
			}
		}
		
		return false;
	}
		
		
	/**
	 * getLatLng( $postcode )
	 * 
	 * @param	string	$postcode ( AZAZ99 )
	 * returns:
	 * @param	object	$opject->lat|lon
	 */
	function getLatLng( $postcode ) {


		$postcode = htmlentities( trim( $postcode ), ENT_QUOTES, 'UTF-8' );
		$postcode = str_split( $postcode, 4 );
		$postcode[1] = trim( $postcode[1] );
		$pc_check = $postcode[0];
			
		if( is_numeric( $postcode[0] ) && !is_numeric( $postcode[1] ) ) {
				
			$postcode = strtoupper( implode( ' ', $postcode ) );

			$getLatLong = $this->db->prepare( 'SELECT * FROM `postcodes` WHERE `postcode`=:postcode;');
			$getLatLong->bindValue( ':postcode', str_replace( ' ', '', $postcode ) , PDO::PARAM_STR );
			
			try{
				$getLatLong->execute();
				
			} catch( PDOException $ex ) {
				return $ex->getMessage();
			}
			
			if( $getLatLong->rowCount() > 0 ) {
				$latlong = $getLatLong->fetch( PDO::FETCH_OBJ );
				return $latlong;
			} else { 
				$this->jsLog( 'Postcode niet in DB');
				return false;
			}

			/*try {
			 if( !isset( $latlong->lat ) && isset( $postcode ) && trim( $postcode ) != '') {

			 $straat = '';
			 $provincie = '';
			 $plaats = '';

			 $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='. str_replace( ' ', '', $postcode ) . ',Netherlands&key=' . GM_KEY;
			 $content = file_get_contents( $url );
			 $mapdata = json_decode( $content );


			 $mapdata = $mapdata->results[0];

			 foreach( $mapdata->address_components AS $component) {
			 if( in_array( 'locality', $component->types ) ) {
			 $plaats = htmlentities( $component->long_name, ENT_QUOTES, 'UTF-8' );
			 } elseif ( in_array( 'administrative_area_level_1', $component->types ) ) {
			 $provincie = $this->db->prepare( 'SELECT `id` FROM `provincie` WHERE `titel`=:provincie LIMIT 1;');
			 $provincie->bindValue( ':provincie', $component->long_name, PDO::PARAM_STR );

			 try{
			 $provincie->execute();
			 $provincie = $provincie->fetch( PDO::FETCH_OBJ );
			 $provincie = (int) $provincie->id;
			 } catch( PDOException $ex ) {
			 $this->jsLog( 'provincie PDO exc: ' . $ex->getMessage() );
			 } catch( Exception $ex ) {
			 $this->jsLog( 'provincie exc: ' . $ex->getMessage() );
			 }

			 } elseif( in_array( 'street_address', $component->types ) ) {
			 $straat = htmlentities( $component->long_name, ENT_QUOTES, 'UTF-8' );
			 }
			 }
			 	
			 if( $provincie == 0 ) {
			 $provincie = $this->db->prepare( 'SELECT `provincie_id` FROM `postcodes` WHERE `plaats`=:plaats AND `postcode` LIKE :postcode LIMIT 1;' );
			 $provincie->bindValue( ':plaats', $plaats, PDO::PARAM_STR );
			 $provincie->bindValue( ':postcode', $pc_check . '%' , PDO::PARAM_STR );
			 $provincie->execute();

			 $provincie = $provincie->fetch( PDO::FETCH_OBJ );
			 $provincie = (int) $provincie->provincie_id;
			 }


			 $latitude = $mapdata->geometry->location->lat;
			 $longitude = $mapdata->geometry->location->lon;


			 $pcIns = $this->db->prepare( 'INSERT INTO `postcodes` (`plaats`, `postcode`, `provincie_id`, `straat`, `latitude`, `longitude`) VALUES ( :plaats, :postcode, :provincie, :straat, :lat, :long );');
			 $pcIns->bindValue( ':plaats', $plaats, PDO::PARAM_STR );
			 $pcIns->bindValue( ':postcode', $postcode, PDO::PARAM_STR );
			 $pcIns->bindValue( ':provincie', $provincie, PDO::PARAM_INT );
			 $pcIns->bindValue( ':straat', $straat, PDO::PARAM_STR );
			 $pcIns->bindValue( ':lat', $latitude, PDO::PARAM_STR );
			 $pcIns->bindValue( ':long', $longitude, PDO::PARAM_STR );

			 try {
			 $pcIns->execute();
			 $this->jsLog( 'Toegevoegd aan postcodes: ' .$plaats . ', ' . $provincie . ', ' . $straat );

			 } catch( PDOException $ex ) {
			 $this->jsLog( 'provincie PDO exc: ' . $ex->getMessage() );

			 }
			 	

			 } else {
			 $latitude = $latlong->lat;
			 $longitude = $latlong->lon;
			 }

				} catch( PDOException $ex  ) {
				$this->jsLog( 'klantenupdate: ' . $ex->getMessage() );
				}

				$returnData = new stdClass();
				$returnData->lat = $latitude;
				$returnData->lon = $longitude;
				$returnData->postcode = $postcode;
			*/

			
		} else {
			return false;
		}

	}
	
	
	/**
	 * getNearest( $postcode )
	 * 
	 * @param	string	$postcode - AZAZ99, 4 characters, 2 numbers.
	 * @param	int		$limit - default=1, amount of dealers returned
	 * returns:
	 * @param	object	$ele[]->postcode|plaats|bedrijf|email|tel|afstand
	 */
	function getNearest( $postcode, $limit = 1 ) {
		
		$postcode = strtoupper( str_replace( ' ', '', $postcode ) );
		$get_zip = $this->getLatLng( $postcode );

		try {
	
			$distSearch = $this->db->prepare( 'SELECT `klanten`.*,
												(6371 * acos(cos(radians(:lat)) * cos(radians(`latitude`)) * cos(radians(`longitude`) - radians(:long)) + sin(radians(:lat)) * sin(radians(`latitude`))))AS `distance`
											FROM `klanten`
											ORDER BY `distance` ASC LIMIT '. $limit .';');
				
			$distSearch->bindValue( ':lat', $get_zip->lat );
			$distSearch->bindValue( ':long', $get_zip->lon );
			$distSearch->execute();
			$postcodes = $distSearch->fetchAll( PDO::FETCH_OBJ );
	
		} catch( PDOException $ex ) {
			$this->jsLog( 'Select klanten error: ' . $ex->getMessage() );
		}
	
	
		$ele = array();
		
		foreach( $postcodes AS $lokatie ) {
			$tmp = new stdClass();
			$tmp->id		= $lokatie->id;
			$tmp->adres		= $lokatie->adres;
			$tmp->postcode 	= $lokatie->postcode;
			$tmp->plaats 	= $lokatie->plaats;
			$tmp->bedrijf 	= $lokatie->bedrijf;
			$tmp->email 	= $lokatie->email;
			$tmp->tel 		= $lokatie->tel;
			$tmp->afstand	= round( $lokatie->distance, 2 );
			
			array_push( $ele, $tmp );
			unset( $tmp );
			
		}
		
		return $ele;
	
	}

}