<?php

class googleRemarketing {
	public $ConversionId = null;
	public $RemarketingOnly = true;
	public $CompanyType = 'detailhandel';
	public $PageType = 'other';
	protected $Parameters = [];
	protected $AllowedPageTypes= [
		'home',
		'searchresults',
		'category',
		'product',
		'cart',
		'purchase',
		'other'
	];
	protected $CustomParam = [
		'detailhandel' => 
		[	'required' => [
				'ecomm_prodid'
			],
			'optional' =>  [ 
				'ecomm_pagetype',
				'ecomm_totalvalue'
			]	
		]	
	];
	
	function __construct( $ConversionId=false) {
		if( $ConversionId && is_string( $ConversionId ) ) $this->ConversionId = $ConversionId;
	}
	
	function setConversionId( $ConversionId ) {
		if( is_string( $ConversionId ) ) {
			$this->ConversionId = $ConversionId;
			return true;	
		} 
		return false;
	}
	
	function setCompanyType( $CompanyType ) {
		if( is_string( $CompanyType ) && in_array( $CompanyType, array_keys( $this->CustomParam ) ) ){
			$this->CompanyType = $CompanyType;
		} else {
			throw new Exception( 'Company Type not set up yet', 5 );
			return false;
		}
	}
	
	function setPageType( $PageType ) {
		if( in_array( $PageType, $this->AllowedPageTypes ) ) {
			$this->PageType = $PageType;
		} else {
			throw new Exception( 'Pagetype '. $PageType . ' not allowed (allowed: '. implode( ', ', $this->AllowedPageTypes ) . ')', 5 );
		}
	}
	
	function setParameter( $Parameter, $Value, $CompanyType = false) {
		if( !is_string( $Value ) ) {
			throw new Exception( 'Incorrect value', 5);
		}
		
		$CompanyType = $CompanyType && is_string( $CompanyType ) ? $CompanyType : $this->CompanyType;
		
		if( isset( $this->CustomParam[ $CompanyType ] ) ){
			if( isset( $this->CustomParam[ $CompanyType ]['required'] ) || isse( $this->CustomParam[ $CompanyType ]['optional'] ) ) {
				if( in_array( $Parameter, $this->CustomParam[ $CompanyType ]['required'] ) || in_array( $Parameter, $this->CustomParam[ $CompanyType ]['optional'] ) ) {
					$Value = (int) $Value == $Value && strlen( $Value ) == strlen( (int) $Value ) ?
						(int) $Value : '"'. $Value . '"';
					
					switch( $Parameter ) {
						case 'ecomm_pagetype':
							$this->setPageType( substr( $Value, 1, -1 ) );
							break;
						case 'ecomm_totalvalue':
							$Value = number_format( strpos( $Value, '"') !== false ? substr( $Value, 1, -1 ) : $Value , 2, ".", "" );
							
							if( isset( $this->Parameters[ $Parameter ] ) )
								$this->Parameters[ $Parameter ][] = $Value;
							else			
								$this->Parameters[ $Parameter ] = [ $Value ];
							
							break;
						default: 
							if( isset( $this->Parameters[ $Parameter ] ) )
								$this->Parameters[ $Parameter ][] = $Value;
							else			
								$this->Parameters[ $Parameter ] = [ $Value ];
					}
				}
			} else {
				throw new Exception( $CompanyType . ' fields not configured', 5 );
	
			}
		} else {
			throw new Exception( $CompanyType . ' not configured', 5 );
		}
		
	}
	
	function getGoogleTagParams(){
		$Required = $this->CustomParam[ $this->CompanyType ]['required'];
		$Optional = $this->CustomParam[ $this->CompanyType ]['optional'];
		
		
		$GoogleTagParams = [];
		
		foreach( $Required AS $RequiredField ) {
			if( !isset( $this->Parameters[ $RequiredField ] ) || empty( $this->Parameters[ $RequiredField ] ) ) 
				$this->Parameters[ $RequiredField ] = ['""'];
		}
		
		if( isset( $this->Parameters ) && $this->Parameters ) {
			foreach( $this->Parameters AS $Field => $Values ) {
				switch( $Field ){
					case 'ecomm_pagetype':
						break;
					default:
					
					if( count( $Values ) > 1 )
						$GoogleTagParams[] = $Field . ': ['. implode( ', ', $Values ) . ']';
					else 
						$GoogleTagParams[] = $Field . ': '. implode( ', ', $Values );
				}
			}
		}
			
		return '<script type="text/javascript">
			var google_tag_params = {
				'. implode( ', ', $GoogleTagParams ) . ', ecomm_pagetype: "'. $this->PageType . '"
			};
		</script>';

	}
		
	function getGoogleScript(){
		if( isset( $this->ConversionId ) && !empty( $this->ConversionId ) && $this->ConversionId ) {
			
			return '<script type="text/javascript">
				/* <![CDATA[ */
					var google_conversion_id = '. $this->ConversionId . ';
					var google_custom_params = window.google_tag_params;
					var google_remarketing_only = '. ( $this->RemarketingOnly ? 'true' : 'false ') . ';
				/* ]]> */
			</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
			</script>
			<noscript>
				<div style="display:inline;">
					<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/'. $this->ConversionId . '/?value=0&guid=ON&script=0"/>
				</div>
			</noscript>';
			
		} else {
			throw new Exception( 'No Conversion ID set.' );
			return false;
		}
		
		
	}
	
	function getJavascript(){
		if( $this->getGoogleTagParams() && $this->getGoogleScript() )
			return $this->getGoogleTagParams(). $this->getGoogleScript();
		else return false;
		
		
	}
	
	function render(){
		$render = $this->getJavascript();
		if( $render ) {
			echo $render;
			return true;
			
		} else return false;
	}
	
}