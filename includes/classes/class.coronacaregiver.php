<?php
date_default_timezone_set('UTC');

require_once 'class.coronamail.php';

class CoronaCareGiver {
    public  $id,
            $voornaam,
            $achternaam,
            $telefoon,
            $email,
            $adres,
            $postcode,
            $plaats,
            $inhoud,
            $datum_aanvraag,
            $laatst_bewerkt,
            $pincode,
            $aktief;
    public $hulpvraag = [];
//    public $adminmail = "reinbert@bandwerk.nl";
    public $adminmail = "info@mannacoronahulp.nl";

    function __construct() {}

    public function NewCaregiver($voornaam, $achternaam, $telefoon, $email, $adres, $postcode, $plaats, $inhoud, $datum_aanvraag = null, $laatst_bewerkt = null, $pincode = null, $aktief = 'Nee') {
        $this->voornaam = $voornaam;
        $this->achternaam = $achternaam;
        $this->telefoon = $telefoon;
        $this->email = $email;
        $this->adres = $adres;
        $this->postcode = $postcode;
        $this->plaats = $plaats;
        $this->inhoud = $inhoud;
        $this->datum_aanvraag = $datum_aanvraag ?? date("Y-m-d H:i:s");
        $this->laatst_bewerkt = $laatst_bewerkt ?? date("Y-m-d H:i:s");
        $this->pincode = $pincode ?? $this->GenerateUniquePin();
        $this->aktief = $aktief;
    }

    public function SaveAsNew()
    {
        global $db;

        if (count($this->hulpvraag) == 0) {
            return;
        }

        try {
            //@todo; ADD HULPVRAGEN IN KOPPELTABEL

            $insertNew = $db->prepare("INSERT INTO `hulpverleners` (`voornaam`, `achternaam`, `telefoon`, `email`, `adres`, `postcode`, `plaats`, `inhoud`, `datum_aanvraag`, `laatst_bewerkt`, `pincode`, `aktief`, `sort`)
                                       VALUES (:voornaam, :achternaam, :telefoon, :email, :adres, :postcode, :plaats, :inhoud, NOW(), NOW(), :pincode, :aktief, :sort);");
            $this->GenerateUniquePin();
            $insertNew->bindValue(":sort", $this->GetNewSort(), PDO::PARAM_INT);
            if ($this->Save($insertNew)) {
                $this->id = $db->lastInsertId();
                $this->UpdateRequestTypes();
            }
            $this->SendBevestiging();
            $this->SendNotification();
            return true;

        } catch (PDOException $e) {
            die("Could not save" . $e->getMessage());
        }
    }

    public function SaveChanges($updateRequest = true) {
        global $db;

        if(!isset($this->id)) {
            $this->SaveAsNew();
            return;
        }

        try {
            $insertNew = $db->prepare("UPDATE `hulpverleners`
                                        SET `voornaam` = :voornaam, `achternaam` = :achternaam, `telefoon` = :telefoon, `email` = :email, `adres` = :adres, `postcode` = :postcode, `plaats` = :plaats, `inhoud` = :inhoud, `laatst_bewerkt` = NOW(), `pincode` = :pincode, `aktief` = :aktief
                                        WHERE `id` = :id");

            $insertNew->bindValue(':id', $this->id, PDO::PARAM_INT);
            $this->Save($insertNew);
            if($updateRequest) {
                $this->UpdateRequestTypes();
            }
        } catch (PDOException $e) {
            die("Could not save" . $e->getMessage());
        }
    }

    private function Save($query) {
        $query->bindValue(':voornaam', $this->voornaam, PDO::PARAM_STR);
        $query->bindValue(':achternaam', $this->achternaam, PDO::PARAM_STR);
        $query->bindValue(':telefoon', $this->telefoon, PDO::PARAM_STR);
        $query->bindValue(':email', $this->email, PDO::PARAM_STR);
        $query->bindValue(':adres', $this->adres, PDO::PARAM_STR);
        $query->bindValue(':postcode', $this->postcode, PDO::PARAM_STR);
        $query->bindValue(':plaats', $this->plaats, PDO::PARAM_STR);
        $query->bindValue(':inhoud', $this->inhoud, PDO::PARAM_STR);
        $query->bindValue(':pincode', $this->pincode, PDO::PARAM_STR);
        $query->bindValue(':aktief', $this->aktief, PDO::PARAM_STR);
        if($query->execute()) {
            return true;
        }
        return false;
    }

    private function UpdateRequestTypes(){
        if(!isset($this->id)) { return false; }

        global $db;
        try {
            $delete = $db->prepare('DELETE FROM `hulpverleners_hulpvraag` WHERE `hulpverlener_id` = :id');
            $delete->bindValue(":id", $this->id, PDO::PARAM_INT);
            $delete->execute();
        } catch (PDOException $e) {
            die("Unable to clear current requesttypes for " . $this->id);
        }

        try {
            foreach ($this->hulpvraag as $hulpvraag_id) {
                $insert = $db->prepare('INSERT INTO `hulpverleners_hulpvraag` (`hulpverlener_id`, `hulpvraag_id`) VALUES (:id1, :id2)');
                $insert->bindValue(":id1", $this->id, PDO::PARAM_INT);
                $insert->bindValue(":id2", $hulpvraag_id, PDO::PARAM_INT);
                $insert->execute();
            }
        } catch (PDOException $e) {
            die("Unable to save requesttypes for " . $this->id);
        }
        return true;
    }

    public function LoadByPincode($pincode) {
        global $db;

        try {
            $getByPincode = $db->prepare("SELECT * FROM `hulpverleners` WHERE `pincode` = :pincode LIMIT 1");
            $getByPincode->bindValue(':pincode', htmlentities($pincode), PDO::PARAM_STR);
            if($getByPincode->execute() && $getByPincode->rowCount() == 1) {
                $caregiver = $getByPincode->fetch(PDO::FETCH_OBJ);
                $this->id = $caregiver->id;
                $this->newFromObject($caregiver);
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die("Unable to get caregiver");
        }
    }

    public function LoadById($id) {
        global $db;

        try {
            $getById = $db->prepare("SELECT * FROM `hulpverleners` WHERE `id` = :id LIMIT 1");
            $getById->bindValue(':id', htmlentities($id), PDO::PARAM_INT);
            if($getById->execute() && $getById->rowCount() == 1) {
                $caregiver = $getById->fetch(PDO::FETCH_OBJ);
                $this->id = $caregiver->id;
                $this->NewFromObject($caregiver);
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die("Unable to get caregiver");
        }
    }

    private function NewFromObject($caregiver) {
        $this->NewCaregiver(
            $caregiver->voornaam,
            $caregiver->achternaam,
            $caregiver->telefoon,
            $caregiver->email,
            $caregiver->adres,
            $caregiver->postcode,
            $caregiver->plaats,
            $caregiver->inhoud,
            $caregiver->datum_aanvraag,
            $caregiver->laatst_bewerkt,
            $caregiver->pincode,
            $caregiver->aktief
        );
    }

    private function GetNewSort() {
        global $db;
        try {
            $getSort = $db->prepare('SELECT MAX(`sort`) + 1 FROM `hulpverleners`');
            if($getSort->execute() && $getSort->rowCount() == 1) {
                return $getSort->fetchColumn();
            }
        } catch (PDOException $e) {
            die('Could not get max sort');
        }
    }

    private function GeneratePIN($digits){
        $pin = "";
        while(strlen($pin) < $digits) {
            $pin .= rand(0, 9);
        };
        return $pin;
    }


    private function GenerateUniquePin()
    {
        $pinExists = true;
        while($pinExists) {
            $tryPin = $this->GeneratePIN(6);

            global $db;
            try {
                $findPin = $db->prepare("SELECT `pincode` FROM `hulpverleners` WHERE `pincode` = :pin");
                $findPin->bindValue(':pin', $tryPin, PDO::PARAM_STR);

                if($findPin->execute() && $findPin->rowCount() == 0) {
                    $pinExists = false;
                    return $this->pincode = $tryPin;
                }
            } catch (PDOException $e) {
                die('Unable to generate pin');
            }
        }
    }

    public function PrintData() {
        echo "<table>";
        $this->PrintRow('naam', $this->voornaam);
        $this->PrintRow('achternaam', $this->achternaam);
        $this->PrintRow('telefoon', $this->telefoon);
        $this->PrintRow('email', $this->email);
        $this->PrintRow('adres', $this->adres);
        $this->PrintRow('postcode', $this->postcode);
        $this->PrintRow('plaats', $this->plaats);
        $this->PrintRow('inhoud', $this->inhoud);
        echo "</table>";
    }

    private function PrintRow($key, $value) {
        $key = ucfirst($key);
        echo "<tr>
                <td>$key</td>
                <td>$value</td>
              </tr>\n";
    }

    public function HasAccessToRequest($hash) {
        global $db;
        try {
            $permission = $db->prepare('SELECT * FROM `hulpaanvraag_hulpverlener` WHERE `hulpverlener_id` = :id AND `hulpaanvraag_id` = (SELECT `id` FROM `hulpaanvraag` WHERE `hash` = :hash)');
            $permission->bindValue(':id', $this->id, PDO::PARAM_INT);
            $permission->bindValue(':hash', $hash, PDO::PARAM_STR);

            if($permission->execute() && $permission->rowCount() == 1) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            return false;
        }
    }

    private function SendBevestiging(){
        $content = "Uw aanmelding is in goede orde ontvangen!<br> Onze medewerkers zullen uw aanmelding zo snel mogelijk beoordelen zodat je snel anderen kunt gaan helpen. Zodra we een geschikte hulpvraag voor u hebben gevonden krijgt u direct bericht van ons via de e-mail.<br><br>
 Met vriendelijke groet, <br> Het corona hulpteam ";
        $mail = new CoronaMail($this->voornaam . " " . $this->achternaam, $this->email, "Uw aanmelding is ontvangen", $content);
        $mail->Send();
    }
    private function SendNotification(){
        $mail = new CoronaMail($this->voornaam . " " . $this->achternaam, $this->adminmail, "Nieuwe hulpaanbieder", "Bevestig deze aanvraag.");
        $mail->Send();
    }

    public function SetRequestTypes($hulpvragen)
    {
        $this->hulpvraag = $hulpvragen;
    }

    public function GetRequestTypes(){
        global $db;
        try {
            $types = $db->prepare('SELECT `vraag` FROM `hulpvraag` WHERE `id` IN (SELECT `hulpvraag_id` FROM `hulpverleners_hulpvraag` WHERE `hulpverlener_id` = :id)');
            $types->bindValue(":id", $this->id, PDO::PARAM_INT);
            if($types->execute()) {
                $res = [];
                while($row = $types->fetch(PDO::FETCH_OBJ)) {
                    array_push($res, $row->vraag);
                }
                return $res;
            }
        } catch (PDOException $e) {
            die("Could not get types");
        }
    }

    public static function GetCareGivers($filter = false) {
        global $db;
        try {
            $in = "('".implode("', '", $_SESSION['plaatsnamen'])."')";
            if($filter != false) {
//                $request = $db->prepare("SELECT `id` FROM `hulpverleners` WHERE `aktief` = :aktief ORDER BY `datum_aanvraag` DESC");
                $request = $db->prepare("SELECT `id` FROM `hulpverleners` WHERE `aktief` = :aktief AND `plaats` IN ".$in." ORDER BY `datum_aanvraag` DESC");
                $request->bindValue(":aktief", htmlentities($filter), PDO::PARAM_STR);
            } else {
//                $request = $db->prepare("SELECT `id` FROM `hulpverleners` ORDER BY `datum_aanvraag` DESC");
                $request = $db->prepare("SELECT `id` FROM `hulpverleners` WHERE `plaats` IN ".$in." ORDER BY `aktief` DESC, `datum_aanvraag` DESC");
            }
            if ($request->execute() && $request->rowCount() >= 1) {
                $res = $request->fetchAll(PDO::FETCH_OBJ);
                $objects = [];
                foreach ($res as $cl) {
                    $caregiver = new CoronaCareGiver();
                    $caregiver->LoadById($cl->id);
                    array_push($objects, $caregiver);
                }
                return $objects;
            } else {
                return [];
            }
        } catch (PDOException $e) {
            die('Unable to get caregivers' . $e->getMessage());
        }
    }

    public static function GetStatuses()
    {
        return ['Ja', 'Nee'];
    }
}