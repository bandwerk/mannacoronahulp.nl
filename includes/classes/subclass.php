<?php

class listmemberInfo extends query {
	protected $catTables;
	private $dataById;
	private $data;
	
	function __construct( $categorie, PDO $pdo = null) {
		if( !isset( $pdo ) ) {
			global $db;
			$pdo = $db;
		}
		
		if( $this->init() ) {
			$cat = trim( $categorie );
			
			$this->joins = false;
			if( array_key_exists( $cat, $this->catTables ) ) {
				$this->table = $this->catTables[ $cat ] ;
				
				parent::__construct( $pdo, $categorie );
				$this->where( null );
				
				if( is_array( $this->table['img'] ) ) {
					foreach( $this->table['img'] AS $table ) {
						if( is_string( $table ) ) $this->join_add_image( true, $table );
						else $this->join_add_image();
						
					}
				} elseif( $this->table['img'] == true ) $this->join_add_image();
				if( $this->table['sort'] == true ) $this->order( 'sort', 'ASC');
				
				$data = $this->get();
				
				if( !is_array( $data ) ) {
					$this->order( null );
					$data = $this->get();
				}
				
				if( !is_array( $data ) ) {
					return false;
				}

					$this->data = [];
					$this->dataById = [];
					
					foreach( $data AS $key => $row ) {
						if( isset( $row->image ) ) $row->image = strpos( $row->image, ',' ) ? explode( ',', $row->image ) : [ $row->image ];
						
						$this->data[] = $row;
						$this->dataById[ (int) $row->id ] = $row;

					}
					
					return true;
				
				$this->order( null );
			}
		
		}
		
		return false;
	}
	
	function init() {
		$init = [
			'btw'		=> [ 'acc_btw', 'btw', 'img' => 0, 'sort' => 0 ],
			'categorie' => [ 'producten_categorie', 'cat', 'img' => 1, 'sort' => 1 ],
			'filtering' => [ 'producten_filtering', 'filter', 'img' => 1 , 'sort' => 0 ],
			'ingredienten'  => ['producten_ingredienten', 'ing', 'img' => 0, 'sort' => 0 ],
			'personen'  => ['producten_aantal_personen', 'pers', 'img' => 0, 'sort' => 1 ],
			'headers'	=> ['headers', 'head', 'img' => 1, 'sort' => 0 ]
		];
		
		foreach( $init AS $cat => $table ) {
			$this->catTables[ $cat ] = $table;
			$this->allowed[] = $table;
		}
		
		return true;
		
	}
	

	function get_by_product( product $product, $field ) {
		$ar = [];
		if( is_object( $product ) ) {
			$productData = $product->data;
			
			foreach( $productData AS $row ) {
				$ar[] = $this->get_by_id( $row->id, $field );
			}
			
			return $ar;
		}
		
		return false;
	}
	
	function get_by_id( $id, $x = '') {
		if( $x === '' ) {
			$x = array_keys( $this->data[0] );
		}
		$x = !is_array( $x ) ? [ $x ] : $x;
		$id = !is_array( $id ) ? [ $id ] : $id;
		$R = [];

		foreach( $id AS $i ) {
			$Rx = [];
			foreach( $x AS $xx ) {
				
				$Rx[ $xx ] = $this->dataById[ (int) $i ][$xx];
			}
			if( count( $Rx ) === 1 ) $Rx = $Rx[0];
			
			$R[] = $Rx;
		}
		
		if( count( $R ) === 1 ) $R=$R[0];
		return $R;
	}
		
	function get_titel( $id ){
		return $this->get_by_id( $id, 'titel' );
	}
	
	function get_active( $randomBoolOrLimitNum = true, $limit = false  ) {
		if( is_numeric( $randomBoolOrLimitNum ) ) {
			$limit = $randomBoolOrLimitNum;
			$random = true;
		} else {
			$random = $randomBoolOrLimitNum;
		}
		
		$ar = $this->data;
		if( $random ) shuffle( $ar );
		$return = [];
		
		foreach( $ar AS $row ) {

			if( ( ( is_numeric( $limit ) && $limit > 0 ) || $limit === false ) && $row['aktief'] === 'ja' ) {
				$return[] = $row;
				if( $limit ) $limit--;
			}
		}
		
		return $return;
	}
	
	function get_all( $type=null ) {
		if( $type ){
			$return = [];
			foreach( $this->data AS $D ) {
				if( $D['type'] == $type ) $return[] = $D;
			}
			return $return;
		} else return $this->data;
	}
	
	function search_for( $term, $strict = false ) {
		$return = $this->dataById;
		
		if( !$strict ) {
			array_walk( $return, function( &$val, $key, $term ) {
		
				$zoekIn = '';
				$zoekIn .= isset( $val['titel'] ) && !empty( $val['titel'] ) ? $val['titel'] . ' ' : '';
				$zoekIn .= isset( $val['subtitel'] ) && !empty( $val['subtitel'] ) ? $val['subtitel'] . ' ' : '';
				$zoekIn .= isset( $val['inhoud'] ) && !empty( $val['inhoud'] ) ? $val['inhoud'] . ' ' : '';
				$zoekIn .= isset( $val['alt'] ) && !empty( $val['alt'] ) ? $val['alt'] . ' ' : '';
				
				if( stripos( $zoekIn, $term  ) !== false ) {
					$val = true;
				}  else {
					$val = null;
				}
			}, $term);
		} else {
			array_walk( $return, function( &$val, $key, $term ) {
		
				if( strtolower( $val['titel'] ) === strtolower( $term ) ) {
					$val = true;
				}  else {
					$val = null;
				}
			}, $term);
			
		}
		
		return array_keys( array_filter( $return ) );
	}
	
	
}