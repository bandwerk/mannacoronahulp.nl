<?php

class FormGen {
    public
        $AfzenderEmail = false,
        $AfzenderNaam = false,
        $FormAction = false,
        $FormMethod = 'POST',
        $FormName = false,
        $FormPrefix = '';

    var
        $FormField = false,
        $FormInput = null,
        $FormInputFile = false,
        $Submitted = false,
        $Success = false;

    private
        $CurrentField = false,
        $Error = false,
        $IgnoreInputFields = [
        'nobots',
        'rendered'
    ],
        $RunSetFunction = false;

    static private
        $Allowed = [
        'Method' => [
            'POST',
            'GET',
            'MULTIPART/FORM-DATA'
        ]
    ],
        $ValidationType = [
        'email' => [
            'filtervar' => FILTER_VALIDATE_EMAIL,
            'error' => 'Voer een geldig emailadres in'
        ],
        'huisnummer' => [
            'pattern' => '[0-9]{1,5}\D{0,3}',
            'error' => 'Een huisnummer is maximaal 5 cijfers lang'
        ],
        'lengte' => [
            'pattern' => '.{$min$,$max$}',
            'error' => 'Voer een geldige $FieldName$ in: ',
            'replace' => [
                'min',
                'max'
            ]
        ],
        'naam' => [
            'pattern' => '\D{2,} {1,}\D{2,}',
            'error' => 'Voer uw voor- en achternaam in (beide minimaal 2 karakters lang)'
        ],
        'adres' => [
            'pattern' => '[\D\s]{2,} [0-9]+\D*',
            'error' => 'Voer een geldig adres in'
        ],
        'password' => [
            'pattern' => '\S*(?=\S{$min$,$max$})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*',
            'error' => 'Een wachtwoord bevat minimaal 1 kleine letter, 1 hoofdletter, 1 nummer, 1 speciaal karakter en is ',
            'replace' => [
                'min',
                'max'
            ]
        ],
        'postcode' => [
            'pattern' => '[0-9]{4}\s?[A-Za-z]{2}',
            'error' => 'Voer een geldige $FieldName$ in'
        ],
        'tel' => [
            'pattern' => '(((\(?(00|\+)31\)? ?)|0)(\d[ \-]*){9})|(((\(?(00|\+)49\)? ?)|0)(\d[ \-]*){11})',
            'error' => 'Voer een geldig $FieldName$ in van 10 cijfers'
        ],
        'tijd' => [
            'delay' => '$delay$',
            'error' => 'Voer een geldig $FieldName$ in'
        ]

    ];

    function __construct(string $FormName, string $FormTarget = null, string $FormMethod = 'POST', string $FormPrefix = '') {
        if (strlen($FormName) < 1) return false;
        $this->FormName = trim($FormName);
        $this->FormAction = filter_var($FormTarget, FILTER_VALIDATE_URL) ? $FormTarget : '';

        if (in_array(strtoupper($FormMethod), self::$Allowed['Method'])) {
            $this->FormMethod = strtoupper($FormMethod);
        } else {
            die('Het formulier heeft geen geldige methode');
        }
        if (strlen($FormPrefix) > 0) $this->FormPrefix = $FormPrefix;

        $this->FormInput = self::GetFormRequest($this->FormPrefix . $this->FormName . '_', $this->FormMethod);
        $this->Submitted = (isset($this->FormInput['rendered']) || isset($this->FormInput['nobots']));
    }

    public function __set($property, $value) {
        if (is_callable(array($this, 'Set' . $property)) && $this->RunSetFunction) {
            $this->RunSetFunction = false;
            call_user_func(array($this, 'Set' . $property), $value);

        } elseif (!isset($this->$property)) {
            $this->RunSetFunction = true;
            $this->data[$property] = $value;


        } else {
            $this->$property = $value;

        }
    }


    public function __get($property) {
        if (is_callable(array($this, 'Get' . $property))) {
            return call_user_func(array($this, 'Get' . $property));

        } elseif ($this->RunSetFunction && property_exists($this, $property) && $property !== 'data') {
            return $this->$property;

        } elseif (isset($this->data) && array_key_exists($property, $this->data)) {
            return $this->data[$property];

        } else {
            return false;

        }
    }


    function GetInput() {
        $this->FormInput = self::GetFormRequest($this->FormPrefix . $this->FormName . '_', $this->FormMethod);

        return $this->FormInput;
    }

    function GetFormData($GetAllRequestedData = false) {
        $Return = [];

        if ($FormData = $this->GetInput()) {
            foreach ($FormData AS $K => $V) {

                // Alleen gedefinieerde velden ( dus geen nobots/rendered )
                if (!is_array($V) && array_key_exists($K, $this->FormField) || ($GetAllRequestedData && !in_array($K, $this->IgnoreInputFields))) {
                    $Sanitize = isset($this->FormField[$K]['sanitize']) ? $this->FormField[$K]['sanitize'] : false;

                    switch ($Sanitize) {
                        case 'trim':
                            $V = trim($V);
                            break;
                    }
                    $Return[$K] = $V;

                } elseif (array_key_exists($K . '[]', $this->FormField) || (is_array($V) && isset($V[0]['tmp_name']))) {
                    if (!is_array($this->FormInputFile)) {
                        $this->FormInputFile = [];
                    }
                    $this->FormInputFile[$K] = $V;
                }
            }
        }

        return !empty($Return) && count($Return) > 0 ? $Return : false;
    }

    public static function GetFormRequest(string $FormFieldPrefix, $FormMethod = 'POST') {
        if (!in_array(strtoupper($FormMethod), self::$Allowed['Method'])) {
            die('Het formulier heeft geen geldige methode');
        }

        $Data = [];

        switch ($FormMethod) {
            case 'POST':
                $INPUT = $_POST;

                break;
            case 'GET':
                $INPUT = $_GET;
                break;
            case 'MULTIPART/FORM-DATA':
                $INPUT = $_POST;

                if (isset($_FILES) && count($_FILES) > 0) {
                    $FieldName = array_keys($_FILES)[0];

                    $FileData = [];
                    foreach ($_FILES[$FieldName] AS $InfoType => $FileInfo) {
                        if (is_array($FileInfo)) {
                            foreach ($FileInfo AS $Key => $Value) {
                                $FileData[$Key][$InfoType] = $Value;

                            }
                        } else {
                            $FileData[0][$InfoType] = $FileInfo;
                        }
                    }

                    if ($FileData[0]['error'] !== UPLOAD_ERR_NO_FILE) {
                        $Data[substr($FieldName, strlen($FormFieldPrefix), strlen($FieldName) - strlen($FormFieldPrefix))] = $FileData;

                    }
                }
                break;
            default:
                $INPUT = $_REQUEST;
        }


        foreach ($INPUT AS $K => $V) {
            if (strpos($K, $FormFieldPrefix) === 0) {
                $Data[substr($K, strlen($FormFieldPrefix), strlen($K) - strlen($FormFieldPrefix))] = $V;
            }
        }

        return !empty($Data) ? $Data : false;
    }

    public static function GetValidation(string $ValidationType = '', array $ValidationSettings = []) {
        if (empty($ValidationType)) return null;


        $Pattern = null;

        if (array_key_exists($ValidationType, self::$ValidationType) && isset(self::$ValidationType[$ValidationType]['pattern'])) {
            $Pattern = self::$ValidationType[$ValidationType]['pattern'];

            foreach (self::$ValidationType[$ValidationType] AS $Key => $Val) {
                switch ($Key) {
                    case 'replace':
                        foreach ($Val AS $FindString) {
                            $ReplaceWith = isset($ValidationSettings[$FindString]) ? $ValidationSettings[$FindString] : '';

                            $Pattern = str_replace('$' . $FindString . '$', $ReplaceWith, $Pattern);
                        }
                        break;
                }
            }


        }

        if (array_key_exists($ValidationType, self::$ValidationType) && isset(self::$ValidationType[$ValidationType]['filtervar'])) {
            $Pattern = 'filter:' . self::$ValidationType[$ValidationType]['filtervar'];
        }

        if (array_key_exists($ValidationType, self::$ValidationType) && isset(self::$ValidationType[$ValidationType]['delay'])) {
            $Pattern = isset($ValidationSettings['delay']) && is_numeric($ValidationSettings['delay']) ? 'delay:' . $ValidationSettings['delay'] : 'delay:' . self::$ValidationType[$ValidationType]['delay'];
        }

        return $Pattern;

    }

    public static function GetValidationErrormessage(string $FieldName, string $ValidationType, array $ValidationSettings) {
        if (empty($ValidationType)) return null;

        $Errormessage = null;

        if (array_key_exists($ValidationType, self::$ValidationType) && isset(self::$ValidationType[$ValidationType]['error'])) {
            $Errormessage = str_replace('$FieldName$', preg_replace('/\d/i', '', strtolower($FieldName)), self::$ValidationType[$ValidationType]['error']);


            foreach (self::$ValidationType[$ValidationType] AS $Key => $Val) {
                switch ($Key) {
                    case 'replace':
                        $ValidationSetting = $ValidationSettings;

                        if (isset($ValidationSetting['min'])) {
                            $Errormessage .= 'minimaal ' . $ValidationSetting['min'];

                            if (isset($ValidationSetting['max']))
                                $Errormessage .= ' en';

                        }

                        if (isset($ValidationSetting['max'])) {
                            $Errormessage .= ' maximaal ' . $ValidationSetting['max'];
                        }

                        $Errormessage .= ' karakters lang';

                        break;
                }
            }

        } else {
            die("Validatieerror $ValidationType nog niet ingesteld.");
        }

        return $Errormessage;

    }

    function Validate() {
        return self::ValidateForm($this->FormInput);
    }

    function AddError(string $FieldName = null, string $ErrorMessage = null) {
        if ($FieldName && $ErrorMessage) {
            $this->FormField[$FieldName]['title'] = $ErrorMessage;
        }
        $FieldName = !empty($FieldName) ? $FieldName : $this->CurrentField;
        $ErrorMessage = empty($ErrorMessage) ? $this->FormField[$FieldName]['title'] : $ErrorMessage;

        if (!isset($this->Error[$FieldName]) || !is_array($this->Error[$FieldName])) $this->Error[$FieldName] = [];
        $this->Error[$FieldName][] = $ErrorMessage;

        return true;
    }

    /*
     * Formulierveld aanmaken
     * string 	$FieldType		Type veld (text, email, tel, number etc )
     * string	$FieldName		Naam van het veld
     * string	$Placeholder	Placeholder value, default false
     * string	$ValidationType	Type om het veld te valideren, bepaald ook het pattern voor HTML validatie en PHP validatie, default niets
     * 							Mogelijke validaties self::$ValidationType
     * array	$ValidationSettings	Verdere instellingen voor validatietype (denk aan min/max bij lengte)
     * bool		$Required		Required field of niet
     * bool		$Readonly		Readonly veld of niet
     */

    function AddField(string $FieldType, string $FieldName, string $Placeholder = '', string $ValidationType = '', array $ValidationSettings = [], bool $Required = false, bool $Readonly = false, $DefaultValue = null) {
        if (!$this->FormField) $this->FormField = [];

        if (empty($FieldType) || empty($FieldName))
            die('Geen veldtype of naam opgegeven');

        $this->CurrentField = $FieldName;

        $this->FormField[$FieldName] = [
            'type' => $FieldType,
            'id' => $this->FormPrefix . $this->FormName . '_' . $FieldName,
            'name' => $this->FormPrefix . $this->FormName . '_' . $FieldName,
            'placeholder' => $Placeholder ? $Placeholder : null,
            'value' => isset($this->FormInput[$FieldName]) ? $this->FormInput[$FieldName] : null,
            'validation' => $ValidationType,
            'pattern' => self::GetValidation($ValidationType, $ValidationSettings),
            'title' => self::GetValidationErrormessage($FieldName, $ValidationType, $ValidationSettings),
            'required' => $Required,
            'readonly' => $Readonly,
            'default' => null

        ];

        if ($FieldType === 'file') {
            $this->FormMethod = 'MULTIPART/FORM-DATA';
        }

        return true;

    }

    function IsRequired(bool $IsRequired = true, string $FieldName = null) {
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;
        $this->FormField[$FieldName]['required'] = $IsRequired;

        return true;
    }

    function IsReadonly(bool $IsReadonly = true, string $FieldName = null) {
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;

        $this->FormField[$FieldName]['readonly'] = $IsReadonly;

        return true;
    }

    function AddOption(array $Options, string $FieldName = null) {
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;

        if (!empty($Options) && is_array($Options) && !empty(array_values($Options)[0])) {
            $this->FormField[$FieldName]['options'] = $Options;
            return true;

        }

        return false;


    }

    function GetOption(string $ReturnAs = 'option', string $FieldName = null) {
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;

        if (isset($this->FormField[$FieldName]['options']) && !empty($this->FormField[$FieldName]['options'])) {
            switch ($ReturnAs) {
                case 'option':
                    $Return = '';
                    foreach ($this->FormField[$FieldName]['options'] AS $Value => $Description) {
                        $Return .= "<option value=\"$Value\"" . (isset($this->FormInput[$FieldName]) && $this->FormInput[$FieldName] == $Value ? ' selected' : '') . ">$Description</option>";
                    }
                    break;
                default:
                    $Return = $this->FormField[$FieldName]['options'];
            }
        } else {
            die('Geen opties ingesteld');
        }

        return $Return;

    }

    function GetAntiSpam() {
        $now = time();
        return "<input type=\"hidden\" name=\"{$this->FormPrefix}{$this->FormName}_nobots\" value=\"\"/><input type=\"hidden\" name=\"{$this->FormPrefix}{$this->FormName}_rendered\" value=\"{$now}\" />";
    }

    function GetSubmit(string $Tekst = 'Verstuur formulier', string $ClassName = '') {
        $ClassName = !empty($ClassName) ? 'class="' . preg_replace('/.*class=\"([A-Za-z\-_ ]+)\".*/i', '\1', $ClassName) . '"' : '';
        return $this->GetAntiSpam() . "<button type=\"submit\" $ClassName>$Tekst</button>";

    }

    function ValidateForm() {
        if ($Data = $this->GetInput()) {
            if (!array_key_exists('nobots', $this->FormField)) {
                $this->AddField('hidden', 'nobots');
            }
            if (!array_key_exists('rendered', $this->FormField)) {
                $this->AddField('hidden', 'rendered');
            }


            foreach ($this->FormField AS $K => $V) {
                $Field = !empty($this->FormPrefix) && strpos($K, $this->FormPrefix) === 0 ? substr($K, strlen($this->FormPrefix), strlen($K) - strlen($this->FormPrefix)) : $K;
                $FieldName = str_replace('_', ' ', $Field);
                $ThisField = isset($this->FormField[$Field]) ? $this->FormField[$Field] : false;

                if (array_key_exists($Field, $Data)) {
                    $V = $Data[$Field];

                    $Pattern = isset($ThisField['pattern']) && !empty($ThisField['pattern']) ? $ThisField['pattern'] : false;
                    $ValidationType = isset($ThisField['validation']) && !empty($ThisField['validation']) ? $ThisField['validation'] : false;

                    if ($Pattern && !empty($V)) {
                        preg_match('/([A-Za-z]+)?:?(.*)/', $Pattern, $Validation);
                        $ValidationType = $Validation[1];
                        $Pattern = $Validation[2];

                        switch ($ValidationType) {
                            case 'filter':
                                $ThisField['error'] = !filter_var($V, $Pattern);
                                break;
                            case 'delay':
                                $ThisField['error'] = (!$V || $V == 0 || !(strtotime($V) >= strtotime(array_values($ThisField['options'])[0]) - $Pattern * 60));
                                break;
                            default:
                                $ThisField['error'] = !preg_match("/{$Pattern}/", $V);
                        }

                        if ($ThisField['error']) {
                            $this->AddError($Field);
                        }

                    } else {
                        switch ($K) {
                            case 'nobots';
                                if (!empty($V)) {
                                    $this->AddError('submit', 'We vermoeden dat het formulier door een bot is ingevoerd');
                                }
                                break;
                            case 'rendered';
                                if (time() - (int)$V < 1) {
                                    $this->AddError('submit', 'Het formulier is sneller ingevoerd dan menselijk waarschijnlijk is');
                                }
                                break;
                        }
                    }


                } else {
                    $V = null;
                }

                if (isset($ThisField['required']) && $ThisField['required'] && (!isset($V) || empty($V))) {
                    $this->AddError($Field, 'Het ' . $FieldName . ' veld is verplicht');
                }


                if (isset($ThisField['options'])) {
                    if ($ThisField['type'] === 'radio ') {
                        if (($ThisField['required'] || !empty($V)) && !array_key_exists($V, $ThisField['options']) && !$Pattern) {
                            $this->AddError($Field, 'Geen geldige keuze voor het ' . $Field . ' veld');
                        }

                    } elseif ($ThisField['type'] === 'checkbox') {
                        if (($ThisField['required'] || !empty($V)) && is_array($ThisField['options']) && is_array($V) && count(array_diff($V, $ThisField['options'])) > 0 && !$Pattern) {
                            $this->AddError($Field, 'Geen geldige keuze voor het ' . $Field . ' veld');
                        }
                    }
                }

            }

            unset($this->FormField['nobots'], $this->FormField['rendered']);

            $this->Success = empty($this->Error);
        }

        return $this->Success;

    }

    function GetFormAttribute() {
        return ' name="' . $this->FormPrefix . $this->FormName . '"' . ($this->FormMethod !== 'MULTIPART/FORM-DATA' ? ' method="' . $this->FormMethod . '"' : ' method="POST" enctype="' . $this->FormMethod . '"') . ' action="' . ($this->FormAction ? $this->FormAction : '#' . $this->FormPrefix . $this->FormName) . '"';
    }

    function GetFormId() {
        return ' id="' . $this->FormPrefix . $this->FormName . '"';
    }

    function GetAttribute(string $FieldName = null) {
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;

        $OutputParam = [
            'default' => [
                'type',
                'id',
                'name',
                'value',
                'pattern',
                'placeholder',
                'title',
                'required',
                'readonly'
            ],
            'textarea' => [
                'id',
                'name',
                'title',
                'placeholder',
                'required',
                'readonly'
            ],
            'radio' => [
                'type',
                'name',
                'title',
                'required',
                'readonly'
            ],
            'checkbox' => [
                'type',
                'name',
                'title',
                'required',
                'readonly'
            ]
        ];

        $Return = '';

        foreach ($this->FormField[$FieldName] AS $Parameter => $Attribute) {
            $AllowedParameter = isset($this->FormField[$FieldName]['type']) && isset($OutputParam[$this->FormField[$FieldName]['type']]) ? $OutputParam[$this->FormField[$FieldName]['type']] : $OutputParam['default'];

            if (!is_array($Attribute) && in_array($Parameter, $AllowedParameter)) {
                $FunctionCheck = 'Get' . ucfirst($Parameter);

                switch ($Parameter) {
                    case  'value':
                        if (is_callable([$this, $FunctionCheck])) {
                            $Attribute = $this->$FunctionCheck($FieldName);
                        }

                        $Return .= " $Parameter=\"$Attribute\"";

                        break;
                    default:
                        if (empty($Attribute)) {
                            if (is_callable([$this, $FunctionCheck])) {
                                $Attribute = $this->$FunctionCheck();
                            }
                        }

                        if (isset($Attribute) && $Attribute && !(strpos($Attribute, 'filter:') === 0)) {
                            $Return .= " $Parameter=\"$Attribute\"";
                        }

                }


            }


        }

        return $Return;
    }

    function GetAttributeByType(string $ParameterName, string $FieldName = null) {
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;

        $OutputParam = [
            'default' => [
                'type',
                'id',
                'name',
                'placeholder',
                'value',
                'pattern',
                'title',
                'required',
                'readonly'
            ],
            'textarea' => [
                'name',
                'id',
                'placeholder',
                'title',
                'required',
                'readonly'
            ],
            'radio' => [
                'type',
                'id',
                'name',
                'placeholder',
                'title',
                'required',
                'readonly'
            ],
            'checkbox' => [
                'type',
                'id',
                'name',
                'placeholder',
                'title',
                'required',
                'readonly'
            ]
        ];

        $Return = '';


        if (isset($this->FormField[$FieldName][$ParameterName])) {
            if (empty($this->FormField[$FieldName][$ParameterName])) {
                $FunctionCheck = 'Get' . ucfirst($ParameterName);
                if (is_callable([$this, $FunctionCheck])) {
                    return $this->$FunctionCheck();

                }
            }
            return $this->FormField[$FieldName][$ParameterName];
        }

        return false;
    }

    function GetError(string $FieldName = null, string $Tag = 'span', string $ClassName = null) {
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;
        $ErrorMessage = $FieldName && isset($this->Error) && is_array($this->Error) && array_key_exists($FieldName, $this->Error) ? implode('<br />', $this->Error[$FieldName]) : false;

        return $ErrorMessage ?
            "<$Tag" . (!empty($ClassName) ? ' class="' . $ClassName . '"' : '') . ">$ErrorMessage</$Tag>" :
            '';
    }

    function GetErrorClass(string $FieldName = null, string $ClassName = null) {
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;
        $ErrorClass = empty($ClassName) ? ' error' : $ClassName;
        return $FieldName && !empty($this->Error) && array_key_exists($FieldName, $this->Error) ? ' ' . $ErrorClass : '';


    }

    function GetErrorMessage(string $FieldName = null, string $Tag = 'span', string $ClassName = null) {
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;

        return "<$Tag" . ($ClassName ? ' class="' . $ClassName . '"' : '') . ">{$this->FormField[ $FieldName ]['title']}</$Tag>";
    }

    function SetInputValue($Value, string $FieldName = null) {
        $FieldName = $FieldName && !empty($FieldName) ? $FieldName : $this->CurrentField;
        $this->FormInput[$FieldName] = $Value;
        if (!array_key_exists($FieldName, $this->FormField)) {
            $this->FormField[$FieldName] = true;
        }

        return true;
    }

    function SetValue($Value, string $FieldName = null, bool $isDefaultValue = true) {
        $FieldName = $FieldName && !empty($FieldName) ? $FieldName : $this->CurrentField;

        if ($isDefaultValue) {
            $this->FormField[$FieldName]['default'] = $Value;

        } else {
            $this->FormField[$FieldName]['forcedValue'] = $Value;
        }

        return true;
    }

    function GetValue(string $FieldName = null) {
        $FieldName = $FieldName && !empty($FieldName) ? $FieldName : $this->CurrentField;

        if (isset($this->FormField[$FieldName]) && array_key_exists('forcedValue', $this->FormField[$FieldName]) && !empty($this->FormField[$FieldName]['forcedValue'])) {
            return $this->FormField[$FieldName]['forcedValue'];

        } elseif (isset($this->FormInput[$FieldName]) && !empty($this->FormInput[$FieldName])) {
            return $this->FormInput[$FieldName];

        } elseif (isset($this->FormField[$FieldName]) && array_key_exists('default', $this->FormField[$FieldName]) && !empty($this->FormField[$FieldName]['default'])) {
            return $this->FormField[$FieldName]['default'];

        }

        return '';

    }

    function SetField(string $FieldName, bool $FieldExists = true) {
        if ($FieldExists && !array_key_exists($FieldName, $this->FormField)) {
            return false;
        }

        $this->CurrentField = $FieldName;
        return true;
    }

    function GetField(string $FieldName = null) {
        $Return = false;
        $FieldName = !isset($FieldName) || empty($FieldName) ? $this->CurrentField : $FieldName;

        if ($Return = $this->FormField[$FieldName]) {
            $Return['value'] = $this->GetValue($FieldName);
        }

        return $Return;

    }

    function GetFile(string $FieldName = null) {
        $Return = false;
        if (!$this->FormInputFile) {
            $this->GetFormData();
        }

        if ($FieldName && isset($this->FormInputFile[$FieldName])) {
            $Return = $this->FormInputFile[$FieldName];
        } elseif (!isset($FieldName) && $this->FormInputFile) {
            $Return = $this->FormInputFile;
        }

        return $Return;

    }

    function SetSuccess() {
        $_SESSION['success'] = $this->FormName;
    }

    function IsSuccess() {
        $this->Success = (isset($_SESSION['success']) && $_SESSION['success'] == $this->FormName);
        unset($_SESSION['success']);
        return $this->Success;
    }

    function deployFormFields($data, $options = null) {
        /*
         * $data = $ContactForm->FormField;
         */
        $form_output = "";
        foreach ($data as $key => $field) {
            $size = (($field['type'] == "submit" || $field['type'] == "textarea") ? '12' : '12');
            $tag = ($field['type'] == "textarea" ? 'textarea' : 'input');
            $type = $field['type'];
            $btn = ($field['type'] == "submit") ? 'class="btn"' : '';
            $textarea_settings = 'cols="30" rows="10"';
            $form_output .= '<div class="col-md-' . $size . ' "><';

            $form_output .= $tag . ' type="' . $type . '"' . $btn . ' id="' . $field['id'] . '" name="' . $field['name'] . '"';
            $form_output .= (isset($field['value']) ? ' value="' . $field['value'] . '"' : '');
            $form_output .= 'placeholder="' . $field['placeholder'] . '"';
            $form_output .= $tag == "textarea" ? $textarea_settings : '';
            $form_output .= ($field['required'] ? 'required' : '');
            $form_output .= (isset($field['pattern']) ? ' pattern="' . $field['pattern'] . '"' : '');
            $form_output .= (isset($field['title']) ? ' title="' . $field['title'] . '"' : '');
            // postcode check input
            switch ($field['name']) {
                case strpos($field['name'], "_postcode") != false :
                    $form_output .= ' data-name="pc" data-class="postcode"';
                    break;

                case strpos($field['name'], "_straat") != false:
                    $form_output .= ' data-name="pc" data-type="straat"';
                    break;

                case strpos($field['name'], "_woonplaats") != false:
                    $form_output .= ' data-name="pc" data-type="woonplaats"';
                    break;

            }

            $form_output .= ' class="form-control" />' . ($tag == "textarea" ? "</textarea>" : "") . '
        </div>
   ';

        }


        if (isset($options)) {
            $form_output .= $this->deployOptions($options);

        }


        return $form_output;
    }

    public function deployOptions($options) {
        $radio_options = '';

        foreach ($options as $option) {
            $radio_options .= '<div class="input-group">
            <input type="checkbox" value="' . $option->id . '" name="hulpvraag[]"> ' . $option->vraag . '
            </div> ';
        }
        return $radio_options;
    }

    public function deployAlgemeneVoorwaarden() {
        $submit = '<div class="input-group av"><input type="checkbox" name="algemenevoorwaarden" required="required">Ja, ik ga akkoord met de &nbsp;<a href="'. SITELINK . '/algemene-voorwaarden" target="_blank">algemene voorwaarden</a>.</div>';
        return $submit;
    }
    public function deploySubmit($placeholder) {
        $id = $this->FormPrefix . $this->FormName;
        $submit = '<span class="help-button"><input type="submit" onclick="$(`#'.$id.'`).submit()" name="submit" placeholder="' . $placeholder . '" value="' . $placeholder . '" ><div class="bg"></div></span>';
        return $submit;
    }
}

?>