<?php 
	/**
	 * 
	 * @author Bandwerk, reclame- en internetburea / BD
	 * 
	 * class getData ( $db )
	 * @param	object	$db - PDO database connection required
	 * 
	 */
	
	
	class getData {
		protected $db;
		protected $allowedTable = array(
				'algemene info' => '`algemene_info`',
				'categorie' 	=> '`producten_categorie`',
				'headers'		=> '`headers`',
				'infoblok'		=> '`infoblok`',
				'nieuws'		=> '`nieuws`',
				'twitter'		=> '`twitter`'
			);
		protected $imgEditor = array(
				'infoblok'		=> '`infoblok_crop_photos`',
				'nieuws'		=> '`nieuws_crop_photos`',
				'headers'		=> '`headers_crop_photos`'
			);
		
		private $debug = false;
		private $foto = true;
		private $table;
		private $categorie;
		private $where;
		private $order;
		private $select;
		private $join;
		private $data;
		
		public function __construct( $pdo_db = null ) { 
			if( $pdo_db ) {
				$this->db = $pdo_db;
			} else {
				global $db;
				$this->db = $db;
			}
			
		}
		
		
		/**
		 * function set_cat
		 *
		 * @categorie 	string	Define categorie to get data for. ( categorie/groep/product );
		 */
		
		public function set( $categorie ) {
			$categorie = trim( strtolower( $categorie ) );
				
			if( array_key_exists( $categorie, $this->allowedTable ) ) {
				$this->table = $this->allowedTable[ $categorie ];
				$this->categorie = $categorie;
				$this->where = null;
				$this->order = null;
				$this->select = null;
				$this->join = null;
				
				if( !array_key_exists( $this->categorie, $this->imgEditor ) ) {
					$this->foto = false;
				}
				
				return true;
							
			} else {
				$this->jsLog( $categorie . ' is geen geldige categorie' );
				return false;
			}
				
		}
		
		
		/**
		 * where 'field' 'operator' 'value' 
		 * 		
		 * @param	string				$field
		 * @param	string|int|bool		$value
		 * @param	string|array			$operator - 
		 * 									Optional / array( 'compare fields', 'comparing to other rules' ) 
		 * 									default string '=';
		 * 									example: array( '=', 'AND' );
		 * 
		 * return	bool
		 */
		
		public function where( $field=null, $value=null, $operator = '=') {
			if( isset( $field ) && $field == 'all' && $value == 'reset' ) {
				$this->where = null;
				
			} elseif( !isset( $field ) && $value === null ) {
				$this->where = null;
				return true;
				
			}
			
			if( !isset( $this->where ) || $this->where === null ) {
				$this->where = new stdClass();
			}
			
			$field = str_replace( ' ', '', $field );
			
			if( is_array( $operator ) ) { 
				$operator[0] = !in_array( trim( $operator[0] ), ['IS NOT','LIKE','NOT LIKE','BETWEEN'] ) ? str_replace( ' ', '', $operator[0] ) : trim( $operator[0] );
				$operator[1] = str_replace( ' ', '', $operator[1] );
				
				$operator_1 = ' '. $operator[0] . ' ';
				$operator_2 = ' '. $operator[1] . ' ';
				
			} else {
				$operator = !in_array( trim( $operator ), ['IS NOT','LIKE','NOT LIKE','BETWEEN'] ) ? str_replace( ' ', '', $operator ) : trim( $operator );
				$operator_1 = ' '. $operator .' ';
				$operator_2 = ' AND ';
				
					
			}
			
			if( is_numeric( $value ) ) {
				$type = PDO::PARAM_INT;
			} elseif ( is_bool( $value ) ) {
				$type = PDO::PARAM_BOOL;	
			} else {
				$type = PDO::PARAM_STR;
			}
			
			if( !isset( $this->where->query ) ) {
				$this->where->query = array();
				$this->where->bindVal = array();
			}
			
			$rc = 0 + count( $this->where->query );
			
			array_push( $this->where->query, array( '`' . $field . '`' . $operator_1 . ':'. $field . $rc , $operator_2 ) );
			array_push( $this->where->bindVal, array( ':' . $field . $rc , $value, $type ) );
			
			unset( $field, $operator, $value, $type );
			return true;
		}
		
		
			/**
		 * How to order returned data ( ORDER BY $fields $order )
		 * $fields = false to reset
		 * 		
		 * @param	string|array	$fields
		 * @param 	string			$order	
		 */
		
		public function order( $fields, $order = 'DESC') {
			if( $fields == false ) { 
				$this->order = null;
				return true;
			}
			if( is_array( $fields ) ) {			
				if( isset( $order ) && trim( strtolower( $order ) ) !== 'desc' && trim( strtolower( $order ) ) !== 'asc') {
					$eachfield = $fields;
					$fields = array();
					$order = '';
					foreach( $eachfield AS $field => $order ) {
						if( strtolower( $order ) == 'desc' || strtolower( $order ) == 'asc' ) 
							array_push( $fields, '`'. $field . '` '. $order );			
					}	
				} else {
					array_walk( $fields, function( &$val, $key, $order) { 
						$val = '`'. trim( $val ) .'` '. $order; 
					}, $order );
				}
				$this->order = ' ORDER BY ' . implode( ', ' , $fields ) . ' ';

				
			} else {
				$this->order = ' ORDER BY ' . $fields . ' ' . $order . ' ';
			}
			
			return true;
		}
		
		
		/**
		 * Include images in returned data 
		 * $fields = false to reset
		 *
		 * @param	bool	@get - Get image or not (function default is false )
		 */
		
		public function img( $get = true ) {
			if( isset( $get ) && is_bool( $get ) ) {
				$this->foto = $get;
				return true;

			} elseif ( !isset( $get ) ) {
				$this->foto = false;
				return true;
			
			}
			return false;
		}
		
		public function join( $table, $on, $fields = null ) {
			if ( $this->table == null ) {
				$this->table = new stdClass();
			}
			
			if( in_array( $table, $this->allowedTable ) ) {
				$this->join->table = $table;
			} elseif( array_key_exists( $table , $this->allowedTable ) ) {
				$this->join->table = $this->allowedTable[ $table ];
			} else { 
				return false;
			}
			
			if( $fields = null ) { 
				$this->join->fields = $this->join->table. '`'. $fields . '`';
			}
			
		}
		
		
		/**
		 * function getThis
		 * Get data from the query
		 *
		 * @param	array	$fields - Define what fields you want returned. Default '*'
		 * @param	int		$limit	- Default is not limti
		 */		
		
		public function getThis( $fields = '*', $limit = '' ) {
			if( $fields === null || $fields === '*' ) {
				$fields = $this->table . '.*';
			}
			
			if( !isset( $this->table ) ) {
				return false;
				
			}
			
			$object = array();
			$bindVal = array();
			
			$where = '';
			$order = '';
			$img_join = '';
			$img_sel = '';
			$img_sel_join = '';
			
			$table = $this->table; 
		
			if( $fields !== $this->table . '.*' ) {
				$fields = !array( $fields ) ? [ $fields ] : $fields;
				
				array_walk( $fields, function( &$val) {
					$val = !in_array( str_replace('`', '', $val ), ['album_id','img','ext'] ) ? str_replace( '`', '', $this->table ) . '`.`'. $val : str_replace( '`', '', $this->imgEditor[ $this->categorie ] ) . '`.`'. $val;

				});
				$fields = '`' . implode( '`, `', $fields ) . '`';
			}
			
			if( isset( $this->where ) ) { 
				$first = true;
				$where = ' WHERE ';
				
				foreach( $this->where->query AS $value ) {
					if( isset( $first ) ) { 
						$value[1] = '';
						unset( $first );
					}

					$value[0] = explode( '`', $value[0] );
					$value[0] = isset( $this->imgEditor[ $this->categorie ] ) && in_array( $value[0][1] ,['album_id','img','ext'] ) ? $this->imgEditor[ $this->categorie ] . '.' . implode( '', $value[0] ) :$table . '.' . implode( '', $value[0] ) ;

					$where .= $value[1] . $value[0] ;
					
				}
				
			}
			
			if( isset( $this->order ) ) { 
				$order = $this->order;
			}
			
			if( isset( $limit ) && is_numeric( $limit ) ) {
				$limit = ' LIMIT ' . $limit;
			} elseif( strpos( $limit, ',' ) !== false ) {
				$limit = explode( ',', $limit );
				foreach( $limit AS $num ) {
					if( !is_numeric( $num ) ) return false;
				}
				$limit = ' LIMIT ' . implode( ',', $limit );
			}
			
			if( $this->foto === true ) {
				$img_join = $this->imgEditor[ $this->categorie ];
				$img_sel = ', '.$img_join.'.`album_id`';
				$img_sel_join = ' LEFT JOIN '. $img_join .' ON '.$table.'.id='. $img_join.'.album_id';
				
				if( strpos( $img_join , '_crop' ) !== false ) {
					$img_join = 'SELECT `id` AS `img`, `ext` FROM '. $img_join;

					
				} elseif ( strpos( $img_join , 'photo' ) !== false ) {
					$img_join = 'SELECT `id` AS `img` FROM '. $img_join;
				} else {
					return false;
				
				}
				
				$fields .= !strpos( $fields, $table . '.`id`' ) ? ', '.$table.'.`id`' : '';
				$where .= ' GROUP BY '. $table.'.id';
			}
			
			$Q = 'SELECT '.$fields.$img_sel.' FROM '. $table . $img_sel_join . $where . $order . $limit . ';';
			$query = $this->db->prepare( $Q );
			
			if( $this->debug ) echo '<pre>'. $Q .'</pre>';
			
			if( isset( $this->where->bindVal ) && $this->where->bindVal !== false ){
				foreach( $this->where->bindVal AS $bind ) {
					if( $this->debug ) {
						echo '<pre>';
						echo $bind[0] . ', ';
						echo $bind[1] . ', ';
						echo $bind[2];
						echo '</pre>';
					}
					
					$query->bindValue( $bind[0], $bind[1], $bind[2] );
				}
			}
			
			
			try {
				$query->execute();
				
				if( $query->rowCount() == 0 ) {
					return 0;
				}
				
				$object = array();
				
				while( $result = $query->fetch( PDO::FETCH_OBJ ) ) {
					
			if( $this->debug ) echo '<pre>'. $result->id .'</pre>';
					if( $this->foto === true ) {
						$img = $this->db->prepare( $img_join . ' WHERE `album_id`=:albumid ORDER BY `albumthumb` DESC, `updated_on` DESC LIMIT 1;');
						$img->bindValue( ':albumid', $result->id, PDO::PARAM_INT );
						
						try{ 
							$img->execute();
							
						} catch( PDOException $ex ) { 
							$img = $this->db->prepare( $img_join . ' WHERE `album_id`=:albumid ORDER BY `updated_on` DESC LIMIT 1;');
							$img->bindValue( ':albumid', $result->id, PDO::PARAM_INT );
							
							try{ 
								$img->execute();
							} catch( PDOException $ex ) {
								return 'error: ' .  $ex->getMessage();
							}
						}
						
						if( $img->rowCount() > 0 ) {
							$img = $img->fetch( PDO::FETCH_OBJ );
							
							$result->img = $img->img;
							if( isset( $img->ext ) ) {
								$result->ext = $img->ext;
							}
						}
						
						
					}
					
					array_push( $object, $result );
				}
				
				
				unset( $query );
		
				
			} catch( PDOException $ex ) {
				$this->jsLog( $ex->getMessage() );
			} catch( Exception $ex ) {
				$this->jsLog( $ex->getMessage() );
			}
			
			if( is_array( $object ) && count( $object ) == 0 && implode( '', $object ) == '' ) {
				return 0;
			
			}
			
			if( isset( $object ) && $object != '' ) {
				$this->data = $object;
				unset( $table, $where, $order, $select, $object);
				
				return $this->data;
				
				
			} else {
				return 0;
				
			}
			
		}
		
		/**
		 * Get images from categorie
		 * 
		 * @param int $album_id - default *
		 * @param array|string $orderBy - array( 'field1' => 'orderby1', 'field2' => 'orderby2' etc.... );
		 */
		
		public function getImages( $album_id = null, $orderBy = null ) {
			if( !is_string( $this->imgEditor[ $this->categorie ] ) ) {
				$this->jsLog( 'Geen afbeeldingstabel aan deze categorie gekoppelt.');
				return false;
			}
			
			$fields = '`id` AS `img`';
			$where = '';
			$order = '';
			
			if( strpos( $this->imgEditor[ $this->categorie ] , '_crop' ) !== false ) {
				$fields .= ', `ext`';
					
			} 
			
			if( is_numeric( $album_id ) ) {
				$album_id = (int) $album_id;
				$where = ' WHERE `album_id`='. $album_id;
			}
			
			if( is_array( $orderBy ) ) {
				$orderAr2 = array();
				
				foreach( $orderBy AS $thisField => $orderBy ) { 
					array_push( $orderAr2, '`'. str_replace('`', '', $thisField ) .'` '. $orderBy );
				}
				
				$order = ' ORDER BY '. implode( ', ', $orderAr2 );
				unset( $orderAr2 );
				
			} elseif( is_string( $orderBy ) ) { 
				$order = ' ORDER BY `' . trim( $orderBy ) . '` DESC';
			} 
			
			try { 
				$images = $this->db->query( 'SELECT '. $fields .' FROM '. $this->imgEditor[ $this->categorie ] . $where . $order . ';');
				$images = $images->fetchAll( PDO::FETCH_OBJ );
				return $images;
				
			} catch( PDOException $ex ) { 
				$this->jsLog( $ex->getMessage() );
			}
			
			$this->jsLog('Geen afbeeldingen gevonden' );
			return false;
		}
		
		
		public function debug( $toggle=null ){
			if( $toggle ) {
				$this->debug = $toggle;
				
			} else {
				$this->debug = $this->debug === true ? false : true;
			}
		}
		
		function getList() {
			
		}
		
		
		
		/**
		 * 
		 * @param string 		$term
		 * @param string|array 	$fields
		 * @param string|array	$return - fields to return in the object
		 * @param string 		$relation - AND / OR search
		 * @return object 
		 */
		
		public function search( $term, $fields, $return = null, $relation = 'OR' ) {
			$cat = $this->categorie;
			$table = $this->table;
			
			if( $return === null ) {
				$return = array( '*' );
			} elseif( is_string( $return ) ) {
				$return = array( trim( $return ) );
			} 
			
			$whereAr = array();
			$where = ' WHERE ';
			
			
			
			if( is_string( $term ) ) {
				$term = array( trim( $term ) );
			}
			if( is_string( $fields ) ) {
				$fields = array( trim( $fields ) );
			}
			
			
			foreach( $term AS $t) {
				$thisQ = array();
				
				foreach( $fields AS $field ) {
					array_push( $thisQ, '`' . $field .'` LIKE "%' . trim( $t ) . '%"' );
				}
				
				array_push( $whereAr, '(' . implode( ' OR ', $thisQ ) . ')' );
				
			}
			
			$where .= implode( ' AND ', $whereAr );
			
			$zoek = $this->db->prepare( 'SELECT '. implode( ', ', $return ) . ' FROM ' . $table . $where . ' ' . $this->order );
			
			try { 
				$zoek->execute();
				$ele = $zoek->fetchAll( PDO::FETCH_OBJ );
			} catch( PDOException $ex ) {
				$this->jsLog( $ex->getMessage() );
			}
	
			return $ele;
			
		}
		
		public function jsLog( $msg ) {
			$msg = htmlentities( $msg, ENT_QUOTES, 'UTF-8');
			echo '<script>alert("'.$msg.'");</script>';
			return true;
		}
				
	}
?>