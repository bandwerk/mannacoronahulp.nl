<?php

class session extends login {
	protected $db;
	protected $table;
	protected $salt;
	public $id;
	public $user;
	
	function __construct( $id, $table, $salt, $PDO=NULL ) {
		$this->id = (string) $id;
		$this->bake( 'id', $id ); 

		parent::__construct( $table, $salt, $PDO );
		
	}
	
	function set_user( user $user ) {
		if( isset( $user->idCard ) ) $user = $user->idCard;
		else $user->id = 0;
		
		$this->bake( 'userid', $user->id );
		
	}
	
	function get_data() {
		$obj = new stdClass();
		$obj->id = session_id();
		$obj->user = isset( $_SESSION['loggedin'] ) ? $_SESSION['loggedin'] : ['id' =>  0 ];
		return $obj;
	}
	
	function bake( $key, $val, $expire = 604800 ) {
		$val = is_array( $val ) ? serialize( $val ) : (string) $val;
		
		if( setcookie( 'ws['. $key .']', $val, time() + $expire ) ) return true;

		return false;
	}
	
	function bite( $key, $sub = false ) {
		if( !empty( $_COOKIE['ws' ][$key ] ) ) {
			$return = is_array( @unserialize( $_COOKIE['ws'][ $key ] ) ) ? unserialize( $_COOKIE['ws'][ $key ] ) : $_COOKIE['ws'][ $key ];
			return $return;
		}
		return false;
	}
	
	function burn( ) {
		if( isset( $_COOKIE['ws']  ) ) {
			foreach( $_COOKIE['ws'] AS $key => $val ) {
				setcookie( 'ws['. $key .']', '', 1 );
			}
			
		}
		
		return true;
		
	} 
	
	function logout() {
		if( $this->burn() ) {
			session_regenerate_id( true );
			
			$this->id = session_id();
			$this->user = 0;
			

			return parent::logout();
		}
		return false;
	}
	
}

?>