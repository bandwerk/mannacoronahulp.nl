<?php
function dump($var){
    echo "<pre>";
        var_dump($var);
        echo "</pre>";
}

function setPageSEO() {
    $data = GetPageId(true);

    if(is_object($data)) {
        if(!defined('SEO_MODULE')) {
            define('SEO_MODULE', $data->tabelnaam);
        }
        if(!defined('SEO_MODULE_ID')) {
            define('SEO_MODULE_ID', $data->tabelid);
        }
    }
}

function getPageSEO($module, $id) {
    global $db, $HTM;

    try {
        $Query = $db->prepare("
            SELECT `seo`.*
            FROM `SEO_Module_link` `seol`
            LEFT JOIN `SEO_Module` `seo`
                ON `seol`.`SEO_ID` = `seo`.`SEO_ID`
            WHERE `seol`.`tabelnaam` = :tabel
            AND `seol`.`tabelid` = :tabelid
        ");

        $Query->bindValue(":tabel", $module, PDO::PARAM_STR);
        $Query->bindValue(":tabelid", $id, PDO::PARAM_STR);

        $Query->execute();
        if($Query->rowCount()) {
            return $Query->fetch(PDO::FETCH_OBJ);
        } else {
            return false;
        }
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

function InputErrorClass($name, $Array){
    if (array_key_exists($name, $Array)){
        return 'errorinput';
    }
}

function BestaatCheck($id, $tabel){
// Voorbeeld gebruik van deze geweldige functie: BestaatCheck(35, 'gemaakte_toetsen', 'Deze toets blijkt niet (meer) te bestaan');
    global $db;
    $id = (int) $id;
    $Query = $db->query("SELECT id FROM $tabel WHERE id = '$id'");
    if ( $Query->rowCount() == 0){
        return false;
    } else {
        return true;
    }
}

function tijdgeleden ($time){
    $time = time() - $time; // to get the time since that moment
    $tokens = array (
        31536000 => 'jaar',
        2592000 => 'maand',
        604800 => 'week',
        86400 => 'dag',
        3600 => 'uur',
        60 => 'minuut',
        1 => 'seconde'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        if ($text == 'maand'){
            if ($numberOfUnits>1){
                $text = 'maanden';
            }
        } else if ($text == 'week'){
            if ($numberOfUnits>1){
                $text = 'weken';
            }
        } else if ($text == 'dag'){
            if ($numberOfUnits>1){
                $text = 'dagen';
            }
        } else if ($text == 'uur'){
            if ($numberOfUnits>1){
                $text = 'uren';
            }
        } else if ($text == 'minuut'){
            if ($numberOfUnits>1){
                $text = 'minuten';
            }
        } else if ($text == 'seconde'){
            if ($numberOfUnits>1){
                $text = 'seconden';
            }
        }

        return $numberOfUnits.' '.$text.' geleden';
    }
}



function DateConvertTo($datum, $formaat){
    if ($formaat == 'nl'){

        $datum = date_create($datum);
        $datum = date_format($datum, 'd-m-Y');

    } else {

        $datum = date_create($datum);
        $datum = date_format($datum, 'Y-m-d');
    }

    return $datum;
}

function MetaKeywords(){
    /*
        Deze functie stelt vast of keywords is ingevuld. Zo niet gebruik keywords van config.

        Update 2014: Titel uit nieuws/projectpagina/fotoalbum
    */
    global $HTM;
    if(defined('SEO_MODULE') && defined('SEO_MODULE_ID')) {
        $seoRes = getPageSEO(SEO_MODULE, SEO_MODULE_ID);
        if(is_object($seoRes) && strlen($seoRes->Keywords) > 1) {
            $HTM->set_meta( $seoRes, 'keyword' );
            return;
        }
    }

    global $website_id, $GET, $HEADER, $themapagina, $Pagina, $HTM;
    $return = array();

    if( $themapagina ) {
        global $themasite;
        return trim( strip_tags( $themasite['keywords'] ) );
    }

    $SEOMETA = GetSEOMETAById($website_id);
    $Returnvalue = '';

    if (empty($SEOMETA['Keywords'])){
        array_push( $return, $HEADER['Keywords'] );
        if( isset( $Pagina ) ) {

            $data = explode( ' ', $HTM->page_title() );

            foreach( $data AS $key => $keyword ) {
                if( is_string( $keyword ) && strlen( $keyword ) > 2 ) {
                    array_push( $return, $keyword );
                }
            }

            $content = html_entity_decode( $HTM->page_content( null, true ) );

            $return = array_merge( $return, array_keys( $HTM->most_used( $content ) ) );
            unset( $content );
        }
    } else {
        array_push( $return, $SEOMETA['Keywords'] );
    }

    if( isset( $Pagina['twitter'] ) ) {
        global $JAAR;
        $jaar = isset( $JAAR ) && is_numeric( $JAAR ) ? $JAAR : strftime( '%Y', time() );
        array_push( $return, 'Nieuwsoverzicht '. $jaar );
    }

    if( isset( $Pagina['actief_filter'] ) ) {
        if( !in_array( $Pagina['actief_filter'], $return ) ) array_push( $return, $Pagina['actief_filter'] );
    }

    if( isset( $Pagina['categorie'] ) && !empty( $Pagina['categorie'] ) && !in_array( $Pagina['categorie'], $return ) ) array_push( $return, $Pagina['categorie'] );
    $return = array_reverse( $return );
    $Returnvalue .= str_replace( [', ,', ',, '], ', ', implode( ', ', $return ) );

    $HTM->set_meta( $Returnvalue, 'keyword' );
}



function MetaDescription(){
    global $GET, $HEADER, $Q, $HTM, $Pagina;
    $website_id=PageID;
    $lengte = 250;

    /*
        Deze functie stelt vast of description is ingevuld. Zo niet gebruik description van config.

        Update 2014: Titel uit nieuws/projectpagina/fotoalbum
    */

    $SEOMETA = GetSEOMETAById($website_id);
    $Returnvalue = '';

    if (empty($SEOMETA['Description'])){
        $Returnvalue = $HEADER['Description'];

        if( isset( $Pagina ) && isset( $Pagina['inhoud'] ) && !empty( $Pagina['inhoud'] ) ) {
            $Returnvalue .= trim( $HTM->text_shorten( $Pagina['inhoud'], $lengte, 0, 0 ) );

        }
    } else {
        return $SEOMETA['Description'];
    }


    foreach( $GET AS $PID => $VAL ) {
        global $HTM;
        $mImg = null;
        $ret = '';

        if( !isset( $HTM ) || !is_object( $HTM ) ) $HTM = new htmlGenerator();

        switch( $PID ) {
            case 'tid':
                break;
            case 'url_titel':
                switch( $VAL ){
                    case 'contact':
                }
                break;
        }
        if( !empty( $ret ) ) $Returnvalue = $ret;
    }

    if( isset( $HTM->meta['description'] ) ) $Returnvalue = $HTM->meta['description'];

    return $Returnvalue;
}

function TitleTag(){
    global $website_id, $HEADER, $GET, $themapagina, $HTM, $Pagina;
    if( !isset ($HTM) || !is_object( $HTM ) ) $HTM = new htmlGenerator();
    $return = [ ];

    if(defined('SEO_MODULE') && defined('SEO_MODULE_ID')) {
        $seoRes = getPageSEO(SEO_MODULE, SEO_MODULE_ID);
        if(is_object($seoRes) && strlen($seoRes->titletag) > 1) {
            return (!empty( $HEADER['MasterTitle'] ) ? $seoRes->titletag .  $HEADER['MasterTitle'] : $seoRes->titletag);
        }
    }

    if( $themapagina ) {
        global $themasite;
        return trim( strip_tags( $themasite['werkbalk'] ) );
    }

    /*
        Deze functie stelt vast of de titletag is ingevuld. Zo niet gebruik website titel, zo niet gebruik menu titel.

        Update 2014: Titel uit nieuws/projectpagina/fotoalbum
    */

    $SEOMETA = GetSEOMETAById( PageID );
    $Returnvalue = '';

    if (empty($SEOMETA)){
        // SEO helemaal niet toegangkelijk.
        if (empty($Pagina['paginatitel'])){
            // Gebruik titel(van het menu)
            array_push( $return, $Pagina['titel'] );
        } else array_push( $return, $Pagina['paginatitel'] );


    } else {
        // gebruik seo
        if (empty($SEOMETA['titletag'])){
            // speciale titletag niet ingevuld.
            if (empty($Pagina['paginatitel'])){
                // Gebruik titel(van het menu)
                array_push( $return, $Pagina['titel'] );
            } else {
                array_push( $return, $Pagina['paginatitel'] );
            }
        } else {
            if( !empty( $HEADER['MasterTitle'] ) ) $SEOMETA['titletag'] .= $HEADER['MasterTitle'];
            return $SEOMETA['titletag'];
        }
    }

    $returnuc = $return;
    array_walk( $returnuc, function( &$val ) { $val = strtolower( $val ); } );

    $return = array_reverse( $return );
    if( isset( $HTM->meta['name'] ) && $T= explode( ' - ', $HTM->meta['name'] ) ) {
        if( count( $T ) > 1 ) array_pop( $T );
        $Returnvalue = implode( ' - ', $T ) . $HEADER['MasterTitle'];

    } else {

        if( !in_array( $HTM->page_title(), $return ) ) array_push( $return, $HTM->page_title() );
        if( isset( $Pagina['categorie'] ) && !empty( $Pagina['categorie'] ) && !in_array( strtolower( $Pagina['categorie'] ), $returnuc ) ) array_push( $return, ucfirst( $Pagina['categorie'] ) );
        if( !in_array( $HEADER['MasterTitle'], $return ) ) array_push( $return, $HEADER['MasterTitle'] );

        $Returnvalue = str_replace([ '  ', '- -', '- |' ] , [' ', '-', '|' ], implode( ' - ', array_filter( $return ) ) );

    }

    return $Returnvalue;
}

function GetChildrenFrom($website_id){
    global $db;
    $R = array();

    $Q = $db->prepare("SELECT id, titel, paginatitel, parent, aktief, script FROM website WHERE parent = '$website_id'");

    if( $Q->execute() ){
        while ($Result = $Q->fetch(PDO::FETCH_ASSOC)){
            $R[$Result['id']] = $Result;
        }
    }
    return $R;
}

function GetWebsiteTable($website_id){
    global $db;
    $Q = $db->prepare("SELECT * FROM website WHERE id = '$website_id'");
    if( $Q->execute() )
        $Result = $Q->fetch(PDO::FETCH_BOTH);

    return $Result;
}

function MakeIdLink($website_id){
    return SITELINK.'/?p='.$website_id;
}

function MakeLink($url){
    if (is_numeric($url)){
        // Het gaat om een id
        return SITELINK.'/?p='.$website_id;
    } else {
        // het gaat om een url-title
        return SEF($url);
    }
}

function GetParentOf($website_id){
    global $db;

    $Q = $db->prepare( 'SELECT `parent` FROM `website` WHERE `id`=:id LIMIT 1;');
    $Q->bindValue( ':id', $website_id, PDO::PARAM_INT );
    if ( $Q->execute() && $Q->rowCount() > 0 ){
        $Result = $Q->fetch( PDO::FETCH_ASSOC);
        return $Result['parent'];
    } else {
        return false;
    }
}

function GetwebsiteTitle($website_id){
    global $db;

    $Q = $db->prepare( 'SELECT `titel` FROM website WHERE id=:id LIMIT 1;');
    $Q->bindValue( ':id', $website_id, PDO::PARAM_INT );
    if( $Q->execute() && $Q->rowCount() > 0 ){
        $Result = $Q->fetch(PDO::FETCH_ASSOC);
        return $Result['titel'];

    }
}

function ReplaceContent($Content){
    $Find = '/klanten/'.DOMAIN.'/uploads/';
    $Replace = SITELINK.'/uploads/'; /* HTACCESS REPLACE VOOR NODIG */

    $Content = str_replace ($Find, $Replace, $Content);
    $Content = str_replace ("http://http://", ( defined( 'PROTOCOL' ) ? PROTOCOL : 'http' ) . "://", $Content);

    //  $content = str_replace ("<p>", "", $content);
    //  $content = str_replace ("</p>", "<br/><br/>", $content);

//	$Content = preg_replace('#<img(.*?)src="([^"]*/)?(([^"/]*)\.[^"]*)"([^>]*?)>((?!</a>))#', '<a rel="group" class="fancybox fancy" title="" href="$2$3"><img$1src="$2$3"$5></a>', $Content);

    return $Content;
}

function get_file_extension($file_name){
    return substr(strrchr($file_name,'.'),1);
}

function Truncate($str, $length=60, $trailing='...'){
    // take off chars for the trailing
    $length-=strlen($trailing);
    if (strlen($str) > $length)
    {
        // string exceeded length, truncate and add trailing dots
        return substr($str,0,$length).$trailing;
    }
    else
    {
        // string was already short enough, return the string
        $res = $str;
    }

    return $res;
}

function Icon($Name){
    global $Sitelink;
    return "<img align=\"absmiddle\" src=\"$Sitelink/Images/Ico/$Name\" width=\"16\" height=\"16\" alt=\"\" />";
}

function safe_text($Text=''){
    if(is_array($Text)) {
        return;
    }
    $Text = trim($Text);
    $Text = addslashes($Text);
    return htmlspecialchars($Text);
}

function DoQuery($Query){
    global $db;

    if (!$Result = $db->query( $Query )){
        return false;
    } else {
        return $Result;
    }
}

function input($Type, $Name, $Value=''){
    echo "<input type=\"$Type\" name=\"$Name\" value=\"$Value\" />";
}

function SendMail($From, $To, $Subject, $Message){
    $Headers = 'From: '.$From;

    if (!mail($To, $Subject, $Message, $Headers)){
        return false;
    } else {
        return true;
    }
}

function ContactForm($Naam='', $Tel='', $Email='', $Bericht=''){

    $Return = '
		<object style="width: 200px;">
		<form class="" action="'.SITELINK.'/contact.html" method="post">
			<fieldset>
				<input style="width: 300px; border: 1px solid #848282; float: none;" class="input-call-me" type="text" name="Naam" placeholder="Uw naam"';

    if (!empty($Naam)){
        $Return .= 'value="'.$Naam.' "';
    }

    $Return .= '/><input style="width: 300px;border: 1px solid #848282; float: none;" class="input-call-me" type="email" name="Email" placeholder="Uw email" ';

    if (!empty($Email)){
        $Return .= 'value="'.$Email.' "';
    }

    $Return .= '/><input style="width: 300px;border: 1px solid #848282; float: none;" class="input-call-me" type="text" name="Tel" placeholder="Uw telefoonnummer" ';
    if (!empty($Tel)){
        $Return .= 'value="'.$Tel.' "';
    }

    $Return .= '/><textarea style="width: 300px; border: 1px solid #848282; float: none; height: 100px;" class="input-call-me" name="Bericht" placeholder="Uw vraag/opmerking">';

    if (!empty($Bericht)){
        $Return .= $Bericht;
    }

    $Return .= '</textarea><br />
				<input style="height: 23px;" class="submit-call-me" type="submit" value="Verstuur" alt="Verzenden" />
			</fieldset>
		</form>
		</object>
		';

    return $Return;
}

function TerugBelForm($Naam='', $Tel=''){
    $Return = '
	<form action="'.SITELINK.'/call-me-back.html" method="post">
		<strong>Uw naam:</strong><br />
		<input style="width: 200px; border: 1px solid #848282; float: none; margin-top: 5px;" class="input-call-me" type="text" placeholder="Laat hier uw naam achter" name="Naam" ';

    if (!empty($Naam)){
        $Return .= 'value="'.$Naam.' "';
    }

    $Return .= '/>
		<strong>Uw telefoonnummer:</strong><br />
		<input style="width: 200px; border: 1px solid #848282; float: none; margin-top: 5px;" class="input-call-me" type="text" placeholder="Laat hier uw telefoonnummer achter" name="Tel" ';

    if (!empty($Tel)){
        $Return .= 'value="'.$Tel.' "';
    }

    $Return .= '/>
		<input style="border: 1px solid #848282; float: none;" class="submit-call-me" type="submit" value="Verstuur" name="submit" />
	</form>';

    return $Return;
}

function Pre($Array){
    echo '<pre style="background-color: black; color: red;">';
    print_r($Array);
    echo '</pre>';
}

function twitterify($ret) {
    $ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
    $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
    $ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
    $ret = preg_replace("/#(\w+)/", "<a href=\"http://www.twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $ret);
    $ret = str_ireplace( '\n', '<br />', $ret );
    return $ret;
}



function makeClickableLinks($s) {
    return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $s);
}

function getCurrentURL()
{
    $currentURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    $currentURL .= $_SERVER["SERVER_NAME"];

    if($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443")
    {
        $currentURL .= ":".$_SERVER["SERVER_PORT"];
    }

    $currentURL .= $_SERVER["REQUEST_URI"];
    return $currentURL;
}

function toMoney( $float, $tags=false, $shortest=true) {
    $ret=number_format( $float , 2, ",", "." );
    if( $tags ) {
        $ret=explode(',', $ret );
        $ret=$ret[0].'<small>,</small><sup>'.$ret[1].'</sup>';
    }
    if( $shortest ){
        $sh_ret = strip_tags( $ret );
        $sh_ret = explode( ',', $sh_ret );
        if( $sh_ret[1] == '00' ) $ret = $sh_ret[0] . ',-';
    }
    return $ret;
}

function getPageTitle( $array ) {
    if( isset( $array['paginatitel'] ) && $array['paginatitel'] !== NULL && str_replace( ' ', '', $array['paginatitel'] ) !== '' )  {
        return $array['paginatitel'];
    } else {
        return $array['titel'];
    }
}

/*	Function to check for content
 *
 *  @param	string|array 	$value - input to check for content
 *  @param	boolean			$get - true to return content (if string), false to return true/false
 */
function hasContent( $value, $get = false ) {
    $ret = false;
    if( !empty( $value ) ):
        switch( gettype( $value ) ) {
            case 'string':
                if( $get ) $ret = strlen( trim( $value ) ) > 0 ? $value : false;
                else $ret = strlen( trim( $value ) ) > 0 ? true : false;

                break;
            case 'array':
                if( isset( array_values( $value )[0] ) && is_object( array_values( $value )[0] ) ) {
                    $ret = get_object_vars( array_values( $value )[0] );
                    $ret = !empty( $ret ) ? true : false;
                } else {
                    $ret = count( $value ) > 0 && !empty( array_values( $value )[0] )? true : false;
                }

                break;
            case 'object':
                $ret = !empty( $value ) ? true : false;
                break;

            default:
                $ret = false;
                break;
        }
    endif;

    return $ret;
}

function getImagePath( $cat, $type = null ) {
    if( isset( $type ) ){
        $type = '/' . generate_url_from_text( trim( $type ) );
    } else {
        $type = '';
    }

    $url = 'http://www.bandwerkplus.nl/klanten/' . DOMAIN . '/uploads/image/' . $cat . $type;
    return $url;

}

function jsLog( $msg, $log='log', $lengte=144 ) {
    return '<script>console.'.$log.'('. htmlGenerator::text_shorten( $msg, $lengte, false, false ) . ');</script>';

}

/**
 *
 * @param string 	$string - de in te korten tekst
 * @param int 		$lengte - maximale lengte tekst
 * @param boolean 	$allowTags strip_tags met/zonder 'ALLOWED_TAGS' global toepassen
 * @return string	Als de tekst binnen het maximum blijft onveranderd, anders gestript en gewrapped in <p></p>
 */
function tekstKorten ( $string, $lengte, $allowTags = false, $wrap = true ) {
    return htmlGenerator::text_shorten( $string, $lengte, $allowTags = false, $wrap = true );
}

function getListInfo( $id, $type ) {
    global $db, $Q;

    if( is_numeric( $id ) && is_string( $type ) ) {
        $id = (int) $id;
        $thisData = $Q;

        $options = array();
        $options['project'] = new stdClass();
        $options['project']->get = '`project`';
        $options['project']->where = '`product`';
        $options['product'] = new stdClass();
        $options['product']->get = '`product`';
        $options['product']->where = '`project`';

        if( strtolower( $type ) == 'project' || strtolower( $type ) =='projecten' ) {
            $type = 'project';
        } elseif( strtolower( $type ) == 'product' || strtolower( $type ) == 'producten' ) {
            $type = 'product';
        } else {
            return false;
        }

        try {
            $getList = $db->query('SELECT '. $options[ $type ]->get .' AS `id` FROM `producten_projecten` WHERE ' . $options[ $type ]->where .'= "'. $id .'";');
            $getList->execute();

        } catch( PDOException $ex ) {
            echo 'PDO Exception getList: ' . $ex->getMessage();

        } catch( Exception $ex ) {
            echo 'Exception getList: '. $ex->getMessage();

        }

        if( $getList->rowCount() == 0 ) {
            return false;
        } else {
            try{
                $list = $getList->fetchAll( PDO::FETCH_OBJ );

            } catch (PDOException $ex ) {
                die( $ex->getMessage() );
            }
        }

        $thisData->set( $type . 'en' );
        $thisData->img( false );
        $thisData->order( 'titel', 'ASC' );
        foreach( $list AS $itemId ) {
            $thisData->where( 'id', $itemId->id, array( '=', 'OR' ) );
        }

        $list = $thisData->getThis( array( 'id', 'titel' ) );

        unset( $thisData );
        return $list;
    }

    return false;
}

function getSizeHeader( $url ) {
    $size = get_headers( $url );

    foreach ($size as $key => $value) {
        if (false !== stripos( $value, 'Content-Length: ') ) {
            $fileSize = str_replace('Content-Length: ', '', $value );
        }
    }
    unset( $size );
    if( isset( $fileSize ) )
        return $fileSize;
}

function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {

        $bytes = '0 bytes';
    }

    return $bytes;
}

class htmlGenerator {
    var $script = array();
    public $meta = array();

    function _construct(){}

    function get_alt( $string, $altonly=true ) {
        $string = htmlentities( trim( $string ) );
        $return = $altonly ? ' alt="'. $string .'"' : ' alt="'. $string .'" title="'. $string .'"';

        return $return;

    }

    function get_bt_handle( $event, $filter_id, $extra = '' ) {
        if( !empty( $extra ) && is_array( $extra ) ) {
            $add = [];
            foreach( $extra AS $type => $val ) {
                $add[] = 'data-'. str_replace(' ', '', $type ).'="'. str_replace(' ','', $val ).'"';
            }
            $extra = ' ' . implode( ' ', $add );
        } else $extra = '';

        return ' bt-event-type="'. trim( $event ) .'" data-filter-id="'. trim( $filter_id ) .'"' . $extra;
    }

    function get_cart_js( $id, $hash, $alt="", $event = '' ) {
        $extra = '';

        if( is_array( $id ) ):
            if( isset( $id['bijproduct'] ) && count( $id['bijproduct'] ) > 0 && !empty( $id['bijproduct'][0] ) ) $extra .= ' data-product-bijproduct="true"';
            if( isset( $id['bijproduct'] ) && isset( $id['bijproduct']['id'] ) ) $extra .= ' data-bijproduct-id="'. (int) $id['bijproduct']['id'] . '"';
            $id = (int) $id['id'];

        elseif( is_object( $id ) ):
            if( isset( $id->bijproduct ) && count( $id->bijproduct ) > 0 && !empty( $id->bijproduct[0] ) ) $extra .= ' data-product-bijproduct="true"';
            if( isset( $id->bijproduct ) && isset( $id->bijproduct->id ) ) $extra .= ' data-bijproduct-id="'. (int) $id->bijproduct->id . '"';
            $id = (int) $id->id;

        else:
            $id = (int) $id;
        endif;


        $event = !empty( $event ) ? str_replace( ' ', '', $event ) : 'add_product';
        $return = ' bt-event-type="'. $event .'" data-product-id="'. $id .'" data-product-check="'. $hash .'"' . $extra;
        if( isset( $alt ) && !empty( $alt ) )
            $return .= $this->get_alt( $alt );

        return $return;

    }

    function get_ele( $ele, $settings = [] ) {
        $blocks = scandir('includes/block');

        $ele = trim( $ele );

        if( in_array( $ele . '.php', array_values( $blocks ) ) ) {
            $file = 'includes/block/';
        } else {
            $file = 'includes/';
        }

        $file .= str_ireplace( array( '.php','.html','.htm','block/block' ), array( '','','','block') , $ele ) .'.php';

        if( file_exists( $file ) ) {
            foreach( $settings AS $key => $val ) {
                if( !empty( $val ) ) $$key = $val;

            }

            require( $file );
            return true;
        }
        return false;
    }

    function get_href( $link, $alt, $class='' ) {
        $ret = ' href="'. trim( $link ) . '"';
        $ret .= !empty( $alt ) ? $this->get_alt( $alt, false ) : '';
        $ret .= !empty( $class ) ? ' class="'. $class . '"' : '';
        return $ret . ' ';
    }
    function get_src( $src, $alt, $class='', $altonly=true ) {
        $ret = strpos( $src, 'lazy:' ) === 0 ? ' src="'. IMAGES . '/info.png" data-lazy="'. substr( trim( $src ) , 5) . '"' : ' src="'. trim( $src ) . '"';
        $ret .= !empty( $alt ) ? $this->get_alt( $alt, $altonly ) : '';
        $ret .= !empty( $class ) ? ' class="'. $class . '"' : '';

        return $ret;
    }

    function get_lightbox( $link, $alt, $groep, $titel, $class = '' ) {
        $this->lightbox = true;

        $ret = ' href="'. trim( $link ) . '"';
        $ret .= !empty( $alt ) ? $this->get_alt( $alt ) : '';
        $ret .= ' title="'. trim( $titel ) . '"';
        $ret .= ' data-rel="lightcase:'. trim( $groep ) . '"';

        $ret .= !empty( $class ) ? ' class="'. $class . '"' : '';

        return $ret;
    }

    function get_fancy_image( $img, $thumb, $full, $alt="", $rel='group', $class="" ) {
        return '<a'. $this->get_href( str_replace( ['//',':/'], ['/','://'], $full . '/' . $img ), 'Vergroot: '. $alt, 'fancybox fancy' ) .' rel="'. $rel .'">
					<img'. $this->get_src( str_replace( ['//',':/'], ['/','://'], $thumb . '/' . $img ) , 'Afbeelding '. $alt, $class ) . ' />
				</a>';
    }


    function get_opsomming( array $array, string $order = 'az' ) {
        switch( $order ) {
            case 'az';
                sort( $array );
                break;
        }
        $return = is_array( $array ) && count( $array ) > 1 ?' en ' . array_pop( $array ) : '';
        $return = implode( ', ', $array ) . $return;
        return $return;
    }

    function get_postcode( $postcode ) {
        $postcode = strtoupper( str_replace( ' ','', $postcode ) );
        return substr( $postcode, 0, 4 ) .' '. substr( $postcode, 4, 2 );
    }

    function get_tel( $tel, $setting = null ) {
        $REG = '/[^0-9]/';
        $landcode = '31 ';
        if( isset( $setting ) && is_array( $setting ) ):
            foreach( $setting AS $adjust => $value ):
                $$adjust = $value;
            endforeach;
        endif;

        if( $landcode ) return '+' . preg_replace( $REG, '', $landcode . substr( trim( $tel ), 1) );
        else return preg_replace( $REG, '', $tel );

    }

    /*
     * string	$script	the javascript or url of the javascript to add
     * bool		$url	(default = true) set to false for javascript, true for URL to javascript
     */
    function set_js( $script, $url = true ) {
        if( $url == true ) {
            if( is_array( $script ) ) $script = array( $script );

            foreach( $script AS $path ) {
                if( is_array( $path ) ) {
                    foreach( $path AS $js ) {
                        if( !is_array( $js ) ) $js = [ $js ];
                        if( !in_array( $js, $this->script ) ) array_push( $this->script, $js );
                    }
                } else
                    if( !in_array( $path, $this->script ) ) array_push( $this->script, $path );
            }

        } else {
            if( is_array($script ) ) {
                foreach( $script AS $js ) {
                    $js = str_replace( [
                        "\n", "\t", '     ', '    ', '   ', '  '],
                        [ "\r", ' ', ' ', ' ', ' ', ' '],
                        $js );
                    if( !in_array( $js, $this->script ) ) array_push( $this->script, $js );
                }

            } else {
                $js = str_replace( [
                    "\n", "\t", '     ', '    ', '   ', '  '],
                    [ "\r", ' ', ' ', ' ', ' ', ' '],
                    $script );
                if( !in_array( $js, $this->script ) ) array_push( $this->script, $js );
            }

        }
    }

    function get_js() {
        if( isset( $this->lightbox ) && $this->lightbox ) {
            $this->set_js( array( JAVASCRIPT . '/lightcase.js' ) );
            $this->set_js('$("a[data-rel^=\'lightcase\']").lightcase({
						showTitle: false,
					maxWidth: "94%",
					maxHeight: "94%"
				});', false );
        }

        if( count( $this->script ) > 0 ) {
            $scripts = $this->script;
            $Hl = '';

            foreach( $scripts AS $script ) {
                if( is_array( $script ) ) {
                    foreach( $script AS $url ) {
                        $attr = '';
                        $url = explode( '##', $url );
                        if( isset( $url[1] ) ) {
                            $tempURL = $url;
                            $url = [ array_shift( $tempURL ) ];
                            $tempUrl = array_shift( $tempURL );
                            $attr = is_string( $tempURL ) ? ' '. implode( ' ', explode( ',', $tempUrl ) ) : '';
                        }
                        $Hl .= '<script src="'. $url[0] .''.$attr.'"></script>';
                    }
                } else {
                    $Hl .= '<script type="text/javascript" language="javascript">'. $script .'</script>';
                }
            }
            return $Hl;
        }

        return false;
    }


    function set_meta( $input, $type = null ) {
        if( isset( $input ) && ( is_array( $input ) || is_object( $input ) ) ) {
            foreach( $input AS $type => $value ):
                $meta[ trim( $type ) ] = trim( $value );
            endforeach;

        } elseif( is_string( $input ) && isset( $type ) && trim( $type ) !== '' ) {
            $cType = 1;

            switch( $type ) {
                case 'keyword':
                case 'keywoord':
                case 'keywords':
                    $cType = 3;
                    $type = array( 'keywords' );
                    break;

                case 'description':
                case 'og:description':
                case 'twitter:description':
                    $cType = 1;
                    $type = array( 'description', 'twitter:description', 'og:description' );
                    break;

                case 'title':
                case 'titel':
                case 'name':
                case 'twitter:title':
                    $cType = 2;
                    $type = array( 'name', 'twitter:title', 'og:title' );
                    break;

                case 'image':
                case 'og:image':
                case 'twitter:image':
                    $cType = 2;
                    $type = array( 'image', 'twitter:image:src', 'og:image' );
                    break;
            }

            if( is_string( $type ) ) {
                $type = array( $type );
            }
            foreach( $type AS $thisType ) {
                $thisType = trim( $thisType );
                switch( $cType ) {
                    case 1:
                        $this->meta[ $thisType ] = htmlentities( $this->text_shorten( str_replace( '&nbsp', ' ', trim( $input ) ), 200, '', false ) );
                        break;
                    case 2:
                        $this->meta[ $thisType ] = trim( $input );
                        break;
                    case 3:
                        if( !isset( $this->meta[ $thisType ] ) ) $this->meta[ $thisType ] = [];

                        $tempInput = explode( ',', $input );
                        foreach( $tempInput AS $input ):
                            $input = strtolower( trim( $input ) );
                            if( !in_array( $input, $this->meta[ $thisType ] ) ) $this->meta[ $thisType ][] = $input;
                        endforeach;
                        break;
                }
            }
        }
    }

    function set_keyword( $input ) {
        if( !empty( $input ) ):
            if( !is_array( $input ) ) $input = strip_tags( str_replace( '\n', ' ', $input ) );
            $this->set_meta( $input, 'keyword' );
        endif;
    }

    function get_meta( $type = null ){

        if($type !== null && defined('SEO_MODULE') && defined('SEO_MODULE_ID')) {
            global $HTM;
            $seoRes = getPageSEO(SEO_MODULE, SEO_MODULE_ID);
            $newType = $type;

            switch ($type) {
                case 'title':
                    $newType = 'titletag';
                    break;
                case 'description':
                    $newType = 'Description';
                    break;
            }

            if(is_object($seoRes) && strlen($seoRes->{$newType}) > 1) {
                $HTM->set_meta( $seoRes->{$newType}, $type );
                return;
            }
        }

        if( count( $this->meta ) == 0 )
            return false;


        $return = '';

        foreach( $this->meta AS $name => $content ) {
            if( !isset( $type ) || stripos( $name, $type ) !== false ){
                if( is_array( $content ) ) {
                    $content = implode( ', ', array_filter( $content ) );
                }
                if( is_string( $content ) ){
                    if( strlen( $content ) > 0 ) {
                        if( strpos( $name, 'og:' ) !== false ) {
                            $return .= '<meta property="'. $name . '" content="'. $content .'" />'."\n";
                        } else {
                            $return .= '<meta name="'. $name . '" content="'. $content .'" />'."\n";
                        }

                    }
                }
            }
        }

        return $return;


    }

    function get_titletag(){
        global $HEADER;
        $titel = ( isset( $this->meta['name'] ) && !empty( $this->meta['name'] ) ) ?
            htmlentities( $this->meta['name'] ) : TitleTag();
        return '<title>'. $titel . '</title>';

    }

    function most_used( $input, $limit=10 ){
        // Array om te retourneren
        $return = [];

        // Array met lidwoorden en veel voorkomende, te negeren worden
        $ignore = ['tot', 'het','een','hij','zij','wij','hen','den','ten','van','deze','voor','dat','dan','met','kan','bij','bijna','voor','heeft','worden','doet','kiest'];
        if( !is_string( $input ) ) $input = implode( ' ', $input );

        // Woorden splitsen en stuk voor stuk optellen
        foreach( explode( ' ', str_replace( [',', '.', ':' ], ' ', strtolower( strip_tags( $input ) ) ) ) AS $WORD ):
            //Spaties weghalen voor en achter het word
            $WORD = trim( $WORD );

            global $dev;

            //Als het woord langer is dan 2 karakters en niet in de ignore array zit toevoegen aan array
            if( strlen( $WORD ) > 2 && !in_array( $WORD, $ignore ) ) {
                // Key aanmaken in array, als bestaat dan bij op tellen
                $return[ $WORD ] = isset( $return[ $WORD ] ) ? $return[ $WORD ] + 1 : 1;
            }
        endforeach;
        // Array sorteren, meestvoorkomend boven
        arsort( $return );

        // Overbodige resultaten (overtallig aan 10/$return) weghalen
        array_splice( $return, $limit );

        // Meest voorkomende woorden retourneren
        return $return;

    }

    function set_modal( $id = 'mijnModal', $settings = null ) {

        $default = array(
            'class' 			=> 'modal fade',
            'tab-index' 		=> '-1',
            'role' 				=> 'dialog',
            'aria-labelledby'	=> 'mijnModalLabel',
            'aria-hidden' 		=> 'true' );

        if( !is_array( $settings ) ) {
            $settings = $default;
        } else {
            $settings = array_merge( $default, $settings );
        }

        $implodeSettings = array();
        $titel = '';
        $size  = '';

        foreach( $settings AS $key => $val ) {
            $push = false;

            switch( $key ) {
                case 'contentBody':
                    $contentBody = trim( $val );
                    break;
                case 'contentTitle':
                    $titel = trim( $val );
                    break;
                case 'contentFooter':
                    $footer = trim( $val );
                case 'size':
                    $size = ' '. trim( $val );
                    break;
                default:
                    array_push( $implodeSettings, $key . '="' . $val .'"' );
                    break;
            }
        }

        if( !isset( $contentBody ) ) $contentBody = '<iframe id="'.$id.'iframe" name="'.$id.'iframe" width="100%" style="min-height: 400px;max-height: 80%" src="about::blank" frameborder="0" allowfullscreen></iframe>';
        if( !isset( $footer ) ) $footer = '<button type="button" class="btn btn-default" data-dismiss="modal" >Sluiten</button>';

        $id = (string) str_replace( '#', '', trim( $id ) );

        $modal = '  <!-- Modal -->
            <div id="'. $id .'" '. implode( ' ', $implodeSettings ) . '>
              <div class="modal-dialog'. $size .'">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="'. $settings['aria-labelledby'] .'">'. $titel . '</h4>
                  </div>
                  <div class="modal-body">
			    		'. $contentBody .'
                  </div>
                  <div class="modal-footer">
					'. $footer . '
	              </div>
                </div>
              </div>
            </div>';

        return $modal;
    }

    function get_modal( $id = 'mijnModal', $extraAttr = "" ) {
        $id=(string) str_replace( "#", '', trim( $id ) );

        if( is_string( $extraAttr ) ) {
            $extraAttr = ' ' . trim( $extraAttr );
        } else {
            $extraAttr = '';
        }

        return 'data-toggle="modal" data-target="#'. $id . '"' . $extraAttr;
    }

    public static function text_link ( $string, $target = '_blank', $altprefix = 'Ga naar:' ) {
        return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" '.$this->get_alt( $altprefix . ' $1' ).' target="'. $target .'">$1</a>', $string);
    }

    public static function text_shorten ( $string, $lengte, $allowTags = false, $wrap = true ) {
        if( !is_string( $string ) ) return;
        $lengte = (int) trim( $lengte );

        if( is_string( $allowTags ) ) {
            $string = strip_tags( $string, $allowTags );
        } elseif( $allowTags == true ) {
            $string = strip_tags( $string, ALLOWED_TAGS );
        } else {
            $string = strip_tags( $string );
        }

        if (strlen($string) > $lengte) {
            // truncate string
            $stringCut = substr($string, 0, $lengte);

            // make sure it ends in a word so assassinate doesn't become ass...
            $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
        }

        if( $wrap == true ) {
            $string = '<p>' . $string . '</p>';
        }

        return $string;
    }

    function text_social( $haystack, $needle, $prefix = '' ) {
        if( $pos = stripos( trim( $haystack ), $needle ) + strlen( $needle ) ) {
            return $prefix . substr( $haystack, $pos, strlen( $haystack ) );
        } else {
            return $prefix . trim( $haystack );
        }
    }

    function page_subtitle( $tag = '', $class = '', $array='') {
        global $Pagina;
        if( empty( $array ) ) $array = $Pagina;
        if( !isset( $array['sub_paginatitel'] ) ) return false;
        $subtitel = trim( htmlentities( $array['sub_paginatitel'] ) );

        if( empty( $subtitel ) ) return '';

        if( isset( $class ) && trim( $class !== '' ) ) {
            $class = ' class="'. $class .'"';
        } else {
            $class = '';
        }

        if( isset( $tag ) && trim( $tag ) !== '' ) {
            $tag = strtolower( str_ireplace( array( '<', '>', '/'), '', $tag ) );

            $subtitel = '<'. $tag . $class .'>' . $subtitel . '</'. $tag .'>';
            return $subtitel;
        } else {
            return $subtitel;
        }

    }

    function page_title( $array = null) {
        if( is_array( $array ) ) {
            $Pagina = $array;
        } elseif( is_string( $array ) ) {
            $Pagina['paginatitel'] = $array;
        } else {
            global $Pagina;
        }

        if( isset( $Pagina['paginatitel'] ) && $Pagina['paginatitel'] !== NULL && str_replace( ' ', '', $Pagina['paginatitel'] ) !== '' )  {
            return $Pagina['paginatitel'];
        } elseif( isset( $Pagina['titel'] ) ) {
            return $Pagina['titel'];
        } else {
            return false;
        }
    }

    function page_content( $array = null, $striptags = false ) {
        if( is_array( $array ) )
            $Pagina = $array;
        elseif( is_string( $array ) )
            $Pagina['inhoud'] = $array;
        else
            global $Pagina;

        if( isset( $Pagina['inhoud'] ) && $Pagina['inhoud'] !== NULL && str_replace( ' ', '', strip_tags( $Pagina['inhoud'] ) ) !== '' )  {
            $return = !( stripos( $Pagina['inhoud'], '<p ' ) >=0 || stripos( $Pagina['inhoud'], '<p>')>=0 ) ?
                '<p>'. trim( $Pagina['inhoud'] ) .'</p>' : trim( $Pagina['inhoud'] );

            if( $striptags ) {
                $return = strip_tags( $Pagina['inhoud'] );
            }

            return $return;


        } else {
            return false;
        }
    }

    function get_script( $source ) {
        global $Pagina;
        $Pagina ['is_script'] = false;
        $Pagina ['script_src'] = 'scripts/content.php';

        if( file_exists( $source ) ) {
            $Pagina ['to_sitemap'] = false;
            return $source;
        }

        return false;

    }

    function set_script( $source, $view=null ) {
        global $Pagina;
        $source = str_ireplace( '.php', '', $source );
        $source = strpos( $source, '/' ) !== false ? 'views' . strrchr( $source, '/' ) : 'views/'. $source;
        $Pagina['script_src'] = $source;

        $Pagina['is_script'] = true;
        $Pagina['script'] = [];
        $Pagina['script']['src'] = $source;
        $Pagina['script']['view'] = isset($view)&&is_string($view)? $view : 'default' ;

        return true;
    }

    function set_view( $view ) {
        global $Pagina;
        $Pagina['script']['view'] = trim( $view );
        return true;
    }

    /*
     * string	$msg	Message to log
     * boolean	$encode	default true, if true, message in htmlentities( $msg, ENT_QUOTES, 'UTF-8' )
     */
    function log( $msg, $encode = true ) {
        if( $encode ) {
            $msg = htmlentities( $msg, ENT_QUOTES, 'UTF-8');
        }
        echo '<script>console.log("'.$msg.'");</script>';
        return true;
    }

}

class money {
    private $input;
    public $bedrag;

    /**
     * Bereken btw over een bedrag
     *
     * @param int $bedrag Het bedrag waarover btw berekend moet worden
     * @param int $percentage Het btw percentage ( 6 voor 6% )
     * @param string $type Aangeven of het $bedrag 'Inclusief' of 'Exclusief' BTW is
     * @return object Inclusief en exclusief BTW.<br />
     * (object)->inc (inclusief BTW )<br />
     * (object)->exc (exclusief BTW )
     *
     */
    function __construct( $bedrag, $id, PDO $db = null ) {
        global $db, $btwArray;

        if( empty( $id ) ) return false;
        if( !isset( $btwArray ) || !is_array( $btwArray ) ) $btwArray = [];

        if( is_numeric( $id ) ):
            if( !array_key_exists( $id, $btwArray ) ):

                try {
                    $opties = $db->prepare( 'SELECT * FROM `acc_btw` WHERE `id`='.$id.';');
                    $opties->execute();

                    $o = $opties->fetch( PDO::FETCH_OBJ );
                    $btwArray[$id] = $o;

                } catch( PDOException $ex ) {
                    die( $ex->getMessage() );
                }

            endif;

            $percentage = (int) $btwArray[$id]->percentage;
            $type = strtolower( $btwArray[$id]->type );

        elseif( is_array( $id ) && array_key_exists( 'type', $id ) && array_key_exists( 'percentage', $id ) ):
            $percentage = (int) $id['percentage'];
            $type = strtolower( $id['type']);

        endif;

        $btw = new stdClass();

        $this->input = (float) $bedrag;

        $pc = (int) $percentage;
        $procent = (int) 100 + $percentage;
        $bedrag = (float) number_format( $bedrag, 2, '.', '');


        if (isset($type)) {
            switch( $type ) {
                case 'exclusief':
                    $btw->inc = number_format( $this->input / 100 * $procent, 2, '.', '' );
                    $btw->exc = $bedrag;
                    break;
                case 'inclusief':
                    $btw->inc = $bedrag;
                    $btw->exc = number_format( $this->input - $this->input * $pc / $procent, 2, '.', '' );
                    break;
            }
        } elseif ($percentage != 0) {
            if ($percentage > 0) {
                $btw->inc = number_format( $this->input / 100 * $procent, 2, '.', '' );
                $btw->exc = $bedrag;
            } else {
                $percentage = $percentage * -1;
                $btw->inc = $bedrag;
                $btw->exc = number_format( $this->input - $this->input * $pc / $procent, 2, '.', '' );
            }
        } else {
            $btw->exc = $bedrag;
            $btw->inc = $bedrag;
        }

        $this->bedrag = $btw;
        return $btw;
    }

    /**
     * $float omzetten naar geld notatie. Of ronden tot 2 decimalen 0.000,00 notatie
     * @param float $float het bedrag dat omgezet moet worden
     * @return float
     */
    function toMoney($float) {
        return number_format( $float, 2, ",", "." );
    }

    function inc() {
        return $this->toMoney( $this->btw->inc );
    }

    function exc() {
        return $this->toMoney( $this->btw->exc );

    }

    function btw() {
        return $this->toMoney( $this->btw->inc - $this->btw->exc );
    }
}

function blog_item( $titel, $inhoud, $extra = '' ) {
    $imgbefore = false;
    $datum = '';
    $doelgroep = '';
    $img = '';
    $ins = '';
    if( is_array( $extra ) ) {
        if( isset( $extra['datum'] ) ) {
            $datum = $extra['datum'];
        }
        if( isset( $extra['img'] ) ) {
            $img = $extra['img'];
        }
        if( isset( $extra['imgbefore'] ) ) {
            $imgbefore = $extra['imgbefore'];
        }
        if( isset( $extra['doelgroep'] ) ) {
            $doelgroep = $extra['doelgroep'];
        }

    } elseif( is_string( $extra ) ) {
        $datum = $extra;
    }

    if( isset( $datum ) && trim( $datum ) !== '' && strtotime( $datum ) ) {
        $datum = '<span class="blog-meta ai-clock">'. strftime( '%A %e %B %Y', strtotime( $datum ) ) .'</span>';
    } else {
        $datum = '<span class="blog-meta">'. trim( $datum ) .'</span>';

    }

    if( isset( $img ) && trim( $img ) !== '' ) {
        if( stripos( $img, ' src=' )  ) {
            if( $imgbefore ) {
                $ins = $img;
                $img = '';
            } else {
                $img = $img;
            }
        } else {
            if( $imgbefore ) {
                $img = '';
                $ins = '<img src="'. $img .'" alt="Afbeelding bij nieuws: ' . $titel .'" title="Afbeelding bij nieuws: ' . $titel .'" />';
            } else {
                $img = '<img src="'. $img .'" alt="Afbeelding bij nieuws: ' . $titel .'" title="Afbeelding bij nieuws: ' . $titel .'" />';
            }
        }
    }

    return	'<article class="blog-bericht padding-small">
	                    '. $ins .'
	                    <header class="blog-header">
	                        <h2 class="blog-title">'. trim( $titel ) .'</h2>
		                    '. $datum .'
	                    </header>
	
	                    <div class="blog-content">'. $img . trim( $inhoud ) .'</div>
	                    '. $doelgroep .'
	                </article> <!-- .blog-bericht -->';
}

class navigatie {
    var $class = [];
    public $data = [];
    public $output = '';
    public $active_page;
    public $type;

    function __construct( $type = 'sidemenu', $pageId = PageID, PDO $db=null ) {
        if( !isset( $db ) || empty( $db ) ) global $db;

        try{
            switch( $type ) {
                case 'kruimel':
                case 'kruimelpad':
                case 'crumbs':
                case 'crumb':

                    $this->type = 1;

                    $Q = 'SELECT
						CONCAT_WS( "/", s3.url_titel, s1.url_titel ) AS page, w.titel pageTitel,
						CONCAT_WS( "/", s3.url_titel ) AS parent, w2.titel AS parentTitel
				FROM website w
					LEFT JOIN `SEO_Module_link` s
						ON w.id=s.website_id
					LEFT JOIN `SEO_Module` s1
						ON s.`SEO_ID`=s1.`SEO_ID`
					LEFT JOIN `SEO_Module_link` s2
						ON w.parent=s2.website_id
					LEFT JOIN `SEO_Module` s3
						ON s2.`SEO_ID`=s3.`SEO_ID`
					LEFT JOIN website w2
						ON s2.website_id=w2.id AND w2.aktief="ja"
				WHERE
					( w.id=:id OR w.id=w.parent OR w.id=1 )
					AND w.aktief="ja"
				ORDER BY w2.sort ASC, w.sort ASC';
                    break;
                case 'sidemenu':
                case 'subnav':
                case 'sub':

                    $this->type = 2;
                    $this->page_id=$pageId;

                    $Q = 'SELECT
						CONCAT_WS( "/", s3.url_titel, s1.url_titel ) AS page,
						w.titel AS pageTitel,
						CASE w.parent
							WHEN 0 THEN
								true
							ELSE
								false
						END AS cat,
						CASE w.id
							WHEN :id THEN
								true
							ELSE false
						END AS active
				FROM website w
					LEFT JOIN `SEO_Module_link` s
						ON w.id=s.website_id
					LEFT JOIN `SEO_Module` s1
						ON s.`SEO_ID`=s1.`SEO_ID`
					LEFT JOIN `SEO_Module_link` s2
						ON w.parent=s2.website_id
					LEFT JOIN `SEO_Module` s3
						ON s2.`SEO_ID`=s3.`SEO_ID`
					LEFT JOIN website w2
						ON s2.website_id=w2.id
				WHERE
					w.aktief="ja" AND (w2.aktief IS NULL OR w2.aktief="ja")
						AND
					(	w.id = ( SELECT parent FROM website WHERE id=:id ) OR
						w.parent= ( SELECT parent FROM website WHERE id=:id AND parent!=1 AND parent !=0) OR
						w.parent=:id OR
						w.id=:id
					) AND w.id!=0 AND w.id!=1
				ORDER BY w2.sort, w.sort';
            }

            $C = $db->prepare( $Q );
            $C->bindValue( ':id', $pageId, PDO::PARAM_INT );
            $C->execute();
        } catch( PDOException $ex ) {
            $this->error = $ex->getMessage();
            return $this->error;
        }

        foreach( $C->fetchAll( PDO::FETCH_ASSOC ) AS $V ) {
            if( !empty( $V['parent'] ) ) $this->add_page( $V['parentTitel'], $V['parent'] );
            if( !empty( $V['page'] ) ) $this->add_page( $V['pageTitel'] , $V['page'] );
            if( isset( $V['cat'] ) && $V['cat'] ) $this->set( 'kop', $V['page'] );
            if( isset( $V['active'] ) && $V['active'] ) $this->active_page = $V['page'];
        }

    }

    function set( $type, $url ) {
        if( !isset( $this->setting ) || !is_array( $this->setting[ $url ] ) ) $this->setting = [];
        if( !array_key_exists( $url, $this->setting ) || !in_array( $type, $this->setting[ $url ] ) ) $this->setting[ $url ] = $type;
    }

    function add_page( $titel, $url ) {
        $this->data[ $url ] = $titel;
    }

    function add_class( $class ) {
        foreach( explode( ' ', $class ) AS $V ) {
            if( !in_array( $V, $this->class ) ) $this->class[] = $V;
        }
    }

    function render( $type='ul', $echo=true ) {
        switch( $type ) {
            case 'ul':
                $tag = 'ul';
                $sub = 'li';

                break;
            default:
                return $this->data;
        }

        $class = is_array( $this->class ) && count( $this->class ) > 0 ?
            ' class="'. implode( ' ', $this->class ) . '"' : '';

        $this->output = "<$tag$class>";
        $U=array_keys( $this->data );
        $V=array_values( $this->data );

        for( $i=0; $i<count($this->data); $i++ ) {
            $titel=$V[$i];
            if( empty( $titel ) ) continue;

            $url=str_ireplace( '.html.html', '.html', $U[$i] . '.html' );
            $type = isset( $this->setting[ $U[$i] ] ) ? $this->setting[ $U[$i] ] : 'default';

            switch( $type ){
                case 'kop':
                    $this->output = "<div class='conttitel'>
										<h1>$titel</h1>
									</div>$this->output";
                    break;
                default:
                    $class = count( $this->data ) == ( $i-1) && $this->type==0 ? " class='active'" : '';
                    $class = !empty( $this->active_page ) && $url == $this->active_page . '.html' ? " class='active'" : '';

                    $this->output .= "
							<li$class>
								<a href='".SITELINK."/$url'
									title='Ga naar $titel'
									alt='Ga naar $titel'
									target='_top'>
										$titel
									</a>
							</li>";

                    break;
            }

        }

        $this->output .= "</$tag>";

        switch( $echo ){
            case true:
                $this->echo_crumb();
                return true;
                break;
            case false:
                return $this->output;
                break;
        }
    }

    function echo_crumb() {
        echo $this->output;
    }

}

function get_crumbs($Pagina = '', $Sitelink = SITELINK, $PageID = PageID, $Titel = '') {
    if( is_array( $Pagina ) ) $Pagina = $Pagina;
    else global $Pagina;

    $crumbs = false;
    $Titel = !isset( $Titel ) ? $Pagina['titel'] : $Titel;


    if ($PageID != '1') {
        $crumbs .= '
	<a href="' . $Sitelink . '" alt="Ga naar de Home pagina" title="Ga naar de Home pagina"><span>Wings Sushi</span></a>';

        $crumbParent .= '/' . GetSEOURL($Pagina['parent']);

        if ($Pagina['parent'] != 0) {

            $ParentPagina = GetPagina($Pagina['parent']);

            if ($ParentPagina['aktief'] == 'Ja') {
                if( empty( $ParentPagina['titel'] ) ) {
                    $ParentPagina['titel'] = getPageTitle( $ParentPagina );
                }

                $PTitel = $ParentPagina['titel'];

                $crumbs .= '
					<a href="' . $Sitelink . '/' . GetSEOURL($ParentPagina['id']) .
                    '.html" alt="Ga naar ' . $PTitel . '"><span>' . $PTitel . '</span></a>';

                unset($ParentPagina);
            }
        }

        if( empty( $Pagina['titel'] ) ) {
            $Pagina['titel'] = getPageTitle( $Pagina );
        }
        $crumbs .= '<span>' . $Pagina['titel'] . '</span>';
    }

    return $crumbs;
}

function echoFormData( $input ) {
    if( is_array( $input ) ) {
        $ret = '<table>';
        foreach( $input AS $f => $v ) {
            if( hasContent( $v ) && !in_array( $f, ['nobots','submit','rendered'] )) {
                $v = is_array( $v ) ? $v : [ $v ];

                $ret .= '<tr>
						<td width="30">&nbsp;</td>
						<td width="150" style="font-size: 0.89rem;vertical-align:top">'. htmlentities( ucfirst( strtolower( str_replace( '_', ' ', $f ) ) ) ) . '</td>
						<td>'. str_replace( '_', ' ', implode( '<br />', $v ) ) . '</td>
						<td width="30">&nbsp;</td>
					</tr>';
            }
        }

        $ret .= '<tr><td colspan="4" height="30px"></td></tr>
				</table>';

    } else {
        $ret = '';
    }
    return $ret;
}

/**
 * @param bool $forceSSL
 * @return bool
 */
function checkProtocol($forceSSL=false ){
    global $dev;

    if( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS']=='on' ):
        define( 'PROTOCOL', 'https');
    else:
        if( $forceSSL ):
            header( "HTTP/1.1 301 Moved Permanently" );
            header( 'Location: https://'. $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] );
        endif;

        define( 'PROTOCOL', 'http');

    endif;

    return true;
}

/**
 * @param $string
 * @return string
 */
function remove_accents(string $string) {
    if ( !preg_match('/[\x80-\xff]/', $string) )
        return $string;

    $chars = array(
        // Decompositions for Latin-1 Supplement
        chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
        chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
        chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
        chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
        chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
        chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
        chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
        chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
        chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
        chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
        chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
        chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
        chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
        chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
        chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
        chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
        chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
        chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
        chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
        chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
        chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
        chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
        chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
        chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
        chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
        chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
        chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
        chr(195).chr(191) => 'y',
        // Decompositions for Latin Extended-A
        chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
        chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
        chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
        chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
        chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
        chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
        chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
        chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
        chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
        chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
        chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
        chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
        chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
        chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
        chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
        chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
        chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
        chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
        chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
        chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
        chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
        chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
        chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
        chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
        chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
        chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
        chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
        chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
        chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
        chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
        chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
        chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
        chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
        chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
        chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
        chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
        chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
        chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
        chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
        chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
        chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
        chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
        chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
        chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
        chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
        chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
        chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
        chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
        chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
        chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
        chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
        chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
        chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
        chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
        chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
        chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
        chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
        chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
        chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
        chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
        chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
        chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
        chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
        chr(197).chr(190) => 'z', chr(197).chr(191) => 's'
    );

    $string = strtr($string, $chars);

    return $string;
}

function Instagram($username) {

    $Feed = get_model( 'instagram/default', [
        'returnAsArray' => true
    ] );

    $count = 1;

    foreach($Feed as $InstaPost):

        ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 instagram-layout anime--inBottom anime-delay--<?= ($count * 100) / 3 ?>" style="background-color: white; padding: 1rem;">
            <a href="https://www.instagram.com/<?php echo $InstaPost->username ?>/" target="_blank" style="width: 100%">
                <p style="font-size: 0.8rem; font-weight: 700; background-color: white; padding: 0.5rem 1rem; display: flex; align-items: center;"><img class="img-instagram-profile" src="<?php echo $InstaPost->profilepic ?>"><?php echo $InstaPost->username ?></p>
                <i class="fab fa-instagram" style="float:right;"></i>
            </a>
            <?php if ($InstaPost->video_link != "" ): ?>
                <a href="<?php echo $InstaPost->source_link ?>" target="_blank" title="<?php echo $InstaPost->caption ?>">
                    <video autoplay playsinline class="img-fluid">
                        <source src="<?php echo $InstaPost->video_link  ?>" type="video/mp4">
                    </video>
                </a>
                <p style="font-size: 0.9rem; font-weight:700; padding: 1rem 1rem 0rem 1rem;"><span class="fa fa-heart"></span> <?php echo $InstaPost->likes ?> likes</p>
                <p style="font-size: 0.9rem; padding: 0rem 1rem 1rem;"><?php echo $InstaPost->caption ?></p>
            <?php else: ?>
                <a href="<?php echo $InstaPost->source_link ?>" target="_blank" title="<?php echo $InstaPost->caption ?>">
                    <img src="<?php echo $InstaPost->photo_link ?>" class="img-fluid b-lazy">
                </a>
                <p style="font-size: 0.9rem; font-weight:700; padding: 1rem 1rem 0rem 1rem;"><span class="fa fa-heart"></span> <?php echo $InstaPost->likes ?> likes</p>
                <p style="font-size: 0.9rem; padding: 0rem 1rem 1rem;"><?php echo $InstaPost->caption ?></p>
            <?php
            endif;
            ?>
        </div>
        <?php
        $count = $count + 3;
    endforeach;
}

function inlineConstant($name) {
    echo '<span bw-edit="constant" bw-edit-attr="'.$name.'">'.constant($name).'</span>';
}