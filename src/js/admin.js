function deployAlert(message, type = 'success', time = 5000){
    var wrapper = document.createElement("div");
    wrapper.innerHTML = `<div class="alert alert-${type}" role="alert">${message}</div>`;
    $(wrapper).appendTo( $('.alertWrap') ).delay(time).fadeOut(1000);
}

function handleInput(e) {
    const name = e.target.name.split('-');
    const value = e.target.value;
    const timestamp = $(e.target).data('last-edit');
    const type = name[0];
    const id = name[1];

    const url = 'localhost/mannacoronahulp.nl';
    switch (type) {
        case 'caregiver':
            $.get( url + "/beheer/update/hulpaanvraag?name=caregiver&timestamp="+timestamp+"&id="+id+"&value="+value, function( data ) {
                if(data.indexOf('true') > -1) {
                    deployAlert("Wijzigingen opgeslagen", "success");
                    $(e.target).prop('disabled', true);
                } else if(data.indexOf('edited') > -1) {
                    deployAlert("Wijzingen konden niet worden opgeslagen omdat deze hulpaanvraag reeds is bewerkt. Herlaad de pagina om de wijzigingen te zien.", "info", 15000);
                } else {
                    deployAlert("Opslaan mislukt, herlaad de pagina", "danger");
                }
            });
            break;
        case 'statushulpvraag':
            $.get( url + "/beheer/update/hulpaanvraag?name=status&timestamp="+timestamp+"&id="+id+"&value="+value, function( data ) {
                if(data.indexOf('true') > -1) {
                    deployAlert("Wijzigingen opgeslagen", "success");
                    $('#request-' + id + " .card-head").removeClass(function (index, className) {
                        return (className.match (/(^|\s)status-\S+/g) || []).join(' ');
                    }).addClass('status-' + value);
                } else if(data.indexOf('edited') > -1) {
                    deployAlert("Wijzingen konden niet worden opgeslagen omdat deze hulpaanvraag reeds is bewerkt. Herlaad de pagina om de wijzigingen te zien.", "info", 15000);
                } else {
                    deployAlert("Opslaan mislukt, herlaad de pagina", "danger");
                }
            });
            break;
        case 'statushulpverlener':
            $.get( url + "/beheer/update/hulpverlener?name=status&timestamp="+timestamp+"&id="+id+"&value="+value, function( data ) {
                if(data.indexOf('true') > -1) {
                    deployAlert("Wijzigingen opgeslagen", "success");
                    $('#request-' + id + " .card-head").removeClass(function (index, className) {
                        return (className.match (/(^|\s)status-\S+/g) || []).join(' ');
                    }).addClass('status-' + value);
                } else if(data.indexOf('edited') > -1) {
                    deployAlert("Wijzingen konden niet worden opgeslagen omdat deze hulpaanvraag reeds is bewerkt. Herlaad de pagina om de wijzigingen te zien.", "info", 15000);
                } else {
                    deployAlert("Opslaan mislukt, herlaad de pagina", "danger");
                }
            });
            break;
        default:
            //Someone is trying to hack me.
            console.error("Oh no");
            break;
    }
}

$(document).ready(function() {
    $('.data-row select').on('input', handleInput);
    $('.data-row select').select2();
});