const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const node_modules = __dirname + '/node_modules';
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    // mode: 'development',

    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin({
            terserOptions: {
                output: {
                    comments: false,
                },
            },
            extractComments: false,
        })],
    },
    plugins: [
        new CompressionPlugin(),
        new MiniCssExtractPlugin({
            filename: "style.css"
        })
    ],
    entry: [
        './src/app.js',
        './src/scss/app.scss',
    ],
    watch: true,
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            // {
            //     test: /\.(scss)$/,
            //     use: [
            //         {
            //             // Adds CSS to the DOM by injecting a `<style>` tag
            //             loader: 'file-loader',
            //             options: {
            //                 name: 'style.css',
            //             }
            //         },
            //         {
            //             loader: 'extract-loader'
            //         },
            //         {
            //             loader: 'css-loader?-url'
            //         },
            //         {
            //             // Loader for webpack to process CSS with PostCSS
            //             loader: 'postcss-loader',
            //             options: {
            //                 plugins: function () {
            //                     return [
            //                         require('autoprefixer')
            //                     ];
            //                 }
            //             }
            //         },
            //         {
            //             // Loads a SASS/SCSS file and compiles it to CSS
            //             loader: 'sass-loader'
            //         }
            //     ]
            //
            //
            // },
            {
                test: /\.s?css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader',
                        options:{
                            plugins: function () {
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {test: /\.(png|woff|woff2|eot|ttf|svg|png|jpg|tiff|gif|jpeg)$/, loader: 'url-loader?limit=100000'},
            // {test: /\.svg$/, use: 'ignore-loader'},
            // {test: /\.eot$/, use: 'ignore-loader'},
            // {test: /\.woff$/, use: 'ignore-loader'},
            // {test: /\.woff2$/, use: 'ignore-loader'},
            // {test: /\.ttf$/, use: 'ignore-loader'},
            // {test: /\.png$/, use: 'ignore-loader'},
            // {test: /\.jpg$/, use: 'ignore-loader'},
            // {test: /\.tiff$/, use: 'ignore-loader'},
            // {test: /\.gif$/, use: 'ignore-loader'},
            // {test: /\.jpeg$/, use: 'ignore-loader'}
        ]

    }
};
