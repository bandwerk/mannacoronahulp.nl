<?php
if( !defined('CatID') ) define('CatID', 0);

require_once('includes/Configu.php');

header('Content-Type: application/xml');
/*echo '<?xml version="1.0" encoding="UTF-8"?>';*/
?>
<urlset
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
    http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
        <?php
		
		
        echo '<url><loc>' . SITELINK . '/</loc><changefreq>monthly</changefreq><priority>1.00</priority></url>' . "\n";
        
        
	foreach( $NAV AS $website_id => $W ) {
			if( strpos( $W['url'], SITELINK ) === 0 )
				$LINK = $W['url'];
			else {
				$W['url'] = generate_url_from_text( $W['url'] );
				$LINK = SITELINK . '/' . $W['url'];
			}
			
			echo '<url><loc>' . $LINK . '</loc><changefreq>monthly</changefreq><priority>0.90</priority></url>' . "\n";


			if( isset( $W['sub'] ) ) {
				foreach( $W['sub'] AS $sub_id => $S ) {
					if( strpos( $S['url'], SITELINK ) === 0 )
						$LINK = $S['url'];
					else {
						$S['url'] = generate_url_from_text( $S['url'] );
						$LINK = SITELINK . '/' . $W['url'] . '/'. $S['url'];
					}

					echo '<url><loc>' . $LINK . '</loc><changefreq>monthly</changefreq><priority>0.80</priority></url>' . "\n";


					if( isset( $S['sub'] ) ) {
						foreach( $S['sub'] AS $det_id => $D ) {
							if( strpos( $D['url'], SITELINK ) === 0 )
								$LINK = $D['url'];
							else {
								$D['url'] = generate_url_from_text( $D['url'] );
								$LINK = SITELINK . '/' . $W['url'] . '/'. $S['url'] . '/'. $D . '.html';
							}

							echo '<url><loc>' . $LINK . '</loc><changefreq>monthly</changefreq><priority>0.75</priority></url>' . "\n";

					}
				}
			}
		}
	}

	if( $DP = get_model( 'diensten', 'dienst', ['returnAsArray'=>false] ) ):
		foreach( $DP AS $W ): ?>
			<url><loc><?php echo $W->url; ?></loc><changefreq>monthly</changefreq><priority>0.775</priority></url>
<?php 	endforeach;
	endif;

	if( $DP = get_model( 'klanten', 'klanten', ['returnAsArray'=>false] ) ):
		foreach( $DP AS $W ): ?>
			<url><loc><?php echo $W->url; ?></loc><changefreq>monthly</changefreq><priority>0.75</priority></url>
<?php 	endforeach;
	endif;
	
	if( $DP = get_model( 'nieuws/row', 'nieuws', ['returnAsArray'=>false] ) ): 
		foreach( $DP AS $W ): ?>
			<url><loc><?php echo $W->url; ?></loc><changefreq>monthly</changefreq><priority>0.625</priority></url>
<?php 	endforeach;
	endif;
	
	if( $DP = get_model( 'medewerker', 'team', ['returnAsArray'=>false] ) ): 
		foreach( $DP AS $W ): ?>
			<url><loc><?php echo SITELINK . "/team/" . strtolower(str_replace(" ", "-", remove_accents($W->url)));; ?></loc><changefreq>monthly</changefreq><priority>0.675</priority></url>
<?php 	endforeach;
	endif;

	
	?></urlset>