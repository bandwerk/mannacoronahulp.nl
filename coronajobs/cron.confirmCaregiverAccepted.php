<?php
define ( '__ROOT__', __DIR__ . '/..' );
require_once (__ROOT__ . '/includes/Configu.php');
require_once __ROOT__ . '/includes/classes/class.coronamail.php';

global $db;

try {
    $getCaregivers = $db->prepare("SELECT * FROM `hulpverleners` WHERE `aktief` = 'ja' AND `email_pin` = 0;");
    if($getCaregivers->execute() && $getCaregivers->rowCount() > 0) {
        while($giver = $getCaregivers->fetch(PDO::FETCH_OBJ)) {
            $content = "Uw aanmelding voor Corona Hulp is goedgekeurd!<br> Onze medewerkers kunnen u vanaf nu koppelen aan hulpaanvragen waarvoor u zich heeft opgegeven.<br>
Wanneer u wordt gekoppeld aan een hulpaanvraag ontvangt u van ons een mail met daarin een <b>beveiligde</b> link. Via deze link kunt u meer informatie over de aanvraag bekijken. Om de aanvraag te kunnen bekijken moet u inloggen met een persoonlijke pin-code, uw pincode is: <br><br><b style='font-size: 20px'> ".$giver->pincode."</b><br>Bewaar deze goed!<br><br><br>
 Met vriendelijke groet, <br> Het corona hulpteam ";
            $mail = new CoronaMail($giver->voornaam . " " . $giver->achternaam, $giver->email, "Uw aanmelding is goedgekeurd!", $content);

            if($mail->Send()) {
                $update = $db->prepare("UPDATE `hulpverleners` SET `email_pin` = 1 WHERE `id` = :id;");
                $update->bindValue(':id', $giver->id, PDO::PARAM_INT);
                $update->execute();
            }
        }
    }
} catch (PDOException $e) {
    die("Failed to mail");
}
