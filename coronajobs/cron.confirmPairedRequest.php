<?php
define ( '__ROOT__', __DIR__ . '/..' );
require_once (__ROOT__ . '/includes/Configu.php');
require_once __ROOT__ . '/includes/classes/class.coronamail.php';

global $db;

try {
    $getMatched = $db->prepare("
        SELECT
            `ha`.`id`,
            `ha`.`voornaam` as 'a_voornaam',
            `ha`.`achternaam` as 'a_achternaam',
            `ha`.`email` as 'a_email',
            `ha`.`hash`,
            `hv`.`voornaam` as 'h_voornaam',
            `hv`.`achternaam` as 'h_achternaam',
            `hv`.`email` as 'h_email',
            `vr`.`vraag`
        FROM `hulpaanvraag` `ha`
        JOIN `hulpaanvraag_hulpverlener` `hh`
            ON `hh`.`hulpaanvraag_id` = `ha`.`id`
        JOIN `hulpverleners` `hv`
            ON `hv`.`id` = `hh`.`hulpverlener_id`
        JOIN `hulpvraag` `vr`
            ON `vr`.`id` = `ha`.`hulpvraag_id`
        WHERE `ha`.`email_gekoppeld` = 0;");
    if($getMatched->execute() && $getMatched->rowCount() > 0) {
        while($match = $getMatched->fetch(PDO::FETCH_OBJ)) {

            $a_content = "Hulpverlener ".$match->h_voornaam." wil u helpen met \"".$match->vraag."\". Wij hebben ".$match->h_voornaam." een mail verzonden om jullie met elkaar in contact te brengen. Heeft u na 24 uur nog niets van ".$match->h_voornaam." gehoord, neem dan telefonisch contact met ons op.<br><br>
 Met vriendelijke groet, <br> Het corona hulpteam ";
            $mail1 = new CoronaMail($match->a_voornaam . " " . $match->a_achternaam, $match->a_email, "Hulp bij " .$match->vraag, $a_content);

            $h_content = $match->a_voornaam . " zoekt hulp bij \"" .$match->vraag. "\". U kunt de onderstaande link gebruiken om met " . $match->a_voornaam . " in contact te komen! <b>Let op: U hebt hiervoor uw unieke pincode nodig die we eerder hebben verzonden.
</b><br>
 <a href='".SITELINK."/hulpaanvraag/".$match->hash."'>".SITELINK."/hulpaanvraag/".$match->hash."</a><br><br>
 Met vriendelijke groet, <br> Het corona hulpteam ";
            $mail2 = new CoronaMail($match->h_voornaam . " " . $match->h_achternaam, $match->h_email, "Help " . $match->a_voornaam . " met " .$match->vraag, $h_content);

            if($mail1->Send() && $mail2->Send()) {
                $update = $db->prepare("UPDATE `hulpaanvraag` SET `email_gekoppeld` = 1 WHERE `id` = :id;");
                $update->bindValue(':id', $match->id, PDO::PARAM_INT);
                $update->execute();
            }
        }
    }
} catch (PDOException $e) {
    die("Failed to mail");
}
