<?php
define ( '__ROOT__', __DIR__ . '/..' );
require_once (__ROOT__ . '/includes/Configu.php');
require_once __ROOT__ . '/includes/classes/class.coronamail.php';

global $db;

try {
    $getRequests = $db->prepare("SELECT * FROM `hulpaanvraag` WHERE `status` = 2 AND `email_status_in_behandeling` = 0;");
    if($getRequests->execute() && $getRequests->rowCount() > 0) {
        while($request = $getRequests->fetch(PDO::FETCH_OBJ)) {
            $content = "Uw hulpaanvraag is goed gekeurd en in behandeling genomen!<br> Onze medewerkers gaan nu proberen om uw vraag aan een hulpverlener te koppelen. U ontvangt een mail zodra dit is gelukt.<br><br>
 Met vriendelijke groet, <br> Het corona hulpteam ";
            $mail = new CoronaMail($request->voornaam . " " . $request->achternaam, $request->email, "Uw aanvraag is in behandeling genomen", $content);
            if($mail->Send()) {
                $update = $db->prepare("UPDATE `hulpaanvraag` SET `email_status_in_behandeling` = 1 WHERE `id` = :id;");
                $update->bindValue(':id', $request->id, PDO::PARAM_INT);
                $update->execute();
            }
        }
    }
} catch (PDOException $e) {
    die("Failed to mail");
}

try {
    $getRequests = $db->prepare("SELECT * FROM `hulpaanvraag` WHERE `status` = 4");
    if($getRequests->execute() && $getRequests->rowCount() > 0) {

        while($request = $getRequests->fetch(PDO::FETCH_OBJ)) {
            $content = "Uw aanvraag is afgewezen.<br> De aanvraag die u heeft ingediend bij Coronahulp is door ons personeel afgewezen, dat betekend dat u niet bent gekoppeld aan een hulpverlener.<br>
 Als u vind dat de aanvraag ten onrechte is afgewezen vragen wij u telefonisch contact met ons op te nemen.<br><br>
 Met vriendelijke groet, <br> Het corona hulpteam ";
            $mail = new CoronaMail($request->voornaam . " " . $request->achternaam, $request->email, "Uw aanvraag is afgewezen", $content);
            if($mail->Send()) {
                $delete = $db->prepare("DELETE FROM `hulpaanvraag` WHERE `id` = :id;");
                $delete->bindValue(':id', $request->id, PDO::PARAM_INT);
                $delete->execute();
            }
        }
    }
} catch (PDOException $e) {
    die("Failed to mail");
}