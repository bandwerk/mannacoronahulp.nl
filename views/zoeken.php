<?php render_element('default/header' ); ?>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
			
<?php if( isset( $Pagina['image'] ) && $Pagina['image'] ): ?>
                <img<?php echo HtmGen::src( $Pagina['image'], 'Foto bij '. HtmGen::page_title(), 'img-responsive bordered rounded marbot' ); ?>">
<?php endif; 

						echo HtmGen::page_subtitle('h2').HtmGen::page_content(); ?>

<div class="row">
						<div class="col-xs-12 search">
							<?php render_zoekform( $Pagina['zoeken']->term, ['noCloseButton'=>true] ); ?>
<?php if( $Pagina['zoeken']->r->totaal > 0 ): ?>
							<span>Resultaten <small>(<?php echo $Pagina['zoeken']->r->totaal; ?>)</small></span>
<?php endif; ?>						
						</div>
						
<?php if( $Pagina['zoeken']->r->totaal > 0 ): ?>
						<div class="col-xs-12 resultaat" ui-equaliser="zoeken">
					<?php
						foreach( $Pagina['zoeken']->r AS $oms => $aantal ):
							switch( $oms ):
								case 'totaal': ?>
							<div>
									<strong>Direct naar:</strong>
					<?php break;
							default:
							if( $aantal > 0 ):?>
									<a<?php echo $HTM->get_href( "#$oms", 'Direct naar '. $oms, 'btn inline' ); ?>>
										<span><?php echo ucfirst( $oms ); ?> <small>(<?php echo $aantal; ?>)</small></span>
									</a>
					<?php	endif;
							break;
						endswitch;
					endforeach;?>
							</div>
						</div>
				
			<?php if( isset( $Pagina['zoeken']->dier ) && $Pagina['zoeken']->dier && $Pagina['zoeken']->r->dier > 0  ): ?>
						<div class="col-xs-12" id="dier">
							<a<?php echo $HTM->get_href( '#content', 'Terug naar boven', 'btn ghost inline pull-right' ); ?>>
								<i class="fa fa-chevron-up"></i>
							</a>
							<h3>Dieren <small>(<?php echo $Pagina['zoeken']->r->dier; ?>)</small></h3>
							<div class="row containers">
				
							<?php render_element( 'dieren/pagina', $Pagina['zoeken']->dier ); ?>
							</div>
						</div>
				
			<?php endif; ?>
			<?php if( isset( $Pagina['zoeken']->dierpagina ) && $Pagina['zoeken']->dierpagina && $Pagina['zoeken']->r->dierpagina > 0  ): ?>
						<div class="col-xs-12" id="dierpagina">
							<a<?php echo $HTM->get_href( '#content', 'Terug naar boven', 'btn ghost inline pull-right' ); ?>>
								<i class="fa fa-chevron-up"></i>
							</a>
							<h3>Dieren <small>(<?php echo $Pagina['zoeken']->r->dierpagina; ?>)</small></h3>
							<div class="row containers">
				
							<?php render_element( 'dieren/pagina', $Pagina['zoeken']->dierpagina ); ?>
							</div>
						</div>
				
			<?php endif; ?>
			<?php if( isset( $Pagina['zoeken']->pagina ) && $Pagina['zoeken']->pagina && $Pagina['zoeken']->r->pagina > 0 ): ?>
						<div class="col-xs-12" id="pagina">
							<a<?php echo $HTM->get_href( '#', 'Terug naar boven', 'btn ghost inline pull-right' ); ?>>
								<i class="fa fa-chevron-up"></i>
							</a>
						
							<h3>Pagina's <small>(<?php echo $Pagina['zoeken']->r->pagina; ?>)</small></h3>
				
							<div class="row zoeken">
				<?php

				foreach( $Pagina['zoeken']->pagina AS $P ):
							$Url = array_keys( $P )[0];
							$P= (object) array_values( $P )[0]; ?>
							
								<div class="col-xs-12">
									<div class="dier-card">
										<a <?php echo HtmGen::href( $Url, $P->alt, 'ptitle' ); ?>><?php echo htmlentities( $P->titel ); ?></a>
											<?php if( isset( $P->subtitel ) && hasContent( $P->subtitel ) ): ?>
												<span <?php echo HtmGen::href( $Url, $P->alt ); ?>> <span
													class="subtitle oneline"><?php echo htmlentities( $P->subtitel ); ?></span>
												</span>
											<?php endif;
								
											if (isset ( $P->inhoud )): ?>
												<p class="content"><?php echo HtmGen::text_shorten( $P->inhoud, 200, false, false ); ?></p>
											<?php endif; ?>
											
										<div class="right">
											<a <?php echo HtmGen::href( $Url, $P->alt, 'btn has-icon' ); ?>>Lees
												meer <i class="fas fa-chevron-right"></i>
											</a>
										</div>
									</div>
								</div>					

				
				<?php 
				endforeach; ?>
							</div>
						</div>
				
<?php endif;
elseif( $Pagina['zoeken']->term ):?>
					<div class="row">
						<div class="col-xs-12">
							<h3>
								<small>Geen resultaten gevonden voor:</small><br /><?php echo $Pagina['zoeken']->term; ?>
							</h3>
						</div>
					</div>

<?php endif; ?>						
						
					</div>
					<hr />

<?php 					render_element ( 'default/share', HtmGen::page_title (), [ 
								'summary' => HtmGen::text_shorten ( HtmGen::page_content (), 256, false, false )
						] );
						
		
						render_element( 'default/call' );
?>

				
			</div>
		</div>
	</div>
</div>