<?php date_default_timezone_set('UTC'); ?>

<div class="hero-banner" style="background-image: url('<?= WEBSITE_SFEER_GROOT . "/" . $sfeer_image ?>')">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title">Coronahulp beheeromgeving</h1>
            </div>
        </div>
    </div>
</div>
<div class="hulp-cta">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cta">
                    <div class="row">
                        <div class="col-md-2"><span class="white-line"></span></div>
                        <div class="col-md-8"><p>Wat wilt u doen?</p></div>
                        <div class="col-md-2"><span class="white-line"></span></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?= SITELINK ?>/<?= $Pagina['url_titel'] ?>/hulpaanvragen" class="help-button left">Hulpaanvragen beheren (<?= $noReq1 ?>)<div class="bg"></div></a>
                        </div>
                        <div class="col-md-6">
                            <a href="<?= SITELINK ?>/<?= $Pagina['url_titel'] ?>/hulpverleners" class="help-button right">Hulpverleners beheren (<?= $noReq2 ?>)<div class="bg"></div></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

