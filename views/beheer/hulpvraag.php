<?php date_default_timezone_set('UTC'); ?>

<div class="hero-banner hero-banner--small"
     style="background-image: url('<?= WEBSITE_SFEER_GROOT . "/" . $sfeer_image ?>')">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title">Hulpaanvragen</h1>
            </div>
        </div>
    </div>
</div>
<div class="alertWrap"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="filters">
                <div class="label">
                    Filter hulpaanvragen:
                </div>
                <?php
                    $filterId = -1;
                    if(isset($_GET['filter'])) {
                        $filterId = intval($_GET['filter']);
                    }
                ?>
                <div class="filterList">
                    <a href="<?= explode("?", $_SERVER['REQUEST_URI'])[0] ?>" class="filterButton <?= ($filterId == -1 ? 'active' : '') ?>">Alles</a>
                    <a href="<?= explode("?", $_SERVER['REQUEST_URI'])[0] ?>?filter=1" class="filterButton status-1 <?= ($filterId == 1 ? 'active' : '') ?>">Aangevraagd</a>
                    <a href="<?= explode("?", $_SERVER['REQUEST_URI'])[0] ?>?filter=2" class="filterButton status-2 <?= ($filterId == 2 ? 'active' : '') ?>">In behandeling</a>
                    <a href="<?= explode("?", $_SERVER['REQUEST_URI'])[0] ?>?filter=3" class="filterButton status-3 <?= ($filterId == 3 ? 'active' : '') ?>">Afgerond</a>
                    <a href="<?= explode("?", $_SERVER['REQUEST_URI'])[0] ?>?filter=5" class="filterButton status-5 <?= ($filterId == 5 ? 'active' : '') ?>">Hulpverlener gekoppeld</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

                <?php
                foreach ($hulpvragen as $hulpvraag) {
                    $options_status = "";
                    foreach ($statuses as $status) {
                        $options_status .= "<option value='" . $status->id . "' " . ($hulpvraag->status == $status->id ? 'selected' : '') . ">" . $status->titel . "</option>";
                    }

                    $caregiverId = $hulpvraag->GetSelectedCaregiver();
                    $options_caregiver = "<option value='-1' disabled ".($caregiverId > 0 ? '' : 'selected').">Selecteer hulpverlener</option>";
                    foreach ($hulpvraag->GetCaregivers() as $caregiver) {
                        $options_caregiver .= "<option value='" . $caregiver->id . "' " . ($caregiverId == $caregiver->id ? 'selected' : '') . ">" . $caregiver->voornaam . " " . $caregiver->achternaam . " | " . $caregiver->telefoon . "</option>";
                    }

                    $opmerking = (strlen($hulpvraag->inhoud) < 1 ? "" : "<div class='inner-row'>
                                <div class='cell cell--wide'>
                                    <div class='label'>Opmerking</div>
                                    <div class='value'>" . $hulpvraag->inhoud . "</div>
                                </div>
                            </div>");

                    echo "
                        <div class='data-row' id='request-".$hulpvraag->id."'>
                            <div class='card-head status-".$hulpvraag->status."'>
                                #".$hulpvraag->id." - " . $hulpvraag->voornaam . " " . $hulpvraag->achternaam . " | " . date('d-m-Y', strtotime($hulpvraag->datum_aanvraag)) . "
                            </div>
                            <div class='inner-row'>
                                <div class='cell'>
                                    <div class='label'>Naam</div>
                                    <div class='value'>" . $hulpvraag->voornaam . " " . $hulpvraag->achternaam . "</div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Telefoon</div>
                                    <div class='value'>" . $hulpvraag->telefoon . "</div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Hulpvraag</div>
                                    <div class='value'>" . $hulpvraag->GetRequestName() . "</div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Status</div>
                                    <div class='value'><select name='statushulpvraag-" . $hulpvraag->id . "' data-last-edit=".strtotime($hulpvraag->laatst_bewerkt)." id='statushulpvraag-" . $hulpvraag->id . "'>" . $options_status . "</select></div>
                                </div>
                            </div>
                            <div class='inner-row'>
                                <div class='cell'>
                                    <div class='label'>Adres</div>
                                    <div class='value'>" . $hulpvraag->adres . "<br/>" . $hulpvraag->postcode . ", " . $hulpvraag->plaats . "</div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Email</div>
                                    <div class='value'><a href='mailto:" . $hulpvraag->email . "'>" . $hulpvraag->email . "</a></div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Datum aanvraag</div>
                                    <div class='value'>" . date('d-m-Y', strtotime($hulpvraag->datum_aanvraag)) . "</div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Hulpverlener</div>
                                    <div class='value'><select " . ($caregiverId > -1 ? 'disabled' : '') . " name='caregiver-" . $hulpvraag->id . "' data-last-edit=".strtotime($hulpvraag->laatst_bewerkt)." id='caregiver-" . $hulpvraag->id . "'>" . $options_caregiver . "</select></div>
                                </div>
                            </div>
                            ".$opmerking."
                        </div>
                    ";
                }
                ?>
        </div>
    </div>
</div>
