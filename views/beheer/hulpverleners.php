<?php date_default_timezone_set('UTC'); ?>
<div class="hero-banner hero-banner--small"
     style="background-image: url('<?= WEBSITE_SFEER_GROOT . "/" . $sfeer_image ?>')">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title">Hulpverleners</h1>
            </div>
        </div>
    </div>
</div>
<div class="alertWrap"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="filters">
                <div class="label">
                    Filter hulpverleners:
                </div>
                <?php
                $filterId = -1;
                if(isset($_GET['filter'])) {
                    $filterId = intval($_GET['filter']);
                }
                ?>
                <div class="filterList">
                    <a href="<?= explode("?", $_SERVER['REQUEST_URI'])[0] ?>" class="filterButton <?= ($filterId == -1 ? 'active' : '') ?>">Alles</a>
                    <a href="<?= explode("?", $_SERVER['REQUEST_URI'])[0] ?>?filter=Ja" class="filterButton status-Ja <?= ($filterId == 'Ja' ? 'active' : '') ?>">Actief</a>
                    <a href="<?= explode("?", $_SERVER['REQUEST_URI'])[0] ?>?filter=Nee" class="filterButton status-Nee <?= ($filterId == 'Nee' ? 'active' : '') ?>">Inactief</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

            <?php
            foreach ($hulpverleners as $hulpverlener) {
                $options_status = "";
                foreach ($statuses as $status) {
                    $options_status .= "<option value='" . $status . "' " . ($hulpverlener->aktief == $status ? 'selected' : '') . ">" . $status . "</option>";
                }

                $opmerking = (strlen($hulpverlener->inhoud) < 1 ? "" : "<div class='inner-row'>
                                <div class='cell cell--wide'>
                                    <div class='label'>Opmerking</div>
                                    <div class='value'>" . $hulpverlener->inhoud . "</div>
                                </div>
                            </div>");


                $hulpvragen = "";
                foreach ($hulpverlener->GetRequestTypes() as $hulpvraag) {
                    $hulpvragen .= $hulpvraag . "<br />";
                }

                echo "
                        <div class='data-row' id='request-".$hulpverlener->id."'>
                            <div class='card-head status-".$hulpverlener->aktief."'>
                                #".$hulpverlener->id." - " . $hulpverlener->voornaam . " " . $hulpverlener->achternaam . " | " . date('d-m-Y', strtotime($hulpverlener->datum_aanvraag)) . "
                            </div>
                            <div class='inner-row'>
                                <div class='cell'>
                                    <div class='label'>Naam</div>
                                    <div class='value'>" . $hulpverlener->voornaam . " " . $hulpverlener->achternaam . "</div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Telefoon</div>
                                    <div class='value'>" . $hulpverlener->telefoon . "</div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Datum aanvraag</div>
                                    <div class='value'>" . date('d-m-Y', strtotime($hulpverlener->datum_aanvraag)) . "</div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Aktief</div>
                                    <div class='value'><select name='statushulpverlener-" . $hulpverlener->id . "' data-last-edit=".strtotime($hulpverlener->laatst_bewerkt)." id='statushulpverlener-" . $hulpverlener->id . "'>" . $options_status . "</select></div>
                                </div>
                            </div>
                            <div class='inner-row'>
                                <div class='cell'>
                                    <div class='label'>Adres</div>
                                    <div class='value'>" . $hulpverlener->adres . "<br/>" . $hulpverlener->postcode . ", " . $hulpverlener->plaats . "</div>
                                </div>
                                <div class='cell'>
                                    <div class='label'>Email</div>
                                    <div class='value'><a href='mailto:" . $hulpverlener->email . "'>" . $hulpverlener->email . "</a></div>
                                </div>
                                <div class='cell cell--semiwide'>
                                    <div class='label'>Hulpvragen</div>
                                    <div class='value'>".$hulpvragen."</div>                                    
                                </div>
                            </div>
                            ".$opmerking."
                        </div>
                    ";
            }
            ?>
        </div>
    </div>
</div>
