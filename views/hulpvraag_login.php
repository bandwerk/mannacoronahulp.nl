<?php render_element('template/header/smaller_hero_banner', $sfeer_image); ?>

<div class="login-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3 col-sm-12">
                <h2>Log in met uw persoonlijke pin</h2>
            </div>
        </div>
    </div>
</div>
<div class="form-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3 col-sm-12">

                <form method="post" id="login">
                    <input  class="form-control" type="password" name="pin" id="pin" placeholder="Pincode invoeren...">
                    <a  class="help-button" type="submit" onclick="$('#login').submit()"><div class="bg"></div>Inloggen</a>
                </form>
            </div>
        </div>
    </div>
</div>

