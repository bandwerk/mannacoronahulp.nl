<div class="container">
    <div class="offset-md-2 col-md-8">
        <div class="userLogin">
            <div class="subtitle">Inloggen</div>
            <form id="forgot-login-form" class="login-form" action="" method="post">
                <p><?= $LoginError ?></p>
                <label for="email">Emailadres:</label>
                <input type="text" id="email" name="email" value="<?= $email ?>" disabled />
                <input type="hidden" name="hash" id="hash" value="<?= $hash ?>">
                <label for="email">Wachtwoord:</label>
                <input type="password" id="password1" name="password1" />
                <label for="email">Herhaal wachtwoord:</label>
                <input type="password" id="password2" name="password2" />

                <p id="errorfield"></p>
                <p id="errorfield2"></p>

                <input id="hiddenloginbutton" disabled="true" type="submit" style="display:none"/>
                <div class="row">
                    <div class="col-md-6">
                        <a id="loginbutton"
                           class="help-button"
                           type="submit"
                           onclick="document.getElementById('forgot-login-form').submit();"
                        >Herstel wachtwoord</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>