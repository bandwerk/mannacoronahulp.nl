<?php if($success) { ?>
    <p>U heeft een mail ontvangen om uw wachtwoord te herstellen.</p>
    <a href="<?php echo SITELINK ?>/<?= $Pagina['url_titel'] ?>/">Ga terug naar de inlogpagina</a>
<?php } else { ?>
    <p>Deze gegevens zijn niet bij ons bekend.</p>
    <a href="<?php echo SITELINK ?>/<?= $Pagina['url_titel'] ?>/wachtwoord-vergeten/">Probeer opnieuw</a>
<?php } ?>