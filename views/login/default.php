
<div class="container">
    <div class="offset-md-2 col-md-8">
        <div class="userLogin">
            <div class="subtitle">Inloggen</div>
            <form id="login-form" class="login-form" action="" method="post">
                <p><?= $LoginError ?></p>
                <label for="username">Gebruikersnaam:</label>
                <input type="text" id="username" name="username" />
                <label for="password">Wachtwoord:</label>
                <input type="password" id="password" name="password" />
                <input id="hiddenloginbutton" disabled="true" type="submit" style="display:none"/>
                <div class="row">
                    <div class="col-md-6">
                        <a id="loginbutton"
                           class="help-button"
                           type="submit"
                           onclick="document.getElementById('login-form').submit();"
                        >Log in</a>
                    </div>
                </div>


                <a class="mt-3" href="<?php echo SITELINK ?>/<?= $Pagina['url_titel'] ?>/wachtwoord-vergeten/">Wachtwoord vergeten</a>
            </form>
        </div>
    </div>
</div>
