<div class="container">
    <div class="offset-md-2 col-md-8">
        <div class="userLogin">
            <div class="subtitle">Herstel wachtwoord</div>
            <form id="forgot-login-form" class="login-form" action="" method="post">
                <p><?= $LoginError ?></p>
                <label for="email">Emailadres:</label>
                <input type="text" id="email" name="email" />
                <input id="hiddenloginbutton" disabled="true" type="submit" style="display:none"/>
                <div class="row">
                    <div class="col-md-8">
                        <a id="loginbutton"
                           class="help-button"
                           type="submit"
                           onclick="document.getElementById('forgot-login-form').submit();"
                        >Herstel wachtwoord</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>