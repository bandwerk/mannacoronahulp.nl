<?php render_element('template/header/smaller_hero_banner', $sfeer_image); ?>
    <div class="hulp-cta">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cta">
                        <div class="row">
                            <div class="col-md-2"><span class="white-line"></span></div>
                            <div class="col-md-8"><p><?= HtmGen::page_subtitle();  ?></p></div>
                            <div class="col-md-2"><span class="white-line"></span></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                   <?= HtmGen::page_content();  ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-sm-6 offset-md-4">
                                <a href="#<?= $AanbiedForm->FormPrefix . $AanbiedForm->FormName ?>" class="help-button left">Direct naar het formulier
                                    <div class="bg"></div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="title">
                        <h2><?= HtmGen::page_subtitle() ?></h2>
                    </div>
                    <div class="text">
                        <?= $Pagina['inhoud_2'];?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="img-wrapper">
                        <img src="<?= WEBSITE_CONTENT_MIDDEL . "/" . $content_images[0] ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="form-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mt-5" bw-edit="table" bw-edit-field="paginatitel" bw-edit-module="website" bw-edit-id="1">Waarmee kan je helpen?</h2>
                </div>
            </div>

            <?php if ($AanbiedForm instanceof FormGen) { ?>
                <form <?= $AanbiedForm->formAttribute ?> style="-webkit-appearance: none;" id="<?= $AanbiedForm->FormPrefix . $AanbiedForm->FormName ?>">
                    <div class="row">
                        <?php
                        if ($AanbiedForm->Success) { ?>
                            <div class="alert alert-success" style="margin-left: 20px;">
                                <h4>Bedankt voor je aanvraag</h4>
                                <p>Wij nemen zo spoedig mogelijk contact met je op.</p>
                            </div>

                            <?php
                        } elseif ($AanbiedForm->FormInput) { ?>
                            <div class="alert alert-danger" style="margin-left: 20px;">
                                <h4>Er is iets mis gegaan.</h4>
                                <p>Het formulier is niet volledig of correct ingevoerd.</p>
                                <?php echo $AanbiedForm->GetError('submit', 'p'); ?>
                            </div>

                            <?php
                        }
                        ?>
                        <div class="col-md-12">
                            <div class="subtitle">Ik kan helpen bij:</div>
                        </div>
                        <?php
                        echo $AanbiedForm->deployOptions($hulpvragen);
                        ?>
                        <div class="col-md-12">
                            <div class="subtitle">Vul hier uw persoonsgegevens in:</div>
                        </div>
                        <?php
                        echo $AanbiedForm->deployFormFields($AanbiedForm->FormField);
                        ?>
                        <textarea class="form-control" name="opmerking" placeholder="Voer hier uw eventuele opmerkingen in"></textarea>

                        <div class="col-md-7 av"> <?php
                            echo $AanbiedForm->deployAlgemeneVoorwaarden();
                            ?>
                        </div>
                        <div class="col-md-5 submit">
                            <?php
                            echo $AanbiedForm->deploySubmit("Aanvragen");
                            ?></div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>

<?php
render_element('cta/tel');
render_element('cta/onze_partners', $partners); ?>