<?php
render_element('template/header/hero_banner', $sfeer_image);
render_element('cta/hulp_nodig_bieden');
?>

<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="page-content-wrapper-left">
                    <div class="img-wrapper">
                        <img src="<?= WEBSITE_CONTENT_GROOT ."/". $content_images[1] ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="page-content-wrapper-right">
                    <h1>MANNA CORONA HULP</h1>
                    <?= HtmGen::page_content() ?>

                    <div class="row">
                        <div class="col-xl-6 col-md-12 col-lg-12">
                            <a href="<?= SITELINK ?>/hulp-aanvragen" class="help-button left">Ik heb hulp nodig<div class="bg"></div>
                            </a>
                        </div>
                        <div class="col-xl-6 col-md-12 col-lg-12">
                            <a href="<?= SITELINK ?>/hulp-aanbieden" class="help-button right">Ik wil graag helpen<div class="bg"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="faq-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="title">Veel gestelde vragen</h2>
                <ul class="faqs">
                    <?php foreach($faqs as $faq){ ?>
                    <li class="item">
                        <div class="question"><?= explode(".",$faq->vraag)[0] ?><br> <?= explode(".",$faq->vraag)[1] ?></div><span class="svg"> <?= Svg::img("enter-svg"); ?></span>
                        <div class="answer"><?= $faq->antwoord ?></div>
                    </li>

                    <?php } ?>
                </ul>
                <a href="<?= SITELINK ?>/veel-gestelde-vragen" class="help-button">Meer vragen?<div class="bg"></div>
                </a>
            </div>
            <div class="col-lg-6">
                <div class="img-wrapper">
                    <img src="<?= WEBSITE_CONTENT_GROOT ."/". $content_images[0] ?>">
                </div>
            </div>
        </div>
    </div>
</div>
<?php render_element('cta/onze_partners', $partners); ?>
