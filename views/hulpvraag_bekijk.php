<?php render_element('template/header/smaller_hero_banner', $sfeer_image); ?>
<div class="hulpvraag-bekijken">
<div class="hulp-cta">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cta">
                    <div class="row">
                        <div class="col-md-2"><span class="white-line"></span></div>
                        <div class="col-md-8"><p>Hallo <?= $caregiver->voornaam ?> <?= $caregiver->achternaam ?>!</p></div>
                        <div class="col-md-2"><span class="white-line"></span></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                <strong>Wij zijn blij dat je <?= $client->voornaam ?> wilt helpen met <?= $client->GetRequestName(); ?><br/>Hieronder vind je meer informatie om met <?= $client->voornaam ?> in contact te komen.</strong>
                                <?= $client->PrintData(); ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-sm-12 offset-sm-0 offset-lg-3">
                            <a href="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>?completed" class="help-button left">Ik heb <?= $client->voornaam ?> geholpen!
                                <div class="bg"></div>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>