<?php render_element('template/header/smaller_hero_banner', $sfeer_image); ?>
<div class="content-page">
    <div class="container">
        <div class="col-md-12">
            <div class="text"> <?php echo HtmGen::page_content(); ?></div>
        </div>
    </div>
</div>
<?php

render_element('cta/tel');
?>