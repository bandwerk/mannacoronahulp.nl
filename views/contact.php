<?php $Pagina ['template'] ['footer'] = "contact"; ?>
<!--====== CONTACT PART START ======-->
<section class="contact-page pt-70 pb-70">
    <div class="container-fluid">
        <div class="row">
            <div class="contact-bg bg_cover col-lg-12 b-lazy" style="background-image: url('images/contact-bg.jpg')"></div>
            <div class="contact-info col-lg-8 offset-lg-2 col-xl-6 anime--inRight" >
                <div class="contact-info">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="contact-info-text">
                                <h3>Kom in contact met ons!</h3>
                                <div class="contact-info-address mt-30"> <?php
                                    if ((defined('CONTACT_POSTCODE') && CONTACT_POSTCODE) || (defined('CONTACT_PLAATS') && CONTACT_PLAATS) || (defined('CONTACT_ADRES') && CONTACT_ADRES)): ?>
                                        <a href="https://www.google.com/maps/dir//Bandwerk+merkenbouwers,+Neonstraat+3a,+7463+PE+Rijssen/"
                                           target="_blank">
                                        <p><?php if (defined('CONTACT_ADRES') && CONTACT_ADRES): echo htmlentities(CONTACT_ADRES);
                                                if ((defined('CONTACT_POSTCODE') && CONTACT_POSTCODE) || (defined('CONTACT_PLAATS') && CONTACT_PLAATS)): echo '<br />';
                                                endif;
                                            endif;
                                            if ((defined('CONTACT_POSTCODE') && CONTACT_POSTCODE) || (defined('CONTACT_PLAATS') && CONTACT_PLAATS)):
                                                $postcodePlaats = [];
                                                if (defined('CONTACT_POSTCODE') && CONTACT_POSTCODE)
                                                    $postcodePlaats[] = HtmGen::postcode(CONTACT_POSTCODE);
                                                if (defined('CONTACT_PLAATS') && CONTACT_PLAATS)
                                                    $postcodePlaats[] = CONTACT_PLAATS;

                                                echo implode(' ', $postcodePlaats);
                                                unset($postcodePlaats);
                                            endif; ?>
                                        </p></a><?php
                                    endif; ?>
                                </div>
                                <?php if ((defined('CONTACT_TEL') && CONTACT_TEL) || (defined('CONTACT_EMAIL') && CONTACT_EMAIL)): ?>
                                    <div class="contact-info-call mt-30">
                                        <?php if (defined('CONTACT_TEL') && CONTACT_TEL): ?>
                                        <ul>
                                            <li>
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 473.806 473.806"
                                                     style="enable-background:new 0 0 473.806 473.806;"
                                                     xml:space="preserve"><g>
                                                        <g>
                                                            <path d="M374.456,293.506c-9.7-10.1-21.4-15.5-33.8-15.5c-12.3,0-24.1,5.3-34.2,15.4l-31.6,31.5c-2.6-1.4-5.2-2.7-7.7-4
                            c-3.6-1.8-7-3.5-9.9-5.3c-29.6-18.8-56.5-43.3-82.3-75c-12.5-15.8-20.9-29.1-27-42.6c8.2-7.5,15.8-15.3,23.2-22.8
                            c2.8-2.8,5.6-5.7,8.4-8.5c21-21,21-48.2,0-69.2l-27.3-27.3c-3.1-3.1-6.3-6.3-9.3-9.5c-6-6.2-12.3-12.6-18.8-18.6
                            c-9.7-9.6-21.3-14.7-33.5-14.7s-24,5.1-34,14.7c-0.1,0.1-0.1,0.1-0.2,0.2l-34,34.3c-12.8,12.8-20.1,28.4-21.7,46.5
                            c-2.4,29.2,6.2,56.4,12.8,74.2c16.2,43.7,40.4,84.2,76.5,127.6c43.8,52.3,96.5,93.6,156.7,122.7c23,10.9,53.7,23.8,88,26
                            c2.1,0.1,4.3,0.2,6.3,0.2c23.1,0,42.5-8.3,57.7-24.8c0.1-0.2,0.3-0.3,0.4-0.5c5.2-6.3,11.2-12,17.5-18.1c4.3-4.1,8.7-8.4,13-12.9
                            c9.9-10.3,15.1-22.3,15.1-34.6c0-12.4-5.3-24.3-15.4-34.3L374.456,293.506z M410.256,398.806
                            C410.156,398.806,410.156,398.906,410.256,398.806c-3.9,4.2-7.9,8-12.2,12.2c-6.5,6.2-13.1,12.7-19.3,20
                            c-10.1,10.8-22,15.9-37.6,15.9c-1.5,0-3.1,0-4.6-0.1c-29.7-1.9-57.3-13.5-78-23.4c-56.6-27.4-106.3-66.3-147.6-115.6
                            c-34.1-41.1-56.9-79.1-72-119.9c-9.3-24.9-12.7-44.3-11.2-62.6c1-11.7,5.5-21.4,13.8-29.7l34.1-34.1c4.9-4.6,10.1-7.1,15.2-7.1
                            c6.3,0,11.4,3.8,14.6,7c0.1,0.1,0.2,0.2,0.3,0.3c6.1,5.7,11.9,11.6,18,17.9c3.1,3.2,6.3,6.4,9.5,9.7l27.3,27.3
                            c10.6,10.6,10.6,20.4,0,31c-2.9,2.9-5.7,5.8-8.6,8.6c-8.4,8.6-16.4,16.6-25.1,24.4c-0.2,0.2-0.4,0.3-0.5,0.5
                            c-8.6,8.6-7,17-5.2,22.7c0.1,0.3,0.2,0.6,0.3,0.9c7.1,17.2,17.1,33.4,32.3,52.7l0.1,0.1c27.6,34,56.7,60.5,88.8,80.8
                            c4.1,2.6,8.3,4.7,12.3,6.7c3.6,1.8,7,3.5,9.9,5.3c0.4,0.2,0.8,0.5,1.2,0.7c3.4,1.7,6.6,2.5,9.9,2.5c8.3,0,13.5-5.2,15.2-6.9
                            l34.2-34.2c3.4-3.4,8.8-7.5,15.1-7.5c6.2,0,11.3,3.9,14.4,7.3c0.1,0.1,0.1,0.1,0.2,0.2l55.1,55.1
                            C420.456,377.706,420.456,388.206,410.256,398.806z"/>
                                                            <path d="M256.056,112.706c26.2,4.4,50,16.8,69,35.8s31.3,42.8,35.8,69c1.1,6.6,6.8,11.2,13.3,11.2c0.8,0,1.5-0.1,2.3-0.2
                            c7.4-1.2,12.3-8.2,11.1-15.6c-5.4-31.7-20.4-60.6-43.3-83.5s-51.8-37.9-83.5-43.3c-7.4-1.2-14.3,3.7-15.6,11
                            S248.656,111.506,256.056,112.706z"/>
                                                            <path d="M473.256,209.006c-8.9-52.2-33.5-99.7-71.3-137.5s-85.3-62.4-137.5-71.3c-7.3-1.3-14.2,3.7-15.5,11
                            c-1.2,7.4,3.7,14.3,11.1,15.6c46.6,7.9,89.1,30,122.9,63.7c33.8,33.8,55.8,76.3,63.7,122.9c1.1,6.6,6.8,11.2,13.3,11.2
                            c0.8,0,1.5-0.1,2.3-0.2C469.556,223.306,474.556,216.306,473.256,209.006z"/>
                                                        </g>
                                                    </g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                </svg><?php echo '<a' . HtmGen::href('tel:' . HtmGen::tel(CONTACT_TEL), 'Bel naar ' . CONTACT_TEL) . '>' . htmlentities(CONTACT_TEL) . '</a>'; ?>
                                            </li>
                                            <?php endif; ?>
                                            <?php if (defined('CONTACT_EMAIL') && CONTACT_EMAIL): ?>
                                                <li>
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 512 512"
                                                         style="enable-background:new 0 0 512 512;"
                                                         xml:space="preserve">
                <g>
                    <g>
                        <path d="M255,0C114.06,0,0,114.05,0,255c0,139.895,113.025,257,255,257c140.644,0,257-115.914,257-257C512,112.87,394.743,0,255,0
                            z M255,482.2C130.935,482.2,29.8,380.168,29.8,255C29.8,130.935,130.935,29.8,255,29.8c125.168,0,227.2,101.135,227.2,225.2
                            C482.2,380.168,380.168,482.2,255,482.2z"/>
                    </g>
                </g>
                                                        <g>
                                                            <g>
                                                                <path d="M255,90c-90.981,0-165,74.019-165,165c0,91.323,73.867,167,165,167c36.874,0,74.175-12.393,102.339-34.001
                            c6.573-5.043,7.813-14.459,2.77-21.032c-5.043-6.573-14.46-7.813-21.032-2.77C316.048,381.866,285.403,392.2,255,392.2
                            c-74.439,0-135-61.658-135-137.2c0-74.439,60.561-135.2,135-135.2c75.542,0,137,60.761,137,135.2v15c0,16.542-13.458,30-30,30
                            s-30-13.458-30-30c0-7.43,0-67.164,0-75c0-8.284-6.716-15-15-15s-15,6.716-15,15v0.951C288.454,185.622,272.068,180,255,180
                            c-41.355,0-75,33.645-75,75s33.645,75,75,75c22.423,0,43.059-9.622,57.735-25.812C323.583,319.772,341.615,330,362,330
                            c33.084,0,60-26.916,60-60v-15C422,163.746,346.21,90,255,90z M255,300.2c-24.813,0-45-20.387-45-45.2s20.187-45.2,45-45.2
                            c25.477,0,47,20.807,47,45.2S280.477,300.2,255,300.2z"/>
                                                            </g>
                                                        </g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                                                        <g></g>
                </svg><?php echo '<a' . HtmGen::href('mailto:' . CONTACT_EMAIL, 'Mail naar ' . CONTACT_EMAIL) . '>' . htmlentities(CONTACT_EMAIL) . '</a>'; ?>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php if ($ContactForm instanceof FormGen): ?>
                            <div class="col-lg-7">
                                <div class="footer-form">
                                    <form<?php echo $ContactForm->formAttribute; ?> class="contact-form">
                                    <div class="row">
                                        <div class="col-lg-12">
                                        <h4>Wat kunnen wij voor je doen <span>?</span></h4>
                                    </div>
                                    </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                            <?php if ($ContactForm->Success): ?>
                                                <div class="feedback success">
                                                    <h4>Bedankt voor je aanvraag</h4>
                                                    <p>Wij nemen zo spoedig mogelijk contact met je op.</p>
                                                </div>

                                            <?php else:
                                            if ($ContactForm->FormInput):?>
                                                <div class="feedback error">
                                                    <h4>Er is iets mis gegaan.</h4>
                                                    <p>Het formulier is niet volledig of correct ingevoerd.</p>
                                                    <?php echo $ContactForm->GetError('submit', 'p'); ?>
                                                </div>

                                            <?php endif; ?>
                                            <div class="contact-input">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <?php if ($ContactForm->setField('naam')): ?>
                                                                <div class="single-form">
                                                                    <input<?php echo $ContactForm->attribute; ?>/>
                                                                </div>
                                                        <?php endif;
                                                        if ($ContactForm->setField('email')):?>
                                                                <div class="single-form">
                                                                    <input<?php echo $ContactForm->attribute; ?>/>
                                                                </div>
                                                        <?php endif; ?>
                                                        <?php if ($ContactForm->setField('tel')): ?>
                                                                <div class="single-form">
                                                                    <input<?php echo $ContactForm->attribute; ?>/>
                                                                </div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                            <?php if ($ContactForm->setField('opmerking')): ?>
                                                                <div class="single-form">
                                                                    <textarea<?php echo $ContactForm->attribute; ?>><?php echo $ContactForm->value; ?></textarea>
                                                                </div>
                                                            <?php endif; ?>
                                                            <div class="single-form text-right">
                                                                <?php echo $ContactForm->GetSubmit('Verzenden <i class="fa fa-angle-right"></i>', 'main-btn'); ?>
                                                            </div>
                                                    </div>
                                    </form>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                </div>
</section>
<section class="work-area" style="padding-top: 2rem; padding-bottom: 2rem; background-color: #eaeaea78;">
    <div class="container">
        <h1 style="text-align: center; padding: 2rem 0rem 1.5rem;">Wij <i class="fas fa-heart"
                                                                          style="color: #E42C6B "></i>
            <strong>merk</strong>waardige berichten.</h1>
        <div class="row row-no-wrap">
            <style>
                p {
                    color: #272727;
                }
            </style>
            <?php Instagram("bandwerkrijssen") ?>
        </div>
    </div>
</section>

<!--====== CONTACT PART ENDS ======-->