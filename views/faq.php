<?php render_element('template/header/smaller_hero_banner', $sfeer_image); ?>


    <div class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="text">
                        <?= HtmGen::page_content() ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="img-wrapper">
                        <img src="<?= WEBSITE_CONTENT_MIDDEL . "/" . $content_images[0] ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="faq-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="title">Veel gestelde vragen</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php foreach ($faqs as $key => $item) { ?>
                        <div class="categorie"> <h3><?= $key ?></h3></div>
                        <ul class="faqs">
                            <?php foreach ($item as $faq) {
                                $faq = (object)$faq; ?>
                                <li class="item">
                                    <div class="question"><?= explode(".", $faq->vraag)[0] ?><br> <?= explode(".", $faq->vraag)[1] ?></div>
                                    <span class="svg"> <?= Svg::img("enter-svg"); ?></span>
                                    <div class="answer"><?= $faq->antwoord ?></div>
                                </li>
                            <?php } ?>

                        </ul>
                    <?php } ?>
                    <a href="<?= SITELINK ?>/contact" class="help-button">Meer vragen?
                        <div class="bg"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>

<?php render_element('cta/onze_partners', $partners); ?>