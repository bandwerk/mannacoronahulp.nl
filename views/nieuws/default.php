
<main role="main" class="container">
<h1 class="mt-5" bw-edit="table" bw-edit-field="paginatitel" bw-edit-module="website" bw-edit-id="4"><?= HtmGen::page_title(); ?></h1>
<h2 class="mt-5" bw-edit="table" bw-edit-field="sub_paginatitel" bw-edit-module="website" bw-edit-id="4"><?= HtmGen::page_subtitle(); ?></h2>

<div class="bw-content" bw-edit="table" bw-edit-field="inhoud" bw-edit-module="website" bw-edit-id="4">
    <?php echo HtmGen::page_content(); ?>
</div>

<?php if (isset($Nieuws) && hasContent($Nieuws)):
    render_element('nieuws/card', $Nieuws);
else: ?>
    Geen nieuws gevonden
<?php endif; ?>

</main>
