<!--====== BANNER PART START ======-->
<section class="banner-area" id="newsbanner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <img bw-edit="image" bw-edit-config="nieuws_header_crop_config" bw-edit-album-id="<?= $ID ?>" bw-edit-replace-id="<?= explode(".", $nieuwsItem->nieuws_header)[0] ?>" src="<?= NIEUWS_HEADER . '/' . explode(",", $nieuwsItem->nieuws_header)[0] ?>" alt="image" style="max-width: 100%" class="img-fluid">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="banner-content  mt-70 mb-70" >
                    <h1 bw-edit="table" bw-edit-field="titel" bw-edit-module="nieuws" bw-edit-id="<?= $ID ?>" class="line"><?php echo $nieuwsItem->titel ?></h1>
                    <p  bw-edit="table" bw-edit-field="subtitel" bw-edit-module="nieuws" bw-edit-id="<?= $ID ?>"><?php echo $nieuwsItem->subtitel ?></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row justify-content-center">
            <div class="col-sm-9">
                <div class="algemeen-content" bw-edit="table" bw-edit-field="bericht" bw-edit-module="nieuws" bw-edit-id="<?= $ID ?>">
                    <?php echo $nieuwsItem->bericht ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="algemeen-right mt-50 anime--inBottom">
                    <div class="algemeen-image">
                        <img src="<?php echo NIEUWS_QUOTE . '/' . $nieuwsItem->nieuws_quote; ?>" alt="">
                    </div>
                    <div class="content anime--inBottom anime-delay--200">
                        <p  bw-edit="table" bw-edit-field="quote" bw-edit-module="nieuws" bw-edit-id="<?= $ID ?>">“<?php echo $nieuwsItem->quote ?>”</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====== ALGEMEEN PART ENDS ======-->
<?php render_element('referentie', $klanten); ?>
<?php render_element('team', $medewerkers); ?>
